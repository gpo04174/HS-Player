﻿using HSAudio.Utility;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace HSAudio.DSP
{ 
    internal static class HSDSP
    {
        public const int Int24MaxValue = 8388608;

        public static float BassBoosta_GainControl(this float sample, float selectivity, float gain, float ratio)
        {
            float gain1 = 1.0f / (selectivity + 1.0f);
            float cap = 0; cap = (sample + cap * selectivity) * gain1;
            sample = Math.Min(Math.Max(-1.0f, sample + cap * ratio), 1.0f) * gain;
            return sample;
        }
        public static float Saturate(float x)
        {
            return Math.Min(Math.Max(-1.0f, x), 1.0f);
        }
        public static double Volume(this double Sample, double Gain)
        {
            if (Gain == 0) return Sample;
            double a = Sample * Math.Pow(10.0, Gain * 0.05);
            /*
            if (a == double.PositiveInfinity ||
               a == double.NegativeInfinity ||
               a == double.NaN) a = double.MinValue;*/
            return a;
        }
        public static float Volume(this float Sample, float Gain)
        {
            if (Gain == 0) return Sample;
            float a = Sample * (float)Math.Pow(10.0, Gain * 0.05);
            /*
            if (a == float.PositiveInfinity ||
               a == float.NegativeInfinity ||
               a == float.NaN) a = float.MinValue;*/
            return a;
        }
        public static float Volume(this float Sample, int Gain)
        {
            float result = 1;
            while (Gain > 0)
            {
                if (Gain % 2 == 1)
                    result *= 10;
                Gain >>= 1;
            }
            return Sample * result;
        }

        /******************************************************************************
**
** Function             : AGC()
**
** Description          : AGC algorithm is used to automatically adjust the 
**                        speech level of an audio signal to a predetermined 
**                        value noramally in db.
**

** Arguments:

**  float *x            : input vector (range from -1 to 1 )
**  float *y            : output vector (range from -1 to 1 )
**  float gain_level    : output power level in db
**  int   N             : number of samples or frame length
**

** Inputs:
**  float *x
**  float gain_level
**  int   N
**
** Outputs:
**  float *y
**
** Return value         : None
**
** NOTE                 : For more details refer matlab files. 
**
** Programmer           : Jaydeep Appasaheb Dhole
**                      : Associate Software Engineer ( DSP )
**                      : Aparoksha Tech. Pvt. Ltd. ,Bangalore.
**                      : http://www.aparoksha.com
**                      : <jaydeepdhole@gmail.com>
**
** Date                 : 26 May 2006.

******************************************************************************/
        /// <summary>
        /// AGC algorithm is used to automatically adjust the speech level of an audio signal to a predetermined value noramally in dB.
        /// (출처: http://www.codeforge.com/read/231796/AGC.c__html)
        /// </summary>
        /// <param name="input">input vector (range from -1 to 1 )</param>
        /// <param name="output">output vector (range from -1 to 1 )</param>
        /// <param name="gain_level">output power level in dB</param>
        /// <param name="N">number of samples or frame length</param>
        public static unsafe void AGC(float* input, float* output, float gain_level, int N)
        {
            int i;

            float energy, output_power_normal, K;
            /* ouput power gain level is in db convert it into normal power */

            output_power_normal = (float)Math.Pow(10, (gain_level / 10));
            /* Calculate the energy of the signal */

            energy = 0;
            for (i = 0; i < N; i++)
                energy += input[i] * input[i];

            /* calculate the multiplication factor */
            K = (float)Math.Sqrt((double)(output_power_normal * N) / (double)energy);
            //K = HS_CSharpUtility.Utility.MathUtility.Sqrt_Fast((output_power_normal * N) / energy);

            /* scale the input signal to achieve the required output power */
            for (i = 0; i < N; i++)
                output[i] = input[i] * K;
        }
        /// <summary>
        /// AGC algorithm is used to automatically adjust the speech level of an audio signal to a predetermined value noramally in dB.
        /// (출처: http://www.codeforge.com/read/231796/AGC.c__html)
        /// </summary>
        /// <param name="input">input vector (range from -1 to 1 )</param>
        /// <param name="output">output vector (range from -1 to 1 )</param>
        /// <param name="gain_level">output power level in dB</param>
        /// <param name="N">number of samples or frame length</param>
        public static unsafe void AGC(float* input, float* output, double gain_level, uint N)
        {
            int i;

            float energy, output_power_normal, K;
            /* ouput power gain level is in db convert it into normal power */

            output_power_normal = (float)Math.Pow(10, (gain_level / 10));
            /* Calculate the energy of the signal */

            energy = 0;
            for (i = 0; i < N; i++)
                energy += input[i] * input[i];

            /* calculate the multiplication factor */
            K = Maths.SqrtFast((output_power_normal * N) / energy);

            /* scale the input signal to achieve the required output power */
            for (i = 0; i < N; i++)
                output[i] = input[i] * K;
        }
        public static unsafe void AGC(float* input, float* output, double gain_level, uint N, int channels)
        {
            int i, count, count2;

            float[] energy = new float[channels],
                    K = new float[channels];
            double output_power_normal;
            /* ouput power gain level is in db convert it into normal power */

            output_power_normal = Math.Pow(10, (gain_level / 10));
            /* Calculate the energy of the signal */

            for (count = 0; count < N; count++)
            {
                for (count2 = 0; count2 < channels; count2++)
                    energy[count2] += input[(count * channels) + count2] * input[(count * channels) + count2];
            }

            /* calculate the multiplication factor */
            for (i = 0; i < channels; i++) K[i] = (float)Math.Sqrt((output_power_normal * N) / energy[i]);

            /* scale the input signal to achieve the required output power */
            for (count = 0; count < N; count++)
            {
                for (count2 = 0; count2 < channels; count2++)
                    output[(count * channels) + count2] = input[(count * channels) + count2] * K[count2];
            }
        }

        #region 스테레오 확장 (StereoExpand)
        static float SQRT_2 = 1.414213562373095f;
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, uint Channel, float Width = 0.5f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - Width);
                S *= 2 * Width;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, uint Channel, float SideWidth = 0.5f, float MidWidth = 0.5f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, uint Channel, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / 2;   // obtain mid-signal from left and right
                float S = (L - R) / 2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                Output[i] = (M + S) / 2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / 2;   // obtain right signal from mid and side
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, uint Channel, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0, float Mix = 1)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / 2;   // obtain mid-signal from left and right
                float S = (L - R) / 2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                Output[i] = SetMix(Input[i], (M + S) / 2, Mix);   // obtain left signal from mid and side
                Output[i + 1] = SetMix(Input[i + 1], (M - S) / 2, Mix);   // obtain right signal from mid and side
            }
        }
        internal static unsafe void StereoExpand(float* Input, float* Output, uint N, uint Channel, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0, float WetMix = 1, float DryMix = 1, float Method = 1.414213562373095f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float y = SetMix(0, Input[i], DryMix),
                      y1 = SetMix(0, Input[i + 1], DryMix);
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / Method;   // obtain mid-signal from left and right
                float S = (L - R) / Method;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                float L1 = (M + S) / Method;
                float R1 = (M - S) / Method;
                float x = SetMix(0, L1, WetMix),
                      x1 = SetMix(0, R1, WetMix);
                Output[i] = SetMix(x + y, x + L, DryMix);   // obtain left signal from mid and side
                //Output[i] = SetWetMix2Value(Output[i], L1, R1, 1-DryMix);
                Output[i + 1] = SetMix(x1 + y1, x1 + R, DryMix);   // obtain right signal from mid and side
                //Output[i + 1] = SetWetMix2Value(Output[i + 1], R1, L1, 1-DryMix);
            }
        }
        internal static unsafe void StereoExpand(float* Input, float* Output, uint N, uint Channel, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0, float Mix = 1, float Method = 1.414213562373095f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / Method;   // obtain mid-signal from left and right
                float S = (L - R) / Method;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                float L1 = (M + S) / Method;
                float R1 = (M - S) / Method;

                Output[i] = SetMix(Input[i], L1, Mix);   // obtain left signal from mid and side
                Output[i + 1] = SetMix(Input[i + 1], R1, Mix); // obtain right signal from mid and side
                //Output[i + 1] = SetWetMix2Value(Output[i + 1], R1, L1, 1-DryMix);
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, out float[] SideValue, out float[] MidValue, uint N, uint Channel, float Width = 0.5f)
        {
            float[] _MidValue = new float[(N / Channel)];
            float[] _SideValue = new float[(N / Channel)];
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                uint div = i / Channel;
                // amplify mid and side signal seperately:
                M *= 2 * (1 - Width);
                if (_MidValue.Length < div) _MidValue[div] = M;
                S *= 2 * Width;
                if (_SideValue.Length < div) _SideValue[div] = S;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side 
            }
            SideValue = _SideValue;
            MidValue = _MidValue;
        }
        public static unsafe void StereoExpand(float* Input, float* Output, out float[] SideValue, out float[] MidValue, uint N, uint Channel, float SideWidth = 0.5f, float MidWidth = 0.5f)
        {
            float[] _MidValue = new float[(N / Channel)];
            float[] _SideValue = new float[(N / Channel)];
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (uint i = 0; i < N * Channel; i += Channel)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                uint div = i / Channel;
                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                if (_MidValue.Length < div) _MidValue[div] = M;
                S *= 2 * SideWidth;
                if (_SideValue.Length < div) _SideValue[div] = S;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
            SideValue = _SideValue;
            MidValue = _MidValue;
        }
        #endregion

        /// <summary>
        /// 여러채널을 레벨에따라 모노로 합성합니다
        /// </summary>
        /// <param name="Input">입력할 사운드 버퍼입니다.</param>
        /// <param name="Output">출력할 사운드 버퍼입니다.</param>
        /// <param name="N">사운드 버퍼의 실제 갯수 입니다.</param>
        /// <param name="Mix">모노로 합성할 모노레벨 입니다.</param>
        /// <param name="Channel">총 채널수 입니다.</param>
        public static unsafe void SetMono(float* Input, float* Output, uint N, int Channel = 2, float Mix = 1, bool First = true)
        {
            float M;
            int j;
            for (int i = 0; i <= N; i += Channel)
            {
                M = 0;
                for (j = 0; j < Channel; j++) //M += Input[i + j];// obtain signal from channel
                {
                    if (Mix > 0) M += Input[i + j];
                    else
                    {
                        M = Input[i + (First ? 0 : 1)];
                        for (j = (First ? 1 : 0); j < Channel - (First ? 0 : 1); j++) M -= Input[i + j];
                    }

                    if (Mix < 0) M = M / Channel;

                    for (j = 0; j < Channel; j++) //Output[i + j] = SetMix(Input[i + j], M, Mix<=0?Mix/2f:Mix);
                        Output[i + j] = Mix > 0 ?
                            SetMix(Input[i + j], M, Mix) :
                            SetMix(Input[i + j], M, -Mix);
                }
                //(Mix * M) + ((1 - Mix) * Input[i + j]);//Input[j] + (M*WetMix);  }
            }
            /*
            float M;
            int j;
            for (int i = 0; i <= N; i += Channel)
            {
                M = 0;
                for (j = 0; j < Channel; j++)
                    M += Input[i + j];// obtain signal from channel

                M = M / Channel;
                for (j = 0; j < Channel; j++)
                    Output[i + j] = SetMix(Input[i + j], M, Mix);//(Mix * M) + ((1 - Mix) * Input[i + j]);//Input[j] + (M*WetMix);
            }*/
        }

        /*
        public static unsafe void LCLP(float* Input, float* Output, uint N, float Channels, float Frequency, float Q, float WetMix)
        {
            for (int i = 0; i < N; i += (int)Channels)
            {
                for (int s = 0; s < Channels; s++)
                {
                    float x = Input[i + s];
                    float x1 = Input[i + s - 1];

                    float c = 1.0f / (float)Math.Tan(Math.PI * Frequency / 48000);

                    float a1 = 1.0f / (1.0f + Frequency * c + c * c);
                    float a2 = 2 * a1;
                    float a3 = a1;
                    float b1 = 2.0f * (1.0f - c * c) * a1;
                    float b2 = (1.0f - Frequency * c + c * c) * a1;
                    //Output[i+s] = a1 * x + a2 * in(n-1) + a3 * in(n-2) - b1*out(n-1) - b2*out(n-2)
                }
            }
        }*/


      //  static 
        public static unsafe void LCLP(float* Input, float* Output, uint Length, int channel,float SampleLate, float Frequency, float Q)
        {
            float O = 2.0f * (float)Math.PI * Frequency / SampleLate;
            float C = Q / O;
            float L = 1f / Q / O;
            //for (int c = 0; c < channel; c++)
            for (int c = 0; c < channel; c++)
            {
                float V = 0, I = 0, T;
                for (int s = 0; s < Length; s++)
                {
                    T = (I - V) / C;
                    float IN = Input[(s * channel) + c];
                    I += (IN * O - V) / L;
                    V += T;
                    Output[(s * channel) + c] = V/O;//(float)(V/O);
                }
            }
        }


        #region 믹싱 알고리즘
        public static float SetMix(float OriginalValue, float Value, float Mix)
        {
            //return SetMix(OriginalValue, Value, Mix, 1, 0, Mix<0);
            return Mix>=0?
                SetMix(OriginalValue, Value, Mix, 1, 0, Mix<0):
                SetMix(Value, OriginalValue, -Mix, 1, 0, Mix < 0);
        }
        public static float SetMix(float OriginalValue, float Value, float Mix, float MaxMix, float MinMix, bool Substract = false)
        {
            if (Mix == MinMix||Mix < MinMix) return OriginalValue;
            else if (Mix == MaxMix || Mix > MaxMix) return Value;
            return Substract?
                ((Mix * Value) - ((MaxMix - Mix) * OriginalValue)) :
                ((Mix * Value) + ((MaxMix - Mix) * OriginalValue));
            //Mix > MaxMix ? (Mix * Value) + ((MaxMix - Mix) * OriginalValue) : ((WetMix) * Value) + ((1 + MaxMix) * OriginalValue);
            //return a;
        }
        public static float SetMix(float OriginalValue, float Value, float WetMix, float DryMix)
        {
            float y = SetMix(0, OriginalValue, DryMix, 1, 0, false);//SetMix(0, OriginalValue, DryMix, 1, 0, false);
            float x = SetMix(0, Value, WetMix, 1, 0, false);//SetMix(0, Value, WetMix, 1, 0, false);
            return x+y;//SetMix(x + y, x + OriginalValue, DryMix);
        }
        public static float SetMix(float OriginalValue, float Value, float WetMix, float DryMix, float MaxMix = 1, float MinMix = 0)
        {
            float y = SetMix(0, OriginalValue, DryMix, MaxMix, MinMix, false);
            float x = SetMix(0, Value, WetMix, MaxMix, MinMix, false);
            return x + y;//SetMix(x + y, x + OriginalValue, DryMix, MaxMix, MinMix, false);
        }
        public static float SetMix2Value(float OriginalValue, float Value, float Value1, float Mix)
        {
            if (Mix == 0) return OriginalValue;
            return Mix > 0 ? (Mix * Value) + ((1 - Mix) * OriginalValue) :
                                ((Mix) * Value1) + ((1 + Mix) * OriginalValue);
        }
        public static float SetMix2Value(float OriginalValue, float Value, float Value1, float Mix, float MaxMix = 1, float MinMix = 0)
        {
            if (Mix == MinMix) return OriginalValue;
            return Mix > MinMix ? (Mix * Value) + ((MaxMix - Mix) * OriginalValue) :
                                  (Mix * Value1) + ((MaxMix + Mix) * OriginalValue);
            //float a = (WetMix * Value) + ((1 - WetMix) * OriginalValue);
            //float b = SetWetMix(a, a, DryMix);
            //return a;
        }

        public static float Mixing(params float[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return (float)a;
        }
        public static double Mixing(params double[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return a;
        }
        public static float MixingAverage(params float[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return (float)(a / Value.Length);
        }
        public static double MixingAverage(params double[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return a / Value.Length;
        }
        #endregion

        /*
        public static unsafe void SoundScript(string Script, float* Input, float* Output, int N)
        {
            Assembly a = CSScriptLibrary.CSScript.LoadCode(@"using System;
                using System.Windows.Forms;
                public class Script 
                {
                    public int Foo(int a, int b)
                    {
                         return a+b;
                    }
                }", "System.Windows.Forms"
                );
            
            //int v = a.Foo(1, 2);
        }
        */

        //https://stackoverflow.com/a/12046026/7080663
        public static float[] Resampling(float[] Sample, int fromSampleRate, int toSampleRate, int quality = 10)
        {
            if (Sample == null || Sample.Length == 0) return Sample;
            int srcLength = Sample.Length;

            //int tmp_floor = (int)Math.Floor(Sample.Length * (toSampleRate / (float)fromSampleRate));
            //int destLength = tmp_floor % 2 == 0 ? tmp_floor : tmp_floor - 1;
            float destLength = Sample.Length * (toSampleRate / (float)fromSampleRate);

            float dx = srcLength / (float)destLength;
            float[] samples = new float[(int)Math.Ceiling(destLength)];
            //List<float> samples = new List<float>();

            // fmax : nyqist half of destination sampleRate
            // fmax / fsr = 0.5;
            float fmaxDivSR = 0.5f;
            float r_g = 2 * fmaxDivSR;

            // Quality is half the window width
            int wndWidth2 = quality;
            int wndWidth = quality * 2;

            float x = 0;
            int i, j;
            double r_y;
            double r_w;
            double r_a;
            double r_snc;
            for (i = 0; i < destLength; ++i)
            {
                r_y = 0.0f;
                for (int tau = -wndWidth2; tau < wndWidth2; ++tau)
                {
                    // input sample index
                    j = (int)(x + tau);

                    // Hann Window. Scale and calculate sinc
                    r_w = 0.5 - 0.5 * Math.Cos(2 * Math.PI * (0.5 + (j - x) / wndWidth));
                    r_a = 2 * Math.PI * (j - x) * fmaxDivSR;
                    r_snc = 1.0;
                    if (r_a != 0)
                        r_snc = Math.Sin(r_a) / r_a;

                    if ((j >= 0) && (j < srcLength))
                    {
                        r_y += r_g * r_w * r_snc * Sample[j];
                    }
                }
                samples[i] = (float)r_y;
                //samples.Add((float)r_y);
                x += dx;
            }

            return samples;//samples.ToArray();

        }
        public static float[] Resampling(float[][] Sample, int fromSampleRate, int toSampleRate, int quality = 10)
        {
            if (Sample == null || Sample.Length == 0) return new float[0];

            //int tmp_floor = (int)Math.Floor(Sample.Length * (toSampleRate / (float)fromSampleRate));
            //int destLength = tmp_floor % 2 == 0 ? tmp_floor : tmp_floor - 1;
            float[] samples = new float[Sample.Length * Sample[0].Length];
            for (int ch = 0; ch < Sample.Length; ch++)
            {
                int srcLength = Sample.Length;
                float destLength = Sample.Length * (toSampleRate / (float)fromSampleRate);

                float dx = srcLength / (float)destLength;
                // fmax : nyqist half of destination sampleRate
                // fmax / fsr = 0.5;
                float fmaxDivSR = 0.5f;
                float r_g = 2 * fmaxDivSR;

                // Quality is half the window width
                int wndWidth2 = quality;
                int wndWidth = quality * 2;

                float x = 0;
                int i, j;
                double r_y;
                int tau;
                double r_w;
                double r_a;
                double r_snc;
                for (i = 0; i < destLength; ++i)
                {
                    r_y = 0.0f;
                    for (tau = -wndWidth2; tau < wndWidth2; ++tau)
                    {
                        // input sample index
                        j = (int)(x + tau);

                        // Hann Window. Scale and calculate sinc
                        r_w = 0.5 - 0.5 * Math.Cos(2 * Math.PI * (0.5 + (j - x) / wndWidth));
                        r_a = 2 * Math.PI * (j - x) * fmaxDivSR;
                        r_snc = 1.0;
                        if (r_a != 0)
                            r_snc = Math.Sin(r_a) / r_a;

                        if ((j >= 0) && (j < srcLength))
                        {
                            r_y += r_g * r_w * r_snc * Sample[ch][j];
                        }
                    }
                    samples[(int)Math.Ceiling(ch * destLength)] = (float)r_y;
                    x += dx;
                }
            }

            return samples;

        }
        public static float[] Resampling(float[] Sample, int channel, int fromSampleRate, int toSampleRate, int quality = 10)
        {
            if (Sample == null || Sample.Length == 0) return Sample;

            //int tmp_floor = (int)Math.Floor(Sample.Length * (toSampleRate / (float)fromSampleRate));
            //int destLength = tmp_floor % 2 == 0 ? tmp_floor : tmp_floor - 1;
            
            int srcLength = Sample.Length / channel;
            float destLength = srcLength * (toSampleRate / (float)fromSampleRate);
            float[] samples = new float[(int)Math.Ceiling(destLength) * channel];
            for (int ch = 0; ch < channel; ch++)
            {
                float dx = srcLength / (float)destLength;
                // fmax : nyqist half of destination sampleRate
                // fmax / fsr = 0.5;
                float fmaxDivSR = 0.5f;
                float r_g = 2 * fmaxDivSR;

                // Quality is half the window width
                int wndWidth2 = quality;
                int wndWidth = quality * 2;

                float x = 0;
                int i, j;
                double r_y;
                int tau;
                double r_w;
                double r_a;
                double r_snc;
                for (i = 0; i < destLength; ++i)
                {
                    r_y = 0.0f;
                    for (tau = -wndWidth2; tau < wndWidth2; ++tau)
                    {
                        // input sample index
                        j = (int)(x + tau);

                        // Hann Window. Scale and calculate sinc
                        r_w = 0.5 - 0.5 * Math.Cos(2 * Math.PI * (0.5 + (j - x) / wndWidth));
                        r_a = 2 * Math.PI * (j - x) * fmaxDivSR;
                        r_snc = 1.0;
                        if (r_a != 0)
                            r_snc = Math.Sin(r_a) / r_a;

                        if ((j >= 0) && (j < srcLength))
                        {
                            r_y += r_g * r_w * r_snc * Sample[(channel * j) + ch];
                        }
                    }
                    samples[(channel * i) + ch] = (float)r_y;
                    x += dx;
                }
            }

            return samples;

        }
        

        public unsafe static void CopyBuffer(IntPtr inbuf, IntPtr outbuf, uint length, int channel)
        {
            float* inbuffer = (float*)inbuf.ToPointer();
            float* outbuffer = (float*)outbuf.ToPointer();
            for (int count = 0; count < length; count++)
                for (int count2 = 0; count2 < channel; count2++)
                    outbuffer[(count * channel) + count2] = inbuffer[(count * channel) + count2];
        }

        /// <summary>
        /// This function adds a low-passed signal to the original signal. The low-pass has a quite wide response. 
        /// (출처: http://www.musicdsp.org/archive.php?classid=0#235)
        /// </summary>
        public class BassBoosta
                {
                    float cap = 0;
                    /// <summary>
                    /// This function adds a low-passed signal to the original signal. The low-pass has a quite wide response. 
                    /// (출처: http://www.musicdsp.org/archive.php?classid=0#235)
                    /// </summary>
                    /// <param name="sample">샘플</param>
                    /// <param name="selectivity">frequency response of the LP (higher value gives a steeper one) [70.0 to 140.0 sounds good]</param>
                    /// <param name="gain">adjusts the final volume to handle cut-offs (might be good to set dynamically)</param>
                    /// <param name="ratio">how much of the filtered signal is mixed to the original</param>
                    /// <returns></returns>
                    public float Process(float sample, float selectivity, float gain, float ratio)
                    {
                        float gain1 = 1.0f / (selectivity + 1.0f);
                        cap = (sample + cap * selectivity) * gain1;
                        sample = Math.Min(Math.Max(-1.0f, sample + cap * ratio), 1.0f) * gain;
                        return sample;
                    }
                    public void Clear()
                    {
                        cap = 0;
                    }
                }

        public class BandPassFilter
        {
            /// <summary>
            /// Array of input values, latest are in front
            /// </summary>
            private float[] inputHistory = new float[2];
            private float[] inputHistory_24 = new float[2];

            /// <summary>
            /// Array of output values, latest are in front
            /// </summary>
            private float[] outputHistory = new float[3];
            private float[] outputHistory_24 = new float[3];
            float a1, a2, a3, b1, b2, c;
            public float Update(float newInput, float Frequency, float Resonance, float Sample, bool Low = true)
            {
                if (Low)
                {
                    c = 1f / (float)Math.Tan(1.75f * Frequency / Sample);//Math.PI * Frequency / Sample

                    a1 = 1.0f / (1 + Resonance * c + c * c);
                    a2 =  a1;
                    a3 = a1;
                    b1 = 2*(1 - c * c) * a1;
                    b2 = (1 - Resonance * c + c * c) * a1;
                }
                else
                {
                    c = (float)Math.Tan(1.75f * Frequency / Sample);
                    a1 = 1f / (1 + Resonance * c + c * c);
                    a2 = -2 * a1;
                    a3 = a1;
                    b1 = 2 * (c * c - 1) * a1;
                    b2 = (1 - Resonance * c + c * c) * a1;
                }

                float newOutput = a1 * newInput + a2 * this.inputHistory[0] + a3 * this.inputHistory[1] - b1 * this.outputHistory[0] - b2 * this.outputHistory[1];

                this.inputHistory[1] = this.inputHistory[0];
                this.inputHistory[0] = newInput;

                this.outputHistory[2] = this.outputHistory[1];
                this.outputHistory[1] = this.outputHistory[0];
                this.outputHistory[0] = newOutput;
                return (float)newOutput;
            }

            float Dive = 1;
            public float Update_16(float newInput, float Frequency, float Resonance, float Sample, bool Low = true)
            {
                if (Low)
                {
                    c = Dive / (float)Math.Tan(1.75f * Frequency / Sample);//Math.PI * Frequency / Sample

                    a1 = Dive / (Dive + Resonance * c + c * c);
                    a2 = 2 * a1;
                    a3 = a1;
                    b1 = 2 * (Dive - c * c) * a1;
                    b2 = (Dive - Resonance * c + c * c) * a1;
                }
                else
                {
                    c = (float)Math.Tan(1.75f * Frequency / Sample);
                    a1 = 1 / (1 + Resonance * c + c * c);
                    a2 = -2f * a1;
                    a3 = a1;
                    b1 = 2 * (c * c - 1) * a1;
                    b2 = (1 - Resonance * c + c * c) * a1;
                }

                float newOutput = 
                    //a1.Float2Short() * newInput.Float2Short() + a2.Float2Short() * this.inputHistory_24[0] + a3.Float2Short() * this.inputHistory_24[1] - b1.Float2Short() * this.outputHistory_24[0] - b2.Float2Short() * this.outputHistory_24[1];
                     a1 * newInput + a2 * this.inputHistory_24[0] + a3 * this.inputHistory_24[1] - b1 * this.outputHistory_24[0] - b2 * this.outputHistory_24[1];

                this.inputHistory_24[1] = this.inputHistory_24[0];
                this.inputHistory_24[0] = newInput;

                this.outputHistory_24[2] = this.outputHistory_24[1];
                this.outputHistory_24[1] = this.outputHistory_24[0];
                this.outputHistory_24[0] =newOutput;
                return 
                    (float)newOutput.ToShort() / (float)short.MaxValue;
                    //newOutput;
            }
            public void Clear()
            {
                inputHistory[0] = inputHistory[1] = inputHistory_24[0] = inputHistory_24[1] = 0;
                outputHistory[0] = outputHistory[1] = outputHistory[2] = outputHistory_24[1] = outputHistory_24[1] = outputHistory_24[2] = 0;
            }
            /*
            {
                float O = 2.0f * (float)Math.PI * Frequency / N;
                float C = Q / O;
                float L = 1f / Q / O;
                for (int c = 0; c < N; c += (int)Channels)
                {
                    float V = 0, I = 0, T;
                    for (int s = 0; s < Channels; s++)
                    {
                        T = (I - V) / C;
                        I += (Input[s + c] * O - V) / L;
                        V += T;
                        Output[s + c] = SetWetMix(Input[s + c], V / O, WetMix);
                    }
                }
            }*/


            /*
            public static void FilterButterworth(float frequency, int sampleRate, float resonance, bool PassType = true)
            {

                switch (PassType)
                {
                    case true:
                    c = 1.0f / (float)Math.Tan(Math.PI * frequency / sampleRate);
                    a1 = 1.0f / (1.0f + resonance * c + c * c);
                    a2 = 2f * a1;
                    a3 = a1;
                    b1 = 2.0f * (1.0f - c * c) * a1;
                    b2 = (1.0f - resonance * c + c * c) * a1;
                    break;
                    case false:
                    c = (float)Math.Tan(Math.PI * frequency / sampleRate);
                    a1 = 1.0f / (1.0f + resonance * c + c * c);
                    a2 = -2f * a1;
                    a3 = a1;
                    b1 = 2.0f * (c * c - 1.0f) * a1;
                    b2 = (1.0f - resonance * c + c * c) * a1;
                    break;
                }
                float newOutput = a1 * newInput + a2 * this.inputHistory[0] + a3 * this.inputHistory[1] - b1 * this.outputHistory[0] - b2 * this.outputHistory[1];

            }*/

            static double log(double Value){return Math.Log(Value);}
            /* - Three one-poles combined in parallel
             * - Output stays within input limits
             * - 18 dB/oct (approx) frequency response rolloff
             * - Quite fast, 2x3 parallel multiplications/sample, no internal buffers
             * - Time-scalable, allowing use with different samplerates
             * - Impulse and edge responses have continuous differential
             * - Requires high internal numerical precision
             */
            /// <summary>
            /// 
            /// </summary>
            /// /* Parameters */
            /// <param name="scale">Number of samples from start of edge to halfway to new value</param>
            /// <param name="smoothness">0 < Smoothness < 1. High is better, but may cause precision problems</param>
            public static unsafe void UU(float* Input, float* Output, uint N, float scale = 100f, float smoothness = 0.999f)
            {
                /* Precalc variables */
                double a = 1.0 - (2.4 / scale); // Could also be set directly
                double b = smoothness;      //         -"-
                double acoef = a;
                double bcoef = a * b;
                double ccoef = a * b * b;
                double mastergain = 1.0 / (-1.0 / (log(a) + 2.0 * log(b)) + 2.0 /
                                (log(a) + log(b)) - 1.0 / log(a));
                double again = mastergain;
                double bgain = mastergain * (log(a * b * b) * (log(a) - log(a * b)) /
                                    ((log(a * b * b) - log(a * b)) * log(a * b))
                                    - log(a) / log(a * b));
                double cgain = mastergain * (-(log(a) - log(a * b)) /
                                (log(a * b * b) - log(a * b)));

                /* Runtime variables */
                long streamofs;
                double areg = 0;
                double breg = 0;
                double creg = 0;

                /* Main loop */
                for (streamofs = 0; streamofs < N; streamofs++)
                {
                    /* Update filters */
                    areg = acoef * areg + Input[streamofs];
                    breg = bcoef * breg + Input[streamofs];
                    creg = ccoef * creg + Input[streamofs];

                    /* Combine filters in parallel */
                    double temp = (again * areg
                                           + bgain * breg
                                           + cgain * creg);

                    /* Check clipping */
                    if (temp > 32767) temp = 32767;
                    else if (temp < -32768) temp = -32768;

                    /* Store new value */
                    Output[streamofs] = (float)temp;
                }
            }
            public static float UU(float Input, float scale = 100f, float smoothness = 0.999f)
            {
                /* Precalc variables */
                double a = 1.0 - (2.4 / scale); // Could also be set directly
                double b = smoothness;      //         -"-
                double acoef = a;
                double bcoef = a * b;
                double ccoef = a * b * b;
                double mastergain = 1.0 / (-1.0 / (log(a) + 2.0 * log(b)) + 2.0 /
                                (log(a) + log(b)) - 1.0 / log(a));
                double again = mastergain;
                double bgain = mastergain * (log(a * b * b) * (log(a) - log(a * b)) /
                                    ((log(a * b * b) - log(a * b)) * log(a * b))
                                    - log(a) / log(a * b));
                double cgain = mastergain * (-(log(a) - log(a * b)) /
                                (log(a * b * b) - log(a * b)));

                /* Runtime variables */
                long streamofs;
                double areg = 0;
                double breg = 0;
                double creg = 0;
                {
                    /* Update filters */
                    areg = acoef * areg + Input;
                    breg = bcoef * breg + Input;
                    creg = ccoef * creg + Input;

                    /* Combine filters in parallel */
                    double temp = (again * areg
                                           + bgain * breg
                                           + cgain * creg);

                    /* Check clipping */
                    if (temp > 32767) temp = 32767;
                    else if (temp < -32768) temp = -32768;

                    /* Store new value */
                    return (float)temp;
                }
            }
        }

        /// <summary>
        /// polyphase filters, used for up and down-sampling
        /// (풀처: http://www.musicdsp.org/archive.php?classid=3#39)
        /// </summary>
        public static class PolyphaseFilters
        {
            public interface IPolyphaseFilters
            {
                double Process(double input);
            }
            public class AllPassFilter : IPolyphaseFilters
            {
                double a;

                double x0;
                double x1;
                double x2;

                double y0;
                double y1;
                double y2;

                public AllPassFilter(double coefficient)
                {
                    a = coefficient;

                    x0 = 0.0;
                    x1 = 0.0;
                    x2 = 0.0;

                    y0 = 0.0;
                    y1 = 0.0;
                    y2 = 0.0;

                }

                public double Process(double input)
                {
                    //shuffle inputs
                    x2 = x1;
                    x1 = x0;
                    x0 = input;

                    //shuffle outputs
                    y2 = y1;
                    y1 = y0;

                    //allpass filter 1
                    double output = x2 + ((input - y2) * a);

                    y0 = output;

                    return output;
                }
            }
            public class AllPassFilterCascade : IPolyphaseFilters
            {
                AllPassFilter[] allpassfilter;
                int numfilters;

                public AllPassFilterCascade(double[] coefficient, int N)
                {
                    allpassfilter = new AllPassFilter[N];

                    for (int i = 0; i < N; i++)
                        allpassfilter[i] = new AllPassFilter(coefficient[i]);

                    numfilters = N;
                }
                public unsafe AllPassFilterCascade(double* coefficient, int N)
                {
                    allpassfilter = new AllPassFilter[N];
                    for (int i = 0; i < N; i++)
                        allpassfilter[i] = new AllPassFilter(coefficient[i]);

                    numfilters = N;
                }

                public double Process(double input)
                {
                    double output = input;

                    int i = 0;

                    do
                    {
                        output = allpassfilter[i].Process(output);
                        i++;

                    } while (i < numfilters);

                    return output;
                }
            }
            public class HalfBandFilter
            {
                AllPassFilterCascade filter_a;
                AllPassFilterCascade filter_b;
                //double oldout = 0;

                public HalfBandFilter(int order, bool steep)
                {
                    if (steep)
                    {
                        if (order == 12)	//rejection=104dB, transition band=0.01
                        {
                            double[] a_coefficients = {
                                0.036681502163648017,
                                0.2746317593794541,
			                    0.56109896978791948,
			                    0.769741833862266,
			                    0.8922608180038789,
			                    0.962094548378084};

                            double[] b_coefficients ={
			                    0.13654762463195771,
			                    0.42313861743656667,
			                    0.6775400499741616,
			                    0.839889624849638,
			                    0.9315419599631839,
			                    0.9878163707328971};

                            filter_a = new AllPassFilterCascade(a_coefficients, 6);
                            filter_b = new AllPassFilterCascade(b_coefficients, 6);
                        }
                        else if (order == 10)	//rejection=86dB, transition band=0.01
                        {
                            double[] a_coefficients ={
			                    0.051457617441190984,
			                    0.35978656070567017,
			                    0.6725475931034693,
			                    0.8590884928249939,
			                    0.9540209867860787};

                            double[] b_coefficients ={
			                    0.18621906251989334,
			                    0.529951372847964,
			                    0.7810257527489514,
			                    0.9141815687605308,
			                    0.985475023014907};

                            filter_a = new AllPassFilterCascade(a_coefficients, 5);
                            filter_b = new AllPassFilterCascade(b_coefficients, 5);
                        }
                        else if (order == 8)	//rejection=69dB, transition band=0.01
                        {
                            double[] a_coefficients ={
			                    0.07711507983241622,
			                    0.4820706250610472,
			                    0.7968204713315797,
			                    0.9412514277740471};

                            double[] b_coefficients =
                            {
                                0.02659685265210946,
			                    0.6651041532634957,
			                    0.8841015085506159,
			                    0.9820054141886075
                            };

                            filter_a = new AllPassFilterCascade(a_coefficients, 4);
                            filter_b = new AllPassFilterCascade(b_coefficients, 4);
                        }
                        else if (order == 6)	//rejection=51dB, transition band=0.01
                        {
                            double[] a_coefficients ={
			                    0.1271414136264853,
			                    0.6528245886369117,
		                    	0.9176942834328115};

                            double[] b_coefficients ={
		                    	0.40056789819445626,
	                    		0.8204163891923343,
	                    		0.9763114515836773};

                            filter_a = new AllPassFilterCascade(a_coefficients, 3);
                            filter_b = new AllPassFilterCascade(b_coefficients, 3);
                        }
                        else if (order == 4)	//rejection=53dB,transition band=0.05
                        {
                            double[] a_coefficients ={
		                    	0.12073211751675449,
		                    	0.6632020224193995};

                            double[] b_coefficients ={
		                    	0.3903621872345006,
		                    	0.890786832653497};

                            filter_a = new AllPassFilterCascade(a_coefficients, 2);
                            filter_b = new AllPassFilterCascade(b_coefficients, 2);
                        }

                        else	//order=2, rejection=36dB, transition band=0.1
                        {
                            double[] a_coefficients = { 0.23647102099689224 };
                            double[] b_coefficients = { 0.7145421497126001 };

                            filter_a = new AllPassFilterCascade(a_coefficients, 1);//(&a_coefficients,1);
                            filter_b = new AllPassFilterCascade(b_coefficients, 1);//(&b_coefficients,1);
                        }
                    }
                    else	//softer slopes, more attenuation and less stopband ripple
                    {
                        if (order == 12)	//rejection=150dB, transition band=0.05
                        {
                            double[] a_coefficients ={
		                    	0.01677466677723562,
		                    	0.13902148819717805,
		                    	0.3325011117394731,
		                    	0.53766105314488,
		                    	0.7214184024215805,
		                    	0.8821858402078155};

                            double[] b_coefficients ={
		                    	0.06501319274445962,
		                    	0.23094129990840923,
		                    	0.4364942348420355,
		                    	0.06329609551399348,
		                    	0.80378086794111226,
		                    	0.9599687404800694};

                            filter_a = new AllPassFilterCascade(a_coefficients, 6);
                            filter_b = new AllPassFilterCascade(b_coefficients, 6);
                        }
                        else if (order == 10)	//rejection=133dB, transition band=0.05
                        {
                            double[] a_coefficients ={
			                    0.02366831419883467,
		                    	0.18989476227180174,
		                    	0.43157318062118555,
		                    	0.6632020224193995,
		                    	0.860015542499582};

                            double[] b_coefficients ={
		                    	0.09056555904993387,
		                    	0.3078575723749043,
		                    	0.5516782402507934,
		                    	0.7652146863779808,
		                    	0.95247728378667541};

                            filter_a = new AllPassFilterCascade(a_coefficients, 5);
                            filter_b = new AllPassFilterCascade(b_coefficients, 5);
                        }
                        else if (order == 8)	//rejection=106dB, transition band=0.05
                        {
                            double[] a_coefficients ={
	                    		0.03583278843106211,
	                    		0.2720401433964576,
	                    		0.5720571972357003,
	                    		0.827124761997324};

                            double[] b_coefficients ={
		                    	0.1340901419430669,
	                    		0.4243248712718685,
		                    	0.7062921421386394,
		                    	0.9415030941737551};

                            filter_a = new AllPassFilterCascade(a_coefficients, 4);
                            filter_b = new AllPassFilterCascade(b_coefficients, 4);
                        }
                        else if (order == 6)	//rejection=80dB, transition band=0.05
                        {
                            double[] a_coefficients ={
		                    	0.06029739095712437,
	                    		0.4125907203610563,
		                    	0.7727156537429234};

                            double[] b_coefficients ={
		                    	0.21597144456092948,
		                    	0.6043586264658363,
		                    	0.9238861386532906};

                            filter_a = new AllPassFilterCascade(a_coefficients, 3);
                            filter_b = new AllPassFilterCascade(b_coefficients, 3);
                        }
                        else if (order == 4)	//rejection=70dB,transition band=0.1
                        {
                            double[] a_coefficients ={
		                    	0.07986642623635751,
		                    	0.5453536510711322};

                            double[] b_coefficients ={
		                    	0.28382934487410993,
		                    	0.8344118914807379};

                            filter_a = new AllPassFilterCascade(a_coefficients, 2);
                            filter_b = new AllPassFilterCascade(b_coefficients, 2);
                        }

                        else	//order=2, rejection=36dB, transition band=0.1
                        {
                            double[] a_coefficients = { 0.23647102099689224 };
                            double[] b_coefficients = { 0.7145421497126001 };

                            filter_a = new AllPassFilterCascade(a_coefficients, 1);//(&a_coefficients,1);
                            filter_b = new AllPassFilterCascade(b_coefficients, 1);//(&b_coefficients,1);
                        }
                    }
                    //oldout = 0.0;
                }
            }
        }
    }
    #region 유틸리티
    internal static class HSDSPUtils
    {
        public const int Int24MaxValue = 8388607;//8388608
        public const int UInt24MaxValue = 16777216;
        //static float PeakValue = float.NaN;
        //public static int ToInt(this float Value){double b = Math.Max(Math.Min(Value, 1), -1);return (int)(int.MaxValue * b);}
        //public static int ToInt_Distoration(this float Value){return (int)(int.MaxValue * Math.Max(Math.Min(Value, 1), -1));}

        public const float DistorationPeak = 1.0000001f;
        public static uint ToUInt(this float Value, bool Distoration = false)
        {
            return (uint)(ToInt(Value, Distoration) + int.MaxValue);
        }
        public static int ToInt(this float Value, bool Distoration = false)
        {
            int MAX = Distoration ? 2147483647 : 2147483646;
            float a1 = Math.Max(Math.Min(Value, 1), -1);
            return (int)(a1 * MAX);

            
            //if (!Distoration)
            //{
            //    double a = Math.Max(Math.Min(Value, 1), -1);
            //    int b = (int)(int.MaxValue * a);
            //    /*
            //    int tmp = (int)(int.MaxValue *Math.Max(Math.Min(Value, 1), -1));
            //    if ((Value > 1 || Value < -1))
            //    {
            //        int asd = tmp.ToString().IndexOf("-");
            //        int asd1 = b.ToString().IndexOf("-");
            //        if (asd != asd1)
            //        {
            //            if ((Math.Abs(Value) - 1) < PeakValue)
            //            {
            //                PeakValue = Math.Abs(Value) - 1;
            //                Console.WriteLine(string.Format("Value={0}({1}), 1차={2} / 2차={3}", Value.ToString(), (Math.Abs(Value) - 1).ToString(), tmp.ToString(), b.ToString()));
            //            }
            //        }
            //    }*/
            //    return b;
            //}
            //else return (int)(int.MaxValue * Math.Max(Math.Min(Value, 1), -1));
        }

        public static int ToInt(this double Value, bool Distoration = false)
        {
            //if (!Distoration){double a = Math.Max(Math.Min(Value, 1), -1); return (int)(int.MaxValue * a);}
            //else return (int)(int.MaxValue * Math.Max(Math.Min(Value, 1), -1));
            uint MAX = Distoration ? 2147483648 : 2147483647;
            double a = Math.Max(Math.Min(Value, 1), -1);
            return (int)(a * MAX);
        }
        //public static int ToInt24(this float Value){return (int)(8388608f * Math.Max(Math.Min(Value, 1), -1)); }
        public static int ToUInt24(this float Value, bool Distoration = false)
        {
            return ToInt24(Value, Distoration) + Int24MaxValue;
        }
        public static int ToInt24(this float Value, bool Distoration = false)
        {
            int a = (int)(Int24MaxValue * Math.Max(Math.Min(Value, 1), -1));//0x7
            return (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
        }
        //public static short ToShort(this float Value){return (short)(short.MaxValue * Math.Max(Math.Min(Value, 1), -1)); }
        public static ushort ToUShort(this float Value, bool Distoration = false)
        {
            float a = Math.Max(Math.Min(Value, 1), -1);
            return Distoration ? (ushort)((a * 32768) + 32768) : (ushort)((a * 32767) + 32768);
        }
        public static short ToShort(this float Value, bool Distoration = false)
        {
            float a = Math.Max(Math.Min(Value, 1), -1);
            return Distoration ? (short)(a * 32768) : (short)(a * 32767);
        }
        public static short ToShort(this double Value, bool Distoration = false)
        {
            double a = Math.Max(Math.Min(Value, 1), -1);
            return Distoration ? (short)(a * 32768) : (short)(a * 32767);
        }
        //public static ushort Float2UShort(this float Value){return (ushort)(ushort.MaxValue * Math.Max(Math.Min(Value, 1), -1));}
        public static sbyte ToSByte(this float Value, bool Distoration = false)
        {
            float a = Math.Max(Math.Min(Value, 1), -1);
            return Distoration ? (sbyte)(a * 128) : (sbyte)(a * 127);
            //double a = Math.Max(Math.Min(Value, 1), -1);
            //return Distoration ? (SByte)(a * 128) : (SByte)(a * 127); 
        }
        public static byte ToByte(this float Value, bool Distoration = false)
        {
            float a = Math.Max(Math.Min(Value, 1), -1);
            return Distoration ? (byte)((a * 128) + 128) : (byte)((a * 127) + 128);
            //double a = Math.Max(Math.Min(Value, 1), -1);
            //return Distoration ? (byte)((a * 128) + 255) : (byte)((a * 127) + 255);
        }
        /*
        public static byte ToByteEx(this float Value, bool Distoration = false)
        {
            double a = Math.Max(Math.Min(Value, 1), -1);
            return Distoration ? (byte)((a * 128) + 128) : (byte)((a * 127) + 127);
        }*/
        //[Obsolete
        //public static short ToByte(this float Value){return (byte)((Math.Max(Math.Min(Value, 1), -1) * 128f) + 128);}
        public static short To11Bit(this float Value, bool Distoration = false, bool Ex = false)
        {
            int MAX = Distoration ? 2048 : 2047;
            int a = (int)(MAX * Math.Max(Math.Min(Value, 1), -1));
            //int tmp = (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
            if (Ex) return (short)(a + 2048);
            return (short)a;
        }
        public static sbyte To7Bit(this float Value, bool Distoration = false, bool Ex = false)
        {
            int MAX = Distoration ? 64 : 63;
            int a = (int)(MAX * Math.Max(Math.Min(Value, 1), -1));
            //int tmp = (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
            if (Ex) return (sbyte)(a + 64);
            return (sbyte)a;
        }
        public static sbyte To6Bit(this float Value, bool Distoration = false, bool Ex = false)
        {
            int MAX = Distoration ? 32 : 31;
            int a = (int)(MAX * Math.Max(Math.Min(Value, 1), -1));
            //int tmp = (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
            if (Ex) return (sbyte)(a + 32);
            return (sbyte)a;
        }
        public static sbyte To5Bit(this float Value, bool Distoration = false, bool Ex = false)
        {
            int MAX = Distoration ? 16 : 15;
            int a = (int)(MAX * Math.Max(Math.Min(Value, 1), -1));
            //int tmp = (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
            if (Ex) return (sbyte)(a + 16);
            return (sbyte)a;
        }
        public static sbyte To4Bit(this float Value, bool Distoration = false, bool Ex = false)
        {
            int MAX = Distoration ? 8 : 7;
            int a = (int)(MAX * Math.Max(Math.Min(Value, 1), -1));
            //int tmp = (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
            if (Ex) return (sbyte)(a + 8);
            return (sbyte)a;
        }
        public static sbyte To3Bit(this float Value, bool Distoration = false, bool Ex = false)
        {
            int MAX = Distoration ? 4 : 3;
            int a = (int)(MAX * Math.Max(Math.Min(Value, 1), -1));
            //int tmp = (Value > 1 || Value < -1) && Distoration ? -(a - 1) : a;
            if (Ex) return (sbyte)(a + 4);
            return (sbyte)a;
        }
        
        
        public static float ToFloat(this int Value){return (float)((double)Value/(double)int.MaxValue);}
        public static float ToFloat_Int24(this int Value){return (float)Value/(float)Int24MaxValue;}
        public static float ToFloat(this short Value){return (float)Value/(float)short.MaxValue;}
        public static float　ToFloat(this byte Value){return (float)Value/254f;}

        #region 데시벨 구하기
        public static float Decibels2(this float Value, float MaxValue = 1){return (float)(20 * Math.Log10(Value / MaxValue)); }
        public static double Decibels2(this double Value, double MaxValue = 1){return 20 * Math.Log10(Value / MaxValue); }
        public static float Decibels(this float Value, float Peak = 0){return Value == 0 && Peak == 0 ? 0 : 10.0f * (float)Math.Log10(mag_sqrd(Value, Peak)); }
        public static double Decibels(this double Value, double Peak = 0){return ((Value == 0 && Peak == 0) ? (0) : 10.0 * Math.Log10(mag_sqrd(Value, Peak)));}
        public static float mag_sqrd(float re, float im){return (re * re + im * im); }
        public static double mag_sqrd(double re, double im){return (re * re + im * im); }
        #endregion

        #region 최댓값 구하기
        public static unsafe float[] GetMax(float* array, uint length, int channel)
        {
            float[] arr = new float[channel];
            for(int i = 0; i < length; i+=channel)
                for(int j = 0; j < channel; j++)
                    if(arr[j] < array[i + j]) arr[j] = array[i+j];
            return arr;
        }
        public static float[] GetMax(this float[] array, uint channel)
        {
            float[] arr = new float[channel];
            for (uint i = 0; i < array.Length; i += channel)
                for (int j = 0; j < channel; j++)
                    if (arr[j] < array[i + j]) arr[j] = array[i + j];
            return arr;
        }
        #endregion
    }
    #endregion
}
