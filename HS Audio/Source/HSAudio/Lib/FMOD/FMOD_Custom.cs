﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using HSAudio;
using HSAudio.Lib.FMOD;

namespace HSAudio.Lib.FMOD
{
    /*
        FMOD version number.  Check this against FMOD::System::getVersion / System_GetVersion
        0xaaaabbcc -> aaaa = major version number.  bb = minor version number.  cc = development version number.
    */
    public class VERSION
    {
        public const int number = 0x00011009;
        //public static readonly string dll = IntPtr.Size == 8 ? "hs_fmod64" : "hs_fmod";

#if LINUX && x64
        public const string dll = "libHSAudioCore64";
#elif LINUX && ARM
        public const string dll = "libHSAudioCoreARM";
#elif  LINUX && ARM64
        public const string dll = "libHSAudioCoreARM64";
#elif LINUX
        public const string dll = "libHSAudioCore";
#elif ANDROID
        public const string dll = "HSAudioCore";
#elif (WIN_XP || WIN_VISTA || WIN_10_1703 || WIN_METRO)
#if x64
       public const string dll = "HSAudioCore64";
#else
        public const string dll = "HSAudioCore";
#endif
#endif
    }


    /*
    [ENUM]
    [
        [DESCRIPTION]
        These output types are used with System::setOutput / System::getOutput, to choose which output method to use.

        [REMARKS]
        To pass information to the driver when initializing fmod use the *extradriverdata* parameter in System::init for the following reasons.

        - FMOD_OUTPUTTYPE_WAVWRITER     - extradriverdata is a pointer to a char * file name that the wav writer will output to.
        - FMOD_OUTPUTTYPE_WAVWRITER_NRT - extradriverdata is a pointer to a char * file name that the wav writer will output to.
        - FMOD_OUTPUTTYPE_DSOUND        - extradriverdata is cast to a HWND type, so that FMOD can set the focus on the audio for a particular window.
        - FMOD_OUTPUTTYPE_PS3           - extradriverdata is a pointer to a FMOD_PS3_EXTRADRIVERDATA struct. This can be found in fmodps3.h.
        - FMOD_OUTPUTTYPE_XBOX360       - extradriverdata is a pointer to a FMOD_360_EXTRADRIVERDATA struct. This can be found in fmodxbox360.h.

        Currently these are the only FMOD drivers that take extra information.  Other unknown plugins may have different requirements.
    
        Note! If FMOD_OUTPUTTYPE_WAVWRITER_NRT or FMOD_OUTPUTTYPE_NOSOUND_NRT are used, and if the System::update function is being called
        very quickly (ie for a non realtime decode) it may be being called too quickly for the FMOD streamer thread to respond to.
        The result will be a skipping/stuttering output in the captured audio.
    
        To remedy this, disable the FMOD streamer thread, and use FMOD_INIT_STREAM_FROM_UPDATE to avoid skipping in the output stream,
        as it will lock the mixer and the streamer together in the same thread.
    
        [SEE_ALSO]
            System::setOutput
            System::getOutput
            System::setSoftwareFormat
            System::getSoftwareFormat
            System::init
            System::update
            FMOD_INITFLAGS
    ]
    */

    public enum OUTPUTTYPE : int
    {
        AUTODETECT,      /* Picks the best output mode for the platform. This is the default. */

        UNKNOWN,         /* All - 3rd party plugin, unknown. This is for use with System::getOutput only. */
        NOSOUND,         /* All - Perform all mixing but discard the final output. */
        WAVWRITER,       /* All - Writes output to a .wav file. */
        NOSOUND_NRT,     /* All - Non-realtime version of FMOD_OUTPUTTYPE_NOSOUND. User can drive mixer with System::update at whatever rate they want. */
        WAVWRITER_NRT,   /* All - Non-realtime version of FMOD_OUTPUTTYPE_WAVWRITER. User can drive mixer with System::update at whatever rate they want. */

        DSOUND,          /* Win                  - Direct Sound.                        (Default on Windows XP and below) */
        WINMM,           /* Win                  - Windows Multimedia. */
        WASAPI,          /* Win/WinStore/XboxOne - Windows Audio Session API.           (Default on Windows Vista and above, Xbox One and Windows Store Applications) */
        ASIO,            /* Win                  - Low latency ASIO 2.0. */
        PULSEAUDIO,      /* Linux                - Pulse Audio.                         (Default on Linux if available) */
        ALSA,            /* Linux                - Advanced Linux Sound Architecture.   (Default on Linux if PulseAudio isn't available) */
        COREAUDIO,       /* Mac/iOS              - Core Audio.                          (Default on Mac and iOS) */
        XAUDIO,          /* Xbox 360             - XAudio.                              (Default on Xbox 360) */
        PS3,             /* PS3                  - Audio Out.                           (Default on PS3) */
        AUDIOTRACK,      /* Android              - Java Audio Track.                    (Default on Android 2.2 and below) */
        OPENSL,          /* Android              - OpenSL ES.                           (Default on Android 2.3 and above) */
        WIIU,            /* Wii U                - AX.                                  (Default on Wii U) */
        AUDIOOUT,        /* PS4/PSVita           - Audio Out.                           (Default on PS4 and PS Vita) */
        AUDIO3D,         /* PS4                  - Audio3D. */
        ATMOS,           /* Win                  - Dolby Atmos (WASAPI). */
        WEBAUDIO,        /* Web Browser          - JavaScript webaudio output.          (Default on JavaScript) */
        NNAUDIO,         /* NX                   - NX nn::audio.                        (Default on NX) */
        WINSONIC,        /* XboxOne              - Windows Sonic. */

        MAX,             /* Maximum number of output types supported. */
        WASAPI_EX        /*WASAPI Exclusive 전용모드*/
    }

    public partial class FD_System
    {
        public RESULT createSound(string name, MODE mode, out FD_Sound sound, out CREATESOUNDEXINFO exinfo)
        {
            CREATESOUNDEXINFO exinfo1 = new CREATESOUNDEXINFO();
            exinfo1.cbsize = Marshal.SizeOf(exinfo1);

            RESULT result = createSound(name, mode, ref exinfo1, out sound);
            exinfo = exinfo1;
            return result;
        }

#if DEBUG
        public RESULT registerCodec(ref CODEC_DESCRIPTION description, out uint handle, uint priority)
        {
            return FMOD_System_RegisterCodec(rawPtr, ref description, out handle, priority);
        }
        public RESULT registerOutput(ref OUTPUT_DESCRIPTION description, out uint handle)
        {
            return FMOD_System_RegisterOutput(rawPtr, ref description, out handle);
        }
        public RESULT registerDSP(ref DSP_DESCRIPTION description, out uint handle)
        {
            return FMOD_System_RegisterDSP(rawPtr, ref description, out handle);
        }
#else
        internal RESULT registerCodec(ref CODEC_DESCRIPTION description, out uint handle, uint priority)
        {
            return FMOD_System_RegisterCodec(rawPtr, ref description, out handle, priority);
        }
        internal RESULT registerOutput(ref OUTPUT_DESCRIPTION description, out uint handle)
        {
            return FMOD_System_RegisterOutput(rawPtr, ref description, out handle);
        }
        internal RESULT registerDSP(ref DSP_DESCRIPTION description, out uint handle)
        {
            return FMOD_System_RegisterDSP(rawPtr, ref description, out handle);
        }
#endif

        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_System_RegisterCodec(IntPtr system, ref CODEC_DESCRIPTION description, out uint handle, uint priority);
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_System_RegisterOutput(IntPtr system, ref OUTPUT_DESCRIPTION description, out uint handle);
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_System_RegisterDSP(IntPtr system, ref DSP_DESCRIPTION description, out uint handle);

        public RESULT createSound(MODE mode, ref CREATESOUNDEXINFO exinfo, out FD_Sound sound)
        {
            sound = null;

            exinfo.cbsize = Marshal.SizeOf(exinfo);

            IntPtr soundraw;
            RESULT result = FMOD_System_CreateSound(rawPtr, null, mode, ref exinfo, out soundraw);
            sound = new FD_Sound(soundraw);

            return result;
        }
        public RESULT createStream(MODE mode, ref CREATESOUNDEXINFO exinfo, out FD_Sound sound)
        {
            sound = null;

            exinfo.cbsize = Marshal.SizeOf(exinfo);

            IntPtr soundraw;
            RESULT result = FMOD_System_CreateStream(rawPtr, null, mode, ref exinfo, out soundraw);
            sound = new FD_Sound(soundraw);

            return result;
        }
    }

    public partial class FD_ChannelControl
    {
        public RESULT set3DAttributes(ref Vector3D pos, ref Vector3D vel, ref Vector3D alt_pan_pos)
        {
            return FMOD_ChannelGroup_Set3DAttributes(rawPtr, ref pos, ref vel, ref alt_pan_pos);
        }
        public RESULT set3DAttributes(ref Vector3D pos, ref Vector3D vel)
        {
            return FMOD_ChannelGroup_Set3DAttributes(rawPtr, ref pos, ref vel, IntPtr.Zero);
        }
        public RESULT get3DAttributes(out Vector3D pos, out Vector3D vel, out Vector3D alt_pan_pos)
        {
            return FMOD_ChannelGroup_Get3DAttributes(rawPtr, out pos, out vel, out alt_pan_pos);
        }
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_ChannelGroup_Set3DAttributes(IntPtr channelgroup, ref Vector3D pos, ref Vector3D vel, IntPtr alt_pan_pos);
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_ChannelGroup_Set3DAttributes(IntPtr channelgroup, ref Vector3D pos, ref Vector3D vel, ref Vector3D alt_pan_pos);
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_ChannelGroup_Get3DAttributes(IntPtr channelgroup, out Vector3D pos, out Vector3D vel, out Vector3D alt_pan_pos);
    }

    public partial class FD_DSP
    {
        public RESULT setParameterData(int index, IntPtr data, uint length)
        {
            return FMOD_DSP_SetParameterData(rawPtr, index, data, length);
        }

        public override string ToString()
        {
            return string.Format("DSP [{0} (0x{1})]", rawPtr.ToInt32(), rawPtr.ToString("X"));
        }

        public override bool Equals(object obj)
        {
            FD_DSP dsp = obj as FD_DSP;
            if (obj == null) return false;
            else return ((FD_DSP)obj).rawPtr == rawPtr;
        }
        public override int GetHashCode(){return base.GetHashCode();}
    }

    public partial class FD_Reverb3D
    {
        public RESULT set3DAttributes(ref Vector3D position, float mindistance, float maxdistance)
        {
            return FMOD_Reverb3D_Set3DAttributes(rawPtr, ref position, mindistance, maxdistance);
        }
        public RESULT get3DAttributes(out Vector3D position, out float mindistance, out float maxdistance)
        {
            return FMOD_Reverb3D_Get3DAttributes(rawPtr, out position, out mindistance, out maxdistance);
        }
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_Reverb3D_Set3DAttributes(IntPtr reverb, ref Vector3D position, float mindistance, float maxdistance);
        [DllImport(VERSION.dll)]
        private static extern RESULT FMOD_Reverb3D_Get3DAttributes(IntPtr reverb, out Vector3D position, out float mindistance, out float maxdistance);
    }

    public partial class FD_HandleBase
    {
        public void setRaw(IntPtr ptr)
        {
            rawPtr = ptr;
        }
    }

    /*
    [STRUCTURE] 
    [
        [DESCRIPTION]   
        Structure describing a globally unique identifier.

        [REMARKS]

        [PLATFORMS]
        Win32, Win64, Linux, Linux64, Macintosh, Xbox360, PlayStation Portable, PlayStation 3, Wii

        [SEE_ALSO]      
        System::getDriverInfo
    ]
    */

    [StructLayout(LayoutKind.Sequential)]
    public struct GUID
    {
        public uint Data1;       /* Specifies the first 8 hexadecimal digits of the GUID */
        public ushort Data2;       /* Specifies the first group of 4 hexadecimal digits.   */
        public ushort Data3;       /* Specifies the second group of 4 hexadecimal digits.  */
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
        public byte[] Data4;       /* Array of 8 bytes. The first 2 bytes contain the third group of 4 hexadecimal digits. The remaining 6 bytes contain the final 12 hexadecimal digits. */


        public override string ToString() { return ToString(false); }
        public string ToString(bool Bracket, bool Upper = false)
        {
            /*
            StringBuilder a = new StringBuilder();
            for (long i = 1; i < Data4.Length; i++)a.Append(Data4[i].ToString("X2"));
            if (Attach) return "{"+string.Format("{0}-{1}-{2}-{3}", Data1.ToString("X8"), Data2.ToString("X4"), Data3.ToString("X4"), a.ToString())+"}";
            else return string.Format("{0}-{1}-{2}-{3}", Data1.ToString("X8"), Data2.ToString("X4"), Data3.ToString("X4"), a.ToString());*/
            Guid GetGuid = this;
            if (Bracket) return "{" + (Upper ? GetGuid.ToString().ToUpper() : GetGuid.ToString().ToLower()) + "}";
            else return (Upper ? GetGuid.ToString().ToUpper() : GetGuid.ToString().ToLower());
        }

        public override bool Equals(object obj)
        {
            GUID d;
            if (obj is GUID) d = (GUID)obj;
            else return false;

            bool tmp = d.Data1 == Data1 &&
                       d.Data2 == Data2 &&
                       d.Data3 == Data3;
            return tmp && EqualByteArray(d.Data4, Data4);
        }
        public override int GetHashCode() {return base.GetHashCode(); }//{ return (int)Data1 | Data2 | Data3; }

        public static bool EqualByteArray(byte[] a, byte[] b)
        {
            if (a == null && b != null) return false;
            if (a != null && b == null) return false;
            if (a == null && b == null) return true;
            if (a.Length != b.Length) return false;

            for (int i = 0; i < a.Length; i++)
                if (a[i] != b[i]) return false;
            return true;
        }

        public bool IsEmpty() { return IsEmpty(this); }
        public static bool IsEmpty(GUID guid)
        {
            for (int i = 0; guid.Data4 != null && i < guid.Data4.Length; i++)
                if (guid.Data4[i] != 0) return false;
            return guid.Data1 == 0 && guid.Data2 == 0 && guid.Data3 == 0;
        }

        public static implicit operator Guid(GUID Guid) { return new Guid((int)Guid.Data1, (short)Guid.Data2, (short)Guid.Data3, Guid.Data4); }
        public static implicit operator GUID(Guid Guid)
        {
            byte[] a = Guid.ToByteArray();
            GUID guid = new GUID();

            guid.Data1 = (uint)(a[3] << 24 | a[2] << 16 | a[1] << 8 | a[0]);
            guid.Data2 = (ushort)(a[5] << 8 | a[4]);
            guid.Data3 = (ushort)(a[7] << 8 | a[6]);
            //a.RemoveRange(0, 7);

            byte[] b = new byte[8];
            for (int i = 0; i < 8; i++) b[i] = a[i + 8];

            guid.Data4 = b;
            return guid;
        }
    }



#region wrapperinternal
    [StructLayout(LayoutKind.Sequential)]
    public struct StringWrapper
    {
        IntPtr nativeUtf8Ptr;

        public static implicit operator string(StringWrapper fstring)
        {
            if (fstring.nativeUtf8Ptr == IntPtr.Zero)
            {
                return "";
            }

            int strlen = 0;
            while (Marshal.ReadByte(fstring.nativeUtf8Ptr, strlen) != 0)
            {
                strlen++;
            }
            if (strlen > 0)
            {
                byte[] bytes = new byte[strlen];
                Marshal.Copy(fstring.nativeUtf8Ptr, bytes, 0, strlen);
                return Encoding.UTF8.GetString(bytes, 0, strlen);
            }
            else
            {
                return "";
            }
        }
        public static implicit operator StringWrapper(string str)
        {
            if (str == null) return null;
            else return new StringWrapper() { nativeUtf8Ptr = Marshal.StringToHGlobalUni(str) };
        }
    }
#endregion

#region CODEC
#if DEBUG
    public delegate RESULT CODEC_OPENCALLBACK(ref CODEC_STATE codec_state, MODE usermode, ref CREATESOUNDEXINFO userexinfo);
    public delegate RESULT CODEC_CLOSECALLBACK(ref CODEC_STATE codec_state);
    public delegate RESULT CODEC_READCALLBACK(ref CODEC_STATE codec_state, IntPtr buffer, uint samples_in, ref uint samples_out);
    public delegate RESULT CODEC_GETLENGTHCALLBACK(ref CODEC_STATE codec_state, ref int length, TIMEUNIT lengthtype);
    public delegate RESULT CODEC_SETPOSITIONCALLBACK(ref CODEC_STATE codec_state, int subsound, uint position, TIMEUNIT postype);
    public delegate RESULT CODEC_GETPOSITIONCALLBACK(ref CODEC_STATE codec_state, ref uint position, TIMEUNIT postype);
    public delegate RESULT CODEC_SOUNDCREATECALLBACK(ref CODEC_STATE codec_state, int subsound, ref FD_Sound sound);
    public delegate RESULT CODEC_METADATACALLBACK(ref CODEC_STATE codec_state, TAGTYPE tagtype, string name, IntPtr data, uint datalen, TAGDATATYPE datatype, int unique);//char* 형 검사하기 (out? in?)
    public delegate RESULT CODEC_GETWAVEFORMATCALLBACK(ref CODEC_STATE codec_state, ref CODEC_WAVEFORMAT waveformat);
#else
    internal delegate RESULT CODEC_OPENCALLBACK(ref CODEC_STATE codec_state, MODE usermode, ref CREATESOUNDEXINFO userexinfo);
    internal delegate RESULT CODEC_CLOSECALLBACK(ref CODEC_STATE codec_state);
    internal delegate RESULT CODEC_READCALLBACK(ref CODEC_STATE codec_state, IntPtr buffer, uint samples_in, ref uint samples_out);
    internal delegate RESULT CODEC_GETLENGTHCALLBACK(ref CODEC_STATE codec_state, ref int length, TIMEUNIT lengthtype);
    internal delegate RESULT CODEC_SETPOSITIONCALLBACK(ref CODEC_STATE codec_state, int subsound, uint position, TIMEUNIT postype);
    internal delegate RESULT CODEC_GETPOSITIONCALLBACK(ref CODEC_STATE codec_state, ref uint position, TIMEUNIT postype);
    internal delegate RESULT CODEC_SOUNDCREATECALLBACK(ref CODEC_STATE codec_state, int subsound, ref FD_Sound sound);
    internal delegate RESULT CODEC_METADATACALLBACK(ref CODEC_STATE codec_state, TAGTYPE tagtype, string name, IntPtr data, uint datalen, TAGDATATYPE datatype, int unique);//char* 형 검사하기 (out? in?)
    internal delegate RESULT CODEC_GETWAVEFORMATCALLBACK(ref CODEC_STATE codec_state, ref CODEC_WAVEFORMAT waveformat);
#endif
    /*
    [STRUCTURE] 
    [
        [DESCRIPTION]
        When creating a codec, declare one of these and provide the relevant callbacks and name for FMOD to use when it opens and reads a file.

        [REMARKS]
        Members marked with [r] mean the variable is modified by FMOD and is for reading purposes only.  Do not change this value.<br>
        Members marked with [w] mean the variable can be written to.  The user can set the value.<br>

        [SEE_ALSO]
        FMOD_CODEC_STATE
        FMOD_CODEC_WAVEFORMAT
    ]
    */
    [StructLayout(LayoutKind.Sequential)]
#if DEBUG
    public struct CODEC_DESCRIPTION
#else
    internal struct CODEC_DESCRIPTION
#endif
    {
        public StringWrapper name;            /* [w] Name of the codec. */
        public uint version;         /* [w] Plugin writer's version number. */
        public int defaultasstream; /* [w] Tells FMOD to open the file as a stream when calling System::createSound, and not a static sample.  Should normally be 0 (FALSE), because generally the user wants to decode the file into memory when using System::createSound.   Mainly used for formats that decode for a very long time, or could use large amounts of memory when decoded.  Usually sequenced formats such as mod/s3m/xm/it/midi fall into this category.   It is mainly to stop users that don't know what they're doing from getting FMOD_ERR_MEMORY returned from createSound when they should have in fact called System::createStream or used FMOD_CREATESTREAM in System::createSound. */
        public TIMEUNIT timeunits;       /* [w] When setposition codec is called, only these time formats will be passed to the codec. Use bitwise OR to accumulate different types. */
        public CODEC_OPENCALLBACK open;            /* [w] Open callback for the codec for when FMOD tries to open a sound using this codec. */
        public CODEC_CLOSECALLBACK close;           /* [w] Close callback for the codec for when FMOD tries to close a sound using this codec.  */
        public CODEC_READCALLBACK read;            /* [w] Read callback for the codec for when FMOD tries to read some data from the file to the destination format (specified in the open callback). */
        public CODEC_GETLENGTHCALLBACK getlength;       /* [w] Callback to return the length of the song in whatever format required when Sound::getLength is called. */
        public CODEC_SETPOSITIONCALLBACK setposition;     /* [w] Seek callback for the codec for when FMOD tries to seek within the file with Channel::setPosition. */
        public CODEC_GETPOSITIONCALLBACK getposition;     /* [w] Tell callback for the codec for when FMOD tries to get the current position within the with Channel::getPosition. */
        public CODEC_SOUNDCREATECALLBACK soundcreate;     /* [w] Sound creation callback for the codec when FMOD finishes creating the sound.  (So the codec can set more parameters for the related created sound, ie loop points/mode or 3D attributes etc). */
        public CODEC_GETWAVEFORMATCALLBACK getwaveformat;   /* [w] Callback to tell FMOD about the waveformat of a particular subsound.  This is to save memory, rather than saving 1000 FMOD_CODEC_WAVEFORMAT structures in the codec, the codec might have a more optimal way of storing this information. */
    }
    /*
[STRUCTURE] 
[
    [DESCRIPTION]
    Set these values marked to tell fmod what sort of sound to create when the codec open callback is called.<br>
    The format, channels, frequency and lengthpcm tell FMOD what sort of sound buffer to create when you initialize your code. <br>
    If you wrote an MP3 codec that decoded to stereo 16bit integer PCM for a 44khz sound, you would specify FMOD_SOUND_FORMAT_PCM16, and channels would be equal to 2, and frequency would be 44100.<br>

    [REMARKS]
    Members marked with [r] mean the variable is modified by FMOD and is for reading purposes only.  Do not change this value.<br>
    Members marked with [w] mean the variable can be written to.  The user can set the value.<br>
    <br>
    1.07 Note.  'blockalign' member which was in bytes has been removed.   'pcmblocksize' is now the replacement, and is measured in PCM samples only, not bytes.  This is purely to support buffering 
    internal to FMOD for codecs that are not sample accurate.
    <br>
    Note: When registering a codec, format, channels, frequency and lengthpcm must be supplied, otherwise there will be an error.<br>
    This structure is optional if FMOD_CODEC_GETWAVEFORMATCALLBACK is specified.<br>
    An array of these structures may be needed if FMOD_CODEC_STATE::numsubsounds is larger than 1.
    
    
    [SEE_ALSO]
    FMOD_CODEC_STATE
    FMOD_SOUND_FORMAT
    FMOD_MODE
    FMOD_CHANNELMASK
    FMOD_CHANNELORDER
    FMOD_CODEC_WAVEFORMAT_VERSION
]
*/
    [StructLayout(LayoutKind.Sequential)]
#if DEBUG
    public struct CODEC_WAVEFORMAT
#else
    internal struct CODEC_WAVEFORMAT
#endif
    {
        public IntPtr name;          /* [w] Name of sound.  Optional. If used, the codec must own the lifetime of the string memory until the codec is destroyed. */
        public SOUND_FORMAT format;        /* [w] Format for (decompressed) codec output, ie FMOD_SOUND_FORMAT_PCM8, FMOD_SOUND_FORMAT_PCM16.  Mandantory - Must be supplied. */
        public int channels;      /* [w] Number of channels used by codec, ie mono = 1, stereo = 2.  Mandantory - Must be supplied.  */
        public int frequency;     /* [w] Default frequency in hz of the codec, ie 44100.  Mandantory - Must be supplied.  */
        public uint lengthbytes;   /* [w] Length in bytes of the source data.  Used for FMOD_TIMEUNIT_RAWBYTES.  Optional. Default = 0. */
        public uint lengthpcm;     /* [w] Length in decompressed, PCM samples of the file, ie length in seconds * frequency.  Used for Sound::getLength and for memory allocation of static decompressed sample data.  Mandantory - Must be supplied. */
        public uint pcmblocksize;  /* [w] Minimum, optimal number of decompressed PCM samples codec can handle.  0 or 1 = no buffering.  Anything higher means FMOD will allocate a PCM buffer of this size to read in chunks.  The codec read callback will be called in multiples of this value.  Optional.  */
        public int loopstart;     /* [w] Loopstart in decompressed, PCM samples of file. Optional. Default = 0. */
        public int loopend;       /* [w] Loopend in decompressed, PCM samples of file. Optional. Default = 0. */
        public MODE mode;          /* [w] Mode to determine whether the sound should by default load as looping, non looping, 2d or 3d.  Optional. Default = FMOD_DEFAULT. */
        public CHANNELMASK channelmask;   /* [w] Defined channel bitmask to describe which speakers the channels in the codec map to, in order of channel count.  See fmod_common.h.  Optional. Leave at 0 to map to the speaker layout defined in FMOD_SPEAKER. */
        public CHANNELORDER channelorder;  /* [w] Defined channel order type, to describe where each sound channel should pan for the number of channels specified.  See fmod_common.h.  Optional.  Leave at 0 to play in default speaker order. */
        public float peakvolume;    /* [w] Peak volume of sound. Optional. Default = 0 if not used. */
    };


    /*
    [DEFINE] 
    [
        [NAME]
        FMOD_CODEC_WAVEFORMAT_VERSION

        [DESCRIPTION]
        Version number of FMOD_CODEC_WAVEFORMAT structure.  Should be set into FMOD_CODEC_STATE in the FMOD_CODEC_OPENCALLBACK.

        [REMARKS]
        Use this for binary compatibility and for future expansion.

        [SEE_ALSO]
        FMOD_CODEC_STATE
        FMOD_CODEC_DESCRIPTION
        FMOD_CODEC_OPENCALLBACK
    ]
#define FMOD_CODEC_WAVEFORMAT_VERSION 3
    */

    /* [DEFINE_END] */


    /*
    [STRUCTURE] 
    [
        [DESCRIPTION]
        Codec plugin structure that is passed into each callback.<br>
        <br>
        Optionally set the numsubsounds and waveformat members when called in FMOD_CODEC_OPENCALLBACK to tell fmod what sort of sound to create.<br>

        [REMARKS]
        Members marked with [r] mean the variable is modified by FMOD and is for reading purposes only.  Do not change this value.<br>
        Members marked with [w] mean the variable can be written to.  The user can set the value.<br>
        <br>
        'numsubsounds' should be 0 if the file is a normal single sound stream or sound.  Examples of this would be .WAV, .WMA, .MP3, .AIFF.<br>
        'numsubsounds' should be 1+ if the file is a container format, and does not contain wav data itself.  Examples of these types would be FSB (contains multiple sounds), DLS (contain instruments).<br>
        The waveformat value should point to an arrays of information based on how many subsounds are in the format.  If the number of subsounds is 0 then it should point to 1 waveformat, the same as if the number of subsounds was 1.  If subsounds was 100 for example, there should be a pointer to an array of 100 waveformat structures.<br>
        <br>
        The waveformat pointer is optional and could be 0, if using FMOD_CODEC_GETWAVEFORMATCALLBACK is preferred.<br>
        <br>
        When a sound has 1 or more subsounds, the caller must play the individual sounds specified by first obtaining the subsound with Sound::getSubSound.

        [SEE_ALSO]
        FMOD_CODEC_WAVEFORMAT
        FMOD_FILE_READCALLBACK
        FMOD_FILE_SEEKCALLBACK
        FMOD_CODEC_METADATACALLBACK
        Sound::getSubSound
        Sound::getNumSubSounds
        FMOD_CODEC_WAVEFORMAT_VERSION
    ]
    */
    [StructLayout(LayoutKind.Sequential)]
#if DEBUG
    public struct CODEC_STATE
#else
    internal struct CODEC_STATE
#endif
    {
        public int numsubsounds;      /* [w] Number of 'subsounds' in this sound.  Anything other than 0 makes it a 'container' format (ie DLS/FSB etc which contain 1 or more subsounds).  For most normal, single sound codec such as WAV/AIFF/MP3, this should be 0 as they are not a container for subsounds, they are the sound by itself. */
        public CODEC_WAVEFORMAT waveformat;        /* [w] Pointer to an array of format structures containing information about each sample.  Can be 0 or NULL if FMOD_CODEC_GETWAVEFORMATCALLBACK callback is preferred.  The number of entries here must equal the number of subsounds defined in the subsound parameter. If numsubsounds = 0 then there should be 1 instance of this structure. */
        public IntPtr plugindata;        /* [w] Plugin writer created data the codec author wants to attach to this object. */

        public IntPtr filehandle;        /* [r] This will return an internal FMOD file handle to use with the callbacks provided.  */
        public uint filesize;          /* [r] This will contain the size of the file in bytes. */
        public FILE_READCALLBACK fileread;          /* [r] This will return a callable FMOD file function to use from codec. */
        public FILE_SEEKCALLBACK fileseek;          /* [r] This will return a callable FMOD file function to use from codec.  */
        public CODEC_METADATACALLBACK metadata;          /* [r] This will return a callable FMOD metadata function to use from codec.  */

        public int waveformatversion; /* [w] Must be set to FMOD_CODEC_WAVEFORMAT_VERSION in the FMOD_CODEC_OPENCALLBACK. */
    };
#endregion

#region OUTPUT

    /*
        FMOD_OUTPUT_DESCRIPTION callbacks
    */
#if DEBUG
    public delegate RESULT OUTPUT_GETNUMDRIVERSCALLBACK(ref OUTPUT_STATE state, ref int numdrivers);
    public delegate RESULT OUTPUT_GETDRIVERINFOCALLBACK(ref OUTPUT_STATE state, int id, IntPtr name, int namelen, ref GUID guid, ref int systemrate, ref SPEAKERMODE speakermode, ref int speakermodechannels);
    public delegate RESULT OUTPUT_INITCALLBACK(ref OUTPUT_STATE state, int selecteddriver, INITFLAGS flags, ref int outputrate, ref SPEAKERMODE speakermode, ref int speakermodechannels, ref SOUND_FORMAT outputformat, int dspbufferlength, int dspnumbuffers, ref IntPtr extradriverdata);
    public delegate RESULT OUTPUT_STARTCALLBACK(ref OUTPUT_STATE state);
    public delegate RESULT OUTPUT_STOPCALLBACK(ref OUTPUT_STATE state);
    public delegate RESULT OUTPUT_CLOSECALLBACK(ref OUTPUT_STATE state);
    public delegate RESULT OUTPUT_UPDATECALLBACK(ref OUTPUT_STATE state);
    public delegate RESULT OUTPUT_GETHANDLECALLBACK(ref OUTPUT_STATE state, ref IntPtr handle);
    public delegate RESULT OUTPUT_GETPOSITIONCALLBACK(ref OUTPUT_STATE state, ref uint pcm);
    public delegate RESULT OUTPUT_LOCKCALLBACK(ref OUTPUT_STATE state, uint offset, uint length, ref IntPtr ptr1, ref IntPtr ptr2, ref uint len1, ref uint len2);
    public delegate RESULT OUTPUT_UNLOCKCALLBACK(ref OUTPUT_STATE state, IntPtr ptr1, IntPtr ptr2, uint len1, uint len2);
    public delegate RESULT OUTPUT_MIXERCALLBACK(ref OUTPUT_STATE state);

    public delegate RESULT OUTPUT_OBJECT3DGETINFOCALLBACK(ref OUTPUT_STATE state, ref int maxhardwareobjects);
    public delegate RESULT OUTPUT_OBJECT3DALLOCCALLBACK(ref OUTPUT_STATE state, ref IntPtr object3d);
    public delegate RESULT OUTPUT_OBJECT3DFREECALLBACK(ref OUTPUT_STATE state, IntPtr object3d);
    public delegate RESULT OUTPUT_OBJECT3DUPDATECALLBACK(ref OUTPUT_STATE state, IntPtr object3d, ref OUTPUT_OBJECT3DINFO info);

    public delegate RESULT OUTPUT_OPENPORTCALLBACK(ref OUTPUT_STATE state, uint portType, ulong portIndex, ref int portId, ref int portRate, ref int portChannels, ref SOUND_FORMAT portFormat);
    public delegate RESULT OUTPUT_CLOSEPORTCALLBACK(ref OUTPUT_STATE state, int portId);

    /*
        FMOD_OUTPUT_STATE functions
    */
    public delegate RESULT OUTPUT_READFROMMIXER_FUNC(ref OUTPUT_STATE state, IntPtr buffer, uint length);
    public delegate RESULT OUTPUT_COPYPORT_FUNC(ref OUTPUT_STATE state, int portId, IntPtr buffer, uint length);
    public delegate RESULT OUTPUT_REQUESTRESET_FUNC(ref OUTPUT_STATE state);
    public delegate IntPtr OUTPUT_ALLOC_FUNC(uint size, uint align, StringWrapper file, int line);
    public delegate void OUTPUT_FREE_FUNC(IntPtr ptr, StringWrapper file, int line);
    public delegate void OUTPUT_LOG_FUNC(DEBUG_FLAGS level, StringWrapper file, int line, StringWrapper function, StringWrapper wrapper); //(const char *string, ...)
#else
    internal delegate RESULT OUTPUT_GETNUMDRIVERSCALLBACK(ref OUTPUT_STATE state, ref int numdrivers);
    internal delegate RESULT OUTPUT_GETDRIVERINFOCALLBACK(ref OUTPUT_STATE state, int id, IntPtr name, int namelen, ref GUID guid, ref int systemrate, ref SPEAKERMODE speakermode, ref int speakermodechannels);
    internal delegate RESULT OUTPUT_INITCALLBACK(ref OUTPUT_STATE state, int selecteddriver, INITFLAGS flags, ref int outputrate, ref SPEAKERMODE speakermode, ref int speakermodechannels, ref SOUND_FORMAT outputformat, int dspbufferlength, int dspnumbuffers, ref IntPtr extradriverdata);
    internal delegate RESULT OUTPUT_STARTCALLBACK(ref OUTPUT_STATE state);
    internal delegate RESULT OUTPUT_STOPCALLBACK(ref OUTPUT_STATE state);
    internal delegate RESULT OUTPUT_CLOSECALLBACK(ref OUTPUT_STATE state);
    internal delegate RESULT OUTPUT_UPDATECALLBACK(ref OUTPUT_STATE state);
    internal delegate RESULT OUTPUT_GETHANDLECALLBACK(ref OUTPUT_STATE state, ref IntPtr handle);
    internal delegate RESULT OUTPUT_GETPOSITIONCALLBACK(ref OUTPUT_STATE state, ref uint pcm);
    internal delegate RESULT OUTPUT_LOCKCALLBACK(ref OUTPUT_STATE state, uint offset, uint length, ref IntPtr ptr1, ref IntPtr ptr2, ref uint len1, ref uint len2);
    internal delegate RESULT OUTPUT_UNLOCKCALLBACK(ref OUTPUT_STATE state, IntPtr ptr1, IntPtr ptr2, uint len1, uint len2);
    internal delegate RESULT OUTPUT_MIXERCALLBACK(ref OUTPUT_STATE state);

    internal delegate RESULT OUTPUT_OBJECT3DGETINFOCALLBACK(ref OUTPUT_STATE state, ref int maxhardwareobjects);
    internal delegate RESULT OUTPUT_OBJECT3DALLOCCALLBACK(ref OUTPUT_STATE state, ref IntPtr object3d);
    internal delegate RESULT OUTPUT_OBJECT3DFREECALLBACK(ref OUTPUT_STATE state, IntPtr object3d);
    internal delegate RESULT OUTPUT_OBJECT3DUPDATECALLBACK(ref OUTPUT_STATE state, IntPtr object3d, ref OUTPUT_OBJECT3DINFO info);

    internal delegate RESULT OUTPUT_OPENPORTCALLBACK(ref OUTPUT_STATE state, uint portType, ulong portIndex, ref int portId, ref int portRate, ref int portChannels, ref SOUND_FORMAT portFormat);
    internal delegate RESULT OUTPUT_CLOSEPORTCALLBACK(ref OUTPUT_STATE state, int portId);

    /*
        FMOD_OUTPUT_STATE functions
    */
    internal delegate RESULT OUTPUT_READFROMMIXER_FUNC(ref OUTPUT_STATE state, IntPtr buffer, uint length);
    internal delegate RESULT OUTPUT_COPYPORT_FUNC(ref OUTPUT_STATE state, int portId, IntPtr buffer, uint length);
    internal delegate RESULT OUTPUT_REQUESTRESET_FUNC(ref OUTPUT_STATE state);
    internal delegate IntPtr OUTPUT_ALLOC_FUNC(uint size, uint align, StringWrapper file, int line);
    internal delegate void OUTPUT_FREE_FUNC(IntPtr ptr, StringWrapper file, int line);
    internal delegate void OUTPUT_LOG_FUNC(DEBUG_FLAGS level, StringWrapper file, int line, StringWrapper function, StringWrapper wrapper); //(const char *string, ...)
#endif


    /*
    [STRUCTURE]
    [
        [DESCRIPTION]
        When creating an output, declare one of these and provide the relevant callbacks and name for FMOD to use when it creates and uses an output of this type.

        [REMARKS]
        There are several methods for driving the FMOD mixer to service the audio hardware.

        * Polled: if the audio hardware must be polled regularly set 'polling' to TRUE, FMOD will create a mixer thread that calls back via FMOD_OUTPUT_GETPOSITION_CALLBACK. Once an entire block of samples have played FMOD will call FMOD_OUTPUT_LOCK_CALLBACK to allow you to provide a destination pointer to write the next mix.
        * Callback: if the audio hardware provides a callback where you must provide a buffer of samples then set 'polling' to FALSE and directly call FMOD_OUTPUT_READFROMMIXER.
        * Synchronization: if the audio hardware provides a synchronization primitive to wait on then set 'polling' to FALSE and give a FMOD_OUTPUT_MIXER_CALLBACK pointer. FMOD will create a mixer thread and call you repeatedly once FMOD_OUTPUT_START_CALLBACK has finished, you must wait on your primitive in this callback and upon wake call FMOD_OUTPUT_READFROMMIXER.
        * Non-realtime: if you are writing a file or driving a non-realtime output call FMOD_OUTPUT_READFROMMIXER from FMOD_OUTPUT_UPDATE_CALLBACK.

        Callbacks marked with 'user thread' will be called in response to the user of the FMOD low level API, in the case of the Studio runtime API, the user is the Studio Update thread.

        Members marked with [r] mean read only for the developer, read/write for the FMOD system.

        Members marked with [w] mean read/write for the developer, read only for the FMOD system.

        [SEE_ALSO]
        FMOD_OUTPUT_STATE
        FMOD_OUTPUT_GETNUMDRIVERS_CALLBACK
        FMOD_OUTPUT_GETDRIVERINFO_CALLBACK
        FMOD_OUTPUT_INIT_CALLBACK
        FMOD_OUTPUT_START_CALLBACK
        FMOD_OUTPUT_STOP_CALLBACK
        FMOD_OUTPUT_CLOSE_CALLBACK
        FMOD_OUTPUT_UPDATE_CALLBACK
        FMOD_OUTPUT_GETHANDLE_CALLBACK
        FMOD_OUTPUT_GETPOSITION_CALLBACK
        FMOD_OUTPUT_LOCK_CALLBACK
        FMOD_OUTPUT_UNLOCK_CALLBACK
        FMOD_OUTPUT_MIXER_CALLBACK
        FMOD_OUTPUT_OBJECT3DGETINFO_CALLBACK
        FMOD_OUTPUT_OBJECT3DALLOC_CALLBACK
        FMOD_OUTPUT_OBJECT3DFREE_CALLBACK
        FMOD_OUTPUT_OBJECT3DUPDATE_CALLBACK
    ]
    */
    [StructLayout(LayoutKind.Sequential)]
#if DEBUG
    public struct OUTPUT_DESCRIPTION
#else
    internal struct OUTPUT_DESCRIPTION
#endif
    {
        public uint apiversion;         /* [w] The output plugin API version this plugin is built for. Set to this to FMOD_OUTPUT_PLUGIN_VERSION. */
        public StringWrapper name;               /* [w] Name of the output plugin. */
        public uint version;            /* [w] Version of the output plugin. */
        public int polling;            /* [w] If TRUE (non-zero) a mixer thread is created that calls FMOD_OUTPUT_GETPOSITIONCALLBACK / FMOD_OUTPUT_LOCKCALLBACK / FMOD_OUTPUT_UNLOCKCALLBACK to drive the mixer. If FALSE (zero) you must call FMOD_OUTPUT_READFROMMIXER to drive the mixer yourself. */
        public OUTPUT_GETNUMDRIVERSCALLBACK getnumdrivers;      /* [w] Required user thread callback to provide the number of attached sound devices. Called from System::getNumDrivers. */
        public OUTPUT_GETDRIVERINFOCALLBACK getdriverinfo;      /* [w] Required user thread callback to provide information about a particular sound device. Called from System::getDriverInfo. */
        public OUTPUT_INITCALLBACK init;               /* [w] Required user thread callback to allocate resources and provide information about hardware capabilities. Called from System::init. */
        public OUTPUT_STARTCALLBACK start;              /* [w] Optional user thread callback just before mixing should begin, calls to OUTPUT_GETPOSITIONCALLBACK / OUTPUT_LOCKCALLBACK / OUTPUT_UNLOCKCALLBACK / OUTPUT_MIXERCALLBACK will start, you may call OUTPUT_READFROMMIXER after this point. Called from System::init. */
        public OUTPUT_STOPCALLBACK stop;               /* [w] Optional user thread callback just after mixing has finished, calls to OUTPUT_GETPOSITIONCALLBACK / OUTPUT_LOCKCALLBACK / OUTPUT_UNLOCKCALLBACK / OUTPUT_MIXERCALLBACK have stopped, you may not call OUTPUT_READFROMMIXER after this point. Called from System::close. */
        public OUTPUT_CLOSECALLBACK close;              /* [w] Required user thread callback to clean up resources allocated during OUTPUT_INITCALLBACK. Called from System::init and System::close. */
        public OUTPUT_UPDATECALLBACK update;             /* [w] Optional user thread callback once per frame to update internal state. Called from System::update. */
        public OUTPUT_GETHANDLECALLBACK gethandle;          /* [w] Optional user thread callback to provide a pointer to the internal device object used to share with other audio systems. Called from System::getOutputHandle. */
        public OUTPUT_GETPOSITIONCALLBACK getposition;        /* [w] Required mixer thread callback (if 'polling' is TRUE) to provide the hardware playback position in the output ring buffer. Called before a mix. */
        public OUTPUT_LOCKCALLBACK @lock;               /* [w] Required mixer thread callback (if 'polling' is TRUE) to provide a pointer the mixer can write to for the next block of audio data. Called before a mix. */
        public OUTPUT_UNLOCKCALLBACK unlock;             /* [w] Optional mixer thread callback (if 'polling' is TRUE) to signify the mixer has finished writing to the pointer from OUTPUT_LOCKCALLBACK. Called after a mix. */
        public OUTPUT_MIXERCALLBACK mixer;              /* [w] Optional mixer thread callback (if 'polling' is FALSE) called repeatedly to give a thread for waiting on an audio hardware synchronization primitive (see remarks for details). Ensure you have a reasonable timeout (~200ms) on your synchronization primitive and allow this callback to return once per wakeup to avoid deadlocks. */
        public OUTPUT_OBJECT3DGETINFOCALLBACK object3dgetinfo;    /* [w] Optional mixer thread callback to provide information about the capabilities of 3D object hardware. Called during a mix. */
        public OUTPUT_OBJECT3DALLOCCALLBACK object3dalloc;      /* [w] Optional mixer thread callback to reserve a hardware resources for a single 3D object. Called during a mix. */
        public OUTPUT_OBJECT3DFREECALLBACK object3dfree;       /* [w] Optional mixer thread callback to release a hardware resource previously acquired with OUTPUT_OBJECT3DALLOCCALLBACK. Called during a mix. */
        public OUTPUT_OBJECT3DUPDATECALLBACK object3dupdate;     /* [w] Optional mixer thread callback once for every acquired 3D object every mix to provide 3D information and buffered audio. Called during a mix. */
        public OUTPUT_OPENPORTCALLBACK openport;           /* [w] Optional main thread callback to open an auxiliary output port on the device. */
        public OUTPUT_CLOSEPORTCALLBACK closeport;          /* [w] Optional main thread callback to close an auxiliary output port on the device. */

    };
    
    /*
    [STRUCTURE]
    [
        [DESCRIPTION]
        Output object state passed into every callback provides access to plugin developers data and system functionality.

        [REMARKS]
        Members marked with [r] mean read only for the developer, read/write for the FMOD system.
        Members marked with [w] mean read/write for the developer, read only for the FMOD system.

        [SEE_ALSO]
        FMOD_OUTPUT_DESCRIPTION
    ]
    */
    [StructLayout(LayoutKind.Sequential)]
#if DEBUG
    public struct OUTPUT_STATE
#else
    internal struct OUTPUT_STATE
#endif
    {
        public IntPtr plugindata;     /* [w] Pointer used to store any plugin specific state so it's available in all callbacks. */
        public OUTPUT_READFROMMIXER_FUNC readfrommixer;  /* [r] Function to execute the mixer producing a buffer of audio. Used to control when the mix occurs manually as an alternative to FMOD_OUTPUT_DESCRIPTION::polling == TRUE. */
        public OUTPUT_ALLOC_FUNC alloc;          /* [r] Function to allocate memory using the FMOD memory system. */
        public OUTPUT_FREE_FUNC free;           /* [r] Function to free memory allocated with FMOD_OUTPUT_ALLOC. */
        public OUTPUT_LOG_FUNC log;            /* [r] Function to write to the FMOD logging system. */
        public OUTPUT_COPYPORT_FUNC copyport;       /* [r] Function to copy the output from the mixer for the given auxiliary port. */
        public OUTPUT_REQUESTRESET_FUNC requestreset;   /* [r] Function to request the output plugin be shutdown then restarted during the next System::update. */
    };

    /*
    [STRUCTURE]
    [
        [DESCRIPTION]
        This structure is passed to the plugin via FMOD_OUTPUT_OBJECT3DUPDATE_CALLBACK, so that whatever object based panning solution available can position it in the speakers correctly.
        Object based panning is a 3D panning solution that sends a mono only signal to a hardware device, such as Dolby Atmos or other similar panning solutions.

        [REMARKS]
        FMOD does not attenuate the buffer, but provides a 'gain' parameter that the user must use to scale the buffer by.  Rather than pre-attenuating the buffer, the plugin developer
        can access untouched data for other purposes, like reverb sending for example.
        The 'gain' parameter is based on the user's 3D custom rolloff model.  
    
        Members marked with [r] mean read only for the developer, read/write for the FMOD system.
        Members marked with [w] mean read/write for the developer, read only for the FMOD system.

        [SEE_ALSO]
        FMOD_OUTPUT_OBJECT3DUPDATE_CALLBACK
    ]
    */
    [StructLayout(LayoutKind.Sequential)]
#if DEBUG
    public struct OUTPUT_OBJECT3DINFO
#else
    internal struct OUTPUT_OBJECT3DINFO
#endif
    {
        public IntPtr buffer;         /* [r] Mono PCM floating point buffer. This buffer needs to be scaled by the gain value to get distance attenuation.  */
        public uint bufferlength;   /* [r] Length in PCM samples of buffer. */
        public VECTOR position;       /* [r] Vector relative between object and listener. */
        public float gain;           /* [r] 0.0 to 1.0 - 1 = 'buffer' is not attenuated, 0 = 'buffer' is fully attenuated. */
        public float spread;         /* [r] 0 - 360 degrees.  0 = point source, 360 = sound is spread around all speakers */
        public float priority;       /* [r] 0.0 to 1.0 - 0 = most important, 1 = least important. Based on height and distance (height is more important). */
    };
#endregion
}
