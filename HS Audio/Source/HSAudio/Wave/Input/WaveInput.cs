﻿using HSAudio.Wave.Device;
using System;

namespace HSAudio.Wave.Input
{
    public enum RecordKind
    {
#if WIN_XP || WIN_VISTA || WIN_10 || WIN_10_1703
        WAVEIN,
        ASIO,
#endif
#if WIN_VISTA || WIN_10 || WIN_10_1703 || WIN_METRO || XBOX
        WASAPI,
        WASAPI_LOOPBACK
#endif
    }
    public interface WaveInput : IDisposable
    {
        event DataInEventHandler WaveDataIn;
        event DataInStoppedEventHandler WaveDataInStopped;
        WaveFormat Format { get; set; }
        RecordKind Kind { get; }
        WaveDevice Device { get; }

        //event DeviceChangedEventHandler DeviceChanged;
        //event DeviceSubChangedEventHandler DeviceSubChanged;

        bool IsBusy { get; }

        int Latency { get; set; }
        bool LatencySupport { get;}

        void Start();
        void Stop();
    }
}
