﻿#if FUNC_PLUGIN
using System;
using HSAudio.PlugIn;
using HSAudio.Resource;
using HS.Setting;
using HS;

namespace HSAudio.Output.Kind
{
    /// <summary>
    /// Plugin output (플러그인 출력)
    /// </summary>
    public class PluginOutput : OutputMethod, ISettingManager
    {
        public Plugin_Output Plugin { get; private set; }
        public override Output Parent { get; internal set; }

        HSAudio Audio = null;
        public PluginOutput(HSAudio Audio, Plugin_Output Plugin)
        {
            Type = OutputType.Plugin;
            this.Plugin = Plugin;
            Name = Plugin.Name;
        }


        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SubSetting.SetValue("Plugin", Plugin.SaveSetting());
            return s;
        }

        public override bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Plugin != null) { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_SETTING_ONLY_INIT"]); else return false; }

            if (Setting.SubSetting.Exist("Plugin"))
                try
                {
                    string path = Setting.SubSetting["Plugin"].GetValue("Path", null);
                    PlugIn.Plugin.Check(Audio, path);
                }
                catch (HSException ex) { if (ThrowException) throw ex; else return false; }

            return true;
        }


        public override object Clone(){return new PluginOutput(Audio, Plugin);}
        protected internal override void Dispose()
        {
            Plugin = null;
            Parent = null;
        }

        public override bool Equals(object obj)
        {
            PluginOutput p = obj as PluginOutput;
            if (p == null) return false;
            else{return p.Plugin.Equals(Plugin);}
        }
#if !NETSTANDARD
        public override int GetHashCode() { return base.GetHashCode(); }
#endif
    }
}
#endif