﻿namespace HSAudio.Core.Sound
{
    public delegate void SoundEventHandler(Sound sender);
    public delegate void LoadEventHandler(object sender, bool Unloaded);
}
