﻿using System;
using System.Collections.Generic;

namespace HSAudio.Resource.Language
{
    public class Default : StringResource
    {
        Dictionary<string, string> str = new Dictionary<string, string>();
        public override string this[string Key] { get { return str.ContainsKey(Key) ? str[Key] : Key; } }
        public override bool Exist(string Key) { return str.ContainsKey(Key); }
        public override int Count { get { return str.Count; } }

        public Default()
        {
            str.Add("STR_ERR_INITIALIZING_FAIL", "Fail to initalizing");
            str.Add("STR_ERR_NOT_INITIALIZE", "Not initalized");
            str.Add("STR_ERR_NOT_EXIST", "{0} is not exist");
            str.Add("STR_ERR_NOT_FOUND", "\"{0}\" is not found");
            str.Add("STR_ERR_NOT_NULL", "{0} is not be null");
            str.Add("STR_ERR_NOT_EMPTY", "{0} is not be empty");
            str.Add("STR_ERR_NOT_PLUGIN", "{0} is not Plugin");
            str.Add("STR_ERR_NOT_PLUGIN_DSP", "{0} is not DSP Plugin");
            str.Add("STR_ERR_NOT_PLUGIN_OUTPUT", "{0} is not Output Plugin");
            str.Add("STR_ERR_NOT_PLUGIN_CODEC", "{0} is not Codec Plugin");
            str.Add("STR_ERR_NOT_SUPPORT", "This function is not support");
            str.Add("STR_ERR_SETTING_WRONG_TAG", "Can't load setting. because tag is wrong.");
            str.Add("STR_ERR_SETTING_NOTFOUND_ITEM", "Can't load setting. because item \"{0}\" is not found.");
            str.Add("STR_ERR_SETTING_NOTFOUND_TYPE", "Can't load setting. because type is not found.");
            str.Add("STR_ERR_SETTING_ONLY_INIT", "You can load setting only when initializing.");
            str.Add("STR_ERR_SETTING_NOTSUPPORT_LOAD", "This class is not support to load setting");
            str.Add("STR_ERR_OUTPUT_BEFORE_CLOSE", "Close output first.");
            //str.Add("STR_ERR_CODEC", "");
            str.Add("STR_ERR_OUTPUT_UNSUPPORT_FORMAT", "This output format is not support");
            str.Add("STR_ERR_WAVE_UNSUPPORT_FORMAT", "This wave format is not support");

            str.Add("STR_PATH", "Path");

            str.Add("STR_ENGINE_ERR_BADCOMMAND", "Tried to call a function on a data type that does not allow this type of functionality");
            str.Add("STR_ENGINE_ERR_CHANNEL_ALLOC", "Error trying to allocate a channel.");
            str.Add("STR_ENGINE_ERR_CHANNEL_STOLEN", "The specified channel has been reused to play another sound.");
            str.Add("STR_ENGINE_ERR_DMA", "DMA Failure.  See debug output for more information.");
            str.Add("STR_ENGINE_ERR_DSP_CONNECTION", "DSP connection error.  Connection possibly caused a cyclic dependancy.");
            str.Add("STR_ENGINE_ERR_DSP_FORMAT", "DSP Format error.  A DSP unit may have attempted to connect to this network with the wrong format.");
            str.Add("STR_ENGINE_ERR_DSP_NOTFOUND", "DSP connection error.  Couldn't find the DSP unit specified.");
            str.Add("STR_ENGINE_ERR_DSP_DONTPROCESS", "DSP return code from a DSP process query callback.  Tells mixer not to call the process callback and therefore not consume CPU.  Use this to optimize the DSP graph.");
            str.Add("STR_ENGINE_ERR_DSP_INUSE", "DSP is already in the mixer's DSP network. It must be removed before being reinserted or released.");
            str.Add("STR_ENGINE_ERR_DSP_RESERVED", "DSP operation error.  Cannot perform operation on this DSP as it is reserved by the system.");
            str.Add("STR_ENGINE_ERR_DSP_SILENCE", "DSP return code from a DSP process query callback.  Tells mixer silence would be produced from read: so go idle and not consume CPU.  Use this to optimize the DSP graph.");
            str.Add("STR_ENGINE_ERR_DSP_TYPE", "DSP operation cannot be performed on a DSP of this type.");
            str.Add("STR_ENGINE_ERR_FORMAT", "Not support this format or Not support this sound");
            str.Add("STR_ENGINE_ERR_FILE_BAD", "Error loading file.");
            str.Add("STR_ENGINE_ERR_FILE_COULDNOTSEEK", "Couldn't perform seek operation.  This is a limitation of the medium (ie netstreams) or the file format.");
            str.Add("STR_ENGINE_ERR_FILE_DISKEJECTED", "Media was ejected while reading.");
            str.Add("STR_ENGINE_ERR_FILE_EOF", "End of file unexpectedly reached while trying to read essential data (truncated data?).");
            str.Add("STR_ENGINE_ERR_FILE_ENDOFDATA", "End of current chunk reached while trying to read data.");
            str.Add("STR_ENGINE_ERR_FILE_NOTFOUND", "File not found.");
            str.Add("STR_ENGINE_ERR_HEADER_MISMATCH", "There is a version mismatch between the FMOD header and either the FMOD Studio library or the FMOD Low Level library.");
            str.Add("STR_ENGINE_ERR_HTTP", "A HTTP error occurred. This is a catch-all for HTTP errors not listed elsewhere.");
            str.Add("STR_ENGINE_ERR_HTTP_ACCESS", "The specified resource requires authentication or is forbidden.");
            str.Add("STR_ENGINE_ERR_HTTP_PROXY_AUTH", "Proxy authentication is required to access the specified resource.");
            str.Add("STR_ENGINE_ERR_HTTP_SERVER_ERROR", "A HTTP server error occurred.");
            str.Add("STR_ENGINE_ERR_HTTP_TIMEOUT", "The HTTP request timed out.");
            str.Add("STR_ENGINE_ERR_INITIALIZATION", "Internal Error. Engine was not initialized correctly to support this function.");
            str.Add("STR_ENGINE_ERR_INITIALIZED", "Internal Error. Cannot call this command after system initalized");
            str.Add("STR_ENGINE_ERR_INTERNAL", "Internal Error. An error occurred that wasn't supposed to.  Contact support.");
            str.Add("STR_ENGINE_ERR_INVALID_FLOAT", "Value passed in was a NaN, Inf or denormalized float.");
            str.Add("STR_ENGINE_ERR_INVALID_HANDLE", "An invalid object handle was used.");
            str.Add("STR_ENGINE_ERR_INVALID_PARAM", "An invalid parameter was passed to this function.");
            str.Add("STR_ENGINE_ERR_INVALID_POSITION", "An invalid seek position was passed to this function.");
            str.Add("STR_ENGINE_ERR_INVALID_SPEAKER", "An invalid speaker was passed to this function based on the current speaker mode.");
            str.Add("STR_ENGINE_ERR_INVALID_SYNCPOINT", "The syncpoint did not come from this sound handle.");
            str.Add("STR_ENGINE_ERR_INVALID_THREAD", "The syncpoint did not come from this sound handle.");
            str.Add("STR_ENGINE_ERR_INVALID_VECTOR", "Tried to call a function on a thread that is not supported.");
            str.Add("STR_ENGINE_ERR_MAXAUDIBLE", "The vectors passed in are not unit length, or perpendicular.");
            str.Add("STR_ENGINE_ERR_MEMORY", "Not enough memory or resources.");
            str.Add("STR_ENGINE_ERR_MEMORY_CANTPOINT", "Internal Error. Can't use OPENMEMORY_POINT on non PCM source data, or non mp3/xma/adpcm data if CREATECOMPRESSEDSAMPLE was used.");
            str.Add("STR_ENGINE_ERR_NEEDS3D", "Tried to call a command on a 2d sound when the command was meant for 3d sound.");
            str.Add("STR_ENGINE_ERR_NEEDSHARDWARE", "Tried to use a feature that requires hardware support.");
            str.Add("STR_ENGINE_ERR_NET_CONNECT", "Couldn't connect to the specified host.");
            str.Add("STR_ENGINE_ERR_NET_SOCKET_ERROR", "A socket error occurred.  This is a catch-all for socket-related errors not listed elsewhere.");
            str.Add("STR_ENGINE_ERR_NET_URL", "The specified URL couldn't be resolved.");
            str.Add("STR_ENGINE_ERR_NET_WOULD_BLOCK", "Operation on a non-blocking socket could not complete immediately.");
            str.Add("STR_ENGINE_ERR_NOTREADY", "Operation could not be performed because specified sound/DSP connection is not ready.");
            str.Add("STR_ENGINE_ERR_OUTPUT_ALLOCATED", "Error initializing output device, but more specifically, the output device is already in use and cannot be reused.");
            str.Add("STR_ENGINE_ERR_OUTPUT_CREATEBUFFER", "Error creating hardware sound buffer.");
            str.Add("STR_ENGINE_ERR_OUTPUT_DRIVERCALL", "A call to a standard soundcard driver failed, which could possibly mean a bug in the driver or resources were missing or exhausted.");
            str.Add("STR_ENGINE_ERR_OUTPUT_FORMAT", "Soundcard does not support the specified format.");
            str.Add("STR_ENGINE_ERR_OUTPUT_INIT", "Error initializing output device.");
            str.Add("STR_ENGINE_ERR_OUTPUT_NODRIVERS", "The output device has no drivers installed.  If pre-init, OUTPUT_NOSOUND is selected as the output mode.  If post-init, the function just fails.");
            str.Add("STR_ENGINE_ERR_PLUGIN", "An unspecified error has been returned from a plugin.");
            str.Add("STR_ENGINE_ERR_PLUGIN_MISSING", "A requested output, dsp unit type or codec was not available.");
            str.Add("STR_ENGINE_ERR_PLUGIN_RESOURCE", "A resource that the plugin requires cannot be found. (ie the DLS file for MIDI playback) ");
            str.Add("STR_ENGINE_ERR_PLUGIN_VERSION", "A plugin was built with an unsupported SDK version.");
            str.Add("STR_ENGINE_ERR_RECORD", "An error occured trying to initialize the recording device.");
            str.Add("STR_ENGINE_ERR_REVERB_CHANNELGROUP", "Internal Error. Reverb properties cannot be set on this channel because a parent channelgroup owns the reverb connection.");
            str.Add("STR_ENGINE_ERR_REVERB_INSTANCE", "Internal Error. Specified instance in REVERB_PROPERTIES couldn't be set. Most likely because it is an invalid instance number or the reverb doesn't exist.");
            str.Add("STR_ENGINE_ERR_SUBSOUNDS", "Internal Error. The error occurred because the sound referenced contains subsounds when it shouldn't have, or it doesn't contain subsounds when it should have.  The operation may also not be able to be performed on a parent sound.");
            str.Add("STR_ENGINE_ERR_SUBSOUND_ALLOCATED", "This subsound is already being used by another sound, you cannot have more than one parent to a sound.  Null out the other parent's entry first.");
            str.Add("STR_ENGINE_ERR_SUBSOUND_CANTMOVE", "Shared subsounds cannot be replaced or moved from their parent stream, such as when the parent stream is an FSB file.");
            str.Add("STR_ENGINE_ERR_TAGNOTFOUND", "The specified tag could not be found or there are no tags.");
            str.Add("STR_ENGINE_ERR_TOOMANYCHANNELS", "Internal Error. The sound created exceeds the allowable input channel count.  This can be increased using the 'maxinputchannels' parameter in setSoftwareFormat.");
            str.Add("STR_ENGINE_ERR_TRUNCATED", "The retrieved string is too long to fit in the supplied buffer and has been truncated.");
            str.Add("STR_ENGINE_ERR_UNIMPLEMENTED", "Internal Error.  Something in Engine hasn't been implemented when it should be! contact support!");
            str.Add("STR_ENGINE_ERR_UNINITIALIZED", "This command failed because setDriver or init was not called.");
            str.Add("STR_ENGINE_ERR_UNSUPPORTED", "A command issued was not supported by this object.  Possibly a plugin without certain callbacks specified.");
            str.Add("STR_ENGINE_ERR_VERSION", "The version number of this file format is not supported.");
            str.Add("STR_ENGINE_ERR_EVENT_ALREADY_LOADED", "The specified bank has already been loaded.");
            str.Add("STR_ENGINE_ERR_EVENT_LIVEUPDATE_BUSY", "The live update connection failed due to the game already being connected.");
            str.Add("STR_ENGINE_ERR_EVENT_LIVEUPDATE_MISMATCH", "The live update connection failed due to the game data being out of sync with the tool.");
            str.Add("STR_ENGINE_ERR_EVENT_LIVEUPDATE_TIMEOUT", "The live update connection timed out.");
            str.Add("STR_ENGINE_ERR_EVENT_NOTFOUND", "The requested event, bus or vca could not be found.");
            str.Add("STR_ENGINE_ERR_STUDIO_UNINITIALIZED", "The Studio system object is not yet initialized.");
            str.Add("STR_ENGINE_ERR_STUDIO_NOT_LOADED", "The specified resource is not loaded, so it can't be unloaded.");
            str.Add("STR_ENGINE_ERR_INVALID_STRING", "An invalid string was passed to this function.");
            str.Add("STR_ENGINE_ERR_ALREADY_LOCKED", "The specified resource is already locked.");
            str.Add("STR_ENGINE_ERR_NOT_LOCKED", "The specified resource is not locked, so it can't be unlocked.");
            str.Add("STR_ENGINE_ERR_RECORD_DISCONNECTED", "The specified recording driver has been disconnected.");
            str.Add("STR_ENGINE_ERR_TOOMANYSAMPLES", "The length provided exceed the allowable limit.");
            str.Add("STR_ENGINE_ERR_UNKNOWN", "Internal Error. Uknown Error.");

            str.Add("STR_DEVICE_NOOUTPUT", "(No sound is output)");//(Unable to select device) //(Output that can not display a sound device)
            str.Add("STR_DEVICE_AUTOOUTPUT", "(Automatically selects a sound device)");

            str.Add("STR_REVERB_OFF", "Off");
            str.Add("STR_REVERB_GENERIC", "Generic");
            str.Add("STR_REVERB_PADDEDCELL", "Padded Cell");
            str.Add("STR_REVERB_ROOM", "Room");
            str.Add("STR_REVERB_BATHROOM", "Bathroom");
            str.Add("STR_REVERB_LIVINGROOM", "Living Room");
            str.Add("STR_REVERB_STONEROOM", "Stone Room");
            str.Add("STR_REVERB_AUDITORIUM", "Auditorium");
            str.Add("STR_REVERB_CONCERTHALL", "Concert Hall");
            str.Add("STR_REVERB_CAVE", "Cave");
            str.Add("STR_REVERB_ARENA", "Arena");
            str.Add("STR_REVERB_HANGAR", "Hangar");
            str.Add("STR_REVERB_CARPETEDHALLWAY", "Carpeted Hallway");
            str.Add("STR_REVERB_HALLWAY", "Hallway");
            str.Add("STR_REVERB_STONECORRIDOR", "Stone Corridor");
            str.Add("STR_REVERB_ALLEY", "Alley");
            str.Add("STR_REVERB_FOREST", "Forest");
            str.Add("STR_REVERB_CITY", "City");
            str.Add("STR_REVERB_MOUNTAINS", "Mountains");
            str.Add("STR_REVERB_QUARRY", "Quarry");
            str.Add("STR_REVERB_PLAIN", "Plain");
            str.Add("STR_REVERB_PARKINGLOT", "Parking Lot");
            str.Add("STR_REVERB_SEWERPIPE", "Sewer Pipe");
            str.Add("STR_REVERB_UNDERWATER", "Under Water");
            //str.Add("STR_REVERB_CUSTOM", "Custom");
        }
    }
}
