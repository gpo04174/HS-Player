﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.PlugIn.Output
{
    class ExtensionInfo
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Description"></param>
        /// <param name="Extension">eg. aac, mp3, mp4 ...</param>
        public ExtensionInfo(string Description, params string[] Extension)
        {
            this.Description = Description;
            this.Extension = Extension;
        }
        public string Description { get; private set; }
        public string[] Extension { get; private set; }
    }
}
