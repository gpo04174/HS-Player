﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Audio
{
    public enum AudioState
    {
        Unknown,
        Dettached,
        Playing,
        Pausing,
        Stopped,
    }
}
