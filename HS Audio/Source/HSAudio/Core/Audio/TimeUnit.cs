﻿namespace HSAudio.Core.Audio
{
    public enum TimeUnit : uint
    {
        MilliSecond = Lib.FMOD.TIMEUNIT.MS,
        PCM = Lib.FMOD.TIMEUNIT.PCM,
        PCMBytes = Lib.FMOD.TIMEUNIT.PCMBYTES,
        PCMFraction = Lib.FMOD.TIMEUNIT.PCMFRACTION,
        RawBytes = Lib.FMOD.TIMEUNIT.RAWBYTES
    }
}
