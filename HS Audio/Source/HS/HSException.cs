﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS
{
    public class HSException : Exception
    {
        public HSException() { }
        public HSException(string Message) : base(Message) { }
        public HSException(Exception InnerException) : base(InnerException.Message, InnerException) { }
        public HSException(string Message, Exception InnerException) : base(Message, InnerException) { }
    }
}
