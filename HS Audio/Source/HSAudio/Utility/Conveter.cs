﻿using System;
using System.Runtime.InteropServices;

namespace HSAudio.Utility
{
    public static class Bits
    {
        public static int GetInt(byte[] Byte4) { return BitConverter.ToInt32(Byte4, 0); }
        public static bool GetBool(byte[] Byte1) { return BitConverter.ToBoolean(Byte1, 0); }
        public static float GetFloat(byte[] Byte4) { return BitConverter.ToSingle(Byte4, 0); }
        public static float GetDouble(byte[] Byte8) { return BitConverter.ToSingle(Byte8, 0); }
#if !NETSTANDARD1x
        public static string GetString(byte[] data, System.Text.Encoding encoding = null) { return encoding == null ? BitConverter.ToString(data) : encoding.GetString(data); }
#else
        public static string GetString(byte[] data, System.Text.Encoding encoding = null) { return encoding == null ? BitConverter.ToString(data) : encoding.GetString(data, 0, data.Length); }
#endif

        public static int GetInt(IntPtr Byte4)
        {
            byte[] data = new byte[4];
            Marshal.Copy(Byte4, data, 0, 4);
            return BitConverter.ToInt32(data, 0);
        }
        public static bool GetBool(IntPtr Byte1)
        {
            byte[] data = new byte[1];
            Marshal.Copy(Byte1, data, 0, 4);
            return BitConverter.ToBoolean(data, 0);
        }
        public static float GetFloat(IntPtr Byte4)
        {
            byte[] data = new byte[4];
            Marshal.Copy(Byte4, data, 0, 4);
            return BitConverter.ToSingle(data, 0);
        }
        public static double GetDouble(IntPtr Byte8)
        {
            byte[] data = new byte[8];
            Marshal.Copy(Byte8, data, 0, 8);
            return BitConverter.ToDouble(data, 0);
        }
        public static string GetString(IntPtr data, int length, System.Text.Encoding encoding = null)
        {
            byte[] data1 = new byte[length];
            Marshal.Copy(data, data1, 0, length);
            return encoding == null ? BitConverter.ToString(data1) :
#if !NETSTANDARD1x
                encoding.GetString(data1);
#else
                encoding.GetString(data1, 0, data1.Length);
#endif
        }

        public static byte[] GetValue(int data) { return BitConverter.GetBytes(data); }
        public static byte[] GetValue(bool data) { return BitConverter.GetBytes(data); }
        public static byte[] GetValue(float data) { return BitConverter.GetBytes(data); }
        public static byte[] GetValue(double data) { return BitConverter.GetBytes(data); }
#if NETSTANDARD
        public static byte[] GetValue(string data, System.Text.Encoding encoding) { return encoding.GetBytes(data); }
#else
        public static byte[] GetValue(string data, System.Text.Encoding encoding = null) { return encoding == null ? System.Text.Encoding.Default.GetBytes(data) : encoding.GetBytes(data); }
#endif
    }

    public static class ArrayConveterN<T>
    {
        public static T[][] Convert(T[,] value)
        {
            if (value == null) return null;
            else if (value.GetLength(1) == 0) return new T[value.GetLength(0)][];
            else if (value.GetLength(0) == 0) return new T[0][];
            else
            {
                int rank = value.Rank, len = value.Length / rank;
                T[][] tmp = new T[rank][];
                for (int i = 0; i < rank; i++)
                {
                    tmp[i] = new T[len];
                    for (int j = 0; j < len; j++)
                        tmp[i][j] = value[i, j];
                }
                return tmp;
            }
        }

        public static T[][] Merge(T[][] value, T[][] merge, bool DeepMerge = false)
        {
            if (value == null) return null;
            else if (value.Length == 0) return value;
            else if (merge == null || value.Length == 0) return value;
            else
            {
                T[][] val = DeepMerge ? new T[value.Length][] : value;
                for (int i = 0; i < value.Length; i++)
                {
                    if (DeepMerge) val[i] = new T[value[i].Length];
                    if (i < merge.Length)
                    {
                        for (int j = 0; j < value[i].Length; j++)
                        {
                            if (j < merge[i].Length)
                                value[i][j] = merge[i][j];
                            else continue;
                        }
                    }
                    else continue;
                }
                return val;
            }
        }

        public static T[] MatrixToArray(T[][] value, bool RowBase)
        {
            if (value == null) return null;
            else if (value.Length == 0) return new T[0];
            else
            {
                int sum = value[0].Length;
                int peak = sum;
                int tmp = 0;
                for (int i = 1; i < value.Length; i++)
                {
                    tmp = value[i].Length;
                    sum += tmp;
                    if (tmp > peak) peak = tmp;
                }

                T[] matrix = new T[sum];
                int k = 0;
                if (RowBase)
                {
                    for (int i = 0; i < peak; i++)
                        for (int j = 0; j < value.Length; j++)
                            if (value[j].Length > i)
                                matrix[k++] = value[j][i];
                            else continue;
                }
                else
                {
                    for (int i = 0; i < value.Length; i++)
                        for (int j = 0; j < value[i].Length; j++)
                            matrix[k++] = value[i][j]; //"k++" is faster then "matrix[(i * value.Length) + (i + j)]" by HSKernel
                }
                return matrix;
            }
        }
        
        public static T[][] ArrayToMatrix(T[] value, int row, int column, bool RowBase)
        {
            if (value == null) return null;
            else if (value.Length == 0) return new T[0][];
            else
            {
                T[][] matrix = new T[row][];
                int k = 0;
                if (RowBase)
                {
                    for (int i = 0; i < column; i++)
                        for (int j = 0; j < row && k < value.Length; j++)
                        {
                            if (matrix[j] == null) matrix[j] = new T[column];
                            matrix[j][i] = value[k++];//for performance;
                        }
                }
                else
                {
                    for (int i = 0; i < row; i++)
                    {
                        matrix[i] = new T[column];
                        for (int j = 0; j < column && k < value.Length; j++)
                            matrix[i][j] = value[k++];//for performance;
                    }
                }
                return matrix;
            }
        }
    }
    public static class ArrayConveterC<T>
    {
        public static T[,] Convert(T[][] value)
        {
            if (value == null) return null;
            else if (value.Length == 0) return new T[0, 0];
            else
            {
                int peak = value[0].Length;
                for (int i = 1; i < value.Length; i++)
                    if (value[i].Length > peak) peak = value[i].Length;

                T[,] tmp = new T[value.Length, peak];
                for (int i = 0; i < value.Length; i++)
                    for (int j = 0; j < value[i].Length; j++)
                        tmp[i, j] = value[i][j];
                return tmp;
            }
        }

        public static T[] MatrixToArray(T[,] value, bool RowBase)
        {
            if (value == null) return null;
            else if (value.Length == 0) return new T[0];
            else
            {
                int row = value.GetLength(0), col = value.GetLength(1);
                T[] matrix = new T[value.Length];
                
                /*
                for (int i = 0; i < row; i++)
                    for (int j = 0; j < col; j++)
                        matrix[k++] = value[i, j];
                */

                for (int i = 0; i < value.Length; i++)
                    matrix[i] = RowBase ?
                        value[i / col, i % col] :
                        value[i % row, i / row];
                return matrix;
            }
        }

        public static T[,] ArrayToMatrix(T[] value, int row, int column, bool RowBase)
        {
            if (value == null) return null;
            else if (value.Length == 0) return new T[row, column];
            else
            {
                T[,] matrix = new T[row, column];
                if (RowBase)
                {
                    for (int i = 0; i < value.Length; i++)
                        matrix[i / column, i % column] = value[i];
                }
                else
                {
                    for (int i = 0; i < value.Length; i++)
                        matrix[i % row, i / row] = value[i];
                }
                return matrix;
            }
        }
    }
}
