﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace HSAudio.Lib.Windows.CoreAudioApi.Interfaces
{
    /// <summary>
    /// implements IMMDeviceEnumerator
    /// </summary>
    [ComImport, Guid("BCDE0395-E52F-467C-8E3D-C4579291692E")]
    class MMDeviceEnumeratorComObject
    {
    }
}
#endif