﻿using HSAudio.Lib.TanjentOGG;
using System;
using System.IO;
using System.Runtime.InteropServices;
using HSAudio.Lib.NVorbis;

namespace HSAudio.Streams
{
    public class VorbisReadStream : Stream
    {
        //TanjentOGG t = new TanjentOGG();
        VorbisReader reader;
        FileStream fs;
        public VorbisReadStream(Stream BaseStream)
        {
            this.BaseStream = BaseStream;
            reader = new VorbisReader(BaseStream, false, true);
            //fs = new FileStream("Z:\\asd.raw", FileMode.Create);
        }

        public Stream BaseStream { get; private set; }


        public override bool CanRead { get { return true; } }
        public override bool CanSeek { get { return BaseStream.CanSeek; } }
        public override bool CanWrite { get { return false; } }

        public override long Length { get { return reader.TotalSamples * reader.Channels * 4; } }

        public override long Position { get { return BaseStream.Position; }set { BaseStream.Position = value; } }

        public override void Flush() { }

        public unsafe override int Read(byte[] buffer, int offset, int count)
        {
            float[] sample = new float[(count - offset) / 4];
            int read = reader.ReadSamples(sample, 0, sample.Length);

            //byte[] buf = new byte[count - offset];
            //BaseStream.Read(buf, offset, count);
            //t.DecodeToFloats(buf);
            //sample = t.DecodedFloats;
            //int read = sample.Length;

            fixed(byte* a = buffer) { Marshal.Copy(sample, 0, new IntPtr(a), sample.Length); }
            //fs.Write(buffer, 0, count);
            //fs.Flush();
            return read;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return BaseStream.Seek(offset, origin);
        }

        public override void SetLength(long value) { BaseStream.SetLength(value); }

        public override void Write(byte[] buffer, int offset, int count) { throw new NotSupportedException(); }
    }
}
