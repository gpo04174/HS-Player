﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HSPlayer.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlayControl : ContentView
	{
		public PlayControl ()
		{
			InitializeComponent ();
            btn_play.Source = ImageSource.FromStream(() => new MemoryStream(Properties.Resources.btn_play));
            btn_stop.Source = ImageSource.FromStream(() => new MemoryStream(Properties.Resources.btn_stop));
            btn_prev.Source = ImageSource.FromStream(() => new MemoryStream(Properties.Resources.btn_previous));
            btn_next.Source = ImageSource.FromStream(() => new MemoryStream(Properties.Resources.btn_next));
        }
    }
}