﻿using System.Threading;

namespace HS.Setting.IO
{
    //T is string T1 is Settings
    public delegate void LoadComplete(object sender, Settings data);
    public delegate void SaveComplete<T>(object sender, T data);
    public abstract partial class SettingIO<T>
    {
        public void LoadAsync(T Data, LoadAsync<T> complete)
        {
            complete.ThreadStart = new ParameterizedThreadStart(complete.start);
            complete.Thread = new Thread(complete.ThreadStart);
            complete.Thread.Start(Data);
        }
        public void SaveAsync(Settings Data, SaveAsync<T> complete)
        {
            complete.ThreadStart = new ParameterizedThreadStart(complete.start);
            complete.Thread = new Thread(complete.ThreadStart);
            complete.Thread.Start(Data);
        }
    }

    public class LoadAsync<T>
    {
        private LoadComplete Complete;
        private SettingIO<T> Instance;
        internal Thread Thread;
        internal ParameterizedThreadStart ThreadStart;
        public LoadAsync(SettingIO<T> Instance, LoadComplete Complete) { this.Instance = Instance; this.Complete = Complete; }

        public void Cancel() { try { if (Thread != null) Thread.Abort(); } catch { } }

        internal void start(object data)
        {
            try
            {
                Settings s = Instance.Load((T)data);
                Complete(Instance, s);
            }
            catch { }
        }
    }
    public class SaveAsync<T>
    {
        private SaveComplete<T> Complete;
        private SettingIO<T> Instance;
        internal Thread Thread;
        internal ParameterizedThreadStart ThreadStart;
        public SaveAsync(SettingIO<T> Instance, SaveComplete<T> Complete) { this.Instance = Instance; this.Complete = Complete; }

        public void Cancel() { try { if (Thread != null) Thread.Abort(); } catch { } }

        internal void start(object data)
        {
            try
            {
                T s = Instance.Save((Settings)data);
                Complete(Instance, s);
            }
            catch { }
        }
    }
}
