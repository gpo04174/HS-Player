﻿using HSAudio.Lib.NVorbis;
using HSAudio.Lib.Windows.NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace Test
{

    public class OGGTest
    {
        public static void asd(string[] args)
        {
            //NAudioStreamingTest_1();//NotSupportException
            //NAudioStreamingTest_2(); //OK
            NVobrisStreamingTest_1();
        }

        public static void NAudioStreamingTest_1()
        {
            WebRequest webRequest = (HttpWebRequest)WebRequest.Create("http://185.33.21.112/classical_128");
            WebResponse resp = webRequest.GetResponse();

            NAudioWriteStream(resp.GetResponseStream()); //NotSupportException

            resp.Close();

        }
        public static void NAudioStreamingTest_2()
        {
            WebRequest webRequest = (HttpWebRequest)WebRequest.Create("http://185.33.21.112/classical_128");//http://theprodigy.ru:8000/live.ogg
            WebResponse resp = webRequest.GetResponse();

            var readFullyStream = new ReadFullyStream(resp.GetResponseStream());
            NAudioWriteStream(readFullyStream); //OK
            resp.Close();
        }
        public static void NVobrisStreamingTest_1()
        {
            Stream stream = null;
            //WebRequest webRequest = (HttpWebRequest)WebRequest.Create("http://theprodigy.ru:8000/live.ogg");
            //WebResponse resp = webRequest.GetResponse();
            //stream = resp.GetResponseStream();

            stream = new FileStream("C:\\Users\\gpo04\\OneDrive\\음악\\[Test]\\シンクロナイザー.ogg", FileMode.Open);
            //var fixStream = new FixBufferStream(stream);
            NVorbisWriteStream(stream); //Invalid OGG File!!
            stream.Close();
        }
        public static void NVobrisStreamingTest_2()
        {
            WebRequest webRequest = (HttpWebRequest)WebRequest.Create("http://theprodigy.ru:8000/live.ogg");
            WebResponse resp = webRequest.GetResponse();

            NVorbisWriteStream(resp.GetResponseStream()); //System.NotSupportedException
            resp.Close();
        }

        public static void NAudioWriteStream(Stream stream)
        {
            FileStream fs = new FileStream("D:\\test.raw", FileMode.Create);
            AcmMp3FrameDecompressor decompressor = new AcmMp3FrameDecompressor(new WaveFormat(44100, 2, 16));
            byte[] buffer = new byte[1024 * 500];
            using (stream)
            {
                long testlen = 0;
                while (testlen < 100000)
                {
                    Mp3Frame frame = Mp3Frame.LoadFromStream(stream);
                    int decompressed = decompressor.DecompressFrame(frame, buffer, 0);
                    fs.Write(buffer, 0, decompressed);
                }
            }
            decompressor.Dispose();
            fs.Close();
        }
        public static void NVorbisWriteStream(Stream stream)
        {
            VorbisReader vorbis = new VorbisReader(stream, true); //System.NotSupportedException
            FileStream fs = new FileStream("D:\\test.raw", FileMode.Create);

            long testlen = 0;
            while (testlen < 1000)
            {
                float[] buf = new float[1024];
                int len = vorbis.ReadSamples(buf, 0, buf.Length);
                for (int i = 0; i < len; i++)
                {
                    byte[] four = BitConverter.GetBytes(buf[i]);
                    fs.Write(four, 0, 4);
                }
                fs.Flush();
                testlen++;
            }
        }
    }
    public class ReadFullyStream : Stream
    {
        private readonly Stream sourceStream;
        private long pos; // psuedo-position
        private readonly byte[] readAheadBuffer;
        private int readAheadLength;
        private int readAheadOffset;

        public ReadFullyStream(Stream sourceStream)
        {
            this.sourceStream = sourceStream;
            readAheadBuffer = new byte[4096];
        }
        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void Flush()
        {
            throw new InvalidOperationException();
        }

        public override long Length
        {
            get { return pos; }
        }

        public override long Position
        {
            get
            {
                return pos;
            }
            set
            {
                throw new InvalidOperationException();
            }
        }


        public override int Read(byte[] buffer, int offset, int count)
        {
            int bytesRead = 0;
            while (bytesRead < count)
            {
                int readAheadAvailableBytes = readAheadLength - readAheadOffset;
                int bytesRequired = count - bytesRead;
                if (readAheadAvailableBytes > 0)
                {
                    int toCopy = Math.Min(readAheadAvailableBytes, bytesRequired);
                    Array.Copy(readAheadBuffer, readAheadOffset, buffer, offset + bytesRead, toCopy);
                    bytesRead += toCopy;
                    readAheadOffset += toCopy;
                }
                else
                {
                    readAheadOffset = 0;
                    readAheadLength = sourceStream.Read(readAheadBuffer, 0, readAheadBuffer.Length);
                    //Debug.WriteLine(String.Format("Read {0} bytes (requested {1})", readAheadLength, readAheadBuffer.Length));
                    if (readAheadLength == 0)
                    {
                        break;
                    }
                }
            }
            pos += bytesRead;
            return bytesRead;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new InvalidOperationException();
        }

        public override void SetLength(long value)
        {
            throw new InvalidOperationException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException();
        }
    }

    public class asd
    {
        public static void StreamngOGG()
        {
            using (var vorbisStream = new VorbisWaveReader("http://theprodigy.ru:8000/live.ogg"))
            using (var waveOut = new WaveOutEvent())
            {
                waveOut.Init(vorbisStream);
                waveOut.Play();
                while (true) Thread.Sleep(10);

                // wait here until playback stops or should stop
            }
        }
    }
    public class VorbisWaveReader : WaveStream, IDisposable, ISampleProvider, IWaveProvider
    {
        VorbisReader _reader;
        WaveFormat _waveFormat;

        public VorbisWaveReader(string fileName)
        {
            _reader = new VorbisReader(fileName);

            _waveFormat = WaveFormat.CreateIeeeFloatWaveFormat(_reader.SampleRate, _reader.Channels);
        }

        public VorbisWaveReader(System.IO.Stream sourceStream)
        {
            _reader = new VorbisReader(sourceStream, false);

            _waveFormat = WaveFormat.CreateIeeeFloatWaveFormat(_reader.SampleRate, _reader.Channels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }

            base.Dispose(disposing);
        }

        public override WaveFormat WaveFormat
        {
            get { return _waveFormat; }
        }

        public override long Length
        {
            get { return (long)(_reader.TotalTime.TotalSeconds * _waveFormat.SampleRate * _waveFormat.Channels * sizeof(float)); }
        }

        public override long Position
        {
            get
            {
                return (long)(_reader.DecodedTime.TotalSeconds * _reader.SampleRate * _reader.Channels * sizeof(float));
            }
            set
            {
                if (value < 0 || value > Length) throw new ArgumentOutOfRangeException("value");

                _reader.DecodedTime = TimeSpan.FromSeconds((double)value / _reader.SampleRate / _reader.Channels / sizeof(float));
            }
        }

        // This buffer can be static because it can only be used by 1 instance per thread
        [ThreadStatic]
        static float[] _conversionBuffer = null;

        public override int Read(byte[] buffer, int offset, int count)
        {
            // adjust count so it is in floats instead of bytes
            count /= sizeof(float);

            // make sure we don't have an odd count
            count -= count % _reader.Channels;

            // get the buffer, creating a new one if none exists or the existing one is too small
            var cb = _conversionBuffer ?? (_conversionBuffer = new float[count]);
            if (cb.Length < count)
            {
                cb = (_conversionBuffer = new float[count]);
            }

            // let Read(float[], int, int) do the actual reading; adjust count back to bytes
            int cnt = Read(cb, 0, count) * sizeof(float);

            // move the data back to the request buffer
            Buffer.BlockCopy(cb, 0, buffer, offset, cnt);

            // done!
            return cnt;
        }

        public int Read(float[] buffer, int offset, int count)
        {
            return _reader.ReadSamples(buffer, offset, count);
        }

        public bool IsParameterChange { get { return _reader.IsParameterChange; } }

        public void ClearParameterChange()
        {
            _reader.ClearParameterChange();
        }

        public int StreamCount
        {
            get { return _reader.StreamCount; }
        }

        public int? NextStreamIndex { get; set; }

        public bool GetNextStreamIndex()
        {
            if (!NextStreamIndex.HasValue)
            {
                var idx = _reader.StreamCount;
                if (_reader.FindNextStream())
                {
                    NextStreamIndex = idx;
                    return true;
                }
            }
            return false;
        }

        public int CurrentStream
        {
            get { return _reader.StreamIndex; }
            set
            {
                if (!_reader.SwitchStreams(value))
                {
                    throw new System.IO.InvalidDataException("The selected stream is not a valid Vorbis stream!");
                }

                if (NextStreamIndex.HasValue && value == NextStreamIndex.Value)
                {
                    NextStreamIndex = null;
                }
            }
        }

        /// <summary>
        /// Gets the encoder's upper bitrate of the current selected Vorbis stream
        /// </summary>
        public int UpperBitrate { get { return _reader.UpperBitrate; } }

        /// <summary>
        /// Gets the encoder's nominal bitrate of the current selected Vorbis stream
        /// </summary>
        public int NominalBitrate { get { return _reader.NominalBitrate; } }

        /// <summary>
        /// Gets the encoder's lower bitrate of the current selected Vorbis stream
        /// </summary>
        public int LowerBitrate { get { return _reader.LowerBitrate; } }

        /// <summary>
        /// Gets the encoder's vendor string for the current selected Vorbis stream
        /// </summary>
        public string Vendor { get { return _reader.Vendor; } }

        /// <summary>
        /// Gets the comments in the current selected Vorbis stream
        /// </summary>
        public string[] Comments { get { return _reader.Comments; } }

        /// <summary>
        /// Gets the number of bits read that are related to framing and transport alone
        /// </summary>
        public long ContainerOverheadBits { get { return _reader.ContainerOverheadBits; } }

        /// <summary>
        /// Gets stats from each decoder stream available
        /// </summary>
        public IVorbisStreamStatus[] Stats { get { return _reader.Stats; } }
    }
}
