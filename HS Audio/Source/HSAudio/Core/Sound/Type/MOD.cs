﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class MOD : ISoundFormat
    {
        public MOD() : base(SoundType.MOD, MIME, Extension, Description) {  }

        public static string[] MIME { get { return new string[] { "audio/mod" }; } }
        public static string[] Extension { get { return new string[] { "mod" }; } }
        public static string[] Description { get { return new string[] { "Protracker / Fasttracker MOD" }; } }

        public override string ToString() { return Description[0]; }
    }
}
