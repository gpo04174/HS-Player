﻿using HSAudio.Lib.FMOD;

namespace HSAudio.PlugIn.DSP
{
    public class ProcessEventArgs : PluginDSPEventArgs
    {
        internal ProcessEventArgs(ref DSP_STATE State, uint length, ref BufferArray inbufferarray, ref BufferArray outbufferarray, bool inputsidle, DSP_PROCESS_OPERATION op)
        {
            this.State = State;
            Legth = length;
            InputsIdle = inputsidle;
            IsOperationPerfome = op == DSP_PROCESS_OPERATION.PROCESS_PERFORM;
            Input = inbufferarray;
            Output = outbufferarray;
        }
        internal protected DSP_STATE State { get; private set; }
        public uint Legth { get; private set; }
        public bool InputsIdle { get; private set; }
        /// <summary>
        /// True is Perform False is Query
        /// </summary>
        public bool IsOperationPerfome { get; private set; }

        public BufferArray Input { get; private set; }
        public BufferArray Output { get; private set; }
    }
}
