﻿using HSAudio.Resource;
using HSAudio.Utility;
using HS.Setting;
using System;
using HS;

namespace HSAudio.DSP
{
    public class ParameterValue : ISettingManager, IDisposable
    {
        private float _Value_Float;
        private int _Value_Int;
        private bool _Value_Bool;
        public ParameterValue(float Value_Float) { this.Value_Float = Value_Float; Type = DSPParameterType.Float; }
        public ParameterValue(int Value_Int) { this.Value_Int = Value_Int; Type = DSPParameterType.Int; }
        public ParameterValue(bool Value_Bool) { this.Value_Bool = Value_Bool; Type = DSPParameterType.Bool; }
        public ParameterValue(DataValue Value_Data) { this.Value_Data = Value_Data; Type = DSPParameterType.Data; }
        internal ParameterValue(DSPParameterType Type) { this.Type = Type; }

        public ParameterValue(Settings setting) { LoadSetting(setting); }

        public DSPParameterType Type { get; private set; }

        public float Value_Float { get {return _Value_Float;} set { _Value_Float = value; _ValueString = value.ToString();} }
        public int Value_Int { get { return _Value_Int; } set { _Value_Int = value; _ValueString = value.ToString(); } }
        public bool Value_Bool { get { return _Value_Bool; } set { _Value_Bool = value; _ValueString = value.ToString(); } }
        public DataValue Value_Data { get; set; }

        private string _ValueString;
        public string ValueString { get { return _ValueString; } set { setValueString(value);} }

        public void setValueString(string str){ _ValueString = str.Length > 16 ? str.Substring(0, 16) : str; }

        #region Setter
        public ParameterValue SetValue(float Value) { Value_Float = Value; return this; }
        public ParameterValue SetValue(int Value) { Value_Int = Value; return this; }
        public ParameterValue SetValue(bool Value) { Value_Bool = Value; return this; }
        public ParameterValue SetValue(DataValue Data) { Value_Data = Data; return this; }
        public ParameterValue SetValue(IntPtr Value, uint Length, DataType Type) { Value_Data = new DataValue(Value, Length, Type); return this; }
        #endregion

        #region Settings
        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Type", new SettingsData(Type.ToString()));
            if (Type == DSPParameterType.Data) s.SubSetting.SetValue("Value", Value_Data.SaveSetting());
            else s.SetValue("Value", new SettingsData(convert(), false));
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            DSPParameterType t = DSPParameterType.Data;
            if (Setting.Exist("Type")) { t = (DSPParameterType)Enum.Parse(typeof(DSPParameterType), Setting["Type"]); }
            else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_SETTING_WRONG"]); return false; }

            if (Setting.Exist("Value")) parse(t, Setting["Value"]);
            else if (Setting.SubSetting.Exist("Value")) Value_Data = new DataValue(Setting.SubSetting["Value"]);
            return true;
        }

        unsafe void parse(DSPParameterType Type, string value)
        {
            if (Type == DSPParameterType.Float) Value_Float = Convert.ToSingle(value);
            else if (Type == DSPParameterType.Int) Value_Int = Convert.ToInt32(value);
            else if (Type == DSPParameterType.Bool) Value_Bool = Convert.ToBoolean(value);
        }
        unsafe string convert()
        {
            if (Type == DSPParameterType.Float) return Value_Float.ToString();
            else if (Type == DSPParameterType.Int) return Value_Int.ToString();
            else if (Type == DSPParameterType.Bool) return Value_Bool.ToString();
            else return null;
        }
        #endregion


        public static implicit operator DataValue(ParameterValue param) { return param.Value_Data; }
        public static implicit operator float(ParameterValue param) { return param.Value_Float; }
        public static implicit operator int(ParameterValue param) { return param.Value_Int; }
        public static implicit operator bool(ParameterValue param) { return param.Value_Bool; }

        public void Dispose() { if (Value_Data != null) Value_Data.Dispose(); }
        ~ParameterValue() { if(Value_Data != null)Value_Data.Dispose(); }
    }
}
