﻿using HSAudio.Lib.FMOD;

namespace HSAudio.Core.Audio
{
    public enum LoopKind : uint
    {
        /// <summary>
        /// Loop off <para/>
        /// 루프 끄기
        /// </summary>
        Off = MODE.LOOP_OFF,
        /// <summary>
        /// Normal loop <para/>
        /// 일반 루프
        /// </summary>
        Normal = MODE.LOOP_NORMAL,
        /// <summary>
        /// Swing loop (Available when Sound.CanReverse is true)<para/>
        /// 스윙 루프 (Sound.CanReverse 가 true 여야만 가능합니다.)
        /// </summary>
        BIDI = MODE.LOOP_BIDI
    }
}
