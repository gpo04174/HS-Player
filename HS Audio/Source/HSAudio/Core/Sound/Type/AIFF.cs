﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class AIFF : ISoundFormat
    {
        public AIFF() : base(SoundType.AIFF, MIME, Extension, Description) {}


        public static string[] MIME { get { return new string[] { "audio/x-aiff", "audio/aiff" }; } }
        public static string[] Extension { get { return new string[] { "aiff", "aif", "aifc" }; } }
        public static string[] Description { get { return new string[] { "Audio Interchange File Format" }; } }

        public override string ToString() { return Description[0]; }
    }
}
