﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HSAudio.Utility;
using System.Runtime.InteropServices;

namespace HSAudio.Core.Sound.Type
{
    public class FSB : ISoundFormat
    {
        public FSB() : base(SoundType.FSB, MIME, Extension, Description) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SubSoundCount">In a user created multi-sample sound</param>
        /// <param name="SubSoundInit">In a multi-sample file format such as .FSB/.DLS/.SF2, specify the initial subsound to seek to, only if CREATESTREAM is used.</param>
        public FSB(int SubSoundCount = 0, int SubSoundInit = 0, byte[] Password = null) : base(SoundType.FSB, MIME, Extension, Description)
        {
            info.numsubsounds = SubSoundCount;
            info.initialsubsound = SubSoundInit;
            this.Password = Password;
            if (Password != null && Password.Length > 0)
            {
                info.encryptionkey = Marshal.AllocHGlobal(sizeof(byte) * Password.Length);
                Marshal.Copy(Password, 0, info.encryptionkey, Password.Length);
            }
        }

        public byte[] Password { get; private set; }

        public static string[] MIME { get { return new string[] { "audio/fsb", "application/fsb" }; } }
        public static string[] Extension { get { return new string[] { "fsb" }; } }
        public static string[] Description { get { return new string[] { "FMOD Sound Bank" }; } }

        public override string ToString() { return Description[0]; }

        ~FSB() { if (info.encryptionkey != IntPtr.Zero) Marshal.FreeHGlobal(info.encryptionkey); }
    }
}
