﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Threading;

namespace FMODTest.Test
{
    public class DSPTest
    {
        public static void Test(string MusicPath)
        {
            FD_System system;
            FD_Sound sound, sound1;
            FD_Channel channel, channel1;

            Factory.System_Create(out system);

            RESULT result = RESULT.OK;
            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            result = system.createStream(MusicPath, MODE.CREATESTREAM, out sound);
            //result = system.createStream(MusicPath, MODE.CREATESTREAM, out sound1);
            result = system.playSound(sound, null, false, out channel);
            //Thread.Sleep(25);
            //result = system.playSound(sound1, null, false, out channel1);

            DSPCol col = new DSPCol(channel);
            FD_DSP dsp, dsp1, dsp2;
            result = system.createDSPByType(DSP_TYPE.THREE_EQ, out dsp);
            result = system.createDSPByType(DSP_TYPE.FLANGE, out dsp1);
            result = system.createDSPByType(DSP_TYPE.THREE_EQ, out dsp2);
            
            result = dsp.setParameterFloat((int)DSP_THREE_EQ.LOWGAIN, 10f);
            result = dsp2.setParameterFloat((int)DSP_THREE_EQ.HIGHGAIN, 10f);
            col.Add(dsp);
            col.Add(dsp1);
            col.Add(dsp2);

            //FD_DSPConnection conn, conn1;
            //result = dsp.addInput(dsp1, out conn, DSPCONNECTION_TYPE.STANDARD);
            //result = dsp1.addInput(dsp2, out conn1, DSPCONNECTION_TYPE.STANDARD);

            //result = channel.addDSP(0, dsp);
            //result = system.playDSP(dsp1, null, false, out channel1);
            //result = channel.addDSP(1, dsp2);
            FD_ChannelGroup gr, gr1;
            channel.getChannelGroup(out gr1);
            system.createChannelGroup("SubDSP - ", out gr);

            FD_DSP[] dsps = col.GetDSP();

            col[1] = dsp2;

            int INPUT, OUTPUT;
            bool active;
            dsp1.getActive(out active); dsp2.getActive(out active);
            result = dsp1.setActive(true); dsp2.setActive(true);
            dsp.getNumInputs(out INPUT); dsp.getNumOutputs(out OUTPUT);
            dsp1.getNumInputs(out INPUT); dsp1.getNumOutputs(out OUTPUT);
            dsp2.getNumInputs(out INPUT); dsp2.getNumOutputs(out OUTPUT);
            result = channel.setMode(MODE._2D);
            //result = channel.setMode(MODE.LOOP_BIDI);
            MODE mode;
            result = channel.getMode(out mode);

            int hash = system.GetHashCode();
            float Freq;
            channel.setPaused(false);

            OPENSTATE state; uint percent; bool starving; bool diskbusy;
            result = sound.getOpenState(out state, out percent, out starving, out diskbusy);
            channel.getFrequency(out Freq);
            //result = channel.setPitch(10f);
            //result = channel.setPitch(1f);
            result = channel.setFrequency(-44110);
        }

        public class DSPCol
        {
            internal List<FD_DSP> list = new List<FD_DSP>();
            internal RESULT Result;
            FD_Channel ch;
            internal DSPCol(FD_Channel ch)
            {
                this.ch = ch;
            }

            public int Count { get { return list.Count; } }
            public FD_DSP this[int index]
            {
                get { return list[index]; }
                set { Insert(value, index); }
            }

            public void Add(FD_DSP DSP)
            {
                Result = ch.addDSP(Count, DSP);
                list.Add(DSP);
            }
            public void Remove(FD_DSP DSP)
            {
                Result = ch.removeDSP(DSP);
                list.Remove(DSP);
            }
            private void Remove(FD_DSP DSP, bool RemoveDSP)
            {
                if (RemoveDSP) Result = ch.removeDSP(DSP);
                list.Remove(DSP);
            }
            public void RemoveAt(int index)
            {
                Result = ch.removeDSP(list[index]);
                list.RemoveAt(index);
            }
            public void Insert(FD_DSP DSP, int index)
            {
                Result = ch.setDSPIndex(DSP, index);
                Remove(DSP, false);
                list.Insert(index, DSP);
            }

            public FD_DSP[] GetDSP()
            {
                int num;
                ch.getNumDSPs(out num);
                FD_DSP[] dsp = new FD_DSP[num];
                for (int i = 0; i < num; i++) ch.getDSP(i, out dsp[i]);
                return dsp;
            }
        }
    }
}
