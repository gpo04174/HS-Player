﻿using HSAudio.Lib.FMOD;
using HSAudio.Output.Devices;
using System;

namespace HSAudio.Output
{
    partial class Output
    {
        private RESULT SYSTEM_CALLBACK_DEVICE(IntPtr systemraw, SYSTEM_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2, IntPtr userdata)
        {
            //unsafe {int* block = stackalloc int[100];}
            if (type == SYSTEM_CALLBACK_TYPE.DEVICELISTCHANGED && DeviceChanged != null)
            {
                //이 이벤트로 인해 사운드가 멈추면 True 안멈추면 False
                Status = OutputStatus.NotOpen;
                try
                {
                    if (DeviceChanged != null) DeviceChanged.Invoke(this);
                    else { Close(); Open(new AutoOutput());}
                }catch { }
            }
            else if (type == SYSTEM_CALLBACK_TYPE.DEVICELOST && DeviceChanged != null)
            {
                Status = OutputStatus.NotOpen;
                try
                {
                    if (DeviceLost != null) DeviceLost.Invoke(this);
                    else { Close(); Open(new AutoOutput()); }
                } catch { }
            }
            return RESULT.OK;
        }

    }
}
