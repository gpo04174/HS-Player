﻿using HS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace HSAudio.Utility.Stream
{
#if NETSTANDARD
    using System.Net.Http;
    public class WebStreamRead : System.IO.Stream, IDisposable
    {
        TcpClient tc;
        bool _CanSeek;
        int Timeout;
        public WebStreamRead(Uri SocketURL, int Timeout = 3000) : base()
        {
            this.SocketURL = SocketURL;
            this.Timeout = Timeout;
            ActiveMode = true;
            _CanSeek = false;

            Connection();
        }
        HttpClient req = null;
        public WebStreamRead(string WebURL, int Timeout = 3000) : base()
        {
            this.WebURL = WebURL;
            this.Timeout = Timeout;
            ActiveMode = true;
            _CanSeek = true;

            Connection();
        }

        Socket s;
        public async void Connection()
        {
            if (BaseStream != null) BaseStream.Dispose();
            if (SocketURL != null)
            {
                if (req != null) try { req.Dispose();  } catch { }
                req = new HttpClient();
                req.Timeout = new TimeSpan(0, 0, 0, 0, Timeout);
                try { BaseStream = await req.GetStreamAsync(WebURL); }
                finally { Dispose(); }
            }
            else
            {
                try
                {
                    if (s != null) try { s.Dispose(); } catch { }
                    s = new Socket(SocketType.Stream, ProtocolType.Tcp);
                    s.ReceiveTimeout = s.SendTimeout = Timeout;
                    await s.ConnectAsync(SocketURL.Host, SocketURL.Port);
                    await req.GetStreamAsync(SocketURL);
                }
                finally { Dispose(); }
            }
        }
        
        public System.IO.Stream BaseStream { get; private set; }
        //public Stream WriteStream { get; private set; }

        /// <summary>
        /// True is when odd offset to WebStream, reconnect and read. <para/>
        /// 참이면 오프셋값을 뺄때 웹스트림이면 연결을 다시하고 읽습니다.
        /// </summary>
        public bool ActiveMode { get; set; }
        public string WebURL { get; private set; }
        public Uri SocketURL { get; private set; }

        public override bool CanRead { get { return true; } }

        public override bool CanSeek { get { return _CanSeek && ActiveMode; } }

        public override bool CanWrite { get { return false; } }

        public override long Length { get { throw new NotSupportedException(); } }

        long _Position;
        public override long Position
        {
            get { return _Position; }
            set
            {
                byte[] dummy = null;
                if (value < _Position) 
                {
                    Connection();
                    dummy = new byte[value];
                }
                else dummy = new byte[value - _Position];
                Read(dummy, 0, dummy.Length);
                _Position = value;
            }
        }

        public override void Flush(){ }

        public override int Read(byte[] buffer, int offset, int count)
        {
            _Position += count - offset;
            return BaseStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            long offcalc = 0;
            if (origin == SeekOrigin.Begin)offcalc = offset;
            else offcalc = Position + offset;

            if (Position == offcalc) return 0;
            else Position = offcalc;
            return offcalc;
        }

        public override void SetLength(long value){throw new NotSupportedException(); }

        public override void Write(byte[] buffer, int offset, int count) { BaseStream.Write(buffer, offset, count); }

        public new int ReadTimeout { get { return BaseStream.ReadTimeout; }set { base.ReadTimeout = value; } }
        public new int WriteTimeout { get { return BaseStream.WriteTimeout; } set { base.WriteTimeout = value; } }

        public new void Dispose()
        {
            if (BaseStream != null) try { BaseStream.Dispose(); } catch { }
            if (s != null) try { s.Dispose(); } catch { }
            if (tc != null) try { tc.Dispose(); } catch { }
        }
    }

#else
    public class WebStreamRead : System.IO.Stream, IDisposable
    {
        TcpClient tc;
        bool _CanSeek;
        int Timeout;
        long _Length;

        public WebStreamRead(Uri SocketURL, int Timeout = 3000, long Length = 0) : base()
        {
            this.SocketURL = SocketURL;
            this.Timeout = Timeout;
            _Length = Length;
            ActiveMode = true;
            _CanSeek = false;

            Connection();
        }
        WebRequest req = null;
        WebResponse res = null;
        public WebStreamRead(string WebURL, int Timeout = 3000, long Length = 0) : base()
        {
            this.WebURL = WebURL;
            this.Timeout = Timeout;
            _Length = Length;
            ActiveMode = true;
            _CanSeek = true;

            Connection();
        }

#if NETCORE
        public async void Connection()
#else
        public void Connection()
#endif
        {
            if (SocketURL != null)
            {
                try
                {
                    if (tc != null) Dispose();
                    tc = new TcpClient(SocketURL.Host, SocketURL.Port) { ReceiveTimeout = Timeout };
                    BaseStream = tc.GetStream();
                    //WriteStream = tc.GetStream();
                }
                catch { Dispose(); }
            }
            else
            {
                try
                {
                    Dispose();
                    req = WebRequest.Create(WebURL);
                    req.Timeout = Timeout;
#if NETCORE
                    res = await req.GetResponseAsync();
#else
                    res = req.GetResponse();
#endif
                    BaseStream = res.GetResponseStream();
                    //WriteStream = req.GetRequestStream();
                }
                catch(Exception ex) { Dispose(); }
            }
        }

        public System.IO.Stream BaseStream { get; private set; }
        //public Stream WriteStream { get; private set; }

        /// <summary>
        /// True is when odd offset to WebStream, reconnect and read. <para/>
        /// 참이면 오프셋값을 뺄때 웹스트림이면 연결을 다시하고 읽습니다.
        /// </summary>
        public bool ActiveMode { get; set; }
        public string WebURL { get; private set; }
        public Uri SocketURL { get; private set; }

        public override bool CanRead { get { return true; } }

        public override bool CanSeek { get { return _CanSeek && ActiveMode; } }

        public override bool CanWrite { get { return false; } }

        public override long Length { get { return _Length; } }

        long _Position;
        public override long Position
        {
            get { return _Position; }
            set
            {
                byte[] dummy = null;
                if (value < _Position)
                {
                    value = Math.Max(value, 0);
                    Connection();
                    dummy = new byte[value];
                }
                else dummy = new byte[value - _Position];
                Read(dummy, 0, dummy.Length);
                _Position = value;
            }
        }

        public override void Flush() { }

        
        private readonly byte[] readAheadBuffer = new byte[4096];
        private int readAheadLength;
        private int readAheadOffset;
        public override int Read(byte[] buffer, int offset, int count)
        {
            //_Position += count - offset;
            //return BaseStream.Read(buffer, offset, count);
            
            int bytesRead = 0;
            while (bytesRead < count)
            {
                int readAheadAvailableBytes = readAheadLength - readAheadOffset;
                int bytesRequired = count - bytesRead;
                if (readAheadAvailableBytes > 0)
                {
                    int toCopy = Math.Min(readAheadAvailableBytes, bytesRequired);
                    Array.Copy(readAheadBuffer, readAheadOffset, buffer, offset + bytesRead, toCopy);
                    bytesRead += toCopy;
                    readAheadOffset += toCopy;
                }
                else
                {
                    readAheadOffset = 0;
                    readAheadLength = BaseStream.Read(readAheadBuffer, 0, readAheadBuffer.Length);
                    if (readAheadLength == 0) break;
                }
            }
            _Position += bytesRead;
            return bytesRead;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            long offcalc = 0;
            if (origin == SeekOrigin.Begin) offcalc = offset;
            else offcalc = Position + offset;

            if (Position == offcalc) return 0;
            else Position = offcalc;
            return offcalc;
        }

        public override int ReadByte()
        {
            return base.ReadByte();
        }

        public override void SetLength(long value) { throw new NotSupportedException(); }

        public override void Write(byte[] buffer, int offset, int count) { BaseStream.Write(buffer, offset, count); }

        public new int ReadTimeout { get { return BaseStream.ReadTimeout; } set { base.ReadTimeout = value; } }
        public new int WriteTimeout { get { return BaseStream.WriteTimeout; } set { base.WriteTimeout = value; } }

        public override void Close() { if (BaseStream != null) try { BaseStream.Close(); } catch { } }

        public new void Dispose()
        {
            if (BaseStream != null) try { BaseStream.Close(); BaseStream = null; } catch { }
            if (res != null) try { res.Close(); } catch { }
            if (req != null) try { req.Abort(); } catch { }
            if (tc != null) try { tc.Close(); } catch { }
        }
    }
#endif
}