﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS.Log
{
    public abstract class Log
    {
        public LogLevel Level { get; protected set; }
        public string Message { get; protected set; }
        public string Source { get; protected set; }
        public DateTime Timestamp { get; protected set; }
        internal Log(string Message, string Source, LogLevel Level)
        {
            this.Message = Message;
            this.Source = Source;
            this.Level = Level;
            //this.Site = Site;
            Timestamp = DateTime.Now;
        }
        //Example: [15:11:05.34] : [Engine][Info] (Sound) - The fatal error has occured!!
        public override string ToString()
        {
            return string.Format("[{0:00}:{1:00}:{2:00}.{3}] : [{4}] ({5}) - {6}",
                Timestamp.Hour, Timestamp.Minute, Timestamp.Second, Timestamp.Millisecond,
                Level.ToString(), Source, Message);
        }
    }
}
