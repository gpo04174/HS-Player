﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Lib.Windows.NAudio.Dmo
{
    [Flags]
    enum DmoSetTypeFlags
    {
        None,
        DMO_SET_TYPEF_TEST_ONLY = 0x00000001,
        DMO_SET_TYPEF_CLEAR = 0x00000002
    }
}

#endif