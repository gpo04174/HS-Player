﻿/********************************************
간단한 재생은 

라고 하시면 됩니다.

HSAudio Made by 박홍식
Copyright 2012 ~ 2018 ⓒ 박홍식 All RIght Reserved
**********************************************/

using HS.Setting;
using HSAudio.Core;
using HSAudio.Core.Audio;
using HSAudio.DSP;
using HSAudio.Lib.FMOD;
using HSAudio.Output;
using System;
using System.Diagnostics;
using System.Threading;

namespace HSAudio
{
    /// <summary>
    /// Realtime update clock. when before system update call
    /// </summary>
    /// <param name="sender">HSAudio</param>
    /// <returns>True is update</returns>
    public delegate void ClockUpdatedEventHandler(object sender);

    public partial class HSAudio : IDisposable
    {
        public static bool IsEmpty(HSAudio Audio) { return Audio == null || Audio.system == null; }

        public FD_System system;
        internal FD_System system_sub;
        internal ReverbManager _Reverb;
        Thread UpdateThread;
        private SYSTEM_CALLBACK system_callback;

        public HSAudio(bool AutoUpdate = true)
        {
            UpdateClock = AutoUpdate;
            Init();
            //Output.Open(new AutoOutput());
        }
        public HSAudio(Settings Setting)
        {
            Init();
            LoadSetting(Setting);
        }
        public HSAudio(OutputMethod Method)
        {
            Init();
            Output.Open(Method, null, OutputResampler.Cubic, false);
        }
        public HSAudio(OutputMethod Method, OutputResampler Resampler)
        {
            Init();
            Output.Open(Method, null, Resampler, false);
        }
        public HSAudio(OutputMethod Method, OutputFormat Format, OutputResampler Resampler)
        {
            Init();
            Output.Open(Method, Format, Resampler, false);
        }
        public HSAudio(OutputMethod Method, OutputFormat Format)
        {
            Init();
            Output.Open(Method, Format, Output.Resampler, false);
        }
        internal HSAudio(FD_System system, FD_System system_sub)
        {
            this.system = system;
            this.system_sub = system_sub;
            Init(false);
        }
        void Init(bool CreateSystem = true)
        {
            if (CreateSystem)
            {
                Factory.System_Create(out system);
                Factory.System_Create(out system_sub);
                
                system_callback = new SYSTEM_CALLBACK(SYSTEM_CALLBACK);
                system.setCallback(system_callback, SYSTEM_CALLBACK_TYPE.ALL);
                UpdateClockMS = 10; //~ 100 FPS
                try { Reverb = new ReverbManager(this); }catch { }

                UpdateThread = new Thread(UpdateLoop);
                if (UpdateClock) UpdateThread.Start();
            }
            
            Output = new Output.Output(this);
            Output.OutputOpened += Output_OutputOpened;
            Output.OutputClosing += Output_OutputClosing;
            //_ChannelGroup = new FD_ChannelGroup();
        }

        #region Event
        public event ClockUpdatedEventHandler ClockUpdated;

        private bool Output_OutputOpened(Output.Output sender, bool Opened)
        {
            if (Opened)
            {
                //system.getMasterChannelGroup(out _ChannelGroup);
                if(Audio == null) Audio = new AudioCollection(this);
                if(DSP == null) DSP = new DSPCollection(Audio.cg);
                Reverb.Reload();

                __UpdateClock = true;
            }
            return false;
        }
        bool __UpdateClock;
        private void Output_OutputClosing(Output.Output sender)
        {
            //UpdateClock = __UpdateClock;
            __UpdateClock = false;
        }

        #endregion

        #region Properties
        #region Private
        FD_ChannelGroup _ChannelGroup;
        #endregion
        #region Public
        public AudioCollection Audio { get; private set; }
        public Output.Output Output { get; private set; }
        public DSPCollection DSP { get; private set; }

        public ReverbManager Reverb { get; private set; }

        /// <summary>
        /// Bit perfect option.
        /// </summary>
        public bool OutputSameSound { get; set; }
        /// <summary>
        /// Do not modified <para/>
        /// 변경하지 마세요
        /// </summary>
        public int UpdateClockMS { get; set; }

        internal RESULT Result { get; set; }

        bool _UpdateClock = true;
        public bool UpdateClock { get { return _UpdateClock; } set { lock (LOCK_Update) _UpdateClock = value; } }

        //Buffer size to stream. (NetworkStream, StreamSound or Stream mode SoundFile, SoundCustom)
        /// <summary>
        /// Buffer size to stream. (NetworkSound, StreamSound or SoundFile is stream mode) <para/>
        /// 스트림의 버퍼크기 입니다. (NetworkSound, StreamSound 또는 SoundFile 이 스트림 모드일때)
        /// </summary>
        public uint BufferSizeStream { get { TimeUnit a; return GetBufferSizeStream(out a); } set { SetBufferSizeStream(value); } }

        //public AudioState State;
        #endregion

        #endregion

        #region Function
        #region Public

        public uint GetBufferSizeStream(out TimeUnit Unit)
        {
            uint time; TIMEUNIT u;
            Result = system.getStreamBufferSize(out time, out u);
            Unit = (TimeUnit)u;
            return time;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Size"></param>
        /// <param name="Unit">Must be RawBytes</param>
        /// <returns></returns>
        public bool SetBufferSizeStream(uint Size, TimeUnit Unit = TimeUnit.RawBytes)
        {
            uint time; TIMEUNIT u;
            Result = system.setStreamBufferSize(Size, (TIMEUNIT)Unit);
            return Result == RESULT.OK;
        }

        public void Update()
        {
            RESULT Result = RESULT.OK;
            if (ClockUpdated != null && ClockUpdated.Target != null) try { ClockUpdated.Invoke(this); } catch { }
            try { if (system != null) { lock (LOCK_Update) Result = system.update(); } }
#if NETSTANDARD
            catch (Exception vio) { Error++; Console.Write("EXCEPTION!!"); Debug.WriteLine("EXCEPTION!!"); Thread.Sleep(100); }
#else
            catch (AccessViolationException vio) { Error++; Console.WriteLine("VIOLATION EXCEPTION!!"); Debug.WriteLine("VIOLATION EXCEPTION!!"); Thread.Sleep(100); }
#endif
        }
        /*
        public void setReverb3D(Reverb3D Reverb3D)
        {
            if (reverb3d != null) reverb3d.Dispose();

            if (Reverb3D == null) reverb3d = null;
            else
            {
                reverb3d = Reverb3D;
                Reverb3D.Audio = this;
                Result = system.createReverb3D(out Reverb3D.reverb3d);
                if (Result != RESULT.OK) throw new HSAudioException(Result);
                else Reverb3D.Registered = true;
            }
        }
        */
        

        public float[][] GetMatrix()
        {
            float[] matrix;
            int inch, outch;
            if(Audio != null && Audio.cg != null)
            {
                Result = Audio.cg.getMixMatrix(out matrix, out inch, out outch, 0);
            }
            else
            {
                FD_ChannelGroup cg;
                Result = system.getMasterChannelGroup(out cg);
                Result = cg.getMixMatrix(out matrix, out inch, out outch, 0);
            }
            return null;
        }
        #endregion
        #region Internal
        #endregion
        #region Private

        int Error = 0;
        string LOCK_Update = "LOCK_Update";
        void UpdateLoop()
        {
            while (true)
            {
                if (UpdateClock && __UpdateClock) Update();
                Thread.Sleep(Math.Max(UpdateClockMS, 1));
            }
        }
        #endregion
        #endregion

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("UpdateClock", UpdateClock);
            s.SetValue("UpdateClockMS", UpdateClockMS);
            s.SetValue("BufferSizeStream", BufferSizeStream);
            s.SetValue("OutputSameSound", OutputSameSound);
            s.SubSetting.SetValue("Output", Output.SaveSetting());
            s.SubSetting.SetValue("Reverb", Reverb.SaveSetting());
            s.SubSetting.SetValue("Audio", Audio.SaveSetting());
            return s;
        }


        internal bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new NotImplementedException();
        }


        internal static bool ThrowOrNot(RESULT Result)
        {
            return Result == RESULT.OK;
        }

        public void Dispose()
        {
            if (Output != null) try { Output.Dispose(); } catch { }
            if (DSP != null) try { DSP.Dispose(); } catch { }
            if (Reverb != null) try { Reverb.Dispose(); } catch { }
            if (Audio != null) try { Audio.Dispose(); } catch { }
            Reverb = null;
            system.release();
        }
    }
}
