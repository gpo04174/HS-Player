﻿using System;
using System.Windows.Forms;
using HSAudio.Output;
using HSAudio.Output.Devices;
using HSAudio.Wave.Device;
using HSAudio.Speakers;

namespace HSAudioTest.Test.GUI
{
    public partial class DeviceControl : UserControl
    {
        #region Designer
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.tbDeviceVolume = new System.Windows.Forms.TrackBar();
            this.label3 = new System.Windows.Forms.Label();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.cbDeviceKind = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbDeviceList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chkVolumeMute = new System.Windows.Forms.CheckBox();
            this.pnlVolume = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.numSample = new System.Windows.Forms.NumericUpDown();
            this.cbSpeaker = new System.Windows.Forms.ComboBox();
            this.btnFormatApply = new System.Windows.Forms.Button();
            this.chkSpeaker512 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cbResample = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.tbDeviceVolume)).BeginInit();
            this.pnlVolume.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSample)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(337, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 25;
            this.label4.Text = "100";
            // 
            // tbDeviceVolume
            // 
            this.tbDeviceVolume.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbDeviceVolume.AutoSize = false;
            this.tbDeviceVolume.Enabled = false;
            this.tbDeviceVolume.Location = new System.Drawing.Point(0, 4);
            this.tbDeviceVolume.Maximum = 100;
            this.tbDeviceVolume.Name = "tbDeviceVolume";
            this.tbDeviceVolume.Size = new System.Drawing.Size(333, 21);
            this.tbDeviceVolume.TabIndex = 24;
            this.tbDeviceVolume.Scroll += new System.EventHandler(this.tbDeviceVolume_Scroll);
            this.tbDeviceVolume.ValueChanged += new System.EventHandler(this.tbDeviceVolume_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 12);
            this.label3.TabIndex = 23;
            this.label3.Text = "Status:   OK";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(394, 30);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(65, 20);
            this.btnRefresh.TabIndex = 22;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // cbDeviceKind
            // 
            this.cbDeviceKind.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceKind.FormattingEnabled = true;
            this.cbDeviceKind.Items.AddRange(new object[] {
            "AUTO (Auto select)",
            "DSOUND (DirectSound)",
            "WINMM (Windows Multimedia)",
            "WASAPI (Windows Audio Session API) [Windows Vista]",
            "ASIO (Low latency ASIO 2.0)",
            "SONIC (Windows Sonic) [Windows 10 1703]",
            "ATMOS (Dolby Atmos) [Windows 10 1703]",
            "NOSOUND (No Sound)"});
            this.cbDeviceKind.Location = new System.Drawing.Point(60, 4);
            this.cbDeviceKind.Name = "cbDeviceKind";
            this.cbDeviceKind.Size = new System.Drawing.Size(333, 20);
            this.cbDeviceKind.TabIndex = 21;
            this.cbDeviceKind.SelectedIndexChanged += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "Kind";
            // 
            // cbDeviceList
            // 
            this.cbDeviceList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbDeviceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceList.FormattingEnabled = true;
            this.cbDeviceList.Location = new System.Drawing.Point(60, 30);
            this.cbDeviceList.Name = "cbDeviceList";
            this.cbDeviceList.Size = new System.Drawing.Size(333, 20);
            this.cbDeviceList.TabIndex = 19;
            this.cbDeviceList.SelectedIndexChanged += new System.EventHandler(this.cbDeviceList_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "Devices";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 12);
            this.label5.TabIndex = 26;
            this.label5.Text = "Volume";
            // 
            // chkVolumeMute
            // 
            this.chkVolumeMute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkVolumeMute.AutoSize = true;
            this.chkVolumeMute.Location = new System.Drawing.Point(364, 7);
            this.chkVolumeMute.Name = "chkVolumeMute";
            this.chkVolumeMute.Size = new System.Drawing.Size(35, 16);
            this.chkVolumeMute.TabIndex = 27;
            this.chkVolumeMute.Text = "M";
            this.chkVolumeMute.UseVisualStyleBackColor = true;
            this.chkVolumeMute.Click += new System.EventHandler(this.chkVolumeMute_Click);
            // 
            // pnlVolume
            // 
            this.pnlVolume.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlVolume.Controls.Add(this.tbDeviceVolume);
            this.pnlVolume.Controls.Add(this.chkVolumeMute);
            this.pnlVolume.Controls.Add(this.label4);
            this.pnlVolume.Location = new System.Drawing.Point(60, 55);
            this.pnlVolume.Name = "pnlVolume";
            this.pnlVolume.Size = new System.Drawing.Size(401, 28);
            this.pnlVolume.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 12);
            this.label6.TabIndex = 29;
            this.label6.Text = "Sample";
            // 
            // numSample
            // 
            this.numSample.Location = new System.Drawing.Point(60, 87);
            this.numSample.Maximum = new decimal(new int[] {
            192000,
            0,
            0,
            0});
            this.numSample.Minimum = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.numSample.Name = "numSample";
            this.numSample.Size = new System.Drawing.Size(66, 21);
            this.numSample.TabIndex = 30;
            this.numSample.ThousandsSeparator = true;
            this.numSample.Value = new decimal(new int[] {
            44100,
            0,
            0,
            0});
            this.numSample.ValueChanged += new System.EventHandler(this.btnFormatApply_Click);
            // 
            // cbSpeaker
            // 
            this.cbSpeaker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSpeaker.FormattingEnabled = true;
            this.cbSpeaker.Items.AddRange(new object[] {
            "Auto",
            "Mono",
            "Stereo",
            "Quad",
            "Surrond",
            "5.1",
            "7.1",
            "7.1.4"});
            this.cbSpeaker.Location = new System.Drawing.Point(377, 87);
            this.cbSpeaker.Name = "cbSpeaker";
            this.cbSpeaker.Size = new System.Drawing.Size(82, 20);
            this.cbSpeaker.TabIndex = 32;
            this.cbSpeaker.SelectedIndexChanged += new System.EventHandler(this.btnFormatApply_Click);
            // 
            // btnFormatApply
            // 
            this.btnFormatApply.Location = new System.Drawing.Point(393, 87);
            this.btnFormatApply.Name = "btnFormatApply";
            this.btnFormatApply.Size = new System.Drawing.Size(65, 21);
            this.btnFormatApply.TabIndex = 33;
            this.btnFormatApply.Text = "Apply";
            this.btnFormatApply.UseVisualStyleBackColor = true;
            this.btnFormatApply.Visible = false;
            this.btnFormatApply.Click += new System.EventHandler(this.btnFormatApply_Click);
            // 
            // chkSpeaker512
            // 
            this.chkSpeaker512.AutoSize = true;
            this.chkSpeaker512.Location = new System.Drawing.Point(281, 113);
            this.chkSpeaker512.Name = "chkSpeaker512";
            this.chkSpeaker512.Size = new System.Drawing.Size(100, 16);
            this.chkSpeaker512.TabIndex = 34;
            this.chkSpeaker512.Text = "Dolby 5.1 to 2";
            this.chkSpeaker512.UseVisualStyleBackColor = true;
            this.chkSpeaker512.CheckedChanged += new System.EventHandler(this.btnFormatApply_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(281, 86);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "Matrix (Spk.)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(131, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 12);
            this.label8.TabIndex = 36;
            this.label8.Text = "Sampler";
            // 
            // cbResample
            // 
            this.cbResample.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbResample.FormattingEnabled = true;
            this.cbResample.Items.AddRange(new object[] {
            "(No)",
            "Linear",
            "Cubic",
            "Spline"});
            this.cbResample.Location = new System.Drawing.Point(186, 87);
            this.cbResample.Name = "cbResample";
            this.cbResample.Size = new System.Drawing.Size(92, 20);
            this.cbResample.TabIndex = 37;
            this.cbResample.SelectedIndexChanged += new System.EventHandler(this.btnFormatApply_Click);
            // 
            // DeviceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.cbResample);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.chkSpeaker512);
            this.Controls.Add(this.cbSpeaker);
            this.Controls.Add(this.numSample);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pnlVolume);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.cbDeviceKind);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbDeviceList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFormatApply);
            this.Name = "DeviceControl";
            this.Size = new System.Drawing.Size(461, 137);
            this.Load += new System.EventHandler(this.DeviceControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tbDeviceVolume)).EndInit();
            this.pnlVolume.ResumeLayout(false);
            this.pnlVolume.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSample)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TrackBar tbDeviceVolume;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ComboBox cbDeviceKind;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbDeviceList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkVolumeMute;
        private System.Windows.Forms.Panel pnlVolume;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numSample;
        private System.Windows.Forms.ComboBox cbSpeaker;
        private System.Windows.Forms.Button btnFormatApply;
        private System.Windows.Forms.CheckBox chkSpeaker512;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbResample;
        #endregion

        public DeviceControl()
        {
            InitializeComponent();
            cbSpeaker.SelectedIndex = 0;
            cbResample.SelectedIndex = 2;
        }

        HSAudio.HSAudio _Audio;
        public HSAudio.HSAudio Audio { get { return _Audio; } set { _Audio = value; cbDeviceKind.SelectedIndex = 0; } }

        OutputMethod[] methods = null;
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            if(method != null && method.Device != null)method.Device.DevicePropertyChanged -= Device_DevicePropertyChanged;
            cbDeviceList.Items.Clear();
            switch (cbDeviceKind.SelectedIndex)
            {
                case 0: methods = new OutputMethod[]{ new AutoOutput() }; break;
                case 1: methods = DirectOutput.GetOutputs(); break;
                case 2: methods = WINMMOutput.GetOutputs(); break;
                case 3: methods = WASAPIOutput.GetOutputs(); break;
                case 4: methods = ASIOOutput.GetOutputs(); break;
                case 5: methods = SonicOutput.GetOutputs(); break;
                case 6: methods = AtmosOutput.GetOutputs(); break;
                default: methods = new OutputMethod[] { new NoOutput() }; break;
            }
            int index = 0;
            for (int i = 0; i < methods.Length; i++)
            {
                cbDeviceList.Items.Add(methods[i].ToString());
                if (method != null && method.Name.IndexOf(methods[i].Name) >= 0) index = i;
            }

            if (cbDeviceList.Items.Count == 0) 
            {
                cbDeviceList.Items.Add("(No sound devices)"); 
                method = null;
                pnlVolume.Enabled = false;
            }
            else
            {
                pnlVolume.Enabled = true;
                cbDeviceList.SelectedIndex = index;
            }
        }

        OutputMethod method;
        private void cbDeviceList_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Audio != null)
            {
                Audio.Output.Close();
                if (cbDeviceList.SelectedIndex < 0) return;
                try
                {
                    if (method != null && method.Device != null) method.Device.DevicePropertyChanged -= Device_DevicePropertyChanged;
                    method = this.methods[cbDeviceList.SelectedIndex];//cbDeviceList.Items[cbDeviceList.SelectedIndex] as OutputMethod;
                    if (method.Device != null) method.Device.DevicePropertyChanged += Device_DevicePropertyChanged;
                    Audio.Output.Open(method, format);
                    label3.Text = Audio.Output.Status == OutputStatus.Open ? "OK" : Audio.Output.Status.ToString();

                    pnlVolume.Enabled = !(method.Device == null || method.Device.MasterControl == null);
                    if (pnlVolume.Enabled)
                    {
                        tbDeviceVolume.Value = (int)(method.Device.MasterControl.VolumeScalar * 100);
                        chkVolumeMute.Checked = method.Device.MasterControl.Mute;
                    }
                }
                catch (Exception ex) { label3.Text = ex.Message; }
            }
        }

        private void tbDeviceVolume_Scroll(object sender, EventArgs e)
        {
            method.Device.MasterControl.VolumeScalar = tbDeviceVolume.Value / 100f;
        }

        private void tbDeviceVolume_ValueChanged(object sender, EventArgs e)
        {
            label4.Text = tbDeviceVolume.Value.ToString();
        }

        private void DeviceControl_Load(object sender, EventArgs e)
        {
            
        }

        private void Device_DevicePropertyChanged(object sender, WaveDevicePropertyChangedEventArgs e)
        {
            tbDeviceVolume.Value = (int)(e.Control.VolumeScalar * 100);
            chkVolumeMute.Checked = e.Control.Mute;
        }

        private void chkVolumeMute_Click(object sender, EventArgs e) { chkVolumeMute.Checked = !chkVolumeMute.Checked; }

        OutputFormat format;
        private void btnFormatApply_Click(object sender, EventArgs e)
        {
            if (Audio != null && Audio.Output != null)
            {
                SpeakerMode mode = SpeakerMode.Default;
                OutputResampler sampler = OutputResampler.Cubic;
                switch (cbSpeaker.SelectedIndex)
                {
                    case 1: mode = SpeakerMode.Mono; break;
                    case 2: mode = SpeakerMode.Stereo; break;
                    case 3: mode = SpeakerMode.Quad; break;
                    case 4: mode = SpeakerMode.Surround; break;
                    case 5: mode = SpeakerMode.System5_1; break;
                    case 6: mode = SpeakerMode.System7_1; break;
                    case 7: mode = SpeakerMode.System7_1_4; break;
                }
                switch (cbResample.SelectedIndex)
                {
                    case 0: sampler = OutputResampler.Not; break;
                    case 1: sampler = OutputResampler.Linear; break;
                    case 2: sampler = OutputResampler.Cubic; break;
                    case 3: sampler = OutputResampler.Spline; break;
                }
                format = new OutputFormat((int)numSample.Value, mode);
                Audio.Output.DownMix5_1to2 = chkSpeaker512.Checked;
                Audio.Output.Close();
                Audio.Output.Open(method, format, sampler);
                Audio.Output.Speaker.DefaultFromMode(mode);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmSpeaker spk = new frmSpeaker(Audio.Output);
            spk.Show();
        }
    }
}
