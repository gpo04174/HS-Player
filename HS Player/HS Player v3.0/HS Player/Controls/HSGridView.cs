﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using HSAudioControlDesktop.Control.Metro;

namespace HSAudioControlDesktop.Control
{
    /// <summary>
    /// Interaction logic for HSGridView.xaml
    /// Author by 박홍식 (gpo04174@naver.com)
    /// </summary>
    public class HSGridView : HSItemView
    {
        protected override void UpdateGrid(Size size)
        {
            if (!StopUpdate && size.Width > 0 && size.Height > 0)
            {
                Size colrow = new Size(ActualColumns, ActualRows);
                int col = 1, row = 1;
                if ((Rows == 0 || Columns == 0))
                {
                    int row1 = 0, col1 = 0;
                    double W = 0;
                    double H = 0;
                    double MarginW = 5, MarginH = 5;
                    for (int i = 0; i < RootGrid.Children.Count; i++)
                    {
                        HSCollectionViewItem view = RootGrid.Children[i] as HSCollectionViewItem;
                        if (Orientation == Orientation.Horizontal)
                        {
                            view.Margin = new Thickness(0, MarginH, 0, MarginH);
                            //view.Height = view.ActualHeight + (MarginH * 2);
                            //view.HorizontalAlignment = HorizontalAlignment;
                            //view.VerticalAlignment = view.VerticalAlignment;
                            double view_W = view.ActualWidth;
                            if (W + view_W >= size.Width)
                            {
                                //MarginW = Math.Floor((size.Width - W) / (col1 * 2));
                                if (col1 > col) col = col1;
                                W = col1 = 0;
                                row++;
                            }
                            else { W += view_W; col1++; }
                        }
                        else
                        {
                            view.Margin = new Thickness(MarginW, 0, MarginW, 0);
                            double view_H = view.ActualHeight;
                            if (W + view_H > size.Height)
                            {
                                //MarginH = size.Height - H;
                                if (row1 > row) row = row1;
                                H = row1 = 0;
                                col++;
                            }
                            else { H += view_H; row1++; }
                        }
                    }
                }
                else { col = Columns; row = Rows; }
                ActualColumns = Orientation == Orientation.Horizontal ? col : Math.Max(col, (int)Math.Ceiling(grid.Children.Count / (double)row));
                ActualRows = Orientation == Orientation.Vertical ? row : Math.Max(row, (int)Math.Ceiling(grid.Children.Count / (double)col));
                //SetColRow(colrow, new Size(ActualColumns, ActualRows));
                if (Orientation == Orientation.Horizontal) SetColRow(new Size(ActualColumns, ActualRows), AutoArrange ? GridUnitType.Star : GridUnitType.Auto);
                else SetColRow(new Size(ActualColumns, ActualRows), GridUnitType.Star, AutoArrange ? GridUnitType.Star : GridUnitType.Auto);
                UpdateGrided = true;
            }
        }
    }
}
