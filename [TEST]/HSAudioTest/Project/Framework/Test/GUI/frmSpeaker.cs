﻿using HSAudio.Core.Audio;
using HSAudio.Output;
using System;
using System.Windows.Forms;

namespace HSAudioTest.Test.GUI
{
    public partial class frmSpeaker : Form
    {
        #region Designer
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // frmSpeaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(805, 584);
            this.Name = "frmSpeaker";
            this.Text = "frmSpeaker";
            this.Load += new System.EventHandler(this.frmSpeaker_Load);
            this.ResumeLayout(false);

        }

        #endregion
        #endregion

        SpeakerControl control;
        public Audio audio { get { return control.Audio; } set { control.Audio = value; } }
        public Output output { get { return control.Output; } set { control.Output = value; } }
        public frmSpeaker(Audio audio)
        {
            InitializeComponent();
            control = new SpeakerControl();
            this.audio = audio;
            control.Size = this.Size;
            //control.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.Controls.Add(control);
            control.setMatrix(audio.Matrix);
        }
        public frmSpeaker(Output output)
        {
            InitializeComponent();
            control = new SpeakerControl();
            this.output = output;
            control.Size = this.Size;
            //control.Anchor = AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Left | AnchorStyles.Bottom;
            this.Controls.Add(control);
            control.setMatrix(output.Matrix);
        }

        private void frmSpeaker_Load(object sender, EventArgs e)
        {

        }
    }
}
