﻿using System.Windows.Forms;
using HSAudio.Core.Audio;
using HSAudio.Output;
using System.Collections.Generic;

namespace HSAudioTest.Test.GUI
{
    public partial class SpeakerControl : UserControl
    {
        #region Designer        
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoSize = true;
            this.panel1.Location = new System.Drawing.Point(34, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(625, 385);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "In →";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 12);
            this.label2.TabIndex = 0;
            this.label2.Text = "Out↓";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.UseMnemonic = false;
            // 
            // SpeakerControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "SpeakerControl";
            this.Size = new System.Drawing.Size(659, 415);
            this.Load += new System.EventHandler(this.SpeakerControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        #endregion

        SpeakerControlSwitch[][] sw = new SpeakerControlSwitch[12][];
        public SpeakerControl()
        {
            InitializeComponent();
            
            int SIzeX = 0, SizeY = 0;
            int LocX = 0, LocY = 0;
            for (int i = 0; i < 12; i++)
            {
                sw[i] = new SpeakerControlSwitch[12];
                for (int o = 0; o < 12; o++)
                {
                    sw[i][o] = new SpeakerControlSwitch() { Coord = new Coord2D(i, o) };
                    /*
                    if (i == 0)
                    {
                        Label lbl = new Label() { Text = i.ToString() };
                        lbl.Location = new System.Drawing.Point((sw.Size.Width / 2) - (lbl.Width / 2), 0);
                        panel1.Controls.Add(lbl);
                    }
                    */
                    if (i == 0) sw[i][o].Text = (o + 1).ToString();
                    else if (o == 0) sw[i][o].Text = (i + 1).ToString();
                    else sw[i][o].Text = "";

                    sw[i][o].Location = new System.Drawing.Point(LocX, LocY);
                    LocX += sw[i][o].Size.Width;
                    sw[i][o].ValueChange += SpeakerControl_ValueChange;
                    panel1.Controls.Add(sw[i][o]);
                }
                LocY += sw[0][i].Size.Height;
                LocX = 0;
            }
            this.Size = new System.Drawing.Size(panel1.Location.X + panel1.Width, panel1.Location.Y + panel1.Height);
        }

        Audio _Audio;
        Output _Output;
        public Audio Audio
        {
            get { return _Audio; }
            set
            {
                if (_Audio != null) _Audio.SoundChanged -= Audio_SoundChanged;
                value.SoundChanged += Audio_SoundChanged;
                _Audio = value;
                setMatrix(Audio.Matrix);
            }
        }

        public Output Output
        {
            get { return _Output; }
            set
            {
                if (_Output != null) _Output.OutputOpened -= value_OutputOpened;
                value.OutputOpened += value_OutputOpened;
                _Output = value;
                setMatrix(Output.Matrix);
            }
        }

        public float[][] Matrix
        {
            get
            {
                List<float[]> list = new List<float[]>();
                for (int i = 0; i < 12; i++)
                {
                    List<float> list1 = new List<float>();
                    for (int o = 0; o < 12; o++)
                        if (sw[i][o].Enabled) list1.Add(sw[i][o].Value);
                    if(list1.Count > 0) list.Add(list1.ToArray());
                }
                return list.ToArray();
            }
        }
        bool IgnoreEvent = true;
        public void setMatrix(float[][] Matrix)
        {
            IgnoreEvent = true;
            label1.Text = string.Format("In → ({0})", Matrix.Length);
            label2.Text = string.Format("Out↓\n({0})", Matrix[0].Length);
            for (int i = 0; i < 12; i++)
            {
                for(int o = 0; o < 12; o++)
                {
                    if (i < Matrix.Length && o < Matrix[i].Length)
                    {
                        sw[i][o].Enabled = true;
                        sw[i][o].Value = Matrix[i][o];
                    }
                    else sw[i][o].Enabled = false;
                }
            }
            IgnoreEvent = false;
        }

        private void SpeakerControl_Load(object sender, System.EventArgs e)
        {

        }

        bool value_OutputOpened(Output sender, bool Opened)
        {
            if (Opened)
                setMatrix(sender.Matrix);
            return true;
        }

        private void SpeakerControl_ValueChange(object sender)
        {
            if (!IgnoreEvent)
            {
                if (Audio != null) Audio.Matrix = Matrix;
                else if (Output != null) Output.Matrix = Matrix;
            }
        }

        private void Audio_SoundChanged(object sender, HSAudio.Core.Sound.Sound sound)
        {
            setMatrix(((Audio)sender).Matrix);
        }
    }
}
