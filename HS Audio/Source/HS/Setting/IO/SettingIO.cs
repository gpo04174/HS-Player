﻿namespace HS.Setting.IO
{
    public abstract partial class SettingIO<T>
    {
        public abstract Settings Load(T Data);
        public abstract T Save(Settings Data);
    }
}
