﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using HS;

namespace HSAudio.DSP
{
    /// <summary>
    /// 파라미터 값
    /// </summary>
#if !SIMPLE
    [Serializable]
    public class Parameter : ISettingManager
    {
        private DSP_PARAMETER_DESC desc;
        internal Parameter(Settings Setting) { LoadSetting(Setting); }

        /// <summary>
        /// DSP구조체의 인스턴스를 초기화합니다.
        /// </summary>
        /// <param name="Index">DSP_TYPE에 대한 각 값의 인덱스 입니다. (만약 DSP_TYPE 를 PARAMEQ 로 줬다면 60HZ의 설정은 (DSP_PARAMEQ.BANDWIDTH, 60) 형태로 입력하면 됩니다.)</param>
        /// <param name="Value">인덱스에 대한 값 입니다.</param>=
        //internal Parameter(float Value, string Name, string Label, string Description) { this.Type = Type; this.Value_Float = Value; }
        //internal Parameter(int Value, string Name, string Label, string Description) { this.Type = Type; this.Value_Int = Value; }
        //internal Parameter(bool Value, string Name, string Label, string Description) { this.Type = Type; this.Value_Bool = Value; }
        //internal Parameter(IntPtr Value, uint Length, string Name, string Label, string Description) { this.Type = Type; this.Value_Data = Value; Value_Data_Length = Length; }
        internal Parameter(float Value, DSP_PARAMETER_DESC desc) {Init(desc); this.Value = new ParameterValue(Value); Default = new ParameterDefault((DSPParameterType)desc.type, desc.desc); }
        internal Parameter(int Value, DSP_PARAMETER_DESC desc) { Init(desc); this.Value = new ParameterValue(Value); Default = new ParameterDefault((DSPParameterType)desc.type, desc.desc);  }
        internal Parameter(bool Value, DSP_PARAMETER_DESC desc) { Init(desc); this.Value = new ParameterValue(Value); Default = new ParameterDefault((DSPParameterType)desc.type, desc.desc); }
        internal Parameter(IntPtr Value, uint Length, DSP_PARAMETER_DESC desc) { Init(desc); this.Value = new ParameterValue(new DataValue(Value, Length, (DataType)desc.desc.datadesc.datatype)); Default = new ParameterDefault((DSPParameterType)desc.type, desc.desc); }

        void Init(DSP_PARAMETER_DESC desc)
        {
            this.desc = desc;
            Name = new string(desc.name);
            Label = new string(desc.label);
            Description = desc.description;
        }

        public ParameterValue Value { get; set; }
        public ParameterDefault Default { get; private set; }
        
        public string Name { get; private set; }
        public string Label { get; private set; }
        public string Description { get; private set; }

        

        internal DSP_PARAMETER_DESC Desc
        {
            get { return desc; }
        }

        #region Setter
        public Parameter SetValue(float Value) { this.Value.Value_Float = Value; return this; }
        public Parameter SetValue(int Value) { this.Value.Value_Int = Value; return this; }
        public Parameter SetValue(bool Value) { this.Value.Value_Bool = Value; return this; }
        public Parameter SetValue(DataValue Data) { Value.Value_Data = Data; return this; }
        public Parameter SetValue(IntPtr Value, uint Length, DataType Type) { this.Value.Value_Data = new DataValue(Value, Length, Type); return this; }
        #endregion

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SetValue("Label", Label);
            s.SetValue("Description", Description);
            s.SubSetting.SetValue("Value", Value.SaveSetting());
            s.SubSetting.SetValue("Default", Default.SaveSetting());
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Name")) Name = Setting["Name"];
            if (Setting.Exist("Label")) Name = Setting["Label"];
            if (Setting.Exist("Description")) Name = Setting["Description"];
            if (Setting.SubSetting.Exist("Value")) Value = new ParameterValue(Setting.SubSetting["Value"]);
            if (Setting.SubSetting.Exist("Default")) Default = new ParameterDefault(Setting.SubSetting["Default"]);
            return true;
        }
    }
#else
    [Serializable]
    public class Parameter : ISettingManager
    {
        internal Parameter(Settings Setting) { LoadSetting(Setting); }
        /// <summary>
        /// DSP구조체의 인스턴스를 초기화합니다.
        /// </summary>
        /// <param name="Index">DSP_TYPE에 대한 각 값의 인덱스 입니다. (만약 DSP_TYPE 를 PARAMEQ 로 줬다면 60HZ의 설정은 (DSP_PARAMEQ.BANDWIDTH, 60) 형태로 입력하면 됩니다.)</param>
        /// <param name="Value">인덱스에 대한 값 입니다.</param>
        public Parameter(DSPParameterType Type, int Index, object Value) { this.Type = Type; this.Index = Index; this.Value = Value; }
        public Parameter(DSPParameterType Type, int Index, object Value, string Name, string Label, string Description) { this.Type = Type; this.Index = Index; this.Value = Value; }
        internal Parameter(int Index, DSP_PARAMETER_DESC desc) { this.Index = Index; Name = new string(desc.name); Label = new string(desc.label); Description = desc.description; Type = (DSPParameterType)desc.type; }
        /// <summary>
        /// DSP_TYPE에 대한 각 값의 인덱스 입니다.
        /// </summary>
        public int Index;
        /// <summary>
        /// 인덱스에 대한 값 입니다.
        /// </summary>
        public object Value;

        public DSPParameterType Type { get; internal set; }
        public string Name { get; internal set; }
        public string Label { get; internal set; }
        public string Description { get; internal set; }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Index", new SettingsData(Index.ToString()));
            s.SetValue("Type", new SettingsData(Type.ToString()));
            s.SetValue("Name", new SettingsData(Name, false));
            s.SetValue("Label", new SettingsData(Label, false));
            s.SetValue("Description", new SettingsData(Description, false));
            s.SetValue("Value", new SettingsData(Forge(Type, Value), false));
            return s;
        }

        public bool LoadSetting(Setting.Settings Setting, bool ThrowException = false)
        {
            DSPParameterType t = DSPParameterType.Data;
            if (Setting.Exist("Index")) Index = Convert.ToInt32(Setting["Index"].Value);
            if (Setting.Exist("Type")) t = (DSPParameterType)Enum.Parse(typeof(DSPParameterType), Setting["Type"]);
            if (Setting.Exist("Name")) Name = Setting["Name"];
            if (Setting.Exist("Label")) Name = Setting["Label"];
            if (Setting.Exist("Description")) Name = Setting["Description"];
            if (Setting.Exist("Value")) Value = convert(t, Setting["Value"]);
            return true;
        }

        static object convert(DSPParameterType Type, string value)
        {
            if (Type == DSPParameterType.Float) return Convert.ToSingle(value);
            else if (Type == DSPParameterType.Int) return Convert.ToInt32(value);
            else if (Type == DSPParameterType.Bool) return Convert.ToBoolean(value);
            else
            {
                string[] val = value.Split(' ');
                byte[] data = new byte[val.Length];
                for (int i = 0; i < val.Length; i++)
                    if (!string.IsNullOrEmpty(val[i])) data[i] = Convert.ToByte(val[i]);
                return data;
            }
        }
        static string Forge(DSPParameterType Type, object value)
        {
            if (Type == DSPParameterType.Data)
            {
                byte[] data = (byte[])value;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                    sb.AppendFormat("{0:X2} ", data[i]);
                return sb.ToString();
            }
            else return value.ToString();
        }
    }
#endif
}
