﻿using HS.Setting;
using System;

namespace HSAudio.Speakers
{
    public class Speaker : ISettingManager
    {
        internal float _X;
        internal float _Y;
        internal bool _Active;
        internal SpeakerKind kind;
        internal Speaker(SpeakerKind kind) { this.kind = kind; }
        
        public float X { get { return _X; } set { _X = value; } }
        public float Y { get {return _Y; } set { _Y = value; } }
        public bool Active { get {return _Active; } set { _Active = value; } }

        public Speaker Set(float X, float Y, bool Active) { this.X = X; this.Y = Y; this.Active = Active; return this; }
        public Speaker Set(float X, float Y) { this.X = X; this.Y = Y; return this; }
        public Speaker Set(bool Active) { this.Active = Active; return this; }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("X")) try { X = float.Parse(Setting["X"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Y")) try { Y = float.Parse(Setting["Y"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Active")) try { Active = bool.Parse(Setting["Active"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("X", X);
            s.SetValue("Y", Y);
            s.SetValue("Active", Active);
            return s;
        }

        public static double DegreeToRadian(double angle) { return Math.PI * angle / 180.0; }
        public static float DegreeToRadian(float angle) { return (float)(Math.PI * angle / 180.0); }
        public static float RadianToDegree(float angle) { return (float)(angle * (180.0 / Math.PI)); }
    }
    /*
    public class Speaker : ISettingManager
    {
        HSAudio Audio;
        SpeakerKind _Kind;
        float _X;
        float _Y;
        bool _Active;
        internal Speaker(HSAudio Audio, SpeakerKind Kind, float X, float Y, bool Active) { this.Audio = Audio; _Kind = Kind; _X = X; _Y = Y;  }

        public SpeakerKind Kind { get; private set; }
        public float X { get { Update(); return _X; } set { Audio.system.setSpeakerPosition((SPEAKER)Kind, value, Y, Active); } }
        public float Y { get { Update(); return _Y; } set { Audio.system.setSpeakerPosition((SPEAKER)Kind, X, value, Active); } }
        public bool Active { get { Update(); return _Active; } private set { Audio.system.setSpeakerPosition((SPEAKER)Kind, X, Y, value); } }

        private void Update() {Audio.system.getSpeakerPosition((SPEAKER)Kind, out _X, out _Y, out _Active);}

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new NotImplementedException();
        }

        public Settings SaveSetting()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            Speaker spk = obj as Speaker;
            if (spk != null) return spk.Kind == Kind && spk.X == X && spk._Y == Y;
            else return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    */
}
