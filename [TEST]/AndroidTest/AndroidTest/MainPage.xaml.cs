﻿using CrossCallback;
using System;
using Xamarin.Forms;

namespace AndroidTest
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        
        private async void btnLoad_Clicked(object sender, EventArgs e)
        {
            if(await PermissionManager.PermissionCheck?.Invoke(this, PermissionKind.Storage))
            {
                string path = null;
                if (Device.RuntimePlatform == Device.Android)  path = await FunctionManager.ShowFileDialog("audio/mpeg", "audio/flac", "audio/ogg");
                else if (Device.RuntimePlatform == Device.iOS) path = await FunctionManager.ShowFileDialog("public.audio");
                else if (Device.RuntimePlatform == Device.UWP) path = await FunctionManager.ShowFileDialog(".mp3", ".flac", ".ogg");
                else if (Device.RuntimePlatform == Device.WPF) path = await FunctionManager.ShowFileDialog("MPEG files (*.mp3)|*.mp3", "FLAC files (*.flac)|*.flac", "OGG files (*.ogg)|*.ogg");

                lblPath.Text = path;

                Exception ex = await AudioManager.AudioChange?.Invoke(path);
            }
            else
            {

            }
        }
    }
}
