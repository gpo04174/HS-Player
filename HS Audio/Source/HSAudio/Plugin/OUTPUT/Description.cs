﻿using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using HSAudio.Speakers;
using HS.Log;
using System;
using System.Text;
using HS;
using System.Runtime.InteropServices;

namespace HSAudio.PlugIn.Output
{
    public class Description
    {
        internal OUTPUT_DESCRIPTION _Description = new OUTPUT_DESCRIPTION();
        internal OUTPUT_STATE _State = new OUTPUT_STATE();

        private OutputInfo[] info;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="Name"></param>
        /// <param name="Version">0x Major.Minor.Build.Revision</param>
        public Description(OutputInfo[] info, string Name, uint Version, //OutputState State,
            Init Init, Closed Close, PositionChanged PosChanged, Lock Lock, UnLock UnLock)
        {
            setInfo(info);
            this.Name = Name;
            this.Version = Version;

            this.init = Init;
            this.closed = Close;
            this.pos = PosChanged;

            //_State = new OUTPUT_STATE();
            //State.Register(ref _State);

            _OUTPUT_GETNUMDRIVERSCALLBACK = new OUTPUT_GETNUMDRIVERSCALLBACK(OUTPUT_GETDRIVERINFOCALLBACK);
            _OUTPUT_GETDRIVERINFOCALLBACK = new OUTPUT_GETDRIVERINFOCALLBACK(OUTPUT_GETDRIVERINFOCALLBACK);
            _OUTPUT_INITCALLBACK = new OUTPUT_INITCALLBACK(OUTPUT_INITCALLBACK);
            _OUTPUT_CLOSECALLBACK = new OUTPUT_CLOSECALLBACK(OUTPUT_CLOSECALLBACK);
            _OUTPUT_GETPOSITIONCALLBACK = new OUTPUT_GETPOSITIONCALLBACK(OUTPUT_GETPOSITIONCALLBACK);
            _OUTPUT_LOCKCALLBACK = new OUTPUT_LOCKCALLBACK(OUTPUT_LOCKCALLBACK);
            _OUTPUT_UNLOCKCALLBACK = new OUTPUT_UNLOCKCALLBACK(OUTPUT_UNLOCKCALLBACK);

            _Description.name = Name;
            _Description.getnumdrivers = _OUTPUT_GETNUMDRIVERSCALLBACK;
            _Description.getdriverinfo = _OUTPUT_GETDRIVERINFOCALLBACK;
            _Description.init = _OUTPUT_INITCALLBACK;
            _Description.close = _OUTPUT_CLOSECALLBACK;
            _Description.getposition = _OUTPUT_GETPOSITIONCALLBACK;
            _Description.@lock = _OUTPUT_LOCKCALLBACK;
            _Description.unlock = _OUTPUT_UNLOCKCALLBACK;

            //_Description.gethandle = new OUTPUT_GETHANDLECALLBACK(OUTPUT_GETHANDLECALLBACK);
        }

        internal void Register()
        {
            if (Start != null) { _OUTPUT_STARTCALLBACK = new OUTPUT_STARTCALLBACK(OUTPUT_STARTCALLBACK); _Description.start = _OUTPUT_STARTCALLBACK; }
            if (Stop != null) { _OUTPUT_STOPCALLBACK = new OUTPUT_STOPCALLBACK(OUTPUT_STOPCALLBACK); _Description.stop = _OUTPUT_STOPCALLBACK; }
            if (Update != null) { _OUTPUT_UPDATECALLBACK = new OUTPUT_UPDATECALLBACK(OUTPUT_UPDATECALLBACK); _Description.update = _OUTPUT_UPDATECALLBACK; }
            if (Mixer != null) { _OUTPUT_MIXERCALLBACK = new OUTPUT_MIXERCALLBACK(OUTPUT_MIXERCALLBACK); _Description.mixer = _OUTPUT_MIXERCALLBACK; }

            if (PortClose != null) { _OUTPUT_CLOSEPORTCALLBACK = new OUTPUT_CLOSEPORTCALLBACK(OUTPUT_CLOSEPORTCALLBACK); _Description.closeport = _OUTPUT_CLOSEPORTCALLBACK; }
            if (PortClose != null) { _OUTPUT_OPENPORTCALLBACK = new OUTPUT_OPENPORTCALLBACK(OUTPUT_OPENPORTCALLBACK); _Description.openport = _OUTPUT_OPENPORTCALLBACK; }

            if (PortClose != null) { _OUTPUT_OBJECT3DGETINFOCALLBACK = new OUTPUT_OBJECT3DGETINFOCALLBACK(OUTPUT_OBJECT3DGETINFOCALLBACK); _Description.object3dgetinfo = _OUTPUT_OBJECT3DGETINFOCALLBACK; }
            if (Object3DAlloc != null) { _OUTPUT_OBJECT3DALLOCCALLBACK = new OUTPUT_OBJECT3DALLOCCALLBACK(OUTPUT_OBJECT3DALLOCCALLBACK); _Description.object3dalloc = _OUTPUT_OBJECT3DALLOCCALLBACK;  }
            if (Object3DFree != null) { _OUTPUT_OBJECT3DFREECALLBACK = new OUTPUT_OBJECT3DFREECALLBACK(OUTPUT_OBJECT3DFREECALLBACK); _Description.object3dfree = _OUTPUT_OBJECT3DFREECALLBACK; }
            if (Object3DUpdate != null) { _OUTPUT_OBJECT3DUPDATECALLBACK = new OUTPUT_OBJECT3DUPDATECALLBACK(OUTPUT_OBJECT3DUPDATECALLBACK); _Description.object3dupdate = _OUTPUT_OBJECT3DUPDATECALLBACK; }
        }

        public uint Version { get; private set; }
        public string Name { get; private set; }
        public OutputInfo[] Info { get { return info; } }

        public void setInfo(OutputInfo[] info)
        {
            if (info == null || info.Length == 0) throw new HSException(StringResource.Resource["STR_ERR_NOT_NULL"]);
            this.info = info;
        }

        #region Event
        private Closed closed;
        private Init init;
        private PositionChanged pos;
        private Lock @lock = null;
        private UnLock UnLock = null;

        //public GetHandle GetHandle;

        public event EventHandler Start;
        public event EventHandler Stop;
        public event EventHandler Update;
        public event EventHandler Mixer;

        public event PortOpen PortOpen;
        public event PortClose PortClose;

        public event Object3DGetInfo Object3DGetInfo;
        public event Object3DAlloc Object3DAlloc;
        public event Object3DFree Object3DFree;
        public event Object3DUpdate Object3DUpdate;

        #endregion

        #region Callback

        private OUTPUT_GETNUMDRIVERSCALLBACK _OUTPUT_GETNUMDRIVERSCALLBACK;
        private OUTPUT_GETDRIVERINFOCALLBACK _OUTPUT_GETDRIVERINFOCALLBACK;
        private OUTPUT_INITCALLBACK _OUTPUT_INITCALLBACK;
        private OUTPUT_CLOSECALLBACK _OUTPUT_CLOSECALLBACK;
        private OUTPUT_GETPOSITIONCALLBACK _OUTPUT_GETPOSITIONCALLBACK;
        private OUTPUT_LOCKCALLBACK _OUTPUT_LOCKCALLBACK;
        private OUTPUT_UNLOCKCALLBACK _OUTPUT_UNLOCKCALLBACK;

        private OUTPUT_STARTCALLBACK _OUTPUT_STARTCALLBACK;
        private OUTPUT_STOPCALLBACK _OUTPUT_STOPCALLBACK;
        private OUTPUT_UPDATECALLBACK _OUTPUT_UPDATECALLBACK;
        private OUTPUT_MIXERCALLBACK _OUTPUT_MIXERCALLBACK;
        private OUTPUT_CLOSEPORTCALLBACK _OUTPUT_CLOSEPORTCALLBACK;
        private OUTPUT_OPENPORTCALLBACK _OUTPUT_OPENPORTCALLBACK;
        private OUTPUT_OBJECT3DGETINFOCALLBACK _OUTPUT_OBJECT3DGETINFOCALLBACK;
        private OUTPUT_OBJECT3DALLOCCALLBACK _OUTPUT_OBJECT3DALLOCCALLBACK;
        private OUTPUT_OBJECT3DFREECALLBACK _OUTPUT_OBJECT3DFREECALLBACK;
        private OUTPUT_OBJECT3DUPDATECALLBACK _OUTPUT_OBJECT3DUPDATECALLBACK;

        private RESULT OUTPUT_GETDRIVERINFOCALLBACK(ref OUTPUT_STATE state, ref int numdrivers) { state = _State; numdrivers = info.Length; return RESULT.OK; }
        //GetDriverCallbackInernal
        private unsafe RESULT OUTPUT_GETDRIVERINFOCALLBACK(ref OUTPUT_STATE state, int id, IntPtr name, int namelen, ref GUID guid, ref int systemrate, ref SPEAKERMODE speakermode, ref int speakermodechannels)
        {
            state = _State;

            try
            {
                if (name != IntPtr.Zero)
                {
#if !NETSTANDARD1x
                    string str = info[id].Name;
                    namelen = Math.Max(Encoding.UTF8.GetByteCount(str), namelen);
                    fixed (char* p = str)
                    {
                        byte* n = (byte*)name.ToPointer();
                        Encoding.UTF8.GetBytes(p, str.Length, n, namelen);
                    }
#endif
                    byte[] tmp = Encoding.UTF8.GetBytes(info[id].Name);
                    namelen = tmp.Length;
                    Marshal.Copy(tmp, 0, name, tmp.Length);
                }

                guid = info[id].Guid; systemrate = info[id].Samplerate; speakermode = (SPEAKERMODE)info[id].Mode; speakermodechannels = info[id].Channel;
                return RESULT.OK;
            }
            catch (Exception ex) { new Log.HSAudioLog(ex.Message, "Output::GetDriverCallbackInernal()", Log.LogKind.Plugin, LogLevel.Error); return RESULT.ERR_INVALID_PARAM; }
        }

        private RESULT OUTPUT_INITCALLBACK(ref OUTPUT_STATE state, int index, INITFLAGS flags, ref int outputrate, ref SPEAKERMODE speakermode, ref int speakermodechannels, ref SOUND_FORMAT outputformat, int dspbufferlength, int dspnumbuffers, ref IntPtr extradriverdata)
        {
            state = _State;

            if (init != null)
            {
                state = _State;

                bool b = true;
                InitInfo info = null;
                try { info = new InitInfo(index, outputrate, (SpeakerMode)speakermode, speakermodechannels, outputformat, dspbufferlength, dspnumbuffers); } catch (Exception ex) { return RESULT.ERR_OUTPUT_FORMAT; }
                try { b = init(this, info); } catch { }

                outputrate = info.SampleRate;
                speakermode = (SPEAKERMODE)info.Mode;
                speakermodechannels = info.channel;
                if (info.IsPCM)
                {
                    if (info.Bit == 8) outputformat = SOUND_FORMAT.PCM8;
                    else if (info.Bit == 16) outputformat = SOUND_FORMAT.PCM16;
                    else if (info.Bit == 24) outputformat = SOUND_FORMAT.PCM24;
                    else if (info.Bit == 32) outputformat = SOUND_FORMAT.PCM32;
                    else if (info.Bit == 1) outputformat = SOUND_FORMAT.BITSTREAM;
                }
                else outputformat = SOUND_FORMAT.PCMFLOAT;
                return b ? RESULT.OK : RESULT.ERR_OUTPUT_INIT;
            }
            else return RESULT.ERR_OUTPUT_INIT;
        }
        private RESULT OUTPUT_CLOSECALLBACK(ref OUTPUT_STATE state)
        {
            state = _State;

            if (closed != null) { state = _State; try { closed(this); } catch { } }
            return RESULT.OK;
        }
        private RESULT OUTPUT_GETPOSITIONCALLBACK(ref OUTPUT_STATE state, ref uint pcm)
        {
            state = _State;

            bool a = true;
            if (pos != null) try { a = pos(this, ref pcm); } catch { }
            return a ? RESULT.OK : RESULT.ERR_BADCOMMAND;
        }
        private RESULT OUTPUT_LOCKCALLBACK(ref OUTPUT_STATE state, uint offset, uint length, ref IntPtr ptr1, ref IntPtr ptr2, ref uint len1, ref uint len2)
        {
            state = _State;

            bool b = true;
            if (@lock != null)
            {
                LockInfo info = new LockInfo(ref state, offset, length, ref ptr1, ref ptr2, ref len1, ref len2);
                try { b = @lock(this, info); } catch { }
                len1 = info.len1;
                len2 = info.len2;
            }
            return b ? RESULT.OK : RESULT.ERR_INVALID_PARAM;
        }
        private RESULT OUTPUT_UNLOCKCALLBACK(ref OUTPUT_STATE state, IntPtr ptr1, IntPtr ptr2, uint len1, uint len2)
        {
            state = _State;

            bool b = true;
            if (UnLock != null)
            {
                UnLockInfo info = new UnLockInfo(ref state, ptr1, ptr2, len1, len2);
                try { b = UnLock(this, info); } catch { }
            }
            return b ? RESULT.OK : RESULT.ERR_INVALID_PARAM;
        }

        /*
        private RESULT OUTPUT_GETHANDLECALLBACK(ref OUTPUT_STATE state, ref IntPtr handle)
        {
            if (GetHandle != null) return GetHandle(this, ref handle) ? RESULT.OK : RESULT.ERR_BADCOMMAND;
            else return RESULT.OK;
        }
        */

        private RESULT OUTPUT_STARTCALLBACK(ref OUTPUT_STATE state) { state = _State; try { Start(this); } catch { }return RESULT.OK; }
        private RESULT OUTPUT_STOPCALLBACK(ref OUTPUT_STATE state) { state = _State; try { Stop(this); } catch { } return RESULT.OK; }
        private RESULT OUTPUT_UPDATECALLBACK(ref OUTPUT_STATE state) { state = _State; try { Update(this); } catch { } return RESULT.OK; }
        private RESULT OUTPUT_MIXERCALLBACK(ref OUTPUT_STATE state) { state = _State; try { Mixer(this); } catch { } return RESULT.OK; }

        private RESULT OUTPUT_CLOSEPORTCALLBACK(ref OUTPUT_STATE state, int portId) { state = _State; try{ return PortClose(this, portId) ? RESULT.OK : RESULT.ERR_BADCOMMAND; } catch { return RESULT.OK; } }
        private RESULT OUTPUT_OPENPORTCALLBACK(ref OUTPUT_STATE state, uint portType, ulong portIndex, ref int portId, ref int portRate, ref int portChannels, ref SOUND_FORMAT portFormat)
        {
            state = _State;

            try
            {
                PortOpenInfo info = new PortOpenInfo(ref state, portType, portIndex, ref portId, ref portRate, ref portChannels, ref portFormat);
                bool b = PortOpen(this, info);
                portId = info.PortId;
                portRate = info.PortRate;
                portChannels = info.PortChannel;
                portFormat = info;

                return b ? RESULT.OK : RESULT.ERR_BADCOMMAND;
            }
            catch { return RESULT.OK; }
        }

        private RESULT OUTPUT_OBJECT3DGETINFOCALLBACK(ref OUTPUT_STATE state, ref int maxhardwareobjects) { state = _State; try { return Object3DGetInfo(this, ref maxhardwareobjects) ? RESULT.OK : RESULT.ERR_BADCOMMAND; } catch { return RESULT.OK; } }
        private RESULT OUTPUT_OBJECT3DALLOCCALLBACK(ref OUTPUT_STATE state, ref IntPtr object3d) { state = _State; try { return Object3DAlloc(this, ref object3d) ? RESULT.OK : RESULT.ERR_BADCOMMAND; } catch { return RESULT.OK; } }
        private RESULT OUTPUT_OBJECT3DFREECALLBACK(ref OUTPUT_STATE state, IntPtr object3d) { state = _State; try { return Object3DFree(this, object3d) ? RESULT.OK : RESULT.ERR_BADCOMMAND; } catch { return RESULT.OK; } }
        private RESULT OUTPUT_OBJECT3DUPDATECALLBACK(ref OUTPUT_STATE state, IntPtr object3d, ref OUTPUT_OBJECT3DINFO info)
        {
            state = _State;
            try
            {
                Object3DUpdateInfo o = new Object3DUpdateInfo(ref state, object3d, ref info);
                bool b = Object3DUpdate(this, o);
                state = o.State;
                return b ? RESULT.OK : RESULT.ERR_BADCOMMAND;
            }
            catch { return RESULT.OK; }
        }
#endregion
    }
}
