﻿namespace HSAudio.Utility
{
    public static class Maths
    {
        public static float SqrtFast(this float x)
        {
            float xhalf = 0.5f * x;
            int i = 0;
            unsafe { i = *(int*)&x; }
            i = 0x5f375a84 - (i >> 1);
            unsafe { x = *(float*)&i; }

            x = x * (1.5f - xhalf * x * x);
            return 1.0f / x;
        }
        public static double SqrtFast(this double x)
        {
            double xhalf = 0.5 * x;
            int i = 0;
            unsafe { i = *(int*)&x; }
            i = 0x5f375a84 - (i >> 1);
            unsafe { x = *(double*)&i; }

            x = x * (1.5 - xhalf * x * x);
            return 1.0 / x;
        }
        public static double PowFast(this double num, int exp)
        {
            double result = 1.0;
            while (exp > 0)
            {
                if (exp % 2 == 1)
                    result *= num;
                exp >>= 1;
                num *= num;
            }

            return result;
        }
        public static float PowFast(this float num, int exp)
        {
            float result = 1;
            while (exp > 0)
            {
                if (exp % 2 == 1)
                    result *= num;
                exp >>= 1;
                num *= num;
            }

            return result;
        }
    }
}
