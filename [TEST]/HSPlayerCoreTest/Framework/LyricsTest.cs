﻿using HSAudio.Output.Devices;
using HSPlayerCore.Lyrics;
using HSPlayerCore.Lyrics.ALSong;
using HSPlayerCore.Lyrics.ALSong.Search;
using HSPlayerCoreTest.GUI;
using System;
using System.IO;
using System.Windows.Forms;

namespace HSPlayerCoreTest
{
    public partial class frmLyricsTest : Form
    {
        public enum Language { No, Korean, Japanese, English }
        public static string GetTime(uint tick)
        {
            uint h, m, s, ms;
            ms = tick % 1000;
            tick = tick / 1000;
            h = tick / 3600;
            tick = tick % 3600;
            m = tick / 60;
            s = tick % 60;
            return string.Format("{0:00}:{1:00}:{2:00}.{3:000}", h, m, s, ms);
        }

        public frmLyricsTest()
        {
            InitializeComponent();
            loadMusic1.Loaded += LoadMusic1_Loaded;
            playControl1.PlayingProgress += PlayControl1_PlayingProgress;

            this.playControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
            this.loadMusic1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right)));
        }

        HSAudio.HSAudio player;
        private void LoadMusic1_Loaded(object sender, EventArgs e)
        {
            if (player == null)
            {
                player = new HSAudio.HSAudio(new AutoOutput());
                deviceControl1.Audio = player;
                playControl1.Audio = player;
            }
            playControl1.Load(loadMusic1.MusicPath);
        }

        string last;
        uint Last_Millisecond;
        private void PlayControl1_PlayingProgress(object sender, uint Millisecond)
        {
            if (Last_Millisecond != Millisecond)
            {
                Last_Millisecond = Millisecond;
                try
                {
                    view.RefreshTime(Millisecond / 1000d);

                    if (view.Manager != null)
                    {
                        if (last != view.Manager.CurrentLyrics) { last = view.Manager.CurrentLyrics; Console.Clear(); }
                        Console.SetCursorPosition(0, 0);
                        Console.WriteLine("\n" + view.Manager.PreviousLyrics + "\n\n    ↑\n");
                        Console.WriteLine(view.Manager.CurrentLyrics + "\n\n    ↓\n");
                        Console.WriteLine(view.Manager.NextLyrics + "\n\n");
                    }
                    Console.WriteLine(GetTime(Millisecond));
                    //if()
                }
                catch { view.RefreshTime(0); }
            }
        }

        LyricsManager manager;
        void RefreshLyrics()
        {
            timer1.Stop();

            bool AutoApply = AutoApplyToolStripMenuItem.Checked;
            AutoApplyToolStripMenuItem.Checked = false;
            
            view.Manager = manager;
            if (manager == null)
            {
                txtTitle.Text = null;
                txtArtist.Text = null;
                txtAlbum.Text = null;
                txtComment.Text = null;
                txtAuthor.Text = null;
                txtMaker.Text = null;
                numOffset.Value = 0;
                txtLength.Text = null;
                txtLyrics.Text = null;
            }
            else
            {
                txtTitle.Text = manager.Title;
                txtArtist.Text = manager.Artist;
                txtAlbum.Text = manager.Album;
                txtComment.Text = manager.Comment;
                txtAuthor.Text = manager.Author;
                txtMaker.Text = manager.By;
                numOffset.Value = manager.Offset;
                txtLength.Text = manager.Length;
                txtLyrics.Text = manager.PureLyrics;
            }

            AutoApplyToolStripMenuItem.Checked = AutoApply;

            Console.Clear();
        }

        LyricsView view;
        private void frmLyricsTest_Load(object sender, EventArgs e)
        {
            view = new LyricsView();
            view.Dock = DockStyle.Fill;
            panel2.Controls.Add(view);
            comboBox1.SelectedIndex = 0;
            cbSearch.SelectedIndex = 0;
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string lrc = File.ReadAllText(openFileDialog1.FileName);
                manager = new LyricsManager(lrc, false);
                RefreshLyrics();
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog1.FileName, manager.ToString());
            }
        }
        private void btnOffsetApply_Click(object sender, EventArgs e)
        {
            manager.ApplyOffset((int)numOffset.Value);
            //numOffset.Value = 0;
            txtLyrics.Text = manager.PureLyrics;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            manager.Title = txtArtist.Text;
            manager.Artist = txtAlbum.Text;
            manager.Comment = txtComment.Text;
            manager.Author = txtAuthor.Text;
            manager.By = txtMaker.Text;
            manager.Offset = (int)numOffset.Value;
            manager.Length = txtLength.Text;
            manager.PureLyrics = txtLyrics.Text;
            RefreshLyrics();
        }

        private void refreshRToolStripMenuItem_Click(object sender, EventArgs e) { RefreshLyrics(); }
        

        private void numOffset_ValueChanged(object sender, EventArgs e)
        {
            if (chkOffsetAuto.Checked && debugModeToolStripMenuItem.Checked)
            {                                                                                                 
                manager.Offset = 0;                                                                           
                btnOffsetApply_Click(sender, e);
            }
            else if (AutoApplyToolStripMenuItem.Checked) manager.Offset = (int)numOffset.Value;
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (manager != null)
            {
                if (comboBox1.SelectedIndex == 1) manager.ExcludeLanguage = LyricsLanguage.Korean;
                else if (comboBox1.SelectedIndex == 2) manager.ExcludeLanguage = LyricsLanguage.Japanese;
                else if (comboBox1.SelectedIndex == 3) manager.ExcludeLanguage = LyricsLanguage.English;
                else manager.ExcludeLanguage = LyricsLanguage.None;
            }
        }
#if NET45
        private async void btnGetLyrics_Click(object sender, EventArgs e)
#else
        private void btnGetLyrics_Click(object sender, EventArgs e)
#endif
        {
            if (string.IsNullOrEmpty(loadMusic1.MusicPath)) MessageBox.Show("Please, open the music file.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                btnGetLyrics.Enabled = false;
                try
                {
                    SearchHash hash = SearchHelper.GetHash(loadMusic1.MusicPath);
#if NET45
                    ALSongLyricHash lyrics = await Search.SearchHashAsync(hash);
                    GetLyrics_Hash(lyrics, null);
#else
                    Search.SearchHashAsync(GetLyrics_Hash_tmp, hash);
#endif
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "오류발생!!", MessageBoxButtons.OK, MessageBoxIcon.Error); btnGetLyrics.Enabled = true; }
            }
        }

        delegate void SearchComplete_Hash(ALSongLyricHash lyrics, Exception ex);
        private void GetLyrics_Hash_tmp(ALSongLyricHash lyrics, Exception ex) { if (InvokeRequired) Invoke(new SearchComplete_Hash(GetLyrics_Hash), lyrics, ex); else GetLyrics_Hash(lyrics, ex); }
        private void GetLyrics_Hash(ALSongLyricHash lyrics, Exception ex)
        {
            if (lyrics != null)
            {
                if (manager == null) manager = new LyricsManager(lyrics);
                else manager.SetLyrics(lyrics);
                setRawXML(lyrics);
                RefreshLyrics();
            }
            else MessageBox.Show("해당 음악의 가사가 존재하지 않습니다.\n\n직접 검색을 해주세요.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            btnGetLyrics.Enabled = true;
        }

        string Backup;
        private void cbSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (manager != null)
            {
                if (Backup == null) Backup = manager.ToString();
                if (cbSearch.SelectedIndex == 0) { manager.SetLyrics(Backup, false); Backup = null; }
                else
                {
                    SearchResult al = cbSearch.SelectedItem as SearchResult;
                    ALSongLyricsKeyword lyrics = Search.SearchKeyword(al);
                    if (al != null) manager.SetLyrics(lyrics);
                    else manager.SetLyrics(null);
                    setRawXML(lyrics);
                }
                //RefreshLyrics();
            }
            else
            {
                if (cbSearch.SelectedIndex > 0)
                {
                    SearchResult al = cbSearch.SelectedItem as SearchResult;
                    if (al != null) { manager = new LyricsManager(Search.SearchKeyword(al));/*RefreshLyrics();*/ }
                }
            }

            RefreshLyrics();
        }

#if NET45
        private async void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            AlsongLyricsManager manager = await AlsongLyricsManager.SearchAsync(txtSearchName.Text, txtSearchArtist.Text);
            GetLyrics_Manual(manager, null);
        }
#else
        private void btnSearch_Click(object sender, EventArgs e)
        {
            btnSearch.Enabled = false;
            SearchHelper.GetKeywordsAsync(GetLyrics_Manual_tmp, txtSearchName.Text, txtSearchArtist.Text);
        }
#endif

        delegate void SearchComplete_Manual(SearchResult[] results, Exception ex);
        private void GetLyrics_Manual_tmp(SearchResult[] results, Exception ex) { if (InvokeRequired) Invoke(new SearchComplete_Manual(GetLyrics_Manual), results, ex); else GetLyrics_Manual(results, ex); }
        private void GetLyrics_Manual(SearchResult[] results, Exception ex)
        {
            SearchUpdate(results); btnSearch.Enabled = true;
        }

        private void SearchUpdate(SearchResult[] results)
        {
            cbSearch.Items.Clear();
            cbSearch.Items.Add("(Current Lyrics)");
            for (int i = 0; i < results.Length; i++) cbSearch.Items.Add(results[i]);
            cbSearch.SelectedIndex = 0;
            txtSearchCount.Text = string.Format("Results: {0}", results.Length);
        }

        private void viewPureLyricsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DateTime ts = DateTime.Now;
            string s = Lyrics.RemoveSync(manager.PureLyrics, true);
            PureLyricsViewer view = new PureLyricsViewer(s, manager.Title, true, DateTime.Now - ts);
            view.Show();
        }

        private void viewPureLyricsOldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DateTime ts = DateTime.Now;
            string s = Lyrics.RemoveSync(manager.PureLyrics, false);
            PureLyricsViewer view = new PureLyricsViewer(s, manager.Title, false, DateTime.Now - ts);
            view.Show();
        }

        private void viewVToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            viewPureLyricsOldToolStripMenuItem.Enabled = viewPureLyricsToolStripMenuItem.Enabled = manager != null && !string.IsNullOrWhiteSpace(manager.PureLyrics);
        }

        private void txtLyrics_TextChanged(object sender, EventArgs e)
        {
            if (AutoApplyToolStripMenuItem.Checked) btnApply_Click(sender, e);
        }

        private void RemappingTimeIs0ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (manager != null)
            {
                string s = manager.ToString();
                manager = new LyricsManager(s, true);
                RefreshLyrics();
            }
        }

        private void debugModeToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            chkOffsetAuto.Visible = panel3.Visible = debugModeToolStripMenuItem.Checked;
        }

        private void newLyricsNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            manager = null;
            RefreshLyrics();
        }

        frmViewXML viewxml;
        ALSongLyrics LastALSongLyrics;
        private void setRawXML(ALSongLyrics lyrics)
        {
            LastALSongLyrics = lyrics;
            if (chkAutoloadXML.Checked && (viewxml != null && !viewxml.IsDisposed))
                viewxml.SetXML(lyrics.OriginXML);
        }
        private void btnViewXML_Click(object sender, EventArgs e)
        {
            if(LastALSongLyrics == null) MessageBox.Show("가사를 검색 후 선택하지 않으셨습니다.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (viewxml == null || viewxml.IsDisposed)
                    viewxml = new frmViewXML(LastALSongLyrics.OriginXML);
                else viewxml.SetXML(LastALSongLyrics.OriginXML);

                viewxml.Show();
            }
        }
    }
}
