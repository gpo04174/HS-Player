﻿using HS;
using HSAudio.Lib.FMOD;
using HSAudio.Resource;

namespace HSAudio
{
    public class HSAudioException : HSException
    {
        public static string ErrorCodeString(RESULT ErrorCode) { return StringResource.Resource["STR_ENGINE_" + ErrorCode.ToString()]; }

        public RESULT ErrorCode { get; private set; }

        public HSAudioException(RESULT result) : base(ErrorCodeString(result))
        {
            ErrorCode = result;
        }
        public HSAudioException(RESULT result, string Message) : base(Message)
        {
            ErrorCode = result;
        }
    }
}
