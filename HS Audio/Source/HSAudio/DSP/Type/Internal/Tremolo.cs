﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.DSP.Type
{
    public class Tremolo : ISettingManager
    {
        private Parameter[] param = new Parameter[8];
        public Tremolo(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.TREMOLO, "Tremolo", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// LFO frequency in Hz.  0.1 to 20.  Default = 4.
        /// </summary>
        public float Frequency
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.FREQUENCY, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.FREQUENCY, value); }
        }
        /// <summary>
        /// Tremolo depth.  0 to 1.  Default = 0
        /// </summary>
        public float Depth
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.DEPTH, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.FREQUENCY, value); }
        }
        /// <summary>
        /// LFO shape morph between triangle and sine.  0 to 1.  Default = 0
        /// </summary>
        public float Shape
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.SHAPE, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.SHAPE, value); }
        }
        /// <summary>
        /// Time-skewing of LFO cycle.  -1 to 1.  Default = 0
        /// </summary>
        public float Skew
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.SKEW, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.SKEW, value); }
        }
        /// <summary>
        /// LFO on-time.  0 to 1.  Default = 0.5
        /// </summary>
        public float Duty
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.DUTY, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.DUTY, value); }
        }
        /// <summary>
        /// Flatness of the LFO shape.  0 to 1.  Default = 0
        /// </summary>
        public float Square
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.SQUARE, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.SQUARE, value); }
        }
        /// <summary>
        /// Instantaneous LFO phase.  0 to 1.  Default = 0
        /// </summary>
        public float Phase
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.PHASE, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.PHASE, value); }
        }
        /// <summary>
        /// Rotation / auto-pan effect.  -1 to 1.  Default = 0
        /// </summary>
        public float Spread
        {
            get { return DSP.GetParameter((int)DSP_TREMOLO.SPREAD, 0f); }
            set { DSP.SetParameter((int)DSP_TREMOLO.SPREAD, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Frequency")) try { Frequency = float.Parse(Setting["Frequency"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Depth")) try { Depth = float.Parse(Setting["Depth"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Shape")) try { Shape = float.Parse(Setting["Shape"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Skew")) try { Skew = float.Parse(Setting["Skew"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Duty")) try { Duty = float.Parse(Setting["Duty"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Square")) try { Square = float.Parse(Setting["Square"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Phase")) try { Phase = float.Parse(Setting["Phase"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Spread")) try { Spread = float.Parse(Setting["Spread"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Frequency", Frequency);
            s.SetValue("Depth", Depth);
            s.SetValue("Shape", Shape);
            s.SetValue("Skew", Skew);
            s.SetValue("Duty", Duty);
            s.SetValue("Square", Square);
            s.SetValue("Phase", Phase);
            s.SetValue("Spread", Spread);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
