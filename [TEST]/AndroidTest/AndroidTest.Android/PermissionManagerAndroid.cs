﻿using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.Content;
using CrossCallback;
using System.Threading.Tasks;

namespace AndroidTest.Droid
{
    public class PermissionManagerAndroid
    {
        public const int PERMISSION_OK = 1;

        //public static PermissionManagerAndroid Instance;
        internal delegate Task<bool> ProcessingPermissionCallback(params string[] Permission);
        internal PermissionManagerAndroid(Context context, ProcessingPermissionCallback Callback)
        {
            ProcessingPermission = Callback;
            this.context = context;
            PermissionManager.PermissionCheck += PermissionManager_PermissionChecking;
            /*
            if (Instance == null)
            {
                Instance = new PermissionManagerAndroid(activity);
                Callback.PermissionManager.PermissionChecking += Instance.PermissionManager_PermissionChecking;
            }
            else Instance.activity = activity;
            */
        }

        internal event ProcessingPermissionCallback ProcessingPermission;
        Context context;

        //private Activity activity;
        //private PermissionManagerAndroid(Activity activity) { this.activity = new ActivityInternal(activity); }

        private async Task<bool> PermissionManager_PermissionChecking(object sender, PermissionKind Kind)
        {
            switch (Kind)
            {
                case PermissionKind.Storage: return await CheckingPermission(Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage);//Manifest.Permission_group.Storage
                case PermissionKind.Phone: return await CheckingPermission(Manifest.Permission.ReadPhoneNumbers, Manifest.Permission.ReadPhoneState);
                default: return await Task.Run(() => { return false; });
            }
        }

        private async Task<bool> CheckingPermission(params string[] Permission)
        {
            if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
            {
                bool grant = true;
                for (int i = 0; i < Permission.Length; i++)
                {
                    // 사용 권한 체크( 사용권한이 없을경우 -1 )
                    if (ContextCompat.CheckSelfPermission(context, Permission[i]) != Android.Content.PM.Permission.Granted)
                    {
                        grant = false;
                        break;
                    }
                }
                if (!grant)
                {
                    if (ProcessingPermission == null) return await Task.Run(() => { return false; });
                    else return await ProcessingPermission(Permission);
                }
                else return true;
            }
            else return true;
        }

        class ActivityInternal : Activity
        {
            public Activity activity;
            public ActivityInternal(Activity activity)
            {
                this.activity = activity;
            }
            public Context Context { get { return ApplicationContext; } }

            public override Context ApplicationContext { get { return activity.ApplicationContext; } }
            public override Context BaseContext { get { return activity.BaseContext; } }
        }
    }
}