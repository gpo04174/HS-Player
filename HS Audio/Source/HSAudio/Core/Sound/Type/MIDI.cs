﻿using HS.Setting;
using HSAudio.Utility.Native;
using System;

namespace HSAudio.Core.Sound.Type
{
    public class MIDI : ISoundFormat, ISettingManager
    {
        public MIDI() : base(SoundType.MIDI, MIME, Extension, Description) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SoundFontPath">MIDI DLS(Level 1) file path [미디 DLS(레벨1) 파일 경로 입니다.]</param>
        /// <param name="MinGranularity">Allows you to set a minimum desired MIDI mixer granularity. Values smaller than 512 give greater than default accuracy at the cost of more CPU and vise versa. Specify 0 for default (512 samples). 
        /// [원하는 최소의 MIDI 믹서 Granularity를 설정할 수 있습니다. 512보다 작은 값은 더 많은 성능으로 기본 정확도보다 더 큰 값을 제공하며 그 반대도 같습니다. 기본값은 0 입니다.]</param>
        public MIDI(string SoundFontPath = null, uint MinGranularity = 0) : base(SoundType.MIDI, MIME, Extension, Description)
        {
            this.SoundFontPath = SoundFontPath;
            info.dlsname = new StringWrapper(SoundFontPath);
            info.minmidigranularity = MinGranularity;
        }

        public uint MinGranularity { get { return info.minmidigranularity; } }
        public string SoundFontPath { get; private set; }


        public static string[] MIME { get { return new string[] { "audio/midi", "audio/x-mid", "audio/x-midi", "application/x-midi" }; } }
        public static string[] Extension { get { return new string[] { "midi", "mid" }; } }
        public static string[] Description { get { return new string[] { "MIDI", "Musical Instrument Digital Interface" }; } }

        public override string ToString() { return Description[1]; }

        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("MinGranularity", MinGranularity);
            s.SetValue("SoundFontPath", new SettingsData(SoundFontPath, false));
            return s;
        }
        public bool LoadSetting(Settings setting, bool ThrowException = false)
        {
            try { if (setting.Exist("MinGranularity")) FileOffset = Convert.ToUInt32(setting["MinGranularity"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("SoundFontPath")) SoundFontPath = setting["SoundFontPath"]; } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("Length")) Length = Convert.ToUInt32(setting["Length"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("DecodeSize")) DecodeSize = Convert.ToUInt32(setting["DecodeSize"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("FileOffset")) FileOffset = Convert.ToUInt32(setting["FileOffset"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }
    }
}
