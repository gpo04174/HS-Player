﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace HSAudio.DSP.Type
{
    public class HighPass : IDSP
    {
        private Parameter[] param = new Parameter[2];
        public HighPass(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.HIGHPASS, "High-Pass", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Highpass cutoff frequency in hz.  1.0 to output 22000.0.  Default = 5000.0
        /// </summary>
        public float Cutoff
        {
            get { return DSP.GetParameter((int)DSP_HIGHPASS.CUTOFF, 0f); }
            set { DSP.SetParameter((int)DSP_HIGHPASS.CUTOFF, value); }
        }
        /// <summary>
        /// Highpass resonance Q value.  1.0 to 10.0.  Default = 1.0
        /// </summary>
        public float Resonance
        {
            get { return DSP.GetParameter((int)DSP_HIGHPASS.RESONANCE, 0f); }
            set { DSP.SetParameter((int)DSP_HIGHPASS.RESONANCE, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Cutoff")) try { Cutoff = float.Parse(Setting["Cutoff"]); } catch(Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Resonance")) try { Resonance = float.Parse(Setting["Resonance"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Cutoff", Cutoff);
            s.SetValue("Resonance", Resonance);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
