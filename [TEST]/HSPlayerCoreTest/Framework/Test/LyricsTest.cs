﻿using HSAudio.Core.Audio;
using HSAudio.Core.Sound;
using HSPlayerCore.Lyrics;
using System;
using System.IO;
using System.Threading;

namespace HSPlayerCoreTest.Test
{
    public class LyricsTest
    {
        public static void Test()
        {
            string path = "C:\\Users\\gpo04\\OneDrive\\음악\\앨범\\Orangestar\\未完成エイトビーツ\\12. アスノヨゾラ哨戒班.";
            FileStream fs = new FileStream(path + "lrc", FileMode.Open, FileAccess.ReadWrite);
            StreamReader sr = new StreamReader(fs);
            string lrc = sr.ReadToEnd();
            sr.Close();
            fs.Close();
            
            LyricsManager manager = new LyricsManager(lrc, false);

            HSAudio.HSAudio audio = new HSAudio.HSAudio();
            Sound s = Sound.FromFile(audio, path + "flac");
            Audio a = audio.Audio.Add(s);
            a.Loop = LoopKind.Normal;
            a.Play();

            manager.Offset = -1000;
            //manager.ApplyOffset(-1000);

            string before = null;
            while(true)
            {
                Console.SetCursorPosition(0, 0);
                Console.WriteLine("{0} / {1}", a.CurrentPosition, s.TotalPosition);

                manager.Refresh(a.CurrentPosition / 1000d);
                string now = manager.CurrentLyrics;
                if (before != now) { Console.Clear(); Console.WriteLine("\n\n"+ now + "\n"); before = now; }
                
                Thread.Sleep(10);
            }  
        }
    }
}
