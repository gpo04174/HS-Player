﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class Limiter : IDSP
    {
        private Parameter[] param = new Parameter[4];
        public Limiter(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.LIMITER, "Limiter", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Time to ramp the silence to full in ms.  1.0 to 1000.0. Default = 10.0
        /// </summary>
        public float ReleaseTime
        {
            get { return DSP.GetParameter((int)DSP_LIMITER.RELEASETIME, 0f); }
            set { DSP.SetParameter((int)DSP_LIMITER.RELEASETIME, value); }
        }
        /// <summary>
        ///  Maximum level of the output signal in dB.  -12.0 to 0.0.  Default = 0.0
        /// </summary>
        public float Ceiling
        {
            get { return DSP.GetParameter((int)DSP_LIMITER.CEILING, 0f); }
            set { DSP.SetParameter((int)DSP_LIMITER.CEILING, value); }
        }
        /// <summary>
        ///  Maximum amplification allowed in dB.  0.0 to 12.0.  Default = 0.0. 0.0 = no amplifaction, higher values allow more boost
        /// </summary>
        public float MaximizerGain
        {
            get { return DSP.GetParameter((int)DSP_LIMITER.MAXIMIZERGAIN, 0f); }
            set { DSP.SetParameter((int)DSP_LIMITER.MAXIMIZERGAIN, value); }
        }
        /// <summary>
        /// Channel processing mode. Default = false. False = Independent (limiter per channel), True = Linked
        /// </summary>
        public bool ChannelProcessing
        {
            get { return DSP.GetParameter((int)DSP_LIMITER.MODE, 0f) == 1; }
            set { DSP.SetParameter((int)DSP_LIMITER.MODE, value ? 1f : 0f); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("ReleaseTime")) try { ReleaseTime = float.Parse(Setting["ReleaseTime"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Ceiling")) try { Ceiling = float.Parse(Setting["Ceiling"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("MaximizerGain")) try { MaximizerGain = float.Parse(Setting["MaximizerGain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("ChannelProcessing")) try { ChannelProcessing = bool.Parse(Setting["ChannelProcessing"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("ReleaseTime", ReleaseTime);
            s.SetValue("Ceiling", Ceiling);
            s.SetValue("MaximizerGain", MaximizerGain);
            s.SetValue("ChannelProcessing", ChannelProcessing.ToString());
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
