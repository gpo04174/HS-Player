﻿#if FUNC_PLUGIN
using HSAudio.Lib.FMOD;
using HSAudio.PlugIn;
using HSAudio.PlugIn.DSP;
using HSAudio.Resource;
using HS.Setting;
using System;
using System.IO;
using HS;

namespace HSAudio.DSP.Type
{
    /// <summary>
    /// Windows VST, WINAMP Plugin
    /// </summary>
    public class DSPPlugin : IDSP
    {
        
        HSAudio Audio;
        public DSPPlugin(HSAudio Audio, Settings Setting) { LoadSetting(Setting); }
        public void Init(HSAudio Audio, Plugin_DSP Plugin)
        {
            this.Plugin = Plugin;
            this.Audio = Audio;

            Name = "DSP Plugin - " + Plugin.Name;
            DSP = new DSP(Audio, Plugin.Handle, Name, null);

            if (DSP.DSPType == DSP_TYPE.UNKNOWN) { Type = DSPPluginType.HSAudioPlugin; }
#if !XBOX
            if (DSP.DSPType == DSP_TYPE.VSTPLUGIN) { Type = DSPPluginType.VST; this.Name = Name.Substring(Name.IndexOf(" ") + 1); }
#endif
#if WIN_XP || WIN_VISTA || WIN_10_1703
            if (DSP.DSPType == DSP_TYPE.WINAMPPLUGIN) { Type = DSPPluginType.WinAMP; this.Name = Name.Substring(Name.IndexOf(" ") + 1); }
#endif
#if LINUX
            if (DSP.DSPType == DSP_TYPE.LADSPAPLUGIN) { Type = DSPPluginType.LADSPA; this.Name = Name.Substring(Name.IndexOf(" ") + 1); }
#endif
        }

        public DSP DSP { get; private set; }

        public Plugin_DSP Plugin { get; private set; }
        //public IntPtr WindowHandle { get { return handle; } }
        //public string Path { get { return Plugin.Path; } }
        public string Name { get; private set; }
        public DSPPluginType Type { get; private set; }

        /// <summary>
        /// WINAMP Plugin Only
        /// </summary>
        /// <param name="Show"></param>
        /// <returns></returns>
        public RESULT ShowConfigDialog(bool Show) {return DSP.DSPInternal.showConfigDialog(IntPtr.Zero, Show); }
        /// <summary>
        /// VST Plugin Only
        /// </summary>
        /// <param name="Window">WIndows Handle</param>
        /// <returns></returns>
        public RESULT ShowConfigDialog(IntPtr Window) { return DSP.DSPInternal.showConfigDialog(Window, true); }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            Audio.system.unloadPlugin(Plugin.Handle);
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Plugin != null) throw new HSException(StringResource.Resource["STR_ERR_SETTING_ONLY_INIT"]);

            if (Setting.Exist("Path"))
            {
                if (PlugIn.Plugin.Check(Audio, Setting["Path"]) != PluginKind.DSP) { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_DSP"]); return false; }
                try { Init(Audio, (Plugin_DSP)PlugIn.Plugin.Load(Audio, Setting["Path"])); } catch(Exception ex) { if(ThrowException) throw new HSException(ex); else return false; }
            }
            else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_SETTING_NOTFOUND_ITEM"]); else return false; }

            if (Setting.SubSetting.Exist("DSP")) DSP.LoadSetting(Setting.SubSetting["DSP"]);
            return true;
            //{ if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_SETTING_WRONG_TAG"]); else return false; }
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Type", new SettingsData(Type.ToString()));
            s.SetValue("Name", new SettingsData(Name));
            s.SetValue("Path", new SettingsData(Plugin.Path, false));
            s.SubSetting.SetValue("Plugin", Plugin.SaveSetting(false));
            s.SubSetting.SetValue("DSP", DSP.SaveSetting());
            return s;
        }
    }
    /*
    /// <summary>
    /// Linux LADSPA Plugin
    /// </summary>
    public class DSPPlugin
    {
        uint version;
        uint handle;
        HSAudio Audio;

        public DSPPlugin(HSAudio Value, string Path)
        {
            if (string.IsNullOrEmpty(Path)) throw new NullReferenceException(StringResource.Resource["STR_ERR_NOT_INITIALIZE"]);
            else if (!File.Exists(Path)) throw new FileNotFoundException(StringResource.Resource["STR_ERR_NOT_EXIST"]);
            this.Path = Path;
            this.Audio = Value;
            RESULT rt = Value.system.loadPlugin(Path, out handle);
            if (rt != RESULT.OK) throw new HSAudioException(rt);

            PLUGINTYPE type;
            StringBuilder sb = new StringBuilder(128);
            Value.system.getPluginInfo(Handle, out type, sb, 128, out version);
            Name = sb.ToString();
            
            if(type != PLUGINTYPE.DSP) { Dispose(); throw new HSAudioException(RESULT.ERR_PLUGIN, StringResource.Resource["STR_ERR_NOT_DSPPLUGIN"]); }

            DSP = new DSP(Value, Handle, "DSP Plugin - " + Name, null);
        }

        public DSP DSP { get; private set; }
        public string Path { get; private set; }
        public uint Handle { get { return handle; } }
        //public IntPtr WindowHandle { get { return handle; } }
        public string Name { get; private set; }
        public uint Version { get { return version; } }

        public RESULT ShowConfigDialog() { return DSP.DSPInternal.showConfigDialog(WindowHandle, true); }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            Audio.system.unloadPlugin(Handle);
            IsDisposed = true;
        }
    }
    */
}
#endif