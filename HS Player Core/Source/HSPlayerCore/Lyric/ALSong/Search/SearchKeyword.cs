﻿namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchKeyword
    {
        internal SearchKeyword() { }
        public SearchKeyword(string Title, string Artist)
        {
            this.Title = Title;
            this.Artist = Artist;
        }
        public string Title { get; protected set; }
        public string Artist { get; protected set; }
    }
}