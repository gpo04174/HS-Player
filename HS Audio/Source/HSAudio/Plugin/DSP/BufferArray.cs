﻿using HSAudio.DSP;
using HSAudio.Lib.FMOD;
using HSAudio.Speakers;
using System;
using System.Runtime.InteropServices;

namespace HSAudio.PlugIn.DSP
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BufferArray
    {
        internal int numbuffers;              /* [r/w] number of buffers */
        internal int[] buffernumchannels;       /* [r/w] array of number of channels for each buffer */
        internal ChannelMask[] bufferchannelmask;       /* [r/w] array of channel masks for each buffer */
        internal IntPtr[] buffers;                 /* [r/w] array of buffers */
        internal SPEAKERMODE speakermode;             /* [r/w] speaker mode for all buffers in the array */

        //internal BufferArray(ref BufferArrayInner array) { this.array = array; }
        //internal BufferArrayInner array;

        public IntPtr[] Buffer { get { return buffers; }}
        public int Length { get { return numbuffers; } }
        public int[] BufferLength { get { return buffernumchannels; } }
        public SpeakerMode Speaker { get { return speakermode.ConvertInternal(); } set { speakermode = value.ConvertFMOD(); } }
        public ChannelMask[] Mask { get { return bufferchannelmask; }set { bufferchannelmask = value; } }
    }
}
