﻿namespace HSAudio.Output
{
    public enum OutputType
    {
        /// <summary>
        /// Auto output [Default] (자동 출력 [기본값])
        /// </summary>
        Auto,
        /// <summary>
        /// Don't output [Dummy output] (출력 안함 [가짜출력])
        /// </summary>
        No = Lib.FMOD.OUTPUTTYPE.NOSOUND,
        /// <summary>
        /// Plugin output (플러그인 출력)
        /// </summary>
        Plugin = Lib.FMOD.OUTPUTTYPE.UNKNOWN,
#if WIN_XP || WIN_VISTA || WIN_10_1703
        /// <summary>
        /// Windows Multimedia output [Windows XP Default] (윈도우 Multimedia 출력 [XP 기본값])
        /// </summary>
        WINMM = Lib.FMOD.OUTPUTTYPE.WINMM,
        /// <summary>
        /// WIndows DirectSound output. (윈도우 DirectSound 출력)
        /// </summary>
        DirectSound = Lib.FMOD.OUTPUTTYPE.DSOUND,
        /// <summary>
        /// Windows Low latency ASIO 2.0 output (윈도우 저 지연 ASIO 2.0 출력)
        /// </summary>
        ASIO = Lib.FMOD.OUTPUTTYPE.ASIO,
#endif
#if WIN_VISTA || WIN_10_1703 || WIN_METRO
        /// <summary>
        /// Windows WASAPI output [Vista Higher] (윈도우 WASAPI 츨력 [비스타 이상 기본값])
        /// </summary>
        WASAPI = Lib.FMOD.OUTPUTTYPE.WASAPI,
#endif
#if WIN_10_1703 || XBOX
        /// <summary>
        /// Windows Sonic output [Windows 10 build 1703 Higher or XBOX] (Sonic 출력 [윈도우 10 빌드 1703 이상 또는 XBOX])
        /// </summary>
        SONIC = Lib.FMOD.OUTPUTTYPE.WINSONIC,
        /// <summary>
        /// Dolby Atmos output [Windows 10 build 1703 Higher or XBOX] (돌비 애트모스 출력 [윈도우 10 빌드 1703 이상 또는 XBOX])
        /// </summary>
        DolbyAtmos = Lib.FMOD.OUTPUTTYPE.ATMOS,
#endif
#if LINUX
        /// <summary>
        /// Linux Pulse Audio [Linux Default] (리눅스 Pulse Audio [리눅스 기본값])
        /// </summary>
        PulseAudio = Lib.FMOD.OUTPUTTYPE.PULSEAUDIO,
        /// <summary>
        /// Linux Advanced Linux Sound Architecture output (리눅스 고급 소리 아키텍처 출력)
        /// </summary>
        ALSA = Lib.FMOD.OUTPUTTYPE.ALSA,
#endif
#if ANDROID
        OpenSL = Lib.FMOD.OUTPUTTYPE.OPENSL,
        AudioTrack = Lib.FMOD.OUTPUTTYPE.AUDIOTRACK
#endif
    }
}
