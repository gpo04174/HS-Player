﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class Pitchshift : IDSP
    {
        public enum PitchshiftQuality { VeryLow = 256, Low = 512, Middle = 1024, High = 2048, VeryHigh = 4096 }

        private Parameter[] param = new Parameter[4];
        public Pitchshift(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.PITCHSHIFT, "Pitchshift", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Pitch value.  0.5 to 2.0.  Default = 1.0. 0.5 = one octave down, 2.0 = one octave up.  1.0 does not change the pitch
        /// </summary>
        public float Pitch
        {
            get { return DSP.GetParameter((int)DSP_PITCHSHIFT.PITCH, 0f); }
            set { DSP.SetParameter((int)DSP_PITCHSHIFT.PITCH, value); }
        }
        /// <summary>
        /// Pitchshift quility (FFT window size). Default = Middle. Increase this to reduce 'smearing'.  This effect is a warbling sound similar to when an mp3 is encoded at very low bitrates
        /// </summary>
        public PitchshiftQuality Quality
        {
            get { return (PitchshiftQuality)DSP.GetParameter((int)DSP_PITCHSHIFT.FFTSIZE, 0f); }
            set { DSP.SetParameter((int)DSP_PITCHSHIFT.FFTSIZE, (float)value); }
        }
        /// <summary>
        /// Window overlap.  1 to 32.  Default = 4.  Increase this to reduce 'tremolo' effect.  Increasing it by a factor of 2 doubles the CPU usage.
        /// </summary>
        public float Overlap
        {
            get { return DSP.GetParameter((int)DSP_PITCHSHIFT.OVERLAP, 0f); }
            set { DSP.SetParameter((int)DSP_PITCHSHIFT.OVERLAP, value); }
        }
        /// <summary>
        ///  Maximum channels supported.  0 to 16.  0 = same as default output polyphony, 1 = mono, 2 = stereo etc.  See remarks for more.  Default = 0.  It is suggested to leave at 0!
        /// </summary>
        public int MaxChannel
        {
            get { return (int)DSP.GetParameter((int)DSP_PITCHSHIFT.MAXCHANNELS, 0f); }
            set { DSP.SetParameter((int)DSP_PITCHSHIFT.MAXCHANNELS, (float)value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Pitch")) try { Pitch = float.Parse(Setting["Pitch"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Quality")) try { Quality = (PitchshiftQuality)Enum.Parse(typeof(PitchshiftQuality), Setting["Quality"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Overlap")) try { Overlap = float.Parse(Setting["Overlap"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("MaxChannel")) try { MaxChannel = int.Parse(Setting["MaxChannel"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Pitch", Pitch);
            s.SetValue("Quality", Quality.ToString());
            s.SetValue("Overlap", Overlap);
            s.SetValue("MaxChannel", MaxChannel);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
