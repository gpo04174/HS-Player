﻿using HSAudio.Lib.FMOD;

namespace HSAudio.PlugIn.DSP
{
    public delegate void ReadEventHandler(object sender, ReadEventArgs e);
    public delegate void ParameterChangedEventHandler(object sender, ParameterEventArgs e);
    public delegate void ProcessEventHandler(object sender, ProcessEventArgs e);
    public delegate void ObjectEventHandler(object sender);
    public delegate void MixChangedEventHandler(object sender, MixEventArgs e);
    public delegate void PositionChangedEventHandler(object sender, PositionEventArgs e);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <returns>True is don't call Read callback (True 면 Read callback을 호출하지 않습니다.)</returns>
    public delegate bool Should_I_ProcesEventHandlers(object sender, Should_I_ProcesEventArgs e);
}
