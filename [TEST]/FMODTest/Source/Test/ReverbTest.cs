﻿using HSAudio.Lib.FMOD;
using System;
using System.Threading;

namespace FMODTest.Test
{
    public class ReverbTest
    {
        public static void Test(string MusicPath)
        {
            RESULT result = RESULT.OK;
            FD_System system;
            FD_Sound sound, sound1;
            FD_Channel channel, channel1;
            FD_ChannelGroup cg, cg1;
            FD_SoundGroup sg;
            FD_DSP dsp;
            Thread th = new Thread(Loop);

            Factory.System_Create(out system);
            th.Start(system);

            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            system.getMasterChannelGroup(out cg);
            //result = system.createChannelGroup("Temp", out cg);
            result = system.createChannelGroup("Temp1", out cg1);
            result = system.createSoundGroup("Temp", out sg);

            result = system.createStream(MusicPath, MODE._3D, out sound);

            //SoundTag st = SoundTag.GetTag(sound);
            result = system.playSound(sound, null, false, out channel);
            channel.setCallback(new CHANNEL_CALLBACK(CHANNEL_CALLBACK));
            cg1.setCallback(new CHANNEL_CALLBACK(CHANNELGROUP_CALLBACK));


            int numch, numgp;
            cg.getNumGroups(out numgp);
            cg.getNumChannels(out numch);

            REVERB_PROPERTIES rev = PRESET.HANGAR();
            REVERB_PROPERTIES off = PRESET.OFF();
            rev.WetLevel = -8;
            result = system.setReverbProperties(3, ref rev);
            //result = system.setReverbProperties(3, ref off);
            //result = cg.setReverbProperties(3, 0.5f);

            result = system.createDSPByType(DSP_TYPE.THREE_EQ, out dsp);
            result = dsp.setParameterFloat((int)DSP_THREE_EQ.LOWGAIN, 10f);
            cg1.addDSP(0, dsp);

            //channel.setPaused(false);

            OPENSTATE state; uint percent; bool starving; bool diskbusy;
            result = sound.getOpenState(out state, out percent, out starving, out diskbusy);
            result = cg1.setPitch(1f);
            result = cg1.setPan(-0.5f);
            //result = channel.setFrequency(44100);
            
            uint len;
            result = sound.getLength(out len, TIMEUNIT.MS);
            result = channel.setPosition(len - 10000, TIMEUNIT.MS);
            while (true)
            {
                Thread.Sleep(1);
                bool isplaying;
                result = channel.isPlaying(out isplaying);
                if(!isplaying)
                {
                    int num;
                    //result = sound.release();
                    result = cg1.getNumChannels(out num);
                    result = system.playSound(sound, cg1, false, out channel);
                }
            }
        }

        protected static RESULT CHANNEL_CALLBACK(IntPtr channelraw, CHANNELCONTROL_TYPE controltype, CHANNELCONTROL_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            RESULT Result;
            if (type == CHANNELCONTROL_CALLBACK_TYPE.END)
            {
            }
            return RESULT.OK;
        }
        protected static RESULT CHANNELGROUP_CALLBACK(IntPtr channelraw, CHANNELCONTROL_TYPE controltype, CHANNELCONTROL_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            RESULT Result;
            if (type == CHANNELCONTROL_CALLBACK_TYPE.END)
            {
            }
            return RESULT.OK;
        }

        static void Loop(object o)
        {
            FD_System system = o as FD_System;
            RESULT Result = RESULT.OK;
            while(system != null)
            {
                Result = system.update();
                if (Result != RESULT.OK) Console.WriteLine("Fail({0}) - System::Update()", Result);
                Thread.Sleep(1);
            }
        }
    }
}
