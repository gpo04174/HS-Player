﻿#if FUNC_PLUGIN
using HSAudio.Lib.FMOD;
using HSAudio.PlugIn.DSP;
using HSAudio.Resource;
using HS.PlugIn;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using HS;

namespace HSAudio.PlugIn
{
    //public enum PluginType {Internal, HSAudio, Integrate }
    public enum PluginKind { Unknown, DSP = PLUGINTYPE.DSP, Codec = PLUGINTYPE.CODEC, Output = PLUGINTYPE.OUTPUT }
    public class Plugin : ISettingManager
    {
        internal Plugin(HSAudio Audio, PluginKind Kind, uint Handle, string Name, string Version) { this.Audio = Audio; this.Handle = Handle; this.Kind = Kind; this.Name = Name; this.Name = Name; this.Version = Version; }
        internal Plugin(HSAudio Audio, PluginPack pack, PluginKind Kind, uint Handle, string Name, string Version) { this.Audio = Audio; this.Kind = Kind; this.Handle = Handle; this.Name = Name; this.Name = Name; this.Version = Version; this.Pack = pack; }

        public static PluginKind Check(HSAudio Audio, string Path)
        {
            if (!File.Exists(Path)) throw new HSException(StringResource.Resource["STR_ERR_NOT_FOUND"]);
            else
            {
                PluginKind kind = PlugInHelper.PluginCheck(Path);
                if (kind == PluginKind.Unknown)
                {
                    uint h = 0;
                    RESULT result = Audio.system_sub.loadPlugin(Path, out h);
                    if (result == RESULT.OK)
                    {
                        PLUGINTYPE t = PLUGINTYPE.MAX;
                        StringBuilder name = new StringBuilder();
                        uint ver = 0;
                        Audio.system_sub.getPluginInfo(h, out t, name, 256, out ver);
                        Audio.system_sub.unloadPlugin(h);
                        return (PluginKind)t;
                    }
                    else throw new HSAudioException(result);
                }
                else return kind;
            }
        }
        public static Plugin Load(HSAudio Audio, string Path) { PluginKind Type; return Load(Audio, Path, out Type); }
        public static Plugin Load(HSAudio Audio, string Path, out PluginKind Type)
        {
            if (string.IsNullOrEmpty(Path)) throw new NullReferenceException(StringResource.Resource["STR_ERR_NOT_INITIALIZE"]);
            else if (!File.Exists(Path)) throw new FileNotFoundException(StringResource.Resource["STR_ERR_NOT_EXIST"]);

            RESULT result = RESULT.OK;
            Type = PluginKind.Unknown;
            uint h = 0;
            PluginPack pack = PlugInHelper.PluginLoader(Path);
            if (pack == null)
            {
                result = Audio.system.loadPlugin(Path, out h);
                if (result != RESULT.OK) throw new HSAudioException(result);
                else
                {
                    PLUGINTYPE t = PLUGINTYPE.MAX;
                    StringBuilder name = new StringBuilder();
                    uint ver = 0;
                    Audio.system.getPluginInfo(h, out t, name, 256, out ver);

                    if (t == PLUGINTYPE.DSP) return new Plugin_DSP(Audio, h, Path, name.ToString(), ver.ToString()) { Path = Path };
                    else if (t == PLUGINTYPE.CODEC) return new Plugin_Codec(Audio, h, Path, name.ToString(), ver.ToString()) { Path = Path };
                    else if (t == PLUGINTYPE.OUTPUT) return new Plugin_Output(Audio, h, Path, name.ToString(), ver.ToString()) { Path = Path };
                    else return null;
                }
            }
            else
            {
                IPluginInformation ip = pack.Base.Information;
                if (pack is IHSAudioOutputPlugin)
                {
                    Output.Description desc = ((IHSAudioOutputPlugin)pack.Base).GetDescription();
                    desc._Description.apiversion = 3;
                    result = Audio.system.registerOutput(ref desc._Description, out h);

                    if (result != RESULT.OK) throw new HSAudioException(result);
                    else return new Plugin_Output(Audio, h, Path, ip.Name, ip.Version) { Path = Path };
                }
#if FUNC_PLUGIN_CODEC
                else if (pack is IHSAudioCodecPlugin)
                {
                    Codec.Description desc = ((IHSAudioCodecPlugin)pack.Base).GetDescription();
                    result = Audio.system.registerCodec(ref desc._Description, out h);

                    if (result != RESULT.OK) throw new HSAudioException(result);
                    else return new Plugin_Codec(Audio, h, Path, ip.Name, ip.Version) { Path = Path };
                }
#endif
                else if (pack is IHSAudioDSPPlugin)
                {
                    Description desc = ((IHSAudioDSPPlugin)pack.Base).GetDescription();
                    result = Audio.system.registerDSP(ref desc._Description, out h);
                    
                    if (result != RESULT.OK) throw new HSAudioException(result);
                    else return new Plugin_DSP(Audio, h, Path, ip.Name, ip.Version) { Path = Path };
                }
                else return null;
            }
        }
        internal uint Handle { get; private set; }

        public HSAudio Audio { get; private set; }
        public PluginKind Kind{ get; private set; }
        public string Path { get; private set; }
        public object Instance { get; private set; }
        public string Name { get; internal set; }
        public string Version { get; private set; }
        internal PluginPack Pack { get; private set; }

        public bool IsHSAudioPlugin {get { return Pack != null; } }

        public Settings SaveSetting() { return SaveSetting(true); }
        public Settings SaveSetting(bool IncludePath)
        {
            Settings s = new Settings();
            s.SetValue("Type", new SettingsData(Kind.ToString()));
            s.SetValue("Name", new SettingsData(Name.ToString()));
            s.SetValue("Version", new SettingsData(Version.ToString()));
            if(IncludePath) s.SetValue("Path", new SettingsData(Path, false));
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new HSException(StringResource.Resource["STR_ERR_SETTING_NOTSUPPORT_LOAD"]);
        }

        public bool IsDisposed { get; private set; }
        internal protected void Dispose()
        {
            Audio.system_sub.unloadPlugin(Handle);
            if (Pack != null) Pack.Dispose();
            IsDisposed = true;
        }
    }
    
    public class Plugin_DSP : Plugin
    {
        internal Plugin_DSP(HSAudio Audio, PluginPack pack, uint Handle, string Path, string Name, string Version) : base(Audio, pack, PluginKind.DSP, Handle, Name, Version) { }
        internal Plugin_DSP(HSAudio Audio, uint Handle, string Path, string Name, string Version) : base(Audio, PluginKind.DSP, Handle, Name.Substring(Name.IndexOf(" ") + 1), Version) { }

        public new Settings SaveSetting(){ return base.SaveSetting().SetValue("Kind", new SettingsData(Kind.ToString()));}
    }
    public class Plugin_Codec : Plugin
    {
        internal Plugin_Codec(HSAudio Audio, PluginPack pack, uint Handle, string Path, string Name, string Version) : base(Audio, pack, PluginKind.DSP, Handle, Name, Version) { }
        internal Plugin_Codec(HSAudio Audio, uint Handle, string Path, string Name, string Version) : base(Audio, PluginKind.DSP, Handle, Name, Version) { }
    }
    public class Plugin_Output : Plugin
    {
        internal Plugin_Output(HSAudio Audio, PluginPack pack, uint Handle, string Path, string Name, string Version) : base(Audio, pack, PluginKind.DSP, Handle, Name, Version) { }
        internal Plugin_Output(HSAudio Audio, uint Handle, string Path, string Name, string Version) : base(Audio, PluginKind.DSP, Handle, Name, Version) { }
    }
    
}
#endif