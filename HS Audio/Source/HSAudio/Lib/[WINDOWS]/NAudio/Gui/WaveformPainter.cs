﻿#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace HSAudio.Lib.Windows.NAudio.Gui
{
    /// <summary>
    /// Windows Forms control for painting audio waveforms
    /// </summary>
    public partial class WaveformPainter : System.Windows.Forms.Control
    {
        Pen foregroundPen;
        List<float> _samples = new List<float>(1000);
        int maxSamples;
        int insertPos;

        /// <summary>
        /// Constructs a new instance of the WaveFormPainter class
        /// </summary>
        public WaveformPainter()
        {
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint |
                ControlStyles.OptimizedDoubleBuffer, true);
            InitializeComponent();
            OnForeColorChanged(EventArgs.Empty);
            OnResize(EventArgs.Empty);
        }

        /// <summary>
        /// On Resize
        /// </summary>
        protected override void OnResize(EventArgs e)
        {
            maxSamples = this.Width;
            pt = new PointF[this.Width];
            base.OnResize(e);
        }

        /// <summary>
        /// On ForeColor Changed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnForeColorChanged(EventArgs e)
        {
            foregroundPen = new Pen(ForeColor);
            base.OnForeColorChanged(e);
        }

        /// <summary>
        /// Add Max Value
        /// </summary>
        /// <param name="maxSample"></param>
        public void AddMax(float maxSample)
        {
            if (maxSamples == 0)
            {
                // sometimes when you minimise, max samples can be set to 0
                return;
            }
            if (_samples.Count <= maxSamples)
            {
                _samples.Add(maxSample);
            }
            else if (insertPos < maxSamples)
            {
                _samples[insertPos] = maxSample;
            }
            insertPos++;
            insertPos %= maxSamples;
            
            this.Invalidate();
        }

        PointF[] pt;
        int Position;
        /// <summary>
        /// On Paint
        /// </summary>
        protected override void OnPaint(PaintEventArgs pe)
        {	
            /*
            base.OnPaint(pe);
            for (int x = 0; x < pt.Length; x++)
            {
                float lineHeight = this.Height * GetSample(x - this.Width + insertPos);
                float y1 = (this.Height - lineHeight) / 2;
                pt[x].X=x;pt[x].Y = y1;
            }
            pe.Graphics.DrawLines(foregroundPen,pt);*/
            
            for (int x = 0; x < this.Width; x++)
            {
                float lineHeight = this.Height * GetSample(x - this.Width + insertPos);
                float y1 = (this.Height - lineHeight) / 2;
                pe.Graphics.DrawLine(foregroundPen, x, y1, x, y1 + lineHeight);
            }
                
        }

        float GetSample(int index)
        {
            if (index < 0)
                index += maxSamples;
            if (index >= 0 & index < _samples.Count)
                return _samples[index];
            return 0;
        }

        public int Sample
        {
            get{return _samples.Count;}
            set{if(value<1)return; _samples.Clear(); _samples = new List<float>(value);}
        }
    }
}
#endif