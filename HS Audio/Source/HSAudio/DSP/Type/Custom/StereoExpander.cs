﻿using HSAudio.PlugIn.DSP;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using HSAudio.Lib.FMOD;

namespace HSAudio.DSP.Type
{
    public class StereoExpander : CustomDSP
    {
        float StereoExpandSideValue = 0.5f;
        float StereoExpandMidValue = 0.5f;
        float StereoExpandRatio = 1.414213562373095f;
        float StereoExpandMix = 1f;

        public float Side { get { return StereoExpandSideValue; }set { StereoExpandSideValue = value; } }
        public float Mid { get { return StereoExpandMidValue; } set { StereoExpandMidValue = value; } }
        public float Ratio { get { return StereoExpandRatio; } set { StereoExpandRatio = value; } }
        public float Mix { get { return StereoExpandMix; } set { StereoExpandMix = value; } }

        public StereoExpander(HSAudio Audio) : base(Audio, 5, "StereoExpander", 1)
        {
            //Callback += StereoExpander_Callback;
            Description.ParameterGet += Description_ParameterGetChanged;
            Description.ParameterSet += Description_ParameterSetChanged;
            //Description.Processing += Description_Processing;
        }

        protected override bool ReadCallback(object sender, DSPCallBackEventArgs e)
        //protected override RESULT OnReadCallback(ref DSP_STATE dsp_state, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, ref int outchannels)
        {
            unsafe
            {
                float* inbuffer = (float*)e.InBuffer.ToPointer();
                float* outbuffer = (float*)e.OutBuffer.ToPointer();
                HSDSP.StereoExpand(inbuffer, outbuffer, e.Length, (uint)e.InChannel, Side, Mid, 1, Mix, Ratio);
                return true;
                //return RESULT.OK;
            }
        }

        private void Description_ParameterSetChanged(object sender, ParameterEventArgs e)
        {
            /*
            if (e.Index == 0) StereoExpandSideValue = (float)e.Value;
            else if (e.Index == 1) StereoExpandMidValue = (float)e.Value;
            else if (e.Index == 2) StereoExpandRatio = (float)e.Value;
            else if (e.Index == 3) StereoExpandMix = (float)e.Value;
            */

            if (e.Index == 0) StereoExpandSideValue = e.Value_Float;
            else if (e.Index == 1) StereoExpandMidValue = e.Value_Float;
            else if (e.Index == 2) StereoExpandRatio = e.Value_Float;
            else if (e.Index == 3) StereoExpandMix = e.Value_Float;
        }

        private void Description_ParameterGetChanged(object sender, ParameterEventArgs e)
        {
            e.Kind = DSPParameterType.Float;
            e.Length = 4;
            if (e.Index == 0)
            {
                e.Value_Float = StereoExpandSideValue;
                e.ValueString = StereoExpandSideValue.ToString();
            }
            else if (e.Index == 1)
            {
                e.Value_Float = StereoExpandMidValue;
                e.ValueString = StereoExpandMidValue.ToString();
            }
            else if (e.Index == 2)
            {
                e.Value_Float = StereoExpandRatio;
                e.ValueString = StereoExpandRatio.ToString();
            }
            else if (e.Index == 3)
            {
                e.Value_Float = StereoExpandMix;
                e.ValueString = StereoExpandMix.ToString();
            }
        }

        public override Settings SaveSetting()
        {
            if (IsDisposed) return null;
            Settings s = new Settings();
            s.SetValue("Name", new SettingsData("StereoExpander"));
            //s.SetValue("Side", new SettingsData(Side.ToString(), false));
            //s.SetValue("Ratio", new SettingsData(Side.ToString(), false));
            //s.SetValue("Mix", new SettingsData(Side.ToString(), false));
            s.SubSetting.SetValue("DSP", DSP.SaveSetting());
            return s;
        }

        public override bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.SubSetting.Exist("DSP")) DSP.LoadSetting(Setting.SubSetting["DSP"]);
            return true;
        }
    }
}
