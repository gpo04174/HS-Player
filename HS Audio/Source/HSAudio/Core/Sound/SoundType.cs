﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound
{
    public enum SoundType
    {
        /// <summary>
        /// MPEG Layer N (*.mp1, *.mp2, *.mp3)
        /// </summary>
        MPEG = SOUND_TYPE.MPEG,
        /// <summary>
        /// Free lossless codec (*.flac)
        /// </summary>
        FLAC = SOUND_TYPE.FLAC,
        /// <summary>
        /// Vorbis (*.ogg, *.oga)
        /// </summary>
        OGG = SOUND_TYPE.OGGVORBIS,
        /// <summary>
        /// Microsoft Advanced Systems Format (*.wma, *.asf, *.wmv)
        /// </summary>
        ASF = SOUND_TYPE.ASF,
        /// <summary>
        /// Musical Instrument Digital Interface (*.midi)
        /// </summary>
        MIDI = SOUND_TYPE.MIDI,
        /// <summary>
        /// Microsoft wave (*.wav)
        /// </summary>
        Wave = SOUND_TYPE.WAV,
        /// <summary>
        /// Audio Interchange File Format (*.aiff, *.aif, *.aifc)
        /// </summary>
        AIFF = SOUND_TYPE.AIFF,
        /// <summary>
        /// FastTracker 2 (*.xm)
        /// </summary>
        XM = SOUND_TYPE.XM,
        /// <summary>
        /// Protracker / Fasttracker MOD (*.mod)
        /// </summary>
        MOD = SOUND_TYPE.MOD,
        /// <summary>
        /// Impulse Tracker (*.it)
        /// </summary>
        IT = SOUND_TYPE.IT,
        /// <summary>
        /// Raw sound (*.raw, *.*)
        /// </summary>
        RAW = SOUND_TYPE.RAW,
        /// <summary>
        /// FMOD Sample Bank (*.fsb)
        /// </summary>
        FSB = SOUND_TYPE.FSB,
#if XBOX
        /// <summary>
        /// XBOX 360 Audio
        /// </summary>
        XMA = SOUND_TYPE.XMA,
#endif
        /// <summary>
        /// User sound
        /// </summary>
        Codec = SOUND_TYPE.USER
    }
}
