﻿using HSAudio.Lib.FMOD;
using System;
using System.Text;

namespace FMODTest.Test
{
    public class HSAudioPluginTest
    {
        public static void Test()
        {
            DSP_DESCRIPTION desc = new DSP_DESCRIPTION();
            string Name = "Test";
            desc.name = Name.Length > 32 ? Name.ToCharArray(0, 32) : Name.ToCharArray();

            FD_System system;
            FD_DSP dsp;

            Factory.System_Create(out system);

            RESULT result = RESULT.OK;
            result = system.init(32, INITFLAGS.NORMAL, IntPtr.Zero);
            uint h;
            //result = system.createDSP(ref desc, out dsp);
            result = system.registerDSP(ref desc, out h);
            result = system.createDSPByPlugin(h, out dsp);
            StringBuilder name = new StringBuilder(256);
            uint version;
            int channel;
            int width;
            int height;
            result = dsp.getInfo(name, out version, out channel, out width, out height);
            DSP_TYPE type;
            dsp.getType(out type);
        }
    }
}
