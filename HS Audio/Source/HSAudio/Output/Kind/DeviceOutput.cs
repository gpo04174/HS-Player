﻿using HS.Setting;

namespace HSAudio.Output.Kind
{
    /// <summary>
    /// Device output (장치 출력)
    /// </summary>
    public abstract class DeviceOutput : OutputMethod
    {
        internal DeviceOutput() { }

        public abstract bool Equals(DeviceOutput other);
        public override bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.SubSetting.Exist("Format")) { Format = new OutputFormat(); Format.LoadSetting(Setting.SubSetting["Format"], ThrowException); }
            return true;
        }
    }
}
