﻿using System;
using System.Windows;
using System.Windows.Controls;
using HSAudioControlDesktop.Control.Metro;

namespace HSAudioControlDesktop.Control
{
    class HSListView : HSItemView
    {
        protected override void UpdateGrid(Size size)
        {
            if (!StopUpdate && size.Width > 0 && size.Height > 0)
            {
                Size colrow = new Size(ActualColumns, ActualRows);
                int col = 1, row = 1;
                if ((Rows == 0 || Columns == 0))
                {
                    double MarginW = 1, MarginH = 1;
                    for (int i = 0; i < RootGrid.Children.Count; i++)
                    {
                        HSCollectionViewItem view = RootGrid.Children[i] as HSCollectionViewItem;
                        //view.Width = size.Width;
                        if (Orientation == Orientation.Horizontal)view.Margin = new Thickness(0, MarginH, MarginW, MarginH);
                        else view.Margin = new Thickness(MarginW, 0, MarginW, 0);
                    }

                    if (Orientation == Orientation.Horizontal) row = RootGrid.Children.Count;
                    else col = RootGrid.Children.Count;
                }
                else { col = Columns; row = Rows; }
                ActualColumns = col;
                ActualRows = row;
                //SetColRow(colrow, new Size(ActualColumns, ActualRows))
                SetColRow(new Size(ActualColumns, ActualRows),  GridUnitType.Star, AutoArrange ? GridUnitType.Star : GridUnitType.Auto);
                UpdateGrided = true;
            }
        }
    }
}
