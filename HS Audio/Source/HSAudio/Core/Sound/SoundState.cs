﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound
{
    public enum SoundState
    {
        Unknown,
        OK,
        Buffering,
        Loading,
        Connecting,
        Unload
    }
}
