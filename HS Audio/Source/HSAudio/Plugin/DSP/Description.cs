﻿using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.PlugIn.DSP
{
    public class Description// : ISettingXML
    {
        public static Description Empty() { return new Description(); }

        internal DSP_DESCRIPTION _Description;

        internal void Register()
        {
            if (ParameterGet != null)
            {
                _Description.getparameterfloat = new DSP_GETPARAM_FLOAT_CALLBACK(DSP_GETPARAM_FLOAT_CALLBACK);
                _Description.getparameterint = new DSP_GETPARAM_INT_CALLBACK(DSP_GETPARAM_INT_CALLBACK);
                _Description.getparameterbool = new DSP_GETPARAM_BOOL_CALLBACK(DSP_GETPARAM_BOOL_CALLBACK);
                _Description.getparameterdata = new DSP_GETPARAM_DATA_CALLBACK(DSP_GETPARAM_DATA_CALLBACK);
            }

            if (ParameterSet != null)
            {
                _Description.setparameterfloat = new DSP_SETPARAM_FLOAT_CALLBACK(DSP_SETPARAM_FLOAT_CALLBACK);
                _Description.setparameterint = new DSP_SETPARAM_INT_CALLBACK(DSP_SETPARAM_INT_CALLBACK);
                _Description.setparameterbool = new DSP_SETPARAM_BOOL_CALLBACK(DSP_SETPARAM_BOOL_CALLBACK);
                _Description.setparameterdata = new DSP_SETPARAM_DATA_CALLBACK(DSP_SETPARAM_DATA_CALLBACK);
            }

            if (Process != null) _Description.process = new DSP_PROCESS_CALLBACK(DSP_PROCESS_CALLBACK);

            if (Created != null) _Description.create = new DSP_CREATECALLBACK(DSP_CREATECALLBACK);
            if (Reseted != null) _Description.release = new DSP_RELEASECALLBACK(DSP_RELEASECALLBACK);
            if (Released != null) _Description.reset = new DSP_RESETCALLBACK(DSP_RESETCALLBACK);
            if (Regestered != null) _Description.sys_register = new DSP_SYSTEM_REGISTER_CALLBACK(DSP_SYSTEM_REGISTER_CALLBACK);
            if (UnRegestered != null) _Description.sys_deregister = new DSP_SYSTEM_DEREGISTER_CALLBACK(DSP_SYSTEM_DEREGISTER_CALLBACK);

            if (PositionChanged != null) _Description.setposition = new DSP_SETPOSITIONCALLBACK(DSP_SETPOSITIONCALLBACK);
            if (MixChanged != null) _Description.sys_mix = new DSP_SYSTEM_MIX_CALLBACK(DSP_SYSTEM_MIX_CALLBACK);

            if (Should_I_Proces != null) _Description.shouldiprocess = new DSP_SHOULDIPROCESS_CALLBACK(DSP_SHOULDIPROCESS_CALLBACK);
        }

        #region Event
        public event ReadEventHandler Read;

        public event ParameterChangedEventHandler ParameterGet;
        public event ParameterChangedEventHandler ParameterSet;
        [Obsolete]
        internal event ProcessEventHandler Process;

        public event ObjectEventHandler Created;
        public event ObjectEventHandler Reseted;
        public event ObjectEventHandler Released;
        public event ObjectEventHandler Regestered;
        public event ObjectEventHandler UnRegestered;

        public event MixChangedEventHandler MixChanged;
        public event PositionChangedEventHandler PositionChanged;
        
        public event Should_I_ProcesEventHandlers Should_I_Proces;
        #endregion

        #region Init
        protected Description(string Name = null, uint Version = 0) { }
        internal Description(DSP_READCALLBACK ReadCallBack, int ParameterCount, string Name, uint Version)
        {
            Init(Name, Version, ParameterCount);
            _Description.read = new DSP_READCALLBACK(ReadCallBack);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Read"></param>
        /// <param name="ParameterCount"></param>
        /// <param name="Name">Fixed 32 Word</param>
        /// <param name="Version"></param>
        [Obsolete]
        public Description(string Name, uint Version, int ParameterCount, ReadEventHandler Read)
        {
            this.Read = Read;
            _Description.read = new DSP_READCALLBACK(DSP_READCALLBACK);
            Init(Name, Version, ParameterCount);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Read"></param>
        /// <param name="ParameterCount"></param>
        /// <param name="Name">Fixed 32 Word</param>
        /// <param name="Version"></param>
        public Description(string Name, uint Version, int ParameterCount, ReadEventHandler Read, ParameterChangedEventHandler ParameterGet, ParameterChangedEventHandler ParameterSet)
        {
            this.ParameterGet = ParameterGet;
            this.ParameterSet = ParameterSet;
            this.Read = Read;
            _Description.read = new DSP_READCALLBACK(DSP_READCALLBACK);
            Init(Name, Version, ParameterCount);
        }
        private void Init(string Name, uint Version, int ParameterCount)
        {
            _Description = new DSP_DESCRIPTION();
            if (Name != null)
            {
                //char[] nm = new char[32];
                //char[] n = Name.ToCharArray();
                //for (int i = 0; i < Math.Min(n.Length, 32); i++) nm[i] = n[i];
                _Description.name = Name.Length > 32 ? Name.ToCharArray(0, 32) : Name.ToCharArray();
            }

            this.Version = Version;
            this.Name = Name;
            _Description.version = Version;
            _Description.numparameters = this.ParameterCount = ParameterCount;

            //_DSPDescription.read = new DSP_READCALLBACK(DSP_READCALLBACK);
        }
        #endregion

        #region Properties
        public string Name { get; private set; }
        public uint Version { get; private set; }
        public int ParameterCount { get; private set; }
        #endregion

        #region ParameterGetChanged
        private RESULT DSP_GETPARAM_FLOAT_CALLBACK(ref DSP_STATE dsp_state, int index, ref float value, IntPtr valuestr)
        {
            if (ParameterGet == null || ParameterGet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value, valuestr);
                ParameterGet(this, e);
                dsp_state = e.State;
                //value = (float)e.Value;
                value = e.Value_Float;
                e.setValueString(ref valuestr);
                return RESULT.OK;
            }
        }
        private RESULT DSP_GETPARAM_INT_CALLBACK(ref DSP_STATE dsp_state, int index, ref int value, IntPtr valuestr)
        {
            if (ParameterGet == null || ParameterGet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value, valuestr);
                ParameterGet(this, e);
                dsp_state = e.State;
                //value = (int)e.Value;
                value = e.Value_Int;
                e.setValueString(ref valuestr);
                return RESULT.OK;
            }
        }
        private RESULT DSP_GETPARAM_BOOL_CALLBACK(ref DSP_STATE dsp_state, int index, ref bool value, IntPtr valuestr)
        {
            if (ParameterGet == null || ParameterGet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value, valuestr);
                ParameterGet(this, e);
                dsp_state = e.State;
                //value = (bool)e.Value;
                value = e.Value_Bool;
                e.setValueString(ref valuestr);
                return RESULT.OK;
            }
        }
        private RESULT DSP_GETPARAM_DATA_CALLBACK(ref DSP_STATE dsp_state, int index, ref IntPtr value, ref uint length, IntPtr valuestr)
        {
            if (ParameterGet == null || ParameterGet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value, length, valuestr);
                ParameterGet(this, e);
                dsp_state = e.State;
                length = e.Value_Data_Length;
                value = e.Value_Data;
                e.setValueString(ref valuestr);

                /*
                try
                {
                    //IntPtr 수정바람.
                    byte[] array = (byte[])e.Value;
                    int size = Marshal.SizeOf(array[0]) * array.Length;
                    value = Marshal.AllocHGlobal(size);
                    Marshal.Copy(array, 0, value, array.Length);
                }
                catch { }
                */

                return RESULT.OK;
            }
        }
        #endregion

        #region ParameterSetChanged
        private RESULT DSP_SETPARAM_FLOAT_CALLBACK(ref DSP_STATE dsp_state, int index,  float value)
        {
            if (ParameterSet == null || ParameterSet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value);
                ParameterSet(this, e);
                dsp_state = e.State;
                return RESULT.OK;
            }
        }
        private RESULT DSP_SETPARAM_INT_CALLBACK(ref DSP_STATE dsp_state, int index, int value)
        {
            if (ParameterSet == null || ParameterSet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value);
                ParameterSet(this, e);
                dsp_state = e.State;
                return RESULT.OK;
            }
        }
        private RESULT DSP_SETPARAM_BOOL_CALLBACK(ref DSP_STATE dsp_state, int index, bool value)
        {
            if (ParameterSet == null || ParameterSet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value);
                ParameterSet(this, e);
                dsp_state = e.State;
                return RESULT.OK;
            }
        }
        private RESULT DSP_SETPARAM_DATA_CALLBACK(ref DSP_STATE dsp_state, int index, IntPtr value, uint length)
        {
            if (ParameterSet == null || ParameterSet.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ParameterEventArgs e = new ParameterEventArgs(ref dsp_state, index, value, length);
                ParameterSet(this, e);
                dsp_state = e.State;
                return RESULT.OK;
            }
        }
        #endregion

        #region Other Handler

        private ReadEventArgs read_e = new ReadEventArgs();
        private RESULT DSP_READCALLBACK(ref DSP_STATE dsp_state, IntPtr inbuffer, IntPtr outbuffer, uint length, int inchannels, ref int outchannels)
        {
            if (Read == null || Read.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                //ReadEventArgs e = new ReadEventArgs(ref dsp_state, inbuffer, outbuffer, length, inchannels, outchannels);
                read_e.Set(ref dsp_state, inbuffer, outbuffer, length, inchannels, outchannels);
                Read(this, read_e);
                dsp_state = read_e.State;
                return read_e.Cancel ? RESULT.ERR_DSP_DONTPROCESS : RESULT.OK;
            }
        }

        private RESULT DSP_PROCESS_CALLBACK(ref DSP_STATE dsp_state, uint length, ref BufferArray inbufferarray, ref BufferArray outbufferarray, bool inputsidle, DSP_PROCESS_OPERATION op)
        {
            if (Process == null || Process.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                ProcessEventArgs e = new ProcessEventArgs(ref dsp_state, length, ref inbufferarray, ref outbufferarray, inputsidle, op);
                Process(this, e);
                dsp_state = e.State; inbufferarray = e.Input; outbufferarray = e.Output;
                return e.Cancel ? RESULT.ERR_DSP_DONTPROCESS : RESULT.OK;
            }
        }

        private RESULT DSP_CREATECALLBACK(ref DSP_STATE dsp_state) { if (Created == null || Created.Target == null) Created(this); return RESULT.OK; }
        private RESULT DSP_RELEASECALLBACK(ref DSP_STATE dsp_state) { if (Released == null || Released.Target == null) Released(this); return RESULT.OK; }
        private RESULT DSP_RESETCALLBACK(ref DSP_STATE dsp_state) { if (Reseted == null || Reseted.Target == null) Reseted(this); return RESULT.OK; }
        private RESULT DSP_SYSTEM_REGISTER_CALLBACK(ref DSP_STATE dsp_state) { if (Regestered == null || Regestered.Target == null) Regestered(this); return RESULT.OK; }
        private RESULT DSP_SYSTEM_DEREGISTER_CALLBACK(ref DSP_STATE dsp_state) { if (UnRegestered == null || UnRegestered.Target == null) UnRegestered(this); return RESULT.OK; }

        private RESULT DSP_SETPOSITIONCALLBACK(ref DSP_STATE dsp_state, uint pos)
        {
            if (PositionChanged == null || PositionChanged.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                PositionEventArgs e = new PositionEventArgs(ref dsp_state, pos);
                PositionChanged(this, e);
                dsp_state = e.State;
                return RESULT.OK;
            }
        }
        private RESULT DSP_SYSTEM_MIX_CALLBACK(ref DSP_STATE dsp_state, int stage)
        {
            if (MixChanged == null || MixChanged.Target == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                MixEventArgs e = new MixEventArgs(ref dsp_state, stage);
                MixChanged(this, e);
                dsp_state = e.State;
                return e.Cancel ? RESULT.ERR_DSP_DONTPROCESS : RESULT.OK;
            }
        }

        private RESULT DSP_SHOULDIPROCESS_CALLBACK(ref DSP_STATE dsp_state, bool inputsidle, uint length, CHANNELMASK inmask, int inchannels, SPEAKERMODE speakermode)
        {
            if (inputsidle || (Should_I_Proces == null || Should_I_Proces.Target == null)) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                Should_I_ProcesEventArgs e = new Should_I_ProcesEventArgs(ref dsp_state, inputsidle, length, inmask, inchannels, speakermode);
                bool silent = Should_I_Proces(this, e);
                dsp_state = e.State;
                return silent ? RESULT.ERR_DSP_SILENCE : RESULT.OK;
            }
        }
        #endregion

        /*
        public XmlNode SaveSetting(XmlDocument Document)
        {
            throw new NotImplementedException();
        }

        public bool LoadSetting(XmlNode Setting)
        {
            throw new NotImplementedException();
        }
        */
        //public class ReadEventArgs{ }
    }
}
