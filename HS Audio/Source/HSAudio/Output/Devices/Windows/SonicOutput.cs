﻿#if WIN_10_1703 || XBOX
using HSAudio.Lib.FMOD;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Windows Sonic output [Windows 10 build 1703 Higher or XBOX] (Sonic 출력 [윈도우 10 빌드 1703 이상 또는 XBOX])
    /// </summary>
    public class SonicOutput : DeviceOutput
    {
        public static SonicOutput[] GetOutputs(HSAudio Audio)
        {
#if !NETSTANDARD
            if (System.Environment.OSVersion.Version.Major < 6) return new SonicOutput[0];
#else
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return new SonicOutput[0];
#endif
            Audio.system_sub.setOutput(OUTPUTTYPE.WINSONIC);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<SonicOutput> device = new List<SonicOutput>();
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                device.Add(new SonicOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Windows Sonic" : "Windows Sonic " + i) : sb.ToString() });
            }
            return device.ToArray();
        }
        public static SonicOutput[] GetOutputs()
        {
#if !NETSTANDARD
            if (System.Environment.OSVersion.Version.Major < 6) return new SonicOutput[0];
#else
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return new SonicOutput[0];
#endif
            Lib.FMOD.FD_System system_sub;
            Lib.FMOD.Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.WINSONIC);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<SonicOutput> device = new List<SonicOutput>();
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        device.Add(new SonicOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Windows Sonic" : "Windows Sonic " + i) : sb.ToString() });
                    }
                    return device.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }

        internal SonicOutput(int index, OutputFormat Format, Guid guid)
        {
            Type = OutputType.SONIC;
            ID = index;
            this.Format = Format;
            //this.Guid = Guid;
        }
        private SonicOutput(SonicOutput before)
        {
            Type = OutputType.SONIC;
            ID = before.ID;
            Name = before.Name;
            Parent = before.Parent;
            Format = before.Format;
        }

        public override Output Parent { get; internal set; }

        public override object Clone() { return new SonicOutput(this); }

        public override bool Equals(DeviceOutput other) { try { return other is SonicOutput ? ((SonicOutput)other).Name.Equals(Name) : false; } catch { return false; } }
        public override bool Equals(object obj) { try { return obj is SonicOutput ? ((SonicOutput)obj).Name.Equals(Name) : false; } catch { return false; } }
        //public override int GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return Name; }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose() { }
    }
}
#endif