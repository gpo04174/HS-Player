﻿using HSAudio.Resource;
using System.Collections.Generic;

namespace HSAudio.Resource.Language
{
    public class Custom : StringResource
    {
        Dictionary<string, string> str = new Dictionary<string, string>();
        public override string this[string Key] { get { return str.ContainsKey(Key) ? str[Key] : Key; } }
        public override bool Exist(string Key) { return str.ContainsKey(Key); }
        public override int Count { get { return str.Count; } }

        public Custom(string[] Resource)
        {
            for(int i = 0; i < Resource.Length; i++)
            {
                if (!string.IsNullOrEmpty(Resource[i]) && 
                    (Resource[i][0] >= 'A' && Resource[i][0] <= 'Z'))
                {
                    int index = Resource[i].IndexOf('=');
                    if(index > 0)
                    {
                        string key = Resource[i].Remove(index);
                        if (!Exist(key)) str.Add(key, Resource[i].Substring(index + 1).Replace("\\n", "\n"));
                    }
                }
            }
        }
    }
}
