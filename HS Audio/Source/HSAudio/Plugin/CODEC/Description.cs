﻿#if PLG_Codec
using System;
using System.Text;
using HSAudio.Core.Sound;
using HSAudio.Lib.FMOD;
using HSAudio.PlugIn.Output;
using HSAudio.Utility;

namespace HSAudio.PlugIn.Codec
{
    public class Description
    {
        internal CODEC_DESCRIPTION _Description = new CODEC_DESCRIPTION();
        private CODEC_WAVEFORMAT _WaveFormat;
        private CODEC_STATE codec_state;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Version"></param>
        /// <param name="IsStream"></param>
        /// <param name="SupportTime">When setposition is called, only these time formats will be passed to the codec. Use bitwise OR to accumulate different types.</param>
        public Description(string Name, uint Version, bool IsStream, TimeUnit SupportTime, 
            OpenCallback Open, CloseCallback Close)
        {
            _Description.name = Name;
            _Description.version = Version;
            _Description.defaultasstream = IsStream ? 1 : 0;
            _Description.timeunits = (TIMEUNIT)SupportTime;
            this.Open = Open;
            this.Close = Close;

        }
        internal void Register()
        {

        }

        public event OpenCallback Open;
        public event CloseCallback Close;


        #region Callback
        private RESULT CODEC_OPENCALLBACK(ref CODEC_STATE codec_state, MODE usermode, ref CREATESOUNDEXINFO userexinfo)
        {
            SoundInfo info = new SoundInfo(ref userexinfo);
            bool open = false;
            if(Open != null) try { open = Open(); } catch { }
            return open ? RESULT.OK :RESULT.ERR_UNINITIALIZED;
        }
        private RESULT CODEC_CLOSECALLBACK(ref CODEC_STATE codec_state)
        {
            this.codec_state = codec_state;

            bool suc = false;
            if (Close != null) try { suc = Close(); } catch { }
            return suc ? RESULT.OK : RESULT.ERR_INTERNAL;
        }
        private RESULT CODEC_READCALLBACK(ref CODEC_STATE codec_state, IntPtr buffer, uint samples_in, ref uint samples_out)
        {

        }
        private RESULT CODEC_GETLENGTHCALLBACK(ref CODEC_STATE codec_state, ref int length, TIMEUNIT lengthtype){}
        private RESULT CODEC_SETPOSITIONCALLBACK(ref CODEC_STATE codec_state, int subsound, uint position, TIMEUNIT postype){}
        private RESULT CODEC_GETPOSITIONCALLBACK(ref CODEC_STATE codec_state, ref uint position, TIMEUNIT postype){}
        private RESULT CODEC_SOUNDCREATECALLBACK(ref CODEC_STATE codec_state, int subsound, ref FD_Sound sound){}
        private RESULT CODEC_METADATACALLBACK(ref CODEC_STATE codec_state, TAGTYPE tagtype, string name, IntPtr data, uint datalen, TAGDATATYPE datatype, int unique){}//char* 형 검사하기 (out? in?)
        private RESULT CODEC_GETWAVEFORMATCALLBACK(ref CODEC_STATE codec_state, ref CODEC_WAVEFORMAT waveformat)
        {

        }
        #endregion
    }
}
#endif