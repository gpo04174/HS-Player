﻿#if WIN_XP || WIN_VISTA || WIN_10_1703
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HSAudio.Wave.Device;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Windows Multimedia output [Windows XP Default] (윈도우 Multimedia 출력 [XP 기본값])
    /// </summary>
    public class WINMMOutput : DeviceOutput
    {
        public static WINMMOutput[] GetOutputs(HSAudio Audio)
        {
            Audio.system_sub.setOutput(OUTPUTTYPE.WINMM);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<WINMMOutput> output = new List<WINMMOutput>();
#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
            OperatingSystem os = Environment.OSVersion;
            WaveDevice[] device;
            if (os.Version.Major < 6) device = WaveDeviceWINMM.GetDevices(false);
            else device = WaveDeviceWASAPI.GetDevices(false);
#elif WIN_VISTA || WIN_10_1703
            WaveDeviceWASAPI[] device = WaveDeviceWASAPI.GetDevices(false);
#endif
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                output.Add(new WINMMOutput(i, format, sb.ToString()) { Name = sb.ToString() });
                for (int j = 0; j < device.Length; j++)
                    if (device[j].ToString().IndexOf(output[i].Name) == 0) { output[i].Device = device[i]; break; }
            }
            return output.ToArray();
        }
        public static WINMMOutput[] GetOutputs()
        {
            Lib.FMOD.FD_System system_sub;
            Lib.FMOD.Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.WINMM);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<WINMMOutput> output = new List<WINMMOutput>();

#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
                    OperatingSystem os = Environment.OSVersion;
                    WaveDevice[] device;
                    if (os.Version.Major < 6) device = WaveDeviceWINMM.GetDevices(false);
                    else device = WaveDeviceWASAPI.GetDevices(false);
#elif WIN_VISTA || WIN_10_1703
                    WaveDeviceWASAPI[] device = WaveDeviceWASAPI.GetDevices(false);
#endif

                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        output.Add(new WINMMOutput(i, format, sb.ToString()) { Name = sb.ToString() });
                        for (int j = 0; j < device.Length; j++)
                            if (device[j].ToString().IndexOf(output[i].Name) == 0) { output[i].Device = device[i]; break; }
                    }
                    return output.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }
#if !NETSTANDARD
        OperatingSystem os;
#endif
        internal WINMMOutput(int index, OutputFormat Format, string Name)
        {
            ID = index;
            this.Format = Format;
            Type = OutputType.WINMM;
            base.Name = Name;
#if !NETSTANDARD
            os = Environment.OSVersion;
#endif
        }
        private WINMMOutput(WINMMOutput before)
        {
            ID = before.ID;
            Name = before.Name;
            Parent = before.Parent;
            Device = before.Device;
            Format = before.Format;
#if !NETSTANDARD
            os = Environment.OSVersion;
#endif
        }

        public override Output Parent { get; internal set; }

        public override object Clone() { return new WINMMOutput(this); }

        public override bool Equals(object obj) { try { return obj is WINMMOutput ? ((WINMMOutput)obj).Name.Equals(Name) : false; } catch { return false; } }
        public override bool Equals(DeviceOutput other) { try { return other is WINMMOutput ? ((WINMMOutput)other).Name.Equals(Name) : false; } catch { return false; } }

        public override int GetHashCode() { return base.GetHashCode(); }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose() { try { Device.Dispose(); } catch (Exception ex) { new HSAudioLog(ex.Message, "WINMMOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); } }
    }
}
#endif