﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using HSAudio.Lib.Windows.CoreAudioApi;
using HSAudio.Lib.Windows.CoreAudioApi.Interfaces;
using HSAudio.Wave.Device;
using HSAudio.Lib.Windows.NAudio.Wave;
using System.Collections.Generic;

namespace HSAudio.Wave.Input
{
    public class WaveInputLoopback : WaveInput
    {
        public static WaveInputLoopback[] GetDevices()
        {
            List<WaveInputLoopback> list = new List<WaveInputLoopback>();
            list.Add(new WaveInputLoopback());

            MMDeviceEnumerator mm = new MMDeviceEnumerator();
            MMDeviceCollection collect = mm.EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active);
            for (int i = 0; i < collect.Count; i++)
                try { list.Add(new WaveInputLoopback(collect[i])); } catch { }

            return list.ToArray();
        }

        internal WasapiLoopbackCapture waveIn;
        //WaveDataInEventArgs args;

        //List<WaveInputAudioSession> sub = new List<WaveInputAudioSession>();
        public WaveInputLoopback()
        {
            Kind = RecordKind.WASAPI_LOOPBACK;
            //args = new WaveDataInEventArgs();

            this.waveIn = new WasapiLoopbackCapture();
            this.waveIn.DataAvailable += DataAvailable_Event;
            this.waveIn.RecordingStopped += WaveDataInStopped_Event;

            Device = new WaveDeviceLoopback(waveIn);
        }
        public WaveInputLoopback(MMDevice Device)
        {
            Kind = RecordKind.WASAPI_LOOPBACK;
            //args = new WaveDataInEventArgs();

            this.waveIn = new WasapiLoopbackCapture(Device);
            this.waveIn.DataAvailable += DataAvailable_Event;
            this.waveIn.RecordingStopped += WaveDataInStopped_Event;

            this.Device = new WaveDeviceWASAPI(Device, false);

            //sub.AddRange(WaveInputAudioSession.GetDevices(Device));
            //Device.AudioSessionManager.OnSessionCreated += OnSessionCreated;
        }

        public event DataInEventHandler WaveDataIn;
        public event DataInStoppedEventHandler WaveDataInStopped;

        public WaveFormat Format { get; set; }//{ get { return args.Format; } set { args.Format = value; } }
        public WaveDevice Device { get; private set; }
        public RecordKind Kind { get; private set; }

        public bool IsBusy { get; private set; }
        public int Latency { get { return 10; } set { } }
        public bool LatencySupport { get { return false; } }

        public void Start() { IsBusy = true; try { waveIn.StartRecording(); } finally { IsBusy = false; } }
        public void Stop() { IsBusy = true; try { waveIn.StopRecording(); } finally { IsBusy = false; } }

        public void Dispose()
        {
            if (waveIn != null) try { Stop(); waveIn.Dispose(); } catch { }
            if (Device != null) try { Device.Dispose(); } catch { }
            //for (int i = 0; SubDevice != null && i < SubDevice.Length; i++) { SubDevice[i].Dispose(); }
            waveIn = null;
            WaveDataIn = null;
            WaveDataInStopped = null;
            Device = null;
        }

        public override string ToString()
        {
            return "[LOOPBACK] " + Device.ToString();
        }

        #region Events
        private void OnSessionCreated(object sender, IAudioSessionControl newSession)
        {

        }

        private void DataAvailable_Event(object o, WaveInEventArgs e)
        {
            if (WaveDataIn != null) 
                try { WaveDataIn.Invoke(this, new DataInEventArgs(new WaveFormat(Format.SampleRate, Format.Channel, 32, WaveType.FLOAT), e.Buffer, e.BytesRecorded)); }catch{}
        }
        private void WaveDataInStopped_Event(object o, StoppedEventArgs e)
        {
            if (WaveDataInStopped != null) WaveDataInStopped.Invoke(this, e);
        }
        #endregion
    }
}
#endif