﻿using System;

namespace HSAudio.DSP.OTHER
{
    internal static class IDGenerator
    {
        static Random rd = new Random(128);
        public static int Generate()
        {
            int id = rd.Next(int.MinValue + 1, int.MaxValue);
            while (DSP.list.ContainsKey(id)) id = rd.Next(int.MinValue + 1, int.MaxValue);
            return id;
        }
    }
}
