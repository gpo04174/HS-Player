﻿using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.PlugIn.Output
{
    public class UnLockInfo
    {
        internal UnLockInfo(ref OUTPUT_STATE state, IntPtr ptr1, IntPtr ptr2, uint len1, uint len2)
        {
            State = state;
            this.ptr1 = ptr1;
            this.ptr2 = ptr2;
            this.len1 = len1;
            this.len2 = len2;
        }

        internal OUTPUT_STATE State { get; set; }

        public IntPtr ptr1 { get; private set; }
        public IntPtr ptr2 { get; private set; }
        public uint len1 { get; private set; }
        public uint len2 { get; private set; }
    }
}
