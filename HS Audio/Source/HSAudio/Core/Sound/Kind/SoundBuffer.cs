﻿using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System.IO;

namespace HSAudio.Core.Sound.Kind
{
    public abstract class SoundBuffer : Sound
    {
        protected WaveFormat _Format;
        protected SoundTag _Tag;
        protected ISoundFormat _SoundType;
        protected MemoryStream ms;
        protected byte[] _Buffer;

        protected CREATESOUNDEXINFO info = new CREATESOUNDEXINFO();
        internal SoundBuffer(HSAudio Audio, byte[] Buffer, bool Compress = true, bool LoadMemory = true) : this(Audio, Buffer, null, null, Compress, LoadMemory) { }
        internal SoundBuffer(HSAudio Audio, byte[] Buffer, ISoundFormat Type, WaveFormat Format, bool Compress, bool LoadMemory = true) : base(Audio)
        {
            this.Compress = Compress;
            this.LoadMemory = LoadMemory;
            _Buffer = Buffer;
            _SoundType = Type;
            _Format = Format;


            if (Format != null) InitFormat(ref info, Format);
            if (Type != null) InitType(ref info, Type.Type);
        }

        public override void Unload()
        {
            ms.Dispose();
            _Tag = null;
            base.Unload();
        }

        public byte[] Buffer { get { return _Buffer; } }

        public override bool CanPitch { get { return true; } }
        public override bool CanPosition { get { return true; } }
        public override bool CanReverse { get { return true; } }

        public override WaveFormat Format { get { return _Format; } }
        public override SoundTag Tag { get { return _Tag; } }
        public override ISoundFormat Type { get { return _SoundType; } }

        public bool LoadMemory { get; protected set; }
        public bool Compress { get; protected set; }

        public override void Dispose()
        {
            Unload();
            base.Dispose();
        }
    }
}
