﻿using HSAudio.Lib.FMOD;

namespace HSAudio.Speakers
{
    public enum SpeakerKind : int
    {
        /// <summary>
        /// The front left speaker
        /// </summary>
        FRONT_LEFT = SPEAKER.FRONT_LEFT,
        /// <summary>
        /// The front right speaker
        /// </summary>
        FRONT_RIGHT = SPEAKER.FRONT_RIGHT,
        /// <summary>
        /// The front center speaker
        /// </summary>
        FRONT_CENTER = SPEAKER.FRONT_CENTER,
        /// <summary>
        /// The LFE or 'subwoofer' speaker
        /// </summary>
        SUBWOOFER = SPEAKER.LOW_FREQUENCY,
        /// <summary>
        /// The surround left (usually to the side) speaker
        /// </summary>
        SURROUND_LEFT = SPEAKER.SURROUND_LEFT,
        /// <summary>
        /// The surround right (usually to the side) speaker
        /// </summary>
        SURROUND_RIGHT = SPEAKER.SURROUND_RIGHT,
        /// <summary>
        /// The back left speaker
        /// </summary>
        BACK_LEFT = SPEAKER.BACK_LEFT,
        /// <summary>
        /// The back right speaker
        /// </summary>
        BACK_RIGHT = SPEAKER.BACK_RIGHT,
        /// <summary>
        /// The top front left speaker
        /// </summary>
        TOP_FRONT_LEFT = SPEAKER.TOP_FRONT_LEFT,
        /// <summary>
        /// The top front right speaker
        /// </summary>
        TOP_FRONT_RIGHT = SPEAKER.TOP_FRONT_RIGHT,
        /// <summary>
        /// The top back left speaker
        /// </summary>
        TOP_BACK_LEFT = SPEAKER.TOP_BACK_LEFT,
        /// <summary>
        /// The top back right speaker
        /// </summary>
        TOP_BACK_RIGHT = SPEAKER.TOP_BACK_RIGHT,
        /// <summary>
        /// Custom speaker types supported.
        /// </summary>
        CUSTOM = SPEAKER.MAX
    }
}
