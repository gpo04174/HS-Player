﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Tag
{
    public enum TagDataType
    {
        String,
        Decimal,
        Boolean,
        ByteArray
    }
}
