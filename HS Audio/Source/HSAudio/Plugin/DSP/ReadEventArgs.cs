﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSAudio.PlugIn.DSP
{
    public class ReadEventArgs : PluginDSPEventArgs
    {
        protected internal ReadEventArgs(){}
        protected internal ReadEventArgs(ref DSP_STATE state, IntPtr inbuf, IntPtr outbuf, uint len, int inch, int outch) { Set(ref state, inbuf, outbuf, len, inch, outch); }
        
        protected internal DSP_STATE State { get; protected set; }
        public IntPtr InBuffer { get; internal set; }
        public IntPtr OutBuffer { get; internal set; }
        public uint Length { get; internal set; }
        public int InChannel { get; internal set; }
        public int OutChannel { get; internal set; }

        protected internal void Set(ref DSP_STATE state, IntPtr inbuf, IntPtr outbuf, uint len, int inch, int outch)
        { State = state; InBuffer = inbuf; OutBuffer = outbuf; Length = len; InChannel = inch; OutChannel = outch; }
    }
}
