﻿using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using HS.Setting;
using System;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Output.Devices;
using HS;
using System.Text;
using System.Runtime.InteropServices;
using HSAudio.Speakers;
using HSAudio.Utility;

namespace HSAudio.Output
{
    public delegate void OpeningEventHandler(Output sender);
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Opened"></param>
    /// <returns>True is throw exception [True면 예외를 발생시킵니다.]</returns>
    public delegate bool OpenedEventHandler(Output sender, bool Opened);
    public delegate void CloseEventHandler(Output sender);
    public delegate void DeviceChangedEventHandler(Output sender);
    public delegate void DeviceLostEventHandler(Output sender);
    //public delegate void MethodChangedEventHandler(Output sender, OutputMethod Method);
    //public delegate void FormatChangedEventHandler(Output sender, OutputFormat Format);
    public partial class Output : ISettingManager
    {
        public event CloseEventHandler OutputClosing;
        public event CloseEventHandler OutputClosed;
        public event OpeningEventHandler OutputOpening;
        /// <summary>
        /// When opened, this event will be raise<para/>
        /// 열렸으면 이 이벤트가 발생합니다.
        /// </summary>
        public event OpenedEventHandler OutputOpened;

        public event DeviceChangedEventHandler DeviceChanged;
        public event DeviceLostEventHandler DeviceLost;

        #region Private var
        OutputFormat _Format;
        OutputMethod _Method;
        OutputSpeaker _Speaker;
        ADVANCEDSETTINGS setting = new ADVANCEDSETTINGS();

        int sample;
        int speakercnt;
        int channel;
        SPEAKERMODE speaker;
        #endregion

        SYSTEM_CALLBACK _SYSTEM_CALLBACK_DEVICE;
        internal Output(HSAudio Audio)
        {
            this.Audio = Audio;
            _SYSTEM_CALLBACK_DEVICE = new SYSTEM_CALLBACK(SYSTEM_CALLBACK_DEVICE);
            Audio.system.setCallback(_SYSTEM_CALLBACK_DEVICE, SYSTEM_CALLBACK_TYPE.DEVICELISTCHANGED);
            Audio.system.setCallback(_SYSTEM_CALLBACK_DEVICE, SYSTEM_CALLBACK_TYPE.DEVICELOST);
            setting.resamplerMethod = DSP_RESAMPLER.CUBIC;
            _Speaker = new OutputSpeaker(Audio);

            Speakers_Init();
        }

        #region Properties
        public HSAudio Audio { get; private set; }
        //public OutputFormat Format { get { return _Format == null ? null :(OutputFormat)_Format.Clone(); } }
        public OutputMethod Method { get { return _Method; } }
        public OutputSpeaker Speaker { get { return _Speaker; } }
        public OutputStatus Status { get; private set; }
        public OutputType Type { get; internal set; }
        public OutputResampler Resampler { get { return (OutputResampler)setting.resamplerMethod; } private set { setting.resamplerMethod = (DSP_RESAMPLER)value; } }
        public bool DownMix5_1to2 { get; set; }

        public RESULT Result { get; private set; }

        float[][] _Matrix;
        public float[][] Matrix
        {
            get
            {
                /*
                FD_ChannelGroup group = null;
                Result = Audio.system.getMasterChannelGroup(out group);
                if (group != null && group.isValid())
                {
                    float[] matrix; int input, output;
                    Result = group.getMixMatrix(out matrix, out input, out output, 0);
                    if (matrix == null || matrix.Length > 0)
                    {
                        int ch;
                        OutputFormat format = getFormat();
                        float[][] a = new float[format.Channel][];
                        for (int i = 0; i < a.Length; i++)
                        {
                            a[i] = new float[format.Channel];
                            a[i][i] = 1;
                        }
                        return a;
                    }
                    else return ArrayConveterN<float>.ArrayToMatrix(matrix, input, output, true);
                }
                else return _Matrix;
                */
                return _Matrix;
            }
            set
            {
                _Matrix = value;
                FD_ChannelGroup group = null;
                Result = Audio.system.getMasterChannelGroup(out group);
                if (value != null && value.Length > 0)
                {
                    float[] matrix = ArrayConveterN<float>.MatrixToArray(value, true);
                    Result = group.setMixMatrix(matrix, value.Length, value[0].Length, 0);
                }
                else Result = group.setMixMatrix(null, 0, 0, 0);
            }
        }

        public float[][] GetMatrix()
        {
            float[] matrix;
            int inch, outch;
            FD_ChannelGroup group = null;
            Result = Audio.system.getMasterChannelGroup(out group);
            Result = group.getMixMatrix(out matrix, out inch, out outch, 0);
            float[][] arr = ArrayConveterN<float>.ArrayToMatrix(matrix, inch, outch, true);
            return arr;
        }
        public float[][] GetMatrixDefault()
        {
            OutputFormat format = getFormat();
            float[][] _Matrix = new float[format.Channel][];
            for (int i = 0; i < _Matrix.Length; i++)
            {
                _Matrix[i] = new float[format.Channel];
                _Matrix[i][i] = 1;
            }
            return _Matrix;
        }
        #endregion

        #region Method

        #region Public

        public bool Open(OutputMethod Method, bool ThrowException = true)
        {
            //if(Method.Equals(_Method))
            return Open(Method, _Format, Resampler, ThrowException);
        }

        public bool Open(OutputFormat Format, OutputResampler Resampler, bool ThrowException = true) { return Open(Method, Format, Resampler, ThrowException); }
        public bool Open(OutputFormat Format, bool ThrowException = true) { return Open(Method, Format, Resampler, ThrowException); }
        public bool Open(OutputMethod Method, OutputFormat Format, bool ThrowException = true) { return Open(Method, Format, Resampler, ThrowException); }
        public bool Open(OutputMethod Method, OutputFormat Format, OutputResampler Resampler, bool ThrowException = true)
        {
            if (OutputOpening != null) try { OutputOpening.Invoke(this); } catch { }
            if (Status == OutputStatus.Open) throw new HSException(StringResource.Resource["STR_ERR_OUTPUT_BEFORE_CLOSE"]);

            this.Resampler = Resampler;
            RESULT Result = RESULT.OK;
            OUTPUTTYPE type = OUTPUTTYPE.UNKNOWN;

            uint version;
            Result = Audio.system.getVersion(out version);

#if ANDROID
            IntPtr driver = IntPtr.Zero;
            Result = Audio.system.init(32, INITFLAGS.NORMAL, driver);
#endif

            #region Info
            StringBuilder name = new StringBuilder(256);
            Guid guid; int rate; SPEAKERMODE spk1; int ch = 0;
            if (Method == null)
            {
                type = OUTPUTTYPE.AUTODETECT;
                Result = Audio.system.setOutput(type);
                Result = Audio.system.getDriverInfo(0, name, 256, out guid, out rate, out spk1, out ch);
            }
            else
            {
                type = (OUTPUTTYPE)Method.Type;
                Result = Audio.system.setOutput(type);
                if (Method is NoOutput) ch = 2;
                else if (Method.Device == null || Method.Device.Formats == null || Method is AutoOutput) Result = Audio.system.getDriverInfo(Method.ID, name, 256, out guid, out rate, out spk1, out ch);
            }
            #endregion

            #region Set the Format
            if (Format != null)
            {
                SPEAKERMODE mode = ConvertSpeakerMode.ConvertFMOD(Format.Mode);
                Result = Audio.system.setSoftwareFormat(Format.SampleRate, mode, Format.Mode == SpeakerMode.Custom ? Format.Channel : 0);
                //Result = Audio.system.setSoftwareChannels(Format.Channel < 0 ? ch * ch : Format.Channel * Format.Channel);
                //setting.maxMPEGCodecs = setting.maxAT9Codecs = setting.maxFADPCMCodecs = setting.maxADPCMCodecs = setting.maxFADPCMCodecs = setting.maxPCMCodecs = setting.maxVorbisCodecs = setting.maxXMACodecs = Format.MaxInputChannel;
            }
            else
            {
                Result = Audio.system.setSoftwareFormat(44100, SPEAKERMODE.DEFAULT, 0);
                //Result = Audio.system.setSoftwareChannels(6);
            }
            #endregion

            Result = Audio.system.setAdvancedSettings(ref setting);

            #region Set the Device
            if (Method == null || Method is AutoOutput)
            {
                Type = OutputType.Auto;
                if (type != OUTPUTTYPE.AUTODETECT) Result = Audio.system.setOutput(OUTPUTTYPE.AUTODETECT);
                Result = Audio.system.init(Format != null? Format.MaxInputChannel : 32, DownMix5_1to2 ? INITFLAGS.PREFER_DOLBY_DOWNMIX : INITFLAGS.NORMAL, IntPtr.Zero);
                if (Method == null) Method = new AutoOutput();
            }
            else
            {
                Type = Method.Type;
                if (Method is DeviceOutput)
                {
                    Audio.system.getOutput(out type);
                    if (type != (OUTPUTTYPE)Type) Result = Audio.system.setOutput((OUTPUTTYPE)Type);
                    Result = Audio.system.setDriver(Method.ID);
                }
#if FUNC_PLUGIN
                else if (Method is PluginOutput) Audio.system.setOutputByPlugin(((PluginOutput)Method).Plugin.Handle);
#endif
                else if(Method is NoOutput) Result = Audio.system.setOutput(OUTPUTTYPE.NOSOUND);
                else { new HSAudioLog(StringResource.Resource["STR_ERR_OUTPUT_UNKNOWN"] + " (" + Method + ")", "HSAudio::Output::Open()", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }

#if (WIN_VISTA || WIN_10 || WIN_METRO || XBOX) && NOTSUPPORT
                bool isExclusive = this.Method is WASAPIOutput ? ((WASAPIOutput)Method).Exclusive : false;
                Result = Audio.system.init(Format.MaxInputChannel, isExclusive ? INITFLAGS.Ex : INITFLAGS.NORMAL);
#else
                Result = Audio.system.init(Format != null ? Format.MaxInputChannel : 32, DownMix5_1to2 ? INITFLAGS.PREFER_DOLBY_DOWNMIX : INITFLAGS.NORMAL, IntPtr.Zero);
#endif
            }
            //Result = Audio.system.getAdvancedSettings(ref setting);
            //string a = PtrForge.ToString1(setting.ASIOSpeakerList);
#endregion

            if (Result == RESULT.OK)
            {
#if WIN_XP || WIN_VISTA || WIN_10_1703
                ASIOOutput asio = Method as ASIOOutput;
                if (asio != null && Format != null)
                {
                    try { if (setting.ASIOSpeakerList != IntPtr.Zero) Marshal.FreeHGlobal(setting.ASIOSpeakerList); } catch { }

                    if (Format.Mode == SpeakerMode.Default) setting.ASIOSpeakerList = IntPtr.Zero;
                    else if (Format.Mode == SpeakerMode.Custom || !speakers.ContainsKey(Format.Mode)) setting.ASIOSpeakerList = CopySpeaker(spk, Format.Channel);
                    else setting.ASIOSpeakerList = CopySpeaker(speakers[Format.Mode], speakers[Format.Mode].Length);

                    Result = Audio.system.setAdvancedSettings(ref setting);
                }
#endif
                Status = OutputStatus.Open;
                Method.Parent = this;
                _Format = getFormat();
                //if (Format != null) _Format = Format;
                //else _Format = getFormat();
                _Method = Method;
                if (_Matrix == null) Matrix = GetMatrixDefault();
                else Matrix = ArrayConveterN<float>.Merge(GetMatrixDefault(), _Matrix);
                if (OutputOpened != null) try { OutputOpened(this, true); } catch(Exception ex) { }
            }
            else
            {
                Status = OutputStatus.Error;
                if (OutputOpened != null) try { ThrowException = OutputOpened.Invoke(this, false); } catch(Exception ex) { }
                if (ThrowException) throw new HSAudioException(Result);
                else return false;
            }

            return true;
        }
        public void Close()
        {
            if (OutputClosing != null) try { OutputClosing.Invoke(this); } catch { }
            Audio.system.close();
            Status = OutputStatus.NotOpen;
            if (OutputClosed != null) try { OutputClosed.Invoke(this); } catch { }
        }

        public OutputFormat getFormat()
        {
            OutputFormat Format = new OutputFormat();
            SPEAKERMODE mode = SPEAKERMODE.DEFAULT;
            int ch = 0;
            Audio.system.getSoftwareFormat(out Format.sampleRate, out mode, out ch);
            Format.MaxInputChannel = 32;
            Format.channel = (short)ch;
            return Format;
        }
        /*
        public void setFormat(OutputFormat Format)
        {
            if (Format == null)
            {
                Audio.system.init(32, INITFLAGS.NORMAL, IntPtr.Zero);
                int Sample, rawspk; SPEAKERMODE spk;
                Audio.system.getSoftwareFormat(out Sample, out spk, out rawspk);
                _Format = new OutputFormat(Sample, (SpeakerMode)spk, (short)rawspk);
            }
            else
            {
                _Format = (OutputFormat)Format.Clone();
                if (Format.channels > 0) Audio.system.setSoftwareChannels(Format.Channel);
                SPEAKERMODE mode = OutputFormat.ConvertMode(Format.Mode);
                Audio.system.setSoftwareFormat(Format.SampleRate, mode, mode == SPEAKERMODE.RAW ? Math.Max(Format.Channel, (short)32) : 0);
                Open(Method);
            }
        }
        */
#endregion

        #region Private
        private void Update()
        {
            Audio.system.getSoftwareFormat(out sample, out speaker, out speakercnt);
            Audio.system.getSoftwareChannels(out channel);
        }
        #endregion
        #endregion


        #region Override Method
        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            OutputFormat Format = null;
            string Name = null;
            Guid guid = new Guid();
            if(Setting.Exist("Matrix"))
            {
                try
                {
                    string[] s = Setting["Matrix"].Value.Split(' ');
                    string[] size = s[0].Split(',');

                    int x = Convert.ToInt32(size[0]), 
                        y = Convert.ToInt32(size[1]);

                    float[][] matrix = new float[x][];

                    int k = 0;
                    for (int i = 0; i < x; i++)
                    {
                        matrix[i] = new float[y];
                        for (int j = 0; j < y && k < s.Length; j++)
                            matrix[i][j] = Convert.ToSingle(s[k++]);
                    }

                    Matrix = Matrix == null ? 
                        ArrayConveterN<float>.Merge(GetMatrixDefault(), matrix) :
                        ArrayConveterN<float>.Merge(Matrix, matrix);
                }
                catch(Exception ex) { if (ThrowException) throw ex; }
            }
            if (Setting.SubSetting.Exist("Format")) { Format = new OutputFormat(); Format.LoadSetting(Setting.SubSetting["Format"], ThrowException); }
            if (Setting.SubSetting.Exist("Method"))
            {
                Settings s = Setting.SubSetting["Method"];
                if (s.Exist("Guid")) guid = new Guid(s["Guid"]);
                if (s.Exist("Name")) Name = s["Name"];
            }
            if (Setting.Exist("OutputType"))
            {
                OutputType t = (OutputType)Enum.Parse(typeof(OutputType), Setting["Output"]);
                if (t == OutputType.Auto) Open(new AutoOutput());
#if WIN_VISTA || WIN_10_1703 || WIN_METRO || XBOX
                else if (t == OutputType.WASAPI)
                {
                    WASAPIOutput[] output = WASAPIOutput.GetOutputs(Audio);
                    for (int i = 0; i < output.Length; i++)
                        if (output[i].Guid == guid) _Method = output[i];
                        else output[i].Dispose();
                }
#endif
#if WIN_XP || WIN_VISTA || WIN_10_1703
                else if (t == OutputType.ASIO)
                {
                    ASIOOutput[] output = ASIOOutput.GetOutputs(Audio);
                    for (int i = 0; i < output.Length; i++)
#if WIN_XP
                        if (output[i].Name == Name){ if (_Method != null) _Method.Dispose(); _Method = output[i];}
#else
                        if (output[i].Guid == guid) { if (_Method != null) _Method.Dispose(); _Method = output[i]; }
#endif
                        else output[i].Dispose();
                }
                else if (t == OutputType.DirectSound)
                {
                    DirectOutput[] output = DirectOutput.GetOutputs(Audio);
                    for (int i = 0; i < output.Length; i++)
#if WIN_XP
                        if (output[i].Name == Name){ if (_Method != null) _Method.Dispose(); _Method = output[i];}
#else
                        if (output[i].Guid == guid) { if (_Method != null) _Method.Dispose(); _Method = output[i]; }
#endif
                        else output[i].Dispose();
                }
                else if (t == OutputType.WINMM)
                {
                    WINMMOutput[] output = WINMMOutput.GetOutputs(Audio);
                    for (int i = 0; i < output.Length; i++)
                        if (output[i].Name == Name) { if (_Method != null) _Method.Dispose(); _Method = output[i]; }
                        else output[i].Dispose();
                }
#endif
#if WIN_10_1703 || XBOX
                else if (t == OutputType.SONIC)
                {
                    WINMMOutput[] output = WINMMOutput.GetOutputs(Audio);
                    for (int i = 0; i < output.Length; i++)
                        if (output[i].Name == Name) { if (_Method != null) _Method.Dispose(); _Method = output[i]; }
                        else output[i].Dispose();
                }
                else if (t == OutputType.DolbyAtmos)
                {
                    AtmosOutput[] output = AtmosOutput.GetOutputs(Audio);
                    for (int i = 0; i < output.Length; i++)
                        if (output[i].Name == Name) { if (_Method != null) _Method.Dispose(); _Method = output[i]; }
                        else output[i].Dispose();
                }
#endif
            }
            //else { if (ThrowException) throw new HSException(); else return false; }

            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("OutputType", Type.ToString());

            if (_Matrix != null && _Matrix.Length > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0},{1}", _Matrix.Length, _Matrix[0].Length);

                for (int i = 0; i < _Matrix.Length; i++)
                    for (int j = 0; j < _Matrix[i].Length; j++)
                        sb.Append((" " + _Matrix[i][j]));

                s.SetValue("Matrix", new SettingsData(sb.ToString(), false));
            }
            s.SubSetting.SetValue("Format", getFormat().SaveSetting());
            s.SubSetting.SetValue("Method", Method.SaveSetting());
            s.SubSetting.SetValue("Speaker", Speaker.SaveSetting());
            return s;
        }
        
        internal void Dispose()
        {
            Close();
            Method.Dispose();
            //Audio.system.release();
            _Method = null;
            _Format = null;
            _Speaker = null;
        }
#endregion
    }
}
