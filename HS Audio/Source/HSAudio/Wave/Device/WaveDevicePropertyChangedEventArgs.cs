﻿using HSAudio.Control.Device;

namespace HSAudio.Wave.Device
{
    public class WaveDevicePropertyChangedEventArgs
    {
        public WaveDevicePropertyChangedEventArgs(int Index, DeviceControl Control)
        {
            this.Index = Index;
            this.Control = Control;
        }
        public int Index { get; private set; }
        public DeviceControl Control { get; private set; }
    }
}
