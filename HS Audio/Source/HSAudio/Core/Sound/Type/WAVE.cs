﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class WAVE : ISoundFormat
    {
        public WAVE() : base(SoundType.Wave, MIME, Extension, Description) { }

        public static string[] MIME { get { return new string[] { "audio/wav", "audio/wave" }; } }
        public static string[] Extension { get { return new string[] { "wav", "wave" }; } }
        public static string[] Description { get { return new string[] { "Waveform Audio (Microsoft)" }; } }

        public override string ToString() { return Description[0]; }
    }
}
