﻿#if PLG_Codec
using HSAudio.Wave;

namespace HSAudio.PlugIn.Codec
{
    public class CodecFormat
    {
        public CodecFormat(WaveFormat Format, string Name, uint SampleCount, uint Length)
        {
            this.Format = Format;
            this.Name = Name;
            this.SampleCount = SampleCount;
            this.Length = Length;
        }
        /// <summary>
        /// [Require] Codec Name ([필수] 코덱 이름)
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// [Require] File format ([필수] 파일 형식)
        /// </summary>
        public WaveFormat Format { get; set; }
        /// <summary>
        /// [Require] PCM Samplate length ([필수] PCM 샘플레이트 길이)
        /// </summary>
        public uint SampleCount { get; set; }
        /// <summary>
        /// [Require] FIle Bytes length ([필수] 파일 바이트 길이)
        /// </summary>
        public uint Length { get; set; }
        /// <summary>
        /// [Optional] PCM block size
        /// </summary>
        public uint BlockSize { get; set; }
    }
}
#endif