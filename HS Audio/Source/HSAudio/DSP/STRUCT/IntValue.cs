﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;

namespace HSAudio.DSP
{
    public class IntValue : ISettingManager
    {
        internal IntValue(DSP_PARAMETER_DESC_INT desc) { this.Value = desc.defaultval; Maximum = desc.max; Minimum = desc.min; }
        public IntValue(Settings setting) { LoadSetting(setting); }
        public IntValue(int  Value, int  Min, int  Max) { this.Value = Value; this.Minimum = Min; this.Maximum = Max; }
        public int Value { get; private set; }

        public int Maximum { get; private set; }
        public int Minimum { get; private set; }

        public static implicit operator int(IntValue param) { return param.Value; }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Value", Value);
            s.SetValue("Minimum", Minimum);
            s.SetValue("Maximum", Maximum);
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Value")) Value = Convert.ToInt32(Setting["Value"]);
            if (Setting.Exist("Maximum")) Maximum = Convert.ToInt32(Setting["Maximum"]);
            if (Setting.Exist("Minimum")) Value = Convert.ToInt32(Setting["Minimum"]);
            return true;
        }
    }
}
