﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HSPlayer.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MusicItemIconLarge : ContentView
    {
        public MusicItemIconLarge()
        {
            InitializeComponent();
            Init();
        }
        public MusicItemIconLarge(string Title, string Artist, string Album)
        {
            InitializeComponent();
            this.Title = Title; this.Artist = Artist; //this.Album = Album;
            Init();
        }

        void Init()
        {
            this.Focused += (sender, e) => control.IsVisible = true;
            this.Unfocused += (sender, e) => control.IsVisible = false;
        }

        public string Title { get { return lbl_title.Text; } set { lbl_title.Text = value == null ? "" : value; } }
        public string Artist { get { return lbl_artist.Text; } set { lbl_artist.Text = value == null? "": value; } }
        public string AlbumArtURL { get; set; } 

        public bool VisiblePlay { get { return btn_play.IsVisible == true; } set { btn_play.IsVisible = value; } }

        public void UpdateAlbumArt()
        {
            pic_album.Source = ImageSource.FromUri(new Uri(AlbumArtURL));
        }
        public void DefaultAlbumArt()
        {
            pic_album.Source = ImageSource.FromUri(new Uri("ms-appx:///Resources/default_album_orgin.jpg"));
        }

        internal protected Image AlbumArtControl { get { return pic_album; } set { pic_album = value; } }
        /*
        public async Task setAlbumArtAsync(byte[] AlbumArt)
        {
            AlbumArtImage = new BitmapImage();
            using (IRandomAccessStream stream = new MemoryStream(AlbumArt).AsRandomAccessStream())
                await AlbumArtImage.SetSourceAsync(stream);
            albumart.Source = AlbumArtImage;
        }
        */
    }
    public class MusicItem
    {
        public string Title { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string AlbumArtURL { get; set; }
    }

}
