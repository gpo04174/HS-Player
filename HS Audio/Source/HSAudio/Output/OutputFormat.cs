﻿using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using HSAudio.Speakers;
using HSAudio.Wave;
using HS.Setting;
using System;
using HS;

namespace HSAudio.Output
{
    public class OutputFormat : ICloneable, ISettingManager, IEquatable<OutputFormat>
    {
        internal int channel;
        internal int sampleRate;
        internal int _MaxInputChannel = 32;

        public OutputFormat() : this(44100, SpeakerMode.Stereo) { }
        public OutputFormat(SpeakerMode Mode) : this(44100, Mode) { }
        public OutputFormat(int Sample) : this(Sample, SpeakerMode.Stereo) { }
        public OutputFormat(int Sample, int Channel) { sampleRate = Sample; Mode = SpeakerMode.Custom; this.Channel = Channel; }
        public OutputFormat(int Sample, SpeakerMode Mode)
        {
            sampleRate = Sample;
            this.Mode = Mode;
            channel = GetChannel(Mode, Channel);
        }
        protected OutputFormat(int Sample, SpeakerMode Mode, int Channel)
        {
            this.SampleRate = SampleRate;
            this.Mode = Mode;
            this.Channel = Channel;
        }
        //public OutputFormat(WaveFormat Wave) { sampleRate = (int)Wave.SampleRate; channels = Wave.Channel; bit = Wave.Bit; ispcm = Wave.Type == WaveType.PCM; }
        //internal OutputFormat(int Samplate = 48000, short Channel = 2, int Bit = 16, bool IsPCM = true) { sampleRate = Samplate; channels = Channel; bit = Bit; }
        //internal OutputFormat(int Samplate = 48000, short Channel = 2, int Bit = 16, bool IsPCM = true, SpeakerMode Mode = SpeakerMode.Default) { sampleRate = Samplate; channels = Channel; bit = Bit; this.Mode = Mode; }

        /// <summary>
        /// Returns the number of channels (1=mono,2=stereo etc)
        /// </summary>
        public int Channel
        {
            get { return channel; }
            set { channel = value; }
        }

        /// <summary>
        /// Returns the sample rate (samples per second)
        /// </summary>
        public int SampleRate
        {
            get { return sampleRate; }
            set { sampleRate = value; }
        }

        public SpeakerMode Mode { get; set; }
        public int MaxInputChannel { get { return _MaxInputChannel; } set { _MaxInputChannel = value; } }

        public object Clone()
        {
            return new OutputFormat(SampleRate, Mode, Channel) { MaxInputChannel = MaxInputChannel };
        }

        
        internal static int GetChannel(SpeakerMode mode, int channel)
        {
            switch (mode)
            {
                case SpeakerMode.Default: return -1;
                case SpeakerMode.Mono: return 1;
                case SpeakerMode.Stereo: return 2;
                case SpeakerMode.Quad: return 4;
                case SpeakerMode.Surround: return 5;
                case SpeakerMode.System5_1: return 6;
                case SpeakerMode.System7_1: return 8;
                case SpeakerMode.System7_1_4: return 12;
                case SpeakerMode.Custom: return channel;
                default: return 0;
            }
        }
        

        public virtual Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Samplate", SampleRate);
            s.SetValue("Channel", Channel);
            s.SetValue("MaxInputChannel", MaxInputChannel);
            s.SetValue("Mode", new SettingsData(Mode.ToString(), false));
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            SampleRate = Convert.ToInt32(Setting.GetValue("Samplate", SampleRate));
            Channel = Convert.ToInt16(Setting.GetValue("Channel", Channel));
            MaxInputChannel = Convert.ToInt32(Setting.GetValue("MaxInputChannel", MaxInputChannel));
            //Bit = Convert.ToInt32(Setting.GetValue("Bit", Bit));
            //ispcm = Convert.ToBoolean(Setting.GetValue("IsPCM", IsPCM));
            //if (Setting.Exist("Bits")) try { Bits = (Bits)Enum.Parse(typeof(Bits), Setting["Bits"]); } catch(Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Mode")) try { Mode = (SpeakerMode)Enum.Parse(typeof(SpeakerMode), Setting["Mode"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public bool Equals(OutputFormat other)
        {
            return
                other.SampleRate == SampleRate &&
                other.MaxInputChannel == MaxInputChannel && other.Channel == Channel;
        }
    }
}
