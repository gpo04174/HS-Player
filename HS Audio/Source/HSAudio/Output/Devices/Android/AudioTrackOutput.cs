#if ANDROID
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Android AudioTrack output [for Android] (안드로이드 전용)
    /// </summary>
    public class AudioTrackOutput : DeviceOutput
    {
        public static AudioTrackOutput[] GetOutputs(HSAudio Audio)
        {
            Audio.system_sub.setOutput(OUTPUTTYPE.AUDIOTRACK);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<AudioTrackOutput> device = new List<AudioTrackOutput>();
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.Channel = (short)ch; format.Mode = (SpeakerMode)spk;

                device.Add(new AudioTrackOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Android AudioTrack" : "Android AudioTrack " + i) : sb.ToString() });
            }
            return device.ToArray();
        }
        public static AudioTrackOutput[] GetOutputs()
        {
            FD_System system_sub;
            Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.AUDIOTRACK);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<AudioTrackOutput> device = new List<AudioTrackOutput>();
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.Channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        device.Add(new AudioTrackOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Linux ALSA" : "Linux ALSA " + i) : sb.ToString() });
                    }
                    return device.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }

        internal AudioTrackOutput(int index, OutputFormat Format, Guid guid)
        {
            Type = OutputType.AudioTrack;
            ID = index;
            this.Format = Format;
            //this.Guid = Guid;
        }
        private AudioTrackOutput(AudioTrackOutput before)
        {
            Type = OutputType.AudioTrack;
            ID = before.ID;
            Name = before.Name;
            Parent = before.Parent;
            Format = before.Format;
        }

        public override Output Parent { get; internal set; }

        public override object Clone() { return new AudioTrackOutput(this); }

        public override bool Equals(DeviceOutput other) { try { return other is AudioTrackOutput ? ((AudioTrackOutput)other).Name.Equals(Name) : false; } catch { return false; } }
        public override bool Equals(object obj) { try { return obj is AudioTrackOutput ? ((AudioTrackOutput)obj).Name.Equals(Name) : false; } catch { return false; } }
        //public override int GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return Name; }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose()
        {
            try { Device.Dispose(); } catch (Exception ex) { new HSAudioLog(ex.Message, "AudioTrackOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }
        }
    }
}
#endif
