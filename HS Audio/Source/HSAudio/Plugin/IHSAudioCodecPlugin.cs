﻿#if PLG_Codec
using HSAudio.PlugIn.Codec;
using System;
using System.Collections.Generic;
using System.Text;
using HS.PlugIn;

namespace HSAudio.PlugIn
{
    public interface IHSAudioCodecPlugin : IHSAudioPlugin
    {
        /// <summary>
        ///  Cannot be null.
        /// </summary>
        /// <returns></returns>
        Description GetDescription();
    }

    /*
    public class asd : IHSAudioCodecPlugin
    {
        public IPluginInformation Information
        {
            get
            {
                return new IPlugin
            }
        }

        public PluginKind Kind
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Description GetDescription()
        {
            throw new NotImplementedException();
        }
    }
    */
}
#endif