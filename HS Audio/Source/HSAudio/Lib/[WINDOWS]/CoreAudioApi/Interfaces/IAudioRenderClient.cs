﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace HSAudio.Lib.Windows.CoreAudioApi.Interfaces
{
    [Guid("F294ACFC-3146-4483-A7BF-ADDCA7C260E2"),
    InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    interface IAudioRenderClient
    {
        int GetBuffer(int numFramesRequested, out IntPtr dataBufferPointer);
        int ReleaseBuffer(int numFramesWritten, AudioClientBufferFlags bufferFlags);
    }
}
#endif