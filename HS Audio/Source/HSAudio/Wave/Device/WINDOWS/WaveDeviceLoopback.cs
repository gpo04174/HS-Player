﻿#if WIN_VISTA || WIN_10 || WIN_10_1703 || WIN_METRO
using HSAudio.Control.Device;
using HSAudio.Lib.Windows.NAudio.Wave;
using System;

namespace HSAudio.Wave.Device
{
    public class WaveDeviceLoopback : WaveDevice
    {
        WasapiCapture Capture;
        public WaveDeviceLoopback(WasapiCapture Capture)
        {
            this.Capture = Capture;
            /*
            WaveFormat format = new WaveFormat(Capture.WaveFormat.SampleRate, Capture.WaveFormat.BitsPerSample, Capture.WaveFormat.Channels) { Encoding = Capture.WaveFormat.Encoding };
            if (format.Channels > 2) format.Channels = 2;
            */
            Formats = new WaveFormat[] { new WaveFormat(Capture.WaveFormat.SampleRate, Capture.WaveFormat.Channels, 32, WaveType.FLOAT) };
        }

        [Obsolete]
        public event DevicePropertyChangedEventHandler DevicePropertyChanged { add { }remove { } }

        public string DeviceName { get { return "Default Device Loopback"; } }
        public object Device { get { return Capture; } }

        public DeviceKind Kind { get { return DeviceKind.Output; } }

        public DeviceControl MasterControl { get { return null; } }
        public DeviceControl[] SubControls { get { return null; } }

        public WaveFormat[] Formats { get; private set; }

        public override string ToString()
        {
            return DeviceName;
        }

        public void Dispose()
        {
            Capture.StopRecording();
            Capture.Dispose();
            Capture = null;
            GC.Collect();
            IsDisposed = true;
        }
        public bool IsDisposed { get; private set; }
    }
}
#endif
