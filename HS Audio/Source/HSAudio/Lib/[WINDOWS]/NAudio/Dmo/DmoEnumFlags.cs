﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Lib.Windows.NAudio.Dmo
{
    [Flags]
    enum DmoEnumFlags
    {
        None,
        DMO_ENUMF_INCLUDE_KEYED = 0x00000001
    }
}

#endif