﻿using System;

namespace HS.Audio.Plugin
{
    public interface IHSAudioPluginDSP
    {
        IHSPluginInfo Information { get; }

    }
}
