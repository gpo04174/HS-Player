using HS.Setting;
using HSAudio.Core.Sound._3D;
using HSAudio.Lib.FMOD;
using HSAudio.Utility;
using System;

namespace HSAudio.Core.Audio._3D
{
    public partial class Audio3D : Audio
    {
        internal Audio3D(HSAudio audio, FD_ChannelGroup parent = null) : base(audio, true, parent) { Init(); }
        internal Audio3D(Sound3D Sound, FD_ChannelGroup parent = null) : base((Sound.Sound)Sound, true, parent) { Init(); }
        internal Audio3D(Sound.Sound Sound, FD_ChannelGroup parent = null) : base(Sound, false, parent) { }
        void Init()
        {

        }

        AudioState state;
        internal override void Unload()
        {
            if (group != null && group.isValid())
            {
                state = Status;
                Update3DAttribute();
                _Level = Level;
                _DopplerLevel = DopplerLevel;
                _Spread = Spread;
            }
            base.Unload();
        }

        internal override void Reload(FD_ChannelGroup group_parent)
        {
            if (!IsDisposed && audio.Output.Status == global::HSAudio.Output.OutputStatus.Open)
            {
                RESULT Result = RESULT.OK;
                this.group_parent = group_parent;
                Result = audio.system.createChannelGroup(string.Format("Audio Session ({0})", ID), out group);
                Result = group_parent.addGroup(group, false, out conn);

                Level = _Level;
                DopplerLevel = _DopplerLevel;
                Spread = _Spread;

                group.set3DAttributes(ref _Position, ref _Velocity);

                base.Reload(group_parent);
                bool load = Sound.Load();
                if (load)
                {
                    Result = audio.system.playSound(Sound.sound, group, true, out channel);

                    Result = channel.setMode(Is3D ? MODE._3D : MODE._2D);
                    Result = channel.setCallback(_CHANNEL_CALLBACK);
                    Result = channel.setMode((MODE)_Loop);
                    Result = channel.setPosition(_CurrentPosition, TIMEUNIT.MS);
                    Repeat.Reload(channel);

                    AudioState state = Status;

                    if (state == AudioState.Playing) Play();
                    else if (state == AudioState.Pausing) Pause();
                    else if (Status != AudioState.Unknown || Status != AudioState.Unknown) Status = state;
                    OnSetStatus(Status);
                }
            }
        }

        #region Properties
        /// <summary>
        /// Address of a variable that receives the position in 3D space used for panning and attenuation
        /// </summary>
        public Vector3D Position { get { Update3DAttribute(); return _Position; } set { Update3DAttribute(); Result = group.set3DAttributes(ref value, ref _Velocity); } }
        /// <summary>
        /// Address of a variable that receives the velocity in 'distance units per second' (see remarks) in 3D space
        /// </summary>
        public Vector3D Velocity { get { Update3DAttribute(); return _Velocity; } set { Update3DAttribute(); Result = group.set3DAttributes(ref _Position, ref value); } }
        [Obsolete("Unimplemented.")]
        /// <summary>
        /// (Unimplemented).
        /// </summary>
        public Vector3D getAltPanPosition()
        {
            Update3DAttribute();
            return _AltPanPosition;
        }
        float _Level;
        /// <summary>
        /// 3D Level
        /// </summary>
        public float Level
        { 
            get { float lv; Result = channel.get3DLevel(out lv); return lv; }
            set { _Level = value; Result = channel.set3DLevel(value); }
        }
        float _DopplerLevel;
        /// <summary>
        /// 3D Doppler Level
        /// </summary>
        public float DopplerLevel
        {
            get { float lv; Result = channel.get3DDopplerLevel(out lv); return lv; }
            set { _DopplerLevel = value; Result = channel.set3DDopplerLevel(value); }
        }
        float _Spread;
        /// <summary>
        /// 3D Spread
        /// </summary>
        public float Spread
        {
            get { float lv; Result = channel.get3DSpread(out lv); return lv; }
            set { _Spread = value; Result = channel.set3DSpread(value); }
        }
        #endregion

        #region Settings IO
        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Level", Level);
            s.SetValue("DopplerLevel", DopplerLevel);
            s.SetValue("Spread", Spread);
            s.SubSetting.SetValue("Position", Position.SaveSetting());
            s.SubSetting.SetValue("Velocity", Velocity.SaveSetting());
            return s;
        }
        public override bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            bool load = base.LoadSetting(Setting, ThrowException);
            if (Setting.Exist("Level")) try { Level = float.Parse(Setting["Level"]); } catch(Exception ex) { if (ThrowException) throw ex; };
            if (Setting.Exist("DopplerLevel")) try { DopplerLevel = float.Parse(Setting["DopplerLevel"]); } catch (Exception ex) { if (ThrowException) throw ex; };
            if (Setting.Exist("Spread")) try { Spread = float.Parse(Setting["Spread"]); } catch (Exception ex) { if (ThrowException) throw ex; };
            if (Setting.SubSetting.Exist("Position")) Position.LoadSetting(Setting.SubSetting["Position"], ThrowException);
            if (Setting.SubSetting.Exist("Velocity")) Velocity.LoadSetting(Setting.SubSetting["Position"], ThrowException);
            return load;
        }
        #endregion

        Vector3D _Position, _Velocity, _AltPanPosition;
        void Update3DAttribute()
        {
            group.get3DAttributes(out _Position, out _Velocity, out _AltPanPosition);
        }
    }
}