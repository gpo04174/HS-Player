﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace HSAudio.DSP.Type
{
    public class Oscillator : IDSP
    {
        public enum WaveForm
        {
            /// <summary>
            /// 사인파
            /// </summary>
            Sine,
            /// <summary>
            /// 사각파
            /// </summary>
            Square,
            /// <summary>
            /// 톱니파
            /// </summary>
            Sawup,
            /// <summary>
            /// 역 톱니파
            /// </summary>
            Sawdown,
            /// <summary>
            /// 삼각파
            /// </summary>
            Triangle,
            /// <summary>
            /// 화이트 노이즈
            /// </summary>
            Noise
        }

        private Parameter[] param = new Parameter[2];
        public Oscillator(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.OSCILLATOR, "Oscillator", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Waveform type
        /// </summary>
        public WaveForm Type
        {
            get { return (WaveForm)DSP.GetParameter((int)DSP_OSCILLATOR.TYPE, 0); }
            set { DSP.SetParameter((int)DSP_OSCILLATOR.TYPE, (int)value); }
        }
        /// <summary>
        /// Frequency of the sinewave in hz.  1.0 to 22000.0.  Default = 220.0
        /// </summary>
        public float Rate
        {
            get { return DSP.GetParameter((int)DSP_OSCILLATOR.RATE, 0f); }
            set { DSP.SetParameter((int)DSP_OSCILLATOR.RATE, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Type")) try { Type = (WaveForm)Enum.Parse(typeof(WaveForm), Setting["Type"]); } catch(Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Rate")) try { Rate = float.Parse(Setting["Rate"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Type", Type.ToString());
            s.SetValue("Rate", Rate);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
