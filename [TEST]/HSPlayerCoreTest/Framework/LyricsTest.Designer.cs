﻿namespace HSPlayerCoreTest
{
    partial class frmLyricsTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.파일FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newLyricsNToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.OpenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RemappingTimeIs0ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.viewPureLyricsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewPureLyricsOldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoApplyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.chkOffsetAuto = new System.Windows.Forms.CheckBox();
            this.txtLength = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtComment = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAlbum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtArtist = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnOffsetApply = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.numOffset = new System.Windows.Forms.NumericUpDown();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnViewXML = new System.Windows.Forms.Button();
            this.btnGetLyrics = new System.Windows.Forms.Button();
            this.txtSearchCount = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cbSearch = new System.Windows.Forms.ComboBox();
            this.txtSearchArtist = new System.Windows.Forms.TextBox();
            this.txtSearchName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.deviceControl1 = new HSAudioTest.Test.GUI.DeviceControl();
            this.playControl1 = new HSAudioTest.Test.GUI.PlayControl();
            this.loadMusic1 = new HSAudioTest.Test.GUI.LoadMusic();
            this.label10 = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtLyrics = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.chkAutoloadXML = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOffset)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.파일FToolStripMenuItem,
            this.viewVToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(802, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 파일FToolStripMenuItem
            // 
            this.파일FToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newLyricsNToolStripMenuItem,
            this.toolStripSeparator2,
            this.OpenToolStripMenuItem,
            this.SaveAsToolStripMenuItem});
            this.파일FToolStripMenuItem.Name = "파일FToolStripMenuItem";
            this.파일FToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.파일FToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.파일FToolStripMenuItem.Text = "File(&F)";
            // 
            // newLyricsNToolStripMenuItem
            // 
            this.newLyricsNToolStripMenuItem.Name = "newLyricsNToolStripMenuItem";
            this.newLyricsNToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newLyricsNToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.newLyricsNToolStripMenuItem.Text = "New Lyrics(&N)";
            this.newLyricsNToolStripMenuItem.Click += new System.EventHandler(this.newLyricsNToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(187, 6);
            // 
            // OpenToolStripMenuItem
            // 
            this.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem";
            this.OpenToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.OpenToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.OpenToolStripMenuItem.Text = "Open(&O)";
            this.OpenToolStripMenuItem.Click += new System.EventHandler(this.OpenToolStripMenuItem_Click);
            // 
            // SaveAsToolStripMenuItem
            // 
            this.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem";
            this.SaveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.SaveAsToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.SaveAsToolStripMenuItem.Text = "Save As(&S)";
            this.SaveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // viewVToolStripMenuItem
            // 
            this.viewVToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshRToolStripMenuItem,
            this.RemappingTimeIs0ToolStripMenuItem,
            this.toolStripSeparator1,
            this.viewPureLyricsToolStripMenuItem,
            this.viewPureLyricsOldToolStripMenuItem});
            this.viewVToolStripMenuItem.Name = "viewVToolStripMenuItem";
            this.viewVToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.viewVToolStripMenuItem.Text = "View(&V)";
            this.viewVToolStripMenuItem.DropDownOpening += new System.EventHandler(this.viewVToolStripMenuItem_DropDownOpening);
            // 
            // refreshRToolStripMenuItem
            // 
            this.refreshRToolStripMenuItem.Name = "refreshRToolStripMenuItem";
            this.refreshRToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshRToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.refreshRToolStripMenuItem.Text = "Refresh(&R)";
            this.refreshRToolStripMenuItem.Click += new System.EventHandler(this.refreshRToolStripMenuItem_Click);
            // 
            // RemappingTimeIs0ToolStripMenuItem
            // 
            this.RemappingTimeIs0ToolStripMenuItem.Name = "RemappingTimeIs0ToolStripMenuItem";
            this.RemappingTimeIs0ToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.RemappingTimeIs0ToolStripMenuItem.Text = "Remapping time 00:00.00";
            this.RemappingTimeIs0ToolStripMenuItem.Click += new System.EventHandler(this.RemappingTimeIs0ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(236, 6);
            // 
            // viewPureLyricsToolStripMenuItem
            // 
            this.viewPureLyricsToolStripMenuItem.Name = "viewPureLyricsToolStripMenuItem";
            this.viewPureLyricsToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.viewPureLyricsToolStripMenuItem.Text = "View Pure Lyrics (Low Memory)";
            this.viewPureLyricsToolStripMenuItem.Click += new System.EventHandler(this.viewPureLyricsToolStripMenuItem_Click);
            // 
            // viewPureLyricsOldToolStripMenuItem
            // 
            this.viewPureLyricsOldToolStripMenuItem.Name = "viewPureLyricsOldToolStripMenuItem";
            this.viewPureLyricsOldToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.viewPureLyricsOldToolStripMenuItem.Text = "View Pure Lyrics";
            this.viewPureLyricsOldToolStripMenuItem.Click += new System.EventHandler(this.viewPureLyricsOldToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AutoApplyToolStripMenuItem,
            this.debugModeToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // AutoApplyToolStripMenuItem
            // 
            this.AutoApplyToolStripMenuItem.CheckOnClick = true;
            this.AutoApplyToolStripMenuItem.Name = "AutoApplyToolStripMenuItem";
            this.AutoApplyToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.AutoApplyToolStripMenuItem.Text = "Auto Apply";
            // 
            // debugModeToolStripMenuItem
            // 
            this.debugModeToolStripMenuItem.CheckOnClick = true;
            this.debugModeToolStripMenuItem.Name = "debugModeToolStripMenuItem";
            this.debugModeToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.debugModeToolStripMenuItem.Text = "Debug Mode";
            this.debugModeToolStripMenuItem.CheckedChanged += new System.EventHandler(this.debugModeToolStripMenuItem_CheckedChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "가사 파일 (*.lrc)|*.lrc|모든 파일|*.*";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "가사 파일 (*.lrc)|*.lrc|모든 파일|*.*";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(802, 162);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tabPage1.Controls.Add(this.chkOffsetAuto);
            this.tabPage1.Controls.Add(this.txtLength);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtMaker);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtAuthor);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtComment);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtAlbum);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtArtist);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.txtTitle);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.btnOffsetApply);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.numOffset);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(794, 136);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tools";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // chkOffsetAuto
            // 
            this.chkOffsetAuto.AutoSize = true;
            this.chkOffsetAuto.Location = new System.Drawing.Point(516, 60);
            this.chkOffsetAuto.Name = "chkOffsetAuto";
            this.chkOffsetAuto.Size = new System.Drawing.Size(49, 16);
            this.chkOffsetAuto.TabIndex = 22;
            this.chkOffsetAuto.Text = "Auto";
            this.chkOffsetAuto.UseVisualStyleBackColor = true;
            this.chkOffsetAuto.Visible = false;
            // 
            // txtLength
            // 
            this.txtLength.Location = new System.Drawing.Point(376, 86);
            this.txtLength.Name = "txtLength";
            this.txtLength.Size = new System.Drawing.Size(134, 21);
            this.txtLength.TabIndex = 21;
            this.txtLength.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(327, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 12);
            this.label9.TabIndex = 20;
            this.label9.Text = "Length";
            // 
            // txtMaker
            // 
            this.txtMaker.Location = new System.Drawing.Point(324, 32);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(186, 21);
            this.txtMaker.TabIndex = 19;
            this.txtMaker.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(281, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 12);
            this.label8.TabIndex = 18;
            this.label8.Text = "Maker";
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(324, 5);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(186, 21);
            this.txtAuthor.TabIndex = 17;
            this.txtAuthor.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(281, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "Author";
            // 
            // txtComment
            // 
            this.txtComment.Location = new System.Drawing.Point(71, 85);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(250, 21);
            this.txtComment.TabIndex = 15;
            this.txtComment.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 12);
            this.label6.TabIndex = 14;
            this.label6.Text = "Comment";
            // 
            // txtAlbum
            // 
            this.txtAlbum.Location = new System.Drawing.Point(48, 58);
            this.txtAlbum.Name = "txtAlbum";
            this.txtAlbum.Size = new System.Drawing.Size(224, 21);
            this.txtAlbum.TabIndex = 13;
            this.txtAlbum.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 62);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 12;
            this.label5.Text = "Album";
            // 
            // txtArtist
            // 
            this.txtArtist.Location = new System.Drawing.Point(48, 31);
            this.txtArtist.Name = "txtArtist";
            this.txtArtist.Size = new System.Drawing.Size(224, 21);
            this.txtArtist.TabIndex = 11;
            this.txtArtist.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "Artist";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(48, 4);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(224, 21);
            this.txtTitle.TabIndex = 9;
            this.txtTitle.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "Title";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "Offset";
            // 
            // btnOffsetApply
            // 
            this.btnOffsetApply.Location = new System.Drawing.Point(448, 57);
            this.btnOffsetApply.Name = "btnOffsetApply";
            this.btnOffsetApply.Size = new System.Drawing.Size(62, 21);
            this.btnOffsetApply.TabIndex = 6;
            this.btnOffsetApply.Text = "Mapping";
            this.btnOffsetApply.UseVisualStyleBackColor = true;
            this.btnOffsetApply.Click += new System.EventHandler(this.btnOffsetApply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "ms";
            // 
            // numOffset
            // 
            this.numOffset.Location = new System.Drawing.Point(325, 57);
            this.numOffset.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numOffset.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            -2147483648});
            this.numOffset.Name = "numOffset";
            this.numOffset.Size = new System.Drawing.Size(88, 21);
            this.numOffset.TabIndex = 4;
            this.numOffset.ValueChanged += new System.EventHandler(this.numOffset_ValueChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel3);
            this.tabPage3.Controls.Add(this.btnGetLyrics);
            this.tabPage3.Controls.Add(this.txtSearchCount);
            this.tabPage3.Controls.Add(this.btnSearch);
            this.tabPage3.Controls.Add(this.cbSearch);
            this.tabPage3.Controls.Add(this.txtSearchArtist);
            this.tabPage3.Controls.Add(this.txtSearchName);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(794, 136);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Search";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnViewXML
            // 
            this.btnViewXML.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewXML.Location = new System.Drawing.Point(86, 2);
            this.btnViewXML.Name = "btnViewXML";
            this.btnViewXML.Size = new System.Drawing.Size(112, 23);
            this.btnViewXML.TabIndex = 11;
            this.btnViewXML.Text = "View Raw XML";
            this.btnViewXML.UseVisualStyleBackColor = true;
            this.btnViewXML.Click += new System.EventHandler(this.btnViewXML_Click);
            // 
            // btnGetLyrics
            // 
            this.btnGetLyrics.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetLyrics.Location = new System.Drawing.Point(337, 5);
            this.btnGetLyrics.Name = "btnGetLyrics";
            this.btnGetLyrics.Size = new System.Drawing.Size(65, 49);
            this.btnGetLyrics.TabIndex = 10;
            this.btnGetLyrics.Text = "Auto Search";
            this.btnGetLyrics.UseVisualStyleBackColor = true;
            this.btnGetLyrics.Click += new System.EventHandler(this.btnGetLyrics_Click);
            // 
            // txtSearchCount
            // 
            this.txtSearchCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchCount.AutoSize = true;
            this.txtSearchCount.Location = new System.Drawing.Point(55, 85);
            this.txtSearchCount.Name = "txtSearchCount";
            this.txtSearchCount.Size = new System.Drawing.Size(61, 12);
            this.txtSearchCount.TabIndex = 7;
            this.txtSearchCount.Text = "Results: 0";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(276, 5);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(55, 49);
            this.btnSearch.TabIndex = 6;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cbSearch
            // 
            this.cbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSearch.FormattingEnabled = true;
            this.cbSearch.Items.AddRange(new object[] {
            "(Current Lyrics)"});
            this.cbSearch.Location = new System.Drawing.Point(57, 60);
            this.cbSearch.Name = "cbSearch";
            this.cbSearch.Size = new System.Drawing.Size(734, 20);
            this.cbSearch.TabIndex = 5;
            this.cbSearch.SelectedIndexChanged += new System.EventHandler(this.cbSearch_SelectedIndexChanged);
            // 
            // txtSearchArtist
            // 
            this.txtSearchArtist.Location = new System.Drawing.Point(57, 33);
            this.txtSearchArtist.Name = "txtSearchArtist";
            this.txtSearchArtist.Size = new System.Drawing.Size(213, 21);
            this.txtSearchArtist.TabIndex = 4;
            // 
            // txtSearchName
            // 
            this.txtSearchName.Location = new System.Drawing.Point(57, 5);
            this.txtSearchName.Name = "txtSearchName";
            this.txtSearchName.Size = new System.Drawing.Size(213, 21);
            this.txtSearchName.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(47, 12);
            this.label14.TabIndex = 2;
            this.label14.Text = "Results";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 12);
            this.label13.TabIndex = 1;
            this.label13.Text = "Artist";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 11);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "Name";
            // 
            // tabPage2
            // 
			this.tabPage2.Controls.Add(this.deviceControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(794, 136);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // playControl1
            // 
            this.playControl1.audio = null;
            this.playControl1.Audio = null;
            this.playControl1.Location = new System.Drawing.Point(3, 30);
            this.playControl1.Name = "playControl1";
            this.playControl1.Size = new System.Drawing.Size(385, 125);
            this.playControl1.TabIndex = 11;
            // 
            // deviceControl1
            // 
            this.deviceControl1.Audio = null;
            this.deviceControl1.Location = new System.Drawing.Point(2, 0);
            this.deviceControl1.Name = "deviceControl1";
            this.deviceControl1.Size = new System.Drawing.Size(461, 137);
            this.deviceControl1.TabIndex = 0;
            // 
            // loadMusic1
            // 
            this.loadMusic1.Location = new System.Drawing.Point(2, 2);
            this.loadMusic1.MusicPath = "";
            this.loadMusic1.Name = "loadMusic1";
            this.loadMusic1.Size = new System.Drawing.Size(385, 28);
            this.loadMusic1.TabIndex = 12;
            //
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Gulim", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.Blue;
            this.label10.Location = new System.Drawing.Point(2, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Sync lyrics test";
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApply.Location = new System.Drawing.Point(0, 606);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(802, 23);
            this.btnApply.TabIndex = 23;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(4, 186);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label10);
            this.splitContainer1.Panel1.Controls.Add(this.comboBox1);
            this.splitContainer1.Panel1.Controls.Add(this.label11);
            this.splitContainer1.Panel1.Controls.Add(this.panel1);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.txtLyrics);
            this.splitContainer1.Size = new System.Drawing.Size(798, 420);
            this.splitContainer1.SplitterDistance = 396;
            this.splitContainer1.TabIndex = 24;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "(No)",
            "한국어",
            "日本語",
            "English"});
            this.comboBox1.Location = new System.Drawing.Point(236, 156);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(141, 160);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 12);
            this.label11.TabIndex = 3;
            this.label11.Text = "Language hide";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.loadMusic1);
            this.panel1.Controls.Add(this.playControl1);
            this.panel1.Location = new System.Drawing.Point(2, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(392, 154);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(2, 178);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(392, 241);
            this.panel2.TabIndex = 2;
            // 
            // txtLyrics
            // 
            this.txtLyrics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLyrics.Location = new System.Drawing.Point(0, 0);
            this.txtLyrics.Multiline = true;
            this.txtLyrics.Name = "txtLyrics";
            this.txtLyrics.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLyrics.Size = new System.Drawing.Size(398, 420);
            this.txtLyrics.TabIndex = 1;
            this.txtLyrics.TextChanged += new System.EventHandler(this.txtLyrics_TextChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 20;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.chkAutoloadXML);
            this.panel3.Controls.Add(this.btnViewXML);
            this.panel3.Location = new System.Drawing.Point(590, 85);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(201, 28);
            this.panel3.TabIndex = 12;
            this.panel3.Visible = false;
            // 
            // chkAutoloadXML
            // 
            this.chkAutoloadXML.AutoSize = true;
            this.chkAutoloadXML.Location = new System.Drawing.Point(4, 6);
            this.chkAutoloadXML.Name = "chkAutoloadXML";
            this.chkAutoloadXML.Size = new System.Drawing.Size(77, 16);
            this.chkAutoloadXML.TabIndex = 12;
            this.chkAutoloadXML.Text = "Auto load";
            this.chkAutoloadXML.Checked = true;
            this.chkAutoloadXML.UseVisualStyleBackColor = true;
            // 
            // frmLyricsTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 629);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmLyricsTest";
            this.Text = "Lyrics Manager";
            this.Load += new System.EventHandler(this.frmLyricsTest_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numOffset)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 파일FToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveAsToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnOffsetApply;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numOffset;
        private System.Windows.Forms.TextBox txtLength;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtAuthor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtComment;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAlbum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtArtist;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem viewVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshRToolStripMenuItem;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox txtLyrics;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkOffsetAuto;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox cbSearch;
        private System.Windows.Forms.TextBox txtSearchArtist;
        private System.Windows.Forms.TextBox txtSearchName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label txtSearchCount;
        private System.Windows.Forms.ToolStripMenuItem viewPureLyricsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewPureLyricsOldToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AutoApplyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RemappingTimeIs0ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newLyricsNToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TabPage tabPage2;
        private HSAudioTest.Test.GUI.PlayControl playControl1;
        private HSAudioTest.Test.GUI.DeviceControl deviceControl1;
        private HSAudioTest.Test.GUI.LoadMusic loadMusic1;
        private System.Windows.Forms.Button btnGetLyrics;
        private System.Windows.Forms.Button btnViewXML;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox chkAutoloadXML;
    }
}