﻿using HSAudio;
using System.Windows.Forms;

namespace HSAudioTest.Test.GUI
{
    public partial class SpeakerControlSwitch : UserControl
    {
        #region Designer        
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            this.numericUpDown1.Location = new System.Drawing.Point(3, 22);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(49, 21);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(1, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Left";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SpeakerControlSwitch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label1);
            this.Name = "SpeakerControlSwitch";
            this.Size = new System.Drawing.Size(55, 45);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        #endregion

        public SpeakerControlSwitch()
        {
            InitializeComponent();
        }
        public SpeakerControlSwitch(Coord2D Coord) :this()
        {
            this.Coord = Coord;
        }

        public Coord2D Coord { get; set; }

        public event EventHandler ValueChange;

        public new string Text
        {
            get { return label1.Text; }
            set
            {
                label1.Text = value;
                //if (value == null) Size = new System.Drawing.Size(80, 25);
                //else Size = new System.Drawing.Size(80, 45);
            }
        }
        public float Value
        {
            get { return (float)numericUpDown1.Value; }
            set
            {
                numericUpDown1.Value = (decimal)value;
                if (ValueChange != null) ValueChange.Invoke(this);
            }
        }

        private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
        {
            if (ValueChange != null) ValueChange.Invoke(this);
        }
        /*
        public bool Use
        {
            get { return checkBox1.Checked; }
            set
            {
                checkBox1.Checked = value;
                if (ValueChange != null) ValueChange.Invoke(this);
            }
        }
        */
    }
}
