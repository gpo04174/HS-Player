﻿using System.Xml;

namespace HSPlayerCore.Lyrics.ALSong.Search
{
    public class SearchResult : SearchKeyword
    {
        internal SearchResult(XmlNode node) : base(node.ChildNodes[1].InnerText, node.ChildNodes[2].InnerText)
        {
            this.ID = node.ChildNodes[0].InnerText;
            this.Album = node.ChildNodes[3].InnerText;
        }
        internal SearchResult(string ID, string Title, string Artist, string Album) : base(Title, Artist)
        {
            this.Artist = Artist;
            this.Album = Album;
        }
        public string Album { get; protected set; }
        public string ID { get; protected set; }

        public override string ToString()
        {
            return string.Format("{0} - {1} [{2}]", Artist, Title, Album);
        }
    }
}
