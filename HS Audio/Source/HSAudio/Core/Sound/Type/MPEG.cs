﻿using System;

namespace HSAudio.Core.Sound.Type
{
    public class MPEG : ISoundFormat
    {
        public MPEG() : base(SoundType.MPEG, MIME, Extension, Description){}

        public static string[] MIME { get { return new string[] { "audio/mpeg" }; } }
        public static string[] Extension { get { return new string[] { "mp3", "mp2", "mp1" }; } }
        public static string[] Description { get { return new string[] { "MPEG-1 Audio Layer III", "MPEG-2 Audio Layer III" }; } }

        public override string ToString() {return Description[0]; }
    }
}
