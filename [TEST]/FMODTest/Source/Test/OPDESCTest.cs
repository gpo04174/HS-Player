﻿using HSAudio.Lib.FMOD;
using System;
using System.Text;

namespace FMODTest.Test
{
    public class OPDESCTest
    {
        public static void Test()
        {
            FD_System system;
            Factory.System_Create(out system);
            RESULT result = RESULT.OK;
            uint h = 0;

            OutputInfo[] oi = new OutputInfo[2];
            oi[0] = new OutputInfo("Temp1", new GUID(), 44100, 2);
            oi[1] = new OutputInfo("Temp2", new GUID(), 96000, 6);

            Description desc = new Description("Test Output", oi);
            uint ver;system.getVersion(out ver);
            desc._Description.apiversion = 3;
            result = system.registerOutput(ref desc._Description, out h);
            result = system.setOutputByPlugin(h);
            result = system.init(10, INITFLAGS.NORMAL, IntPtr.Zero);

        }

        class Description
        {
            internal OUTPUT_DESCRIPTION _Description;

            private OutputInfo[] info;
            public Description(string Name, OutputInfo[] info)
            {
                if (info == null || info.Length == 0) ;
                this.info = info;
                this.Name = Name;
                _Description.name = Name;
                _Description.getnumdrivers = new OUTPUT_GETNUMDRIVERSCALLBACK(OUTPUT_GETDRIVERINFOCALLBACK);
                _Description.getdriverinfo = new OUTPUT_GETDRIVERINFOCALLBACK(OUTPUT_GETDRIVERINFOCALLBACK);
                _Description.init = new OUTPUT_INITCALLBACK(OUTPUT_INITCALLBACK);
                _Description.close = new OUTPUT_CLOSECALLBACK(OUTPUT_CLOSECALLBACK);
                _Description.getposition = new OUTPUT_GETPOSITIONCALLBACK(OUTPUT_GETPOSITIONCALLBACK);
                _Description.@lock = new OUTPUT_LOCKCALLBACK(OUTPUT_LOCKCALLBACK);
            }

            public string Name { get; private set; }

            #region Callback
            private RESULT OUTPUT_GETDRIVERINFOCALLBACK(ref OUTPUT_STATE state, ref int numdrivers) { numdrivers = info.Length; return RESULT.OK; }
            //GetDriverCallbackInernal
            private unsafe RESULT OUTPUT_GETDRIVERINFOCALLBACK(ref OUTPUT_STATE state, int id, IntPtr name, int namelen, ref GUID guid, ref int systemrate, ref SPEAKERMODE speakermode, ref int speakermodechannels)
            {
                try
                {
                    if (name != IntPtr.Zero)
                    {
                        string str = info[id].Name;
                        namelen = Math.Max(Encoding.UTF8.GetByteCount(str), namelen);
                        fixed (char* p = str)
                        {
                            byte* n = (byte*)name.ToPointer();
                            Encoding.UTF8.GetBytes(p, str.Length, n, namelen);
                        }
                    }

                    guid = info[id].Guid; systemrate = info[id].Samplerate; speakermode = (SPEAKERMODE)info[id].Mode; speakermodechannels = info[id].Channel;
                    return RESULT.OK;
                }
                catch (Exception ex) { return RESULT.ERR_INVALID_PARAM; }
            }

            private RESULT OUTPUT_INITCALLBACK(ref OUTPUT_STATE state, int selecteddriver, INITFLAGS flags, ref int outputrate, ref SPEAKERMODE speakermode, ref int speakermodechannels, ref SOUND_FORMAT outputformat, int dspbufferlength, int dspnumbuffers, ref IntPtr extradriverdata)
            {
                return RESULT.OK;
            }
            private RESULT OUTPUT_CLOSECALLBACK(ref OUTPUT_STATE state)
            {
                return RESULT.OK;
            }
            private RESULT OUTPUT_GETPOSITIONCALLBACK(ref OUTPUT_STATE state, ref uint pcm)
            {
                return RESULT.OK;
            }
            private RESULT OUTPUT_LOCKCALLBACK(ref OUTPUT_STATE state, uint offset, uint length, ref IntPtr ptr1, ref IntPtr ptr2, ref uint len1, ref uint len2)
            {
                return RESULT.OK;
            }
            #endregion
        }

        class OutputInfo
        {
            public OutputInfo(string Name, GUID Guid, int Samplerate, SPEAKERMODE Mode)
            {
                this.Name = Name;
                this.Samplerate = Samplerate;
                this.Mode = Mode;
            }
            public OutputInfo(string Name, GUID Guid, int Samplerate, int Channel)
            {
                this.Name = Name;
                this.Samplerate = Samplerate;
                Mode = SPEAKERMODE.RAW;
                this.Channel = Channel;
            }
            internal OUTPUT_STATE State { get; set; }
            public string Name { get; set; }
            public GUID Guid { get; set; }
            public int Samplerate { get; set; }
            public SPEAKERMODE Mode { get; set; }
            public int Channel { get; set; }
        }
    }
}
