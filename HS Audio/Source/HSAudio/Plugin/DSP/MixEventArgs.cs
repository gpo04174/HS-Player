﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSAudio.PlugIn.DSP
{
    public class MixEventArgs : PluginDSPEventArgs
    {
        internal MixEventArgs(ref DSP_STATE state, int stage) { State = state; Stage = stage; }

        protected internal DSP_STATE State { get; private set; }
        public int Stage { get; private set; }
    }
}
