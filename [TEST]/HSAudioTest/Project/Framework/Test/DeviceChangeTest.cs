﻿using HSAudio.Core.Audio;
using HSAudio.Core.Sound;
using HSAudio.Lib.FMOD;
using HSAudio.Output.Devices;
using System;
using System.Threading;

namespace HSAudioTest.Test
{
    class DeviceChangeTest
    {
        public static void Test()
        {
            RESULT result = RESULT.OK;
            FD_System system;
            FD_Sound sound;
            FD_Channel channel;
            FD_ChannelGroup cg;
            Thread th = new Thread(Loop);

            Factory.System_Create(out system);
            //th.Start(system);

            result = system.setOutput(OUTPUTTYPE.WASAPI);
            result = system.setDriver(0);
            result = system.init(32, INITFLAGS.NORMAL, IntPtr.Zero);
            result = system.createChannelGroup("Temp1", out cg);
            result = system.createSound("C:\\Users\\gpo04\\OneDrive\\음악\\보컬로이드\\IA\\Orangestar\\シンクロナイザー.mp3", MODE.DEFAULT, out sound);
            result = system.playSound(sound, cg, false, out channel);
            Thread.Sleep(5000);
            system.close();
            result = system.setDriver(1);
            result = system.init(32, INITFLAGS.NORMAL, IntPtr.Zero);
            //result = system.createChannelGroup("Temp1", out cg);
            result = system.createSound("C:\\Users\\gpo04\\OneDrive\\음악\\보컬로이드\\IA\\Orangestar\\シンクロナイザー.mp3", MODE.DEFAULT, out sound);
            result = system.playSound(sound, cg, false, out channel);
            //while (true) { Thread.Sleep(1); }
            Thread.Sleep(5000);
        }
        public static void Test1()
        {
            WASAPIOutput[] output = WASAPIOutput.GetOutputs();

            HSAudio.HSAudio audio = new HSAudio.HSAudio(output[0]);
            Sound s = Sound.FromFile(audio, "C:\\Users\\gpo04\\OneDrive\\음악\\보컬로이드\\IA\\Orangestar\\シンクロナイザー.mp3");
            Audio a = audio.Audio.Add(s);
            a.Play();
            Thread.Sleep(5000);
            //while (true) { Thread.Sleep(1); }
            
            RESULT result = RESULT.OK;
            FD_Channel channel;
            FD_Sound sound;
            FD_ChannelGroup cg;
            audio.Output.Close();
            audio.Output.Open(output[1]);
            //result = audio.system.setDriver(1);
            //result = audio.system.init(32, INITFLAGS.NORMAL, IntPtr.Zero);
            //result = audio.system.createChannelGroup("Temp1", out cg);
            result = audio.system.createSound("C:\\Users\\gpo04\\OneDrive\\음악\\보컬로이드\\IA\\Orangestar\\シンクロナイザー.mp3", MODE.DEFAULT, out s.sound);
            a.setSound(s);
            a.Play();
            Thread.Sleep(5000);
        }
        static void Loop(object o)
        {
            FD_System system = o as FD_System;
            RESULT Result = RESULT.OK;
            while (system != null)
            {
                Result = system.update();
                if (Result != RESULT.OK) Console.WriteLine("Fail({0}) - System::Update()", Result);
                Thread.Sleep(1);
            }
        }
    }
}
