﻿using HSAudio.Core.Sound;
using HSAudio.Core.Sound._2D;
using HSAudio.Utility.Stream;
using System;

namespace TagPlugin
{                                                                                                                                     
    //나중에 자체 플러그인 인터페이스 구현하기.                                                                                       
    public class TagPlugin                                                                                                            
    {                                                                                                                                 
        public TagPlugin()                                                                                                            
        {                                                                                                                             
                                                                                                                                      
        }                                                                                                                             
        public TagManager GetTagManager(Sound sound)                                                                                  
        {                                                                                                                             
            if (sound is SoundNetwork2D) return GetTagManager_Network((SoundNetwork2D)sound, true);                                   
            return null;                                                                                                              
        }                                                                                                                             
                                                                                                                                      
                                                                                                                                      
        #region GetTagManager                                                                                                         
        public TagManager GetTagManager_Network(SoundNetwork2D sound, bool ThrowException = true)                                     
        {                                                                                                                             
            if (string.IsNullOrEmpty(sound.NetworkURL) || sound.NetworkURL.Length < 4) return null;                                   
            else                                                                                                                      
            {                                                                                                                         
                string protocol = sound.NetworkURL.Substring(0, 3).ToUpper();
                if (protocol == "HTT" || protocol == "FTP") return GetTagManager_Network(sound, 3000, false, ThrowException);
                else return GetTagManager_Network(sound, 3000, true, ThrowException);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Timeout"></param>
        /// <param name="IsSocket">True is TCP socket, False is Web http</param>
        /// <param name="ThrowException"></param>
        /// <returns></returns>
        public TagManager GetTagManager_Network(SoundNetwork2D sound, int Timeout = 3000, bool IsSocket = false, bool ThrowException = true)
        {
            WebStreamRead stream = null;
            try
            {
                if (IsSocket)
                {
                    Uri uri = new Uri(sound.NetworkURL);
                    stream = new WebStreamRead(uri, Timeout);
                    return new TagManager(sound, sound.NetworkURL, stream, false, sound.Type.MIMEs[0]);
                }
                else
                {

                    stream = new WebStreamRead(sound.NetworkURL, Timeout);
                    return new TagManager(sound, sound.NetworkURL, stream, false, sound.Type.MIMEs[0]);
                }
            }

            catch (Exception ex) { if (ThrowException) throw ex; }
            finally { if (stream != null) try { stream.Dispose(); } catch { } }
            return null;
        }
        #endregion                                                                                                                   
    }                                                                                                                                   
}                                                                                                                                       
                                                                                                                                        