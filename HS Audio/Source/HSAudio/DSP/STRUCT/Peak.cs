﻿using HSAudio.Lib.FMOD;

namespace HSAudio.DSP.Inner
{
    public class Peak
    {
        DSP dsp;
        internal Peak(DSP dsp) { this.dsp = dsp; }
        public float[] InPeak { get; internal set; }
        public float[] OutPeak { get; internal set; }
        public float[] InRMS { get; internal set; }
        public float[] OutRMS { get; internal set; }


        private DSP_METERING_INFO _Inner = new DSP_METERING_INFO();
        private DSP_METERING_INFO _Outer = new DSP_METERING_INFO();
        private void UpdateInfo()
        {
            dsp.DSPInternal.getMeteringInfo(_Inner, _Outer);
            InPeak = _Inner.peaklevel;
            InRMS = _Inner.rmslevel;
            OutPeak = _Inner.peaklevel;
            OutRMS = _Inner.rmslevel;
        }
    }
}
