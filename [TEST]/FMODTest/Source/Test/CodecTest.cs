﻿using HSAudio.Lib.FMOD;
using System;
using System.Diagnostics;
using System.Text;

namespace FMODTest.Test
{
    public class CodecTest
    {
        public static void Test(string Path)
        {
            Description desc = new Description();

            FD_System system;
            FD_Sound sound;

            Factory.System_Create(out system);
            RESULT result = RESULT.OK;
            uint h = 0;

            result = system.registerCodec(ref desc._Description, out h, 0);

            PLUGINTYPE type;
            StringBuilder name = new StringBuilder(256);
            uint ver;
            system.getPluginInfo(h, out type, name, 256, out ver);

            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            result = system.createSound(Path, MODE.DEFAULT, out sound);
        }

    }
    public class Description
    {
        internal CODEC_DESCRIPTION _Description = new CODEC_DESCRIPTION();
        private CODEC_WAVEFORMAT _WaveFormat;
        private CODEC_STATE codec_state;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Version"></param>
        /// <param name="IsStream"></param>
        /// <param name="SupportTime">When setposition is called, only these time formats will be passed to the codec. Use bitwise OR to accumulate different types.</param>
        public Description()
        {
            _Description.name = "TestCodec";
            _Description.version = 1;
            _Description.defaultasstream = 1;
            _Description.timeunits = TIMEUNIT.MS | TIMEUNIT.RAWBYTES | TIMEUNIT.PCMBYTES;
            _Description.open = new CODEC_OPENCALLBACK(CODEC_OPENCALLBACK);
            _Description.close = new CODEC_CLOSECALLBACK(CODEC_CLOSECALLBACK);
            _Description.read = new CODEC_READCALLBACK(CODEC_READCALLBACK);
            _Description.setposition = new CODEC_SETPOSITIONCALLBACK(CODEC_SETPOSITIONCALLBACK);
            _Description.getposition = new CODEC_GETPOSITIONCALLBACK(CODEC_GETPOSITIONCALLBACK);
            _Description.getwaveformat = new CODEC_GETWAVEFORMATCALLBACK(CODEC_GETWAVEFORMATCALLBACK);
            _Description.getlength = new CODEC_GETLENGTHCALLBACK(CODEC_GETLENGTHCALLBACK);

            codec_state.fileread = new FILE_READCALLBACK(FILE_READCALLBACK);
        }
        internal void Register()
        {

        }


        #region Callback
        private RESULT CODEC_OPENCALLBACK(ref CODEC_STATE codec_state, MODE usermode, ref CREATESOUNDEXINFO userexinfo)
        {
            uint read = 0;
            codec_state.fileread(IntPtr.Zero, IntPtr.Zero, 0, ref read, IntPtr.Zero);
            codec_state.fileseek += FILE_SEEKCALLBACK;
            return RESULT.OK;
        }
        private RESULT CODEC_CLOSECALLBACK(ref CODEC_STATE codec_state)
        {
            Debug.WriteLine(codec_state.waveformatversion.ToString());
            return RESULT.OK;
        }
        private RESULT CODEC_READCALLBACK(ref CODEC_STATE codec_state, IntPtr buffer, uint samples_in, ref uint samples_out)
        {
            return RESULT.OK;
        }
        private RESULT CODEC_GETLENGTHCALLBACK(ref CODEC_STATE codec_state, ref int length, TIMEUNIT lengthtype) { return RESULT.OK; }
        private RESULT CODEC_SETPOSITIONCALLBACK(ref CODEC_STATE codec_state, int subsound, uint position, TIMEUNIT postype) { return RESULT.OK; }
        private RESULT CODEC_GETPOSITIONCALLBACK(ref CODEC_STATE codec_state, ref uint position, TIMEUNIT postype) { return RESULT.OK; }
        private RESULT CODEC_SOUNDCREATECALLBACK(ref CODEC_STATE codec_state, int subsound, ref FD_Sound sound) { return RESULT.OK; }
        private RESULT CODEC_METADATACALLBACK(ref CODEC_STATE codec_state, TAGTYPE tagtype, string name, IntPtr data, uint datalen, TAGDATATYPE datatype, int unique)
        {
            return RESULT.OK;
        }//char* 형 검사하기 (out? in?)
        private RESULT CODEC_GETWAVEFORMATCALLBACK(ref CODEC_STATE codec_state, ref CODEC_WAVEFORMAT waveformat)
        {
            return RESULT.OK;
        }


        private RESULT FILE_READCALLBACK(IntPtr handle, IntPtr buffer, uint sizebytes, ref uint bytesread, IntPtr userdata)
        {
            return RESULT.OK;
        }
        private RESULT FILE_OPENCALLBACK(StringWrapper name, ref uint filesize, ref IntPtr handle, IntPtr userdata)
        {
            return RESULT.OK;
        }
        private RESULT FILE_CLOSECALLBACK(IntPtr handle, IntPtr userdata)
        {
            return RESULT.OK;
        }
        private RESULT FILE_SEEKCALLBACK(IntPtr handle, uint pos, IntPtr userdata)
        {
            return RESULT.OK;
        }
        #endregion
    }
}
