﻿using HS.Attributes;
using HSAudio.Core.Sound._2D;
using HSAudio.Core.Sound.Type;
using HSAudio.DSP;
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Resource;
using HSAudio.Wave;
using HS.Setting;
using System;
using HS;
using HSAudio.DSP.OTHER;
using System.Threading;

namespace HSAudio.Core.Audio
{
    public partial class Audio : ISettingManager, IDisposable
    {
        internal readonly int ID = IDGenerator.Generate();
        //Matrix 구현하기
        protected internal AudioState LastStatus = AudioState.Unknown;
        protected internal FD_Channel channel;
        protected internal FD_ChannelGroup group;
        protected internal FD_DSPConnection conn;
        private HSAudio audio;
        float _Pan;
        //MixMatrix _Matrix;
        AudioRepeat _Repeat;
        Thread LoopThread;

        CHANNEL_CALLBACK _CHANNEL_CALLBACK;
        internal Audio(Sound.Sound Sound, FD_ChannelGroup parent)
        {
            audio = Sound.Audio;
            Result = audio.system.createChannelGroup(string.Format("Audio Session ({0})", ID), out group);
            Result = parent.addGroup(group, false, out conn);
            LogCheck(Result, "Init()");
            if (Result == RESULT.OK)
            {
                _CHANNEL_CALLBACK = new CHANNEL_CALLBACK(CHANNEL_CALLBACK_);
                setSound(Sound);
                DSP = new DSPCollection(this);
                _Repeat = new AudioRepeat(this);
            }
            else throw new HSAudioException(Result);
        }
        internal Audio(Sound.Sound Sound)
        {
            audio = Sound.Audio;
            Result = audio.system.createChannelGroup(string.Format("Audio Session ({0})", ID), out group);
            LogCheck(Result, "Init()");
            if (Result == RESULT.OK)
            {
                _CHANNEL_CALLBACK = new CHANNEL_CALLBACK(CHANNEL_CALLBACK_);
                setSound(Sound);
                DSP = new DSPCollection(this);
                _Repeat = new AudioRepeat(this);
            }
            else throw new HSAudioException(Result);
        }

        #region Method
        public void Play()
        {
            bool isplaying;
            RESULT Result = channel.isPlaying(out isplaying);
            if (Result == RESULT.OK && isplaying) Result = channel.setPaused(false);
            else Result = Init(true);

            Status = AudioState.Playing;
            LogCheck(Result, "Play()");
        }
        public void Pause()
        {
            RESULT Result = channel.setPaused(true);

            Status = AudioState.Pausing;
            LogCheck(Result, "Pause()");
        }
        public void Stop()
        {
            RESULT Result = channel.stop();
            Result = Init(false);

            Status = AudioState.Stopped;
            LogCheck(Result, "Stop()"); 
        }

        public bool setPosition(uint position, TimeUnit unit) { return channel.setPosition(position, (TIMEUNIT)unit) == RESULT.OK; }
        public uint getPosition(TimeUnit unit) { uint output; Result = channel.getPosition(out output, (TIMEUNIT)unit); return output; }

        internal bool setSound(Sound.Sound Sound, bool ThrowException = true)
        {
            if (!IsDisposed)
            {
                if (SoundChanging != null)try { SoundChanging.Invoke(this, Sound); } catch { } 
                try
                {
                    //if (Sound == null) { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_NULL"]); }
                    //else
                    if (Sound != null)
                    {
                        FD_Channel channel;
                        RESULT Result = audio.system.playSound(Sound.sound, group, true, out channel);
                        if (Result == RESULT.OK)
                        {
                            if (this.channel != null) this.channel.stop();
                            if (this.Sound != null)
                            {
                                this.Sound.Unloading -= OnUnloading;
                                this.Sound.Loaded -= OnLoaded;
                            }
                            this.Sound = Sound;
                            Sound.Unloading += OnUnloading;
                            Sound.Loaded += OnLoaded;
                            this.channel = channel;
                            channel.setCallback(_CHANNEL_CALLBACK);
                            return true;
                        }
                        else
                        {
                            if (ThrowException) throw new HSAudioException(Result);
                            else LogCheck(Result, "setSound()");
                        }
                    }
                    else
                    {
                        this.Sound = null;
                        //if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_NULL"].Replace("{0}", "Sound"));
                        //else LogCheck(Result, "setSound()");
                    }
                }
                finally { if (SoundChanging != null)try { SoundChanged.Invoke(this, Sound); } catch { } }
            }
            return false;
        }

        #endregion

        #region Event
        public event FloatValueChangedEventHandler VolumeChanged;
        public event FloatValueChangedEventHandler PitchChanged;
        public event FloatValueChangedEventHandler PanChanged;
        public event AudioStatusChangedEventHandler StatusChanged;
        public event UintChangedEventHandler PositionChanged;
        public event BoolChangedEventHandler ReverseChanged;

        public event SoundChangeEventHandler SoundChanging;
        public event SoundChangeEventHandler SoundChanged;
        #endregion

        #region Properties
        protected RESULT Result { get; set; }

        [NotNull]
        public Sound.Sound Sound { get; private set; }
        [NotNull]
        public DSPCollection DSP { get; private set; }
        //[NotNull]
        //public MixMatrix Matrix { get { _Matrix.Update(true); return _Matrix; } }
        [NotNull]
        public AudioRepeat Repeat { get { _Repeat.Update(true); return _Repeat; } }
        public AudioState Status
        {
            get
            {
                if (Sound == null || channel == null || !channel.isValid()) return AudioState.Unknown;
                else
                {
                    bool isplaying;
                    RESULT Result = channel.isPlaying(out isplaying);
                    if (Result == RESULT.OK && isplaying) return LastStatus;
                    else return AudioState.Stopped;
                }
            }
            protected set { LastStatus = value; if (StatusChanged != null) try { StatusChanged.Invoke(this, value); } catch { } }
        }

        public float Volume
        {
            get { float vol; group.getVolume(out vol); return vol; }
            set
            {
                LogCheck(group.setVolume(value), "Volume");
                try { if (VolumeChanged != null) VolumeChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }

        public float Pitch
        {
            get { float pit; group.getPitch(out pit); return pit; }
            set
            {
                LogCheck(group.setPitch(value), "Pitch");
                try { if (PitchChanged != null) PitchChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }

        public bool Reverse
        {
            get {return Frequency < 0; }
            set
            {
                LogCheck(channel.setFrequency(value ? -Math.Abs(Frequency) : Math.Abs(Frequency)), "Reverse");
                try { if (ReverseChanged != null) ReverseChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }

        public float Pan
        {
            get { return _Pan; }
            set
            {
                RESULT r = channel.setPan(value);
                if (r == RESULT.OK) _Pan = value;
                LogCheck(r, "Pan");
                try { if (PanChanged != null) PanChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }

        public bool Mute { get { bool mute; group.getMute(out mute); return mute; }set {Result = group.setMute(value); } }
        //public bool RampVolume { get { bool mute; channel.loop return mute; } set { Result = group.setMute(value); } }

        public float Frequency { get { float freq; channel.getFrequency(out freq); return freq; } }

        public uint CurrentPosition
        {
            get { return getPosition(TimeUnit.MilliSecond); }
            set
            {
                RESULT result = channel.setPosition(value, TIMEUNIT.MS);
                LogCheck(result, "CurrentPosition");
                try { if (PositionChanged != null) PositionChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }

        LoopKind _Loop;
        public LoopKind Loop
        {
            get
            {
                return LoopThread != null ? LoopKind.Normal : LoopKind.Off; // && ((int)(LoopThread.ThreadState | ~ThreadState.Running) == -1)
                //return _Loop;
                /*
                MODE mode; channel.getMode(out mode);
                if ((int)(mode | ~MODE.LOOP_BIDI) == -1) return LoopKind.BIDI;
                else if ((int)(mode | ~MODE.LOOP_NORMAL) == -1) return LoopKind.Normal;
                else return LoopKind.Off;
                */
            }
            set
            {
                _Loop = value;
                if (value == LoopKind.Off) try { LoopThread.Abort(); } catch { } finally { LoopThread = null; }
                else
                {
                    if (Loop == LoopKind.Normal) return;
                    else
                    {
                        LoopCountCurrent = 0;
                        LoopThread = new Thread(Loop_Loop);
                        LoopThread.Start();
                    }
                }
                /*
                MODE mode; channel.getMode(out mode);
                //mode &= ~(MODE.LOOP_BIDI | MODE.LOOP_NORMAL | MODE.LOOP_OFF);
                if (value == LoopKind.BIDI) Result = channel.setMode(MODE.LOOP_BIDI);//mode |= MODE.LOOP_BIDI;
                else if (value == LoopKind.Normal) Result = channel.setMode(MODE.LOOP_NORMAL);//mode |= MODE.LOOP_NORMAL;
                else Result = channel.setMode(MODE.LOOP_OFF);
                */
            }
        }
        public uint LoopCount { get; set; }//{ get { int count; channel.getLoopCount(out count); return count; } set {Result = channel.setLoopCount(value); } }
        public uint LoopCountCurrent { get; private set; }
        #endregion

        #region Setting IO
        public virtual Settings SaveSetting()
        {
            if (IsDisposed) return null;

            Settings s = new Settings();
            if (Sound is SoundFile2D) s.SubSetting.SetValue("2D_File", Sound.SaveSetting());
            else if (Sound is SoundNetwork2D) s.SubSetting.SetValue("2D_Network", Sound.SaveSetting());
            else if (Sound is SoundBuffer2D) s.SubSetting.SetValue("2D_Buffer", Sound.SaveSetting());

            return s;
        }
        public virtual bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.SubSetting.Exist("2D_File")) return setSound(SoundFile2D.LoadSetting(audio, Setting.SubSetting["2D_File"], ThrowException));
            else if (Setting.SubSetting.Exist("2D_Network")) return setSound(SoundNetwork2D.LoadSetting(audio, Setting.SubSetting["2D_Network"], ThrowException));
            else if (Setting.SubSetting.Exist("2D_Buffer")) return setSound(SoundBuffer2D.LoadSetting(audio, Setting.SubSetting["2D_Buffer"], ThrowException));
            else return false;
        }
        #endregion

        #region Method (Private)
        private void LogCheck(RESULT Result, string Func) { HSAudioLog.LogCheck(Result, "Audio::" + Func);}
        //internal void LogCheck(RESULT Result, string Func, bool Is3D){ HSAudioLog.LogCheck(Result, (Is3D ? "Sound3D::" : "Sound2D::") + Func);}
        private RESULT Init(bool Play)
        {
            RESULT Result = audio.system.playSound(Sound.sound, group, !Play, out channel);
            channel.setCallback(_CHANNEL_CALLBACK);
            //group.setCallback(_CHANNEL_CALLBACK);
            return Result;
        }
        private void Loop_Loop()
        {
            while(_Loop == LoopKind.Normal && (LoopCountCurrent == 0 || LoopCount >= LoopCountCurrent))
            {
                if (channel != null && channel.isValid())
                {
                    bool play = true;
                    while (play) { channel.isPlaying(out play); Thread.Sleep(5); }
                    LoopCountCurrent++;
                    Play();
                }
            }
        }
        #endregion

        #region Method (Event)
        //protected void OnStatusChanged(SoundStatus Status) { if (StatusChanged != null) try { StatusChanged(this, Status); } catch { } }
        private RESULT CHANNEL_CALLBACK_(IntPtr channelraw, CHANNELCONTROL_TYPE controltype, CHANNELCONTROL_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            if(type == CHANNELCONTROL_CALLBACK_TYPE.END && Loop == LoopKind.Off)
            {
                Init(false);
                if (Loop == LoopKind.Normal) Play();
                else if (StatusChanged != null) try { StatusChanged.Invoke(this, AudioState.Stopped); } catch { }
            }
            return RESULT.OK;
        }
        
        uint _tmp_pos; AudioState _tmp_stat;
        private void OnUnloading(object sender)
        {
            _tmp_pos = CurrentPosition;
            _tmp_stat = Status;
        }
        bool IsUnload;
        private void OnLoaded(object sender)
        {
            if (IsUnload)
            {
                RESULT Result = audio.system.playSound(Sound.sound, group, true, out channel);
                //this.channel = channel;
                channel.setCallback(_CHANNEL_CALLBACK);
                Pan = _Pan;
                CurrentPosition = _tmp_pos;

                if (_tmp_stat == AudioState.Playing) Play();
                else if (_tmp_stat == AudioState.Pausing) Pause();
            }
        }
        #endregion

        public bool IsDisposed { get; private set; }
        public virtual void Dispose()
        {
            LastStatus = AudioState.Unknown;
            try { LoopThread.Abort(); LoopThread = null; } catch { }
            IsDisposed = true;
            if (Sound != null) { Sound.Dispose(); Sound = null; }
            try { if (channel != null) channel.stop(); channel = null; } catch { }
            try { if (group != null) group.release(); group = null; } catch { }
            DSP.Dispose();
        }
        ~Audio() { if (!IsDisposed) Dispose(); }
    }
}
