﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace HS.Setting
{
    [Flags]
    //public enum TYPE { Array = 1, Object = 2, String = 4, Bool = 8, Byte = 16, SByte = 32, Short = 64, UShort = 128, Int = 256, UInt = 512, Long = 1024, ULong = 2048, Decimal = 4096, Single = 8072, Float = Single, Double = 16384 }
    public enum TYPE { Array = 1, Object = 2, String = 4, Bool = 10, Byte = 20, SByte = 30, Short = 40, UShort = 50, Int = 60, UInt = 70, Long = 80, ULong = 90, Decimal = 100, Single = 110, Float = Single, Double = 120 }
    public class Settings : Setting<SettingsData>
    {
        public new Settings SetValue(string Name, SettingsData Value)
        {
            base.SetValue(Name, Value);
            return this;
        }

        public void Dispose() {}
    }
    public class SettingsData
    {
        public static implicit operator string(SettingsData data) { return data.Value; }
        //public static explicit operator string(Data data) { return data.Value; }
        public static implicit operator SettingsData(string data) { return new SettingsData(data); }
        public static implicit operator SettingsData(int data) { return new SettingsData(data); }
        public static implicit operator SettingsData(bool data) { return new SettingsData(data); }
        public static implicit operator SettingsData(float data) { return new SettingsData(data); }
        public static implicit operator SettingsData(byte[] data) { return new SettingsData(data); }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="IsHead">[Optional] like tag</param>
        public SettingsData(string Value, bool IsHead = true) { this.Value = Value; this.IsHead = IsHead; }
        public SettingsData(int Value, bool IsHead = true) { this.Value = Value.ToString(); this.IsHead = IsHead; }
        public SettingsData(bool Value, bool IsHead = true) { this.Value = Value.ToString(); this.IsHead = IsHead; }
        public SettingsData(float Value, bool IsHead = true) { this.Value = Value.ToString(); this.IsHead = IsHead; }
        public SettingsData(byte[] Value, bool IsHead = true)
        {
            byte[] data = Value;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
                sb.AppendFormat("{0:X2} ", data[i]);
            this.Value = sb.ToString();
        }
        public string Value { get; set; }
        public bool IsHead { get; set; }
        public override string ToString()
        {
            return Value;
        }
    }
    public static class DataCaster
    {
        public static SettingsData GetData(string Value, bool IsHead = true) { return new SettingsData(Value, IsHead); }
        public static SettingsData GetData(int Value, bool IsHead = true) { return new SettingsData(Value, IsHead); }
        public static SettingsData GetData(bool Value, bool IsHead = true) { return new SettingsData(Value, IsHead); }
        public static SettingsData GetData(float Value, bool IsHead = true) { return new SettingsData(Value, IsHead); }
        public static SettingsData GetData(byte[] Value, bool IsHead = true) { return new SettingsData(Value, IsHead); }
    }
}
