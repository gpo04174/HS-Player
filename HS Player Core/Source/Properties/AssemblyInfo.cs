﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("HS Player Core")]
[assembly: AssemblyDescription("HS Player Core Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("이지정보 (HS)")]
[assembly: AssemblyProduct("HS Player Core")]
[assembly: AssemblyCopyright("Copyright © 2018 HSKernel(HongSic)")]
[assembly: AssemblyTrademark("HSPlayerCore")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("10f9a5f5-74e1-4a6a-9d35-e7fb53f72a96")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("3.0.0.0")]
[assembly: AssemblyFileVersion("3.0.0.0")]
