﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.DSP.Type
{
    public class FFT : IDSP
    {
        public enum FFTWindow
        {
            /// <summary>
            /// w[n] = 1.0 
            /// </summary>
            Rect,
            /// <summary>
            /// w[n] = TRI(2n/N) 
            /// </summary>
            Triangle,
            /// <summary>
            /// w[n] = 0.54 - (0.46 * COS(n/N) ) 
            /// </summary>
            Hamming,
            /// <summary>
            /// w[n] = 0.5 * (1.0 - COS(n/N) ) 
            /// </summary>
            Hanning,
            /// <summary>
            /// w[n] = 0.42 - (0.5 * COS(n/N) ) + (0.08 * COS(2.0 * n/N) ) 
            /// </summary>
            Blackman,
            /// <summary>
            /// w[n] = 0.35875 - (0.48829 * COS(1.0 * n/N)) + (0.14128 * COS(2.0 * n/N)) - (0.01168 * COS(3.0 * n/N)) 
            /// </summary>
            BlackmanHarris
        }
        public enum FFTSize { Size_128 = 128, Size_256 = 256, Size_512 = 512, Size_1024 = 1024, Size_2048 = 2048, Size_4096 = 4096, Size_8192 = 8192, Size_16384 = 16384 }

        private Parameter[] param = new Parameter[1];
        public FFT(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.FFT, "FFT", null);
            _Data = new FFT.FFTData(this);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// FFT window size. Default = 2048
        /// </summary>
        public FFTSize Size
        {
            get { return (FFTSize)DSP.GetParameter((int)DSP_FFT.WINDOWSIZE, 0); }
            set { DSP.SetParameter((int)DSP_FFT.WINDOWTYPE, (int)value); }
        }
        /// <summary>
        /// FFT window type. Default = Hamming
        /// </summary>
        public FFTWindow Window
        {
            get { return (FFTWindow)DSP.GetParameter((int)DSP_FFT.WINDOWTYPE, 0); }
            set { DSP.SetParameter((int)DSP_FFT.WINDOWTYPE, (int)value); }
        }

        FFTData _Data;
        public FFTData Data { get { _Data.Update(); return _Data; } }


        public IntPtr GetRaw(out uint Length, out float DominantFrequency)
        {
            DominantFrequency = DSP.GetParameter((int)DSP_FFT.DOMINANT_FREQ, 0f);
            return DSP.GetParameter((int)DSP_FFT.SPECTRUMDATA, out Length);
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Size")) try { Size = (FFTSize)Enum.Parse(typeof(FFTSize), Setting["Size"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Window")) try { Window = (FFTWindow)Enum.Parse(typeof(FFTWindow), Setting["Window"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Size", Size.ToString());
            s.SetValue("Window", Window.ToString());
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }

        public class FFTData
        {
            FFT parent;
            DSP_PARAMETER_FFT fft;
            internal FFTData(FFT parent)
            {
                this.parent = parent;
            }
            internal void Update()
            {
                uint len;
                IntPtr ptr = parent.DSP.GetParameter((int)DSP_FFT.SPECTRUMDATA, out len);
#if !NETCORE && !NETSTANDARD && !NETFX
                fft = (DSP_PARAMETER_FFT)Marshal.PtrToStructure(ptr, typeof(DSP_PARAMETER_FFT));
#else
                fft = Marshal.PtrToStructure<DSP_PARAMETER_FFT>(ptr);
#endif
                Spectrum = fft.spectrum;
            }

            public int Length { get { return fft.length; } }
            public int Channel { get { return fft.numchannels; } }
            public IntPtr[] SpectrumPtr { get { return fft.spectrum_internal; } }

            public float[][] Spectrum { get; private set; }
        }
    }
}
