﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class EnvelopeFollower : IDSP
    {
        private Parameter[] param = new Parameter[4];
        public EnvelopeFollower(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.ENVELOPEFOLLOWER, "EnveloperFollower", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Attack time (milliseconds), in the range from 0.1 through 1000. The default value is 20
        /// </summary>
        public float Attack
        {
            get { return DSP.GetParameter((int)DSP_ENVELOPEFOLLOWER.ATTACK, 0f); }
            set { DSP.SetParameter((int)DSP_ENVELOPEFOLLOWER.ATTACK, value); }
        }
        /// <summary>
        ///  Release time (milliseconds), in the range from 10 through 5000. The default value is 100
        /// </summary>
        public float Release
        {
            get { return DSP.GetParameter((int)DSP_ENVELOPEFOLLOWER.RELEASE, 0f); }
            set { DSP.SetParameter((int)DSP_ENVELOPEFOLLOWER.RELEASE, value); }
        }
        /// <summary>
        ///  Maximum amplification allowed in dB.  0.0 to 12.0.  Default = 0.0. 0.0 = no amplifaction, higher values allow more boost
        /// </summary>
        public float Envelope
        {
            get { return DSP.GetParameter((int)DSP_ENVELOPEFOLLOWER.ENVELOPE, 0f); }
            set { DSP.SetParameter((int)DSP_ENVELOPEFOLLOWER.ENVELOPE, value); }
        }
        /// <summary>
        /// Whether to analyse the sidechain signal instead of the input signal. The default value is fals
        /// </summary>
        public bool UseSideChain
        {
            get { return DSP.GetParameter((int)DSP_ENVELOPEFOLLOWER.USESIDECHAIN, true); }
            set { DSP.SetParameter((int)DSP_ENVELOPEFOLLOWER.USESIDECHAIN, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Attack")) try { Attack = float.Parse(Setting["Attack"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Release")) try { Release = float.Parse(Setting["Release"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Envelope")) try { Envelope = float.Parse(Setting["Envelope"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("UseSideChain")) try { UseSideChain = bool.Parse(Setting["UseSideChain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Attack", Attack);
            s.SetValue("Release", Release);
            s.SetValue("Envelope", Envelope);
            s.SetValue("UseSideChain", UseSideChain.ToString());
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
