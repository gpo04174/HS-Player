﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace HSPlayer
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //GetOutputs(OUTPUTTYPE.ASIO);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }


        public static Output[] GetOutputs(OUTPUTTYPE output)
        {
            FD_System system;
            Factory.System_Create(out system);
            Output[] o = GetOutputs(system, output);
            system.release();
            return o;
        }
        public static Output[] GetOutputs(FD_System system, OUTPUTTYPE output)
        {
            RESULT result = system.setOutput(output);
            int num;
            system.getNumDrivers(out num);
            List<Output> device = new List<Output>();
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);

                Guid guid; SPEAKERMODE spk; int ch;
                int sample;
                system.getDriverInfo(i, sb, 256, out guid, out sample, out spk, out ch);

                device.Add(new Output(i, guid, sb.ToString(), sample, ch, spk));
            }
            show(device, output);
            return device.ToArray();
        }
        static void show(List<Output> o, OUTPUTTYPE output)
        {
            Console.WriteLine("======{0} Devices======", output);
            for (int i = 0; i < o.Count; i++)
            {
                Console.WriteLine(i + ". {0}\n->[{1}] [{2}] [{3}] {4}", o[i].name, o[i].sample, o[i].channel, o[i].spk, o[i].guid);
            }
            Console.WriteLine();
        }
    }
    class Output
    {
        public Output(int index, Guid guid, string name, int sample, int channel, SPEAKERMODE spk)
        {
            this.index = index;
            this.guid = guid;
            this.name = name;
            this.sample = sample;
            this.channel = channel;
            this.spk = spk;
        }

        public int index;
        public Guid guid;
        public string name;
        public int sample;
        public int channel;
        public SPEAKERMODE spk;

        public override string ToString()
        {
            return name;
        }
    }


    [StructLayout(LayoutKind.Sequential)]
    public class StringPtr : IDisposable
    {
        IntPtr nativeUtf8Ptr;

        public static implicit operator IntPtr(StringPtr fstring) { return fstring.nativeUtf8Ptr; }
        public static implicit operator string(StringPtr fstring)
        {
            if (fstring.nativeUtf8Ptr == IntPtr.Zero) return null;
            else return Marshal.PtrToStringUni(fstring.nativeUtf8Ptr);
        }
        public static implicit operator StringPtr(string str)
        {
            if (str == null) return null;
            else return new StringPtr() { nativeUtf8Ptr = Marshal.StringToHGlobalUni(str) };
        }
        public unsafe static implicit operator StringPtr(char[] str)
        {
            if (str == null || str.Length == 0) return null;
            else
            {
                int num = Encoding.UTF8.GetByteCount(str);
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * num);
                byte* b = (byte*)ptr;
                fixed (char* c = str)
                    Encoding.UTF8.GetBytes(c, num, b, sizeof(byte) * num);
                return new StringPtr() { nativeUtf8Ptr = ptr };
            }
        }

        public static IntPtr Copy(char[] str)
        {
            int len = str.Length * sizeof(char);
            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.Copy(str, 0, ptr, len);
            return ptr;
        }
        public void Dispose() { if (nativeUtf8Ptr != IntPtr.Zero) Marshal.FreeHGlobal(nativeUtf8Ptr); }
        ~StringPtr() { if (nativeUtf8Ptr != IntPtr.Zero) Marshal.FreeHGlobal(nativeUtf8Ptr); }
    }
}
