﻿using HSAudio.Lib.FMOD;
using System;
using System.Threading;

namespace FMODTest.Test
{
    public class StreamTest
    {
        public static void Test(string MusicPath)
        {
            RESULT result = RESULT.OK;
            FD_System system;
            FD_Sound sound;
            FD_Channel channel;
            FD_ChannelGroup group;

            Thread th = new Thread(Loop);

            Factory.System_Create(out system);
            //th.Start(system);

            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            result = system.createChannelGroup("Temp", out group);

            result = system.createStream(MusicPath, MODE.ACCURATETIME, out sound);
            result = system.playSound(sound, null, false, out channel);
            result = group.setPaused(true);

            channel.setCallback(new CHANNEL_CALLBACK(CHANNEL_CALLBACK)); uint len;

            result = sound.getLength(out len, TIMEUNIT.MS);
            result = channel.setPosition(len - 10000, TIMEUNIT.MS);
            while (true)
            {
                Thread.Sleep(1);
                bool isplaying;
                result = channel.isPlaying(out isplaying);
                if (!isplaying)
                {
                    int num;
                    //result = sound.release();
                    result = group.getNumChannels(out num);
                    result = system.playSound(sound, group, false, out channel);
                }
            }
        }
        
        protected static RESULT CHANNEL_CALLBACK(IntPtr channelraw, CHANNELCONTROL_TYPE controltype, CHANNELCONTROL_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            RESULT Result;
            if (type == CHANNELCONTROL_CALLBACK_TYPE.END)
            {
            }
            return RESULT.OK;
        }

        static void Loop(object o)
        {
            FD_System system = o as FD_System;
            RESULT Result = RESULT.OK;
            while (system != null)
            {
                Result = system.update();
                if (Result != RESULT.OK) Console.WriteLine("Fail({0}) - System::Update()", Result);
                Thread.Sleep(1);
            }
        }
    }
}
