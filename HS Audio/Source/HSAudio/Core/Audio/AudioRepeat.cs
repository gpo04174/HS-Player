﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.Core.Audio
{
    public class AudioRepeat : ISettingManager
    {
        Audio audio;
        uint _Start, _End;
        TimeUnit _Unit = TimeUnit.MilliSecond;
        internal AudioRepeat(Audio audio) { this.audio = audio; }
        internal void Update(bool IsGet)
        {
            if (IsGet) audio.channel.getLoopPoints(out _Start, (TIMEUNIT)Unit, out _End, (TIMEUNIT)Unit);
            else
            {
                _Start = Math.Min(Start, End);
                _End = Math.Max(Start, End);
                if (End != 0)
                {
                    audio.Loop = LoopKind.Normal;
                    audio.channel.setLoopPoints(Start, (TIMEUNIT)Unit, End, (TIMEUNIT)Unit);
                }
            }
        }

        internal void Reload(FD_Channel channel)
        {
            channel.setLoopPoints(_Start, (TIMEUNIT)_Unit, _End, (TIMEUNIT)_Unit);
        }

        public TimeUnit Unit { get { return _Unit; }set { _Unit = value; Update(true); } }
        public uint Start { get { if (_End == 0) return _Start; else { Update(true); return _Start; } }set { _Start = value; Update(false); } }
        public uint End { get { Update(true); return _End; } set { _End = value; Update(false); } }
        

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Unit", new SettingsData(Unit.ToString()));
            s.SetValue("Start", Start);
            s.SetValue("End", End);
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Unit")) try { Unit = (TimeUnit)Enum.Parse(typeof(TimeUnit), Setting["Unit"]); } catch(Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Start")) try { Start = uint.Parse(Setting["Start"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("End")) try { End = uint.Parse(Setting["End"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }
    }
}
