﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.Core.Sound._3D
{
    public class Cone3D
    {
        FD_Sound sound;
        float _InsideAngle;
        float _OutsideAngle;
        float _OutsideVolume;
        internal Cone3D(FD_Sound sound) { this.sound = sound; }

        public float InsideAngle { get { Update(); return _InsideAngle; } set { sound.set3DConeSettings(value, OutsideAngle, OutsideVolume); } }
        public float OutsideAngle { get { Update(); return _OutsideAngle; } set { sound.set3DConeSettings(InsideAngle, value, OutsideVolume); } }
        public float OutsideVolume { get { Update(); return _OutsideVolume; } set { sound.set3DConeSettings(InsideAngle, OutsideAngle, value); } }

        void Update() { sound.get3DConeSettings(out _InsideAngle, out _OutsideAngle, out _OutsideVolume); }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            try { if (Setting.Exist("InsideAngle")) InsideAngle = Convert.ToSingle(Setting["InsideAngle"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("OutsideAngle")) OutsideAngle = Convert.ToSingle(Setting["OutsideAngle"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("OutsideVolume")) OutsideVolume = Convert.ToSingle(Setting["OutsideVolume"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("InsideAngle", InsideAngle);
            s.SetValue("OutsideAngle", OutsideAngle);
            s.SetValue("OutsideVolume", OutsideVolume);
            return s;
        }
    }
}
