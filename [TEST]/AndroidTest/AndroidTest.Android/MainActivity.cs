﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content;
using Android;
using Android.Support.V4.Content;
using Android.Support.V4.App;
using Android.Content.Res;
using System.Threading.Tasks;
using System.Threading;
using CrossCallback;

namespace AndroidTest.Droid
{
    [Activity(Label = "AndroidTest", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        PermissionManagerAndroid per;
        FunctionManagerAndroid func;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            per = new PermissionManagerAndroid(this, ProcessingPermission);
            func = FunctionManagerAndroid.Init(this);
            AudioManagerAndroid.Init(this);

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }

        #region Permission
        async Task<bool> ProcessingPermission(params string[] Permission)
        {
            // 권한이 없을경우
            
            //권한요청
            ActivityCompat.RequestPermissions(this, Permission, PermissionManagerAndroid.PERMISSION_OK);
            return await Task.Run(() => { while (!IsConfirm) { Thread.Sleep(10); } IsConfirm = false; return IsGranted; });
        }

        bool IsConfirm;
        bool IsGranted;
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            IsGranted = true;
            //requestCode == PermissionManagerAndroid.PERMISSION_OK;
            for (int i = 0; i < grantResults.Length; i++) IsGranted &= grantResults[i] == Permission.Granted; 
            IsConfirm = true;
        }
        #endregion
    }
}