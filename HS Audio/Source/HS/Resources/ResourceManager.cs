﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS.Resource
{
    public enum Type {String, Bitmap, Sound, Binary, Other }
    public delegate void ResourceChangedEventHandler(object sender);
    public class HSResourceManager
    {
        public static HSResourceManager Manager { get; internal protected set; }

        public event ResourceChangedEventHandler StringChanged;
        public event ResourceChangedEventHandler BitmapChanged;
        public event ResourceChangedEventHandler SoundChanged;
        public event ResourceChangedEventHandler Changed;

        HSResourceManager Parent;
        internal protected HSResourceManager()
        {
        }

        internal protected HSResourceManager(HSResourceManager Parent)
        {
            this.Parent = Parent;
        }

        protected void OnStringChanged()
        {
            if (Parent != null) Parent.OnStringChanged();
            if (StringChanged != null) try { StringChanged(this); } catch { }
        }
        protected void OnBitmapChanged()
        {
            if (Parent != null) Parent.OnBitmapChanged();
            if (BitmapChanged != null) try { BitmapChanged(this); } catch { }
        }
        protected void OnSoundChanged()
        {
            if (Parent != null) Parent.OnSoundChanged();
            if (SoundChanged != null) try { SoundChanged(this); } catch { }
        }
        /// <summary>
        /// Other Type
        /// </summary>
        protected void OnChanged()
        {
            if (Parent != null) Parent.OnChanged();
            if (Changed != null) try { Changed(this); } catch { }
        }

        internal protected void Dispose()
        {
            StringChanged = null;
            BitmapChanged = null;
            SoundChanged = null;
            Changed = null;
        }
    }
}
