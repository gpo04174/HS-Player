﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.Utility.Native
{
    public static class PtrForge
    {
        public static void Free(this IntPtr Data) { if (Data != IntPtr.Zero) Marshal.FreeHGlobal(Data); }

        public static IntPtr ToPointer(this byte[] Data, int Offset = 0) { return Data == null ? IntPtr.Zero : ToPointer(Data, Offset, Data.Length); }
        public static IntPtr ToPointer(this byte[] Data, int Offset, int Length)
        {
            if (Data == null || Data.Length == 0) return IntPtr.Zero;
            else
            {
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * Data.Length);
                Marshal.Copy(Data, Offset, ptr, Length);
                return ptr;
            }
        }

        public static byte[] ToByte(this IntPtr Data, int Length) { return Data == IntPtr.Zero ? null : ToByte(Data, 0, Length); }
        public static byte[] ToByte(this IntPtr Data, int Offset, int Length)
        {
            if (Data == IntPtr.Zero) return null;
            else
            {
                byte[] data = new byte[Length];
                Marshal.Copy(Data, data, Offset, Length);
                return data;
            }
        }

#if !NETSTANDARD
        public static IntPtr ToPointer(this string Data) { return Data == null ? IntPtr.Zero : Marshal.StringToHGlobalAuto(Data); }
#endif
        public static IntPtr ToPointerUni(this string Data) { return Data == null ? IntPtr.Zero : Marshal.StringToHGlobalUni(Data); }
        public static IntPtr ToPointerAnsi(this string Data) { return Data == null ? IntPtr.Zero : Marshal.StringToHGlobalAnsi(Data); }
        
        public unsafe static IntPtr ToPointerUni(this char[] Data)
        {
            if (Data == null || Data.Length == 0) return IntPtr.Zero;
            else
            {
#if !NETSTANDARD1x
                int num = Encoding.UTF8.GetByteCount(Data);
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * num);
                byte* b = (byte*)ptr;
                fixed (char* c = Data)
                    Encoding.UTF8.GetBytes(c, num, b, sizeof(byte) * num);
#else
                byte[] tmp = Encoding.UTF8.GetBytes(Data);
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * tmp.Length);
                Marshal.Copy(tmp, 0, ptr, tmp.Length);
#endif
                return ptr;
            }
        }
#if !NETSTANDARD1x
        public unsafe static IntPtr ToPointerAnsi(this char[] Data)
        {
            if (Data == null || Data.Length == 0) return IntPtr.Zero;
            else
            {
                int num = Encoding.Default.GetByteCount(Data);
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * num);
                byte* b = (byte*)ptr;
                fixed (char* c = Data)
                    Encoding.Default.GetBytes(c, num, b, sizeof(byte) * num);
                return ptr;
            }
        }
#endif

#if !NETSTANDARD1x
        public static string ToString(this IntPtr Data) { return Data == IntPtr.Zero ? null : Marshal.PtrToStringAuto(Data); }
        public unsafe static string ToString1(this IntPtr Data) { return Data == IntPtr.Zero ? null : new string((char*)Data.ToPointer()); }
        public unsafe static string ToString(this IntPtr Data, Encoding Encoding)
        {
            if (Data == IntPtr.Zero) return null;
            List<byte> chars = new List<byte>();
            byte* data = (byte*)Data.ToPointer();
            for(int i = 0; i < int.MaxValue; i++)
            {
                if(data[i] == 0)break;
                else chars.Add(data[i]);
            }
            return Encoding.GetString(chars.ToArray(), 0, chars.Count); 
        }
#endif
        public static string ToStringUni(this IntPtr Data, int Length) { return Data == IntPtr.Zero ? null : Marshal.PtrToStringUni(Data, Length); }
        public static string ToStringAnsi(this IntPtr Data, int Length) { return Data == IntPtr.Zero ? null : Marshal.PtrToStringAnsi(Data, Length); }
        public static string ToStringUni(this IntPtr Data) { return Data == IntPtr.Zero ? null : Marshal.PtrToStringUni(Data); }
        public static string ToStringAnsi(this IntPtr Data) { return Data == IntPtr.Zero ? null : Marshal.PtrToStringAnsi(Data); }
    }
}
