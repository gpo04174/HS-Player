﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HSPlayerCoreTest
{
    public partial class PureLyricsViewer : Form
    {
        string Title;
        public PureLyricsViewer(string Text, string Title, bool LowMem, TimeSpan ts)
        {
            InitializeComponent();
            textBox1.Text = Text;
            this.Title = Title;
            this.Text = string.Format("Pure Lyrics Viewer{0} [Process time: {1:0.00} ms] - {2}", LowMem ? " (Low Memory)" : null, ts.Ticks / 100f, Title);
        }

        private void PureLyricsViewer_Load(object sender, EventArgs e)
        {

        }

        private void exitXToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void wordWrapWToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.WordWrap = wordWrapWToolStripMenuItem.Checked;
        }

        private void saveSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = Title;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                File.WriteAllText(saveFileDialog1.FileName, textBox1.Text);
        }
    }
}
