﻿using HSAudio.Core.Audio;
using HSAudio.Output.Devices;
using HSAudio.Utility;
using HSAudioTest.Test;
using HSAudioTest.Test.GUI;
using System;
using System.Windows.Forms;
using Test;

namespace HSAudioTest
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //ASIOOutput[] output = ASIOOutput.GetOutputs();

            //"/mnt/hgfs/OneDrive/음악/보컬로이드/IA/Orangestar/未完成タイムリミッター【オリジナル】.mp3"
            //PlayTest.Test("/mnt/hgfs/OneDrive/음악/보컬로이드/IA/Orangestar/未完成タイムリミッター【オリジナル】.mp3");
            //LoopTest.Test("D:\\D데이터\\프로그램 개발\\Microsoft Visual Studio\\C#\\프로그램\\프로그램1\\HS Audio\\[TEST]\\[SOUND]\\drumloop.wav");

            //"http://185.33.21.112/classical_128"
            //"http://46.252.154.133:8000/stream.ogg" //FLAC
            //"http://theprodigy.ru:8000/live.ogg" //OGG
            //"http://87.116.153.100:8054"//버퍼링 심함
            //StreamingTest.Test("http://185.33.21.112/classical_128");

            //StreamingTest.BufferTest();
            //StreamingTest.Test("D:\\未完成タイムリミッター.raw");
            //OGGTest.asd(args);

            //DeviceChangeTest.Test1();
            //DeviceChangeTest.Test();


            float[][] Matrix = new float[2][];
            Matrix[0] = new float[3];
            Matrix[1] = new float[4];
            Matrix[0][0] = 1; Matrix[1][0] = 2;
            Matrix[0][1] = 3; Matrix[1][1] = 4;
            Matrix[0][2] = 5; Matrix[1][2] = 6;
            float[,] M = ArrayConveterC<float>.Convert(Matrix);
            /*
             1,2,3,4,5,6 = true
             1,3,5,2,4,6 = false
             */
            float[] m = ArrayConveterN<float>.MatrixToArray(Matrix, true);
            float[][] m4 = ArrayConveterN<float>.ArrayToMatrix(m, 4, 2, true);
            float[][] Matrix2 = ArrayConveterN<float>.Convert(M);

            float[,] Matrix1 = new float[,]
            {
                /*
                0.0f, 0.0f,
                0.0f, 0.0f,
                0.0f, 0.0f,
                0.0f, 0.0f,
                1.0f, 1.0f,
                1.0f, 1.0f
                */
                { 1f, 2f },
                { 3f, 4f },
                { 5f, 6f }
            };
            float[] m1 = ArrayConveterC<float>.MatrixToArray(Matrix1, true);
            float[,] m2 = ArrayConveterC<float>.ArrayToMatrix(m1, 2, 6, true);
            float[] m3 = ArrayConveterC<float>.MatrixToArray(m2, true);
            m = ArrayConveterN<float>.MatrixToArray(Matrix, false);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SoundDeviceTest());
        }

        public static string GetTime(uint tick)
        {
            uint h, m, s, ms; 
            ms = tick % 1000;
            tick = tick / 1000;
            h = tick / 3600;
            tick = tick % 3600;
            m = tick / 60;
            s = tick % 60;
            return string.Format("{0:00}:{1:00}:{2:00}.{3:000}", h, m, s, ms);
        }
        public static void A_StatusChanged(Audio sender, AudioState status)
        {
            if (status == AudioState.Stopped) { sender.Play();}
        }
    }
}
