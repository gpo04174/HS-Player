﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using HSAudio.Lib.Windows.CoreAudioApi;
using HSAudio.Lib.Windows.CoreAudioApi.Interfaces;
using HSAudio.Wave.Device;
using System;

namespace HSAudio.Control.Device
{
    public class DeviceControlWASAPI : DeviceControl, IDisposable
    {
        internal AudioEndpointVolume ControlE;
        internal AudioMeterInformation ControlEInfo;

        internal AudioSessionControl ControlS;
        private AudioSessionEventsHandler ControlHandler;

        private WaveDeviceWASAPI _Device;
        internal int index;
        internal protected DeviceControlWASAPI(WaveDeviceWASAPI Device, AudioEndpointVolume Control, AudioMeterInformation Meter, int index)
        {
            ControlEInfo = Meter;
            this.ControlE = Control;
            this._Device = Device;
            this.index = index;
            //Name = Device.
        }
        internal protected DeviceControlWASAPI(WaveDeviceWASAPI Device, AudioSessionControl Control, int index)
        {
            this.ControlS = Control;
            this._Device = Device;
            this.index = index;
            Name = Control.DisplayName;
            ControlHandler = new AudioSessionEventsHandler(this);
            Control.RegisterEventClient(ControlHandler);
        }
        public float VolumeMax { get { return ControlE == null ? 1 : ControlE.VolumeRange.MaxDecibels; } }
        public float VolumeMin { get { return ControlE == null ? 0 : ControlE.VolumeRange.MinDecibels; } }
        public float Volume { get { return ControlE == null ? ControlS.SimpleAudioVolume.Volume : ControlE.MasterVolumeLevel; } set { if (ControlE == null) ControlS.SimpleAudioVolume.Volume = value; else ControlE.MasterVolumeLevel = value; } }
        public float VolumePercent { get { return (Volume - VolumeMin) / (VolumeMax - VolumeMin); } set { Volume = VolumeMin - (value * (VolumeMax + VolumeMin)); } }
        public float VolumeScalar { get { return ControlE == null ? ControlS.SimpleAudioVolume.Volume : ControlE.MasterVolumeLevelScalar; } set { if (ControlE == null) ControlS.SimpleAudioVolume.Volume = value; else ControlE.MasterVolumeLevelScalar = value; } }
        public bool Mute { get { return ControlE == null ? ControlS.SimpleAudioVolume.Mute : ControlE.Mute; } set { if (ControlE == null) ControlS.SimpleAudioVolume.Mute = value; else ControlE.Mute = value; } }

        public WaveDevice Device { get { return _Device; } }
        public string Name { get; private set; }
        public object Control { get { if (ControlE != null) return ControlE; else return ControlS; } }

        public AudioMeterInformation Meter { get { return ControlEInfo != null ? ControlEInfo : ControlS.AudioMeterInformation; } }

        public void Dispose()
        {
            if (ControlS != null)
            {
                ControlS.UnRegisterEventClient(ControlHandler);
                ControlS.Dispose();
            }
            if (ControlE != null) ControlE.Dispose();
        }

        public override bool Equals(object obj)
        {
            if (obj is DeviceControlWASAPI)
            {
                DeviceControlWASAPI w = ((DeviceControlWASAPI)obj);
                return w.index == index && w.Name == Name;
            }
            else return false;
        }
        public override int GetHashCode() { return base.GetHashCode(); }

        #region Private

        private void RaiseEvent(float volume, bool isMuted)
        {
            _Device.RaiseVolumeEvent(index, this);
        }


        private class AudioSessionEventsHandler : IAudioSessionEventsHandler
        {
            private DeviceControlWASAPI Instance;

            public AudioSessionEventsHandler(DeviceControlWASAPI Instance) { this.Instance = Instance; }
            public void OnVolumeChanged(float volume, bool isMuted) { Instance.RaiseEvent(volume, isMuted); }
        }

        #endregion
    }
}
#endif