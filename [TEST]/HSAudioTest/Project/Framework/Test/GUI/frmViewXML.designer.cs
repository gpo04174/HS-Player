﻿namespace HSPlayerCoreTest.GUI
{
    partial class frmViewXML
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadXMLFileOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveXMLFileSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wordWrapWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeXML = new System.Windows.Forms.TreeView();
            this.txtXML = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.brToNewLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileFToolStripMenuItem,
            this.viewVToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(893, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileFToolStripMenuItem
            // 
            this.fileFToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadXMLFileOToolStripMenuItem,
            this.saveXMLFileSToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitXToolStripMenuItem});
            this.fileFToolStripMenuItem.Name = "fileFToolStripMenuItem";
            this.fileFToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.fileFToolStripMenuItem.Text = "File(&F)";
            // 
            // loadXMLFileOToolStripMenuItem
            // 
            this.loadXMLFileOToolStripMenuItem.Name = "loadXMLFileOToolStripMenuItem";
            this.loadXMLFileOToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.loadXMLFileOToolStripMenuItem.Text = "Load XML File(&O)";
            this.loadXMLFileOToolStripMenuItem.Click += new System.EventHandler(this.loadXMLFileOToolStripMenuItem_Click);
            // 
            // saveXMLFileSToolStripMenuItem
            // 
            this.saveXMLFileSToolStripMenuItem.Name = "saveXMLFileSToolStripMenuItem";
            this.saveXMLFileSToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveXMLFileSToolStripMenuItem.Text = "Save XML File(&S)";
            this.saveXMLFileSToolStripMenuItem.Click += new System.EventHandler(this.saveXMLFileSToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(162, 6);
            // 
            // exitXToolStripMenuItem
            // 
            this.exitXToolStripMenuItem.Name = "exitXToolStripMenuItem";
            this.exitXToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exitXToolStripMenuItem.Text = "Exit(&X)";
            // 
            // viewVToolStripMenuItem
            // 
            this.viewVToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.brToNewLineToolStripMenuItem,
            this.toolStripSeparator2,
            this.wordWrapWToolStripMenuItem});
            this.viewVToolStripMenuItem.Name = "viewVToolStripMenuItem";
            this.viewVToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.viewVToolStripMenuItem.Text = "View(&V)";
            // 
            // wordWrapWToolStripMenuItem
            // 
            this.wordWrapWToolStripMenuItem.CheckOnClick = true;
            this.wordWrapWToolStripMenuItem.Name = "wordWrapWToolStripMenuItem";
            this.wordWrapWToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.wordWrapWToolStripMenuItem.Text = "WordWrap(&W)";
            this.wordWrapWToolStripMenuItem.CheckedChanged += new System.EventHandler(this.wordWrapWToolStripMenuItem_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 518);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(893, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtStatus
            // 
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(39, 17);
            this.txtStatus.Text = "Ready";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtXML);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.treeXML);
            this.splitContainer1.Size = new System.Drawing.Size(893, 494);
            this.splitContainer1.SplitterDistance = 433;
            this.splitContainer1.TabIndex = 2;
            // 
            // treeXML
            // 
            this.treeXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeXML.Location = new System.Drawing.Point(0, 0);
            this.treeXML.Name = "treeXML";
            this.treeXML.Size = new System.Drawing.Size(456, 494);
            this.treeXML.TabIndex = 0;
            // 
            // txtXML
            // 
            this.txtXML.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtXML.Location = new System.Drawing.Point(0, 0);
            this.txtXML.Multiline = true;
            this.txtXML.Name = "txtXML";
            this.txtXML.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtXML.Size = new System.Drawing.Size(433, 494);
            this.txtXML.TabIndex = 0;
            this.txtXML.WordWrap = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.xml";
            this.openFileDialog1.Filter = "XML File (*.xml)|*.xml";
            this.openFileDialog1.Title = "Select XML file...";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.xml";
            this.saveFileDialog1.Filter = "XML File (*.xml)|*.xml";
            this.saveFileDialog1.Title = "Save XML file...";
            // 
            // brToNewLineToolStripMenuItem
            // 
            this.brToNewLineToolStripMenuItem.Checked = true;
            this.brToNewLineToolStripMenuItem.CheckOnClick = true;
            this.brToNewLineToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.brToNewLineToolStripMenuItem.Name = "brToNewLineToolStripMenuItem";
            this.brToNewLineToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.brToNewLineToolStripMenuItem.Text = "<br> to NewLine";
            this.brToNewLineToolStripMenuItem.CheckedChanged += new System.EventHandler(this.brToNewLineToolStripMenuItem_CheckedChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(177, 6);
            // 
            // frmViewXML
            // 
            this.ClientSize = new System.Drawing.Size(893, 540);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmViewXML";
            this.Text = "HS XML Viewer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadXMLFileOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveXMLFileSToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewVToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wordWrapWToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel txtStatus;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeXML;
        private System.Windows.Forms.TextBox txtXML;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem brToNewLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}