﻿using HSAudio.Lib.FMOD;
using HSAudio.Speakers;
using HS.Setting;
using System;
using System.Xml;
using HS;

namespace HSAudio.DSP
{
    /// <summary>
    /// DSP 스피커 속성 입니다.
    /// </summary>
    [Serializable]
    public class DSPSpeaker : ISettingManager
    {
        DSP dsp;
        internal DSPSpeaker(DSP dsp) { this.dsp = dsp; }

        internal CHANNELMASK _Mask;
        internal int _ChannelCount;
        internal SPEAKERMODE _Mode;
        public ChannelMask Mask
        {
            get { Update(); return (ChannelMask)_Mask; }
            set { dsp.Result = dsp.DSPInternal.setChannelFormat((CHANNELMASK)value, ChannelCount, (SPEAKERMODE)Mode); }
        }
        public int ChannelCount
        {
            get { Update(); return _ChannelCount; }
            set { dsp.Result = dsp.DSPInternal.setChannelFormat((CHANNELMASK)Mask, value, (SPEAKERMODE)Mode); }
        }
        public SpeakerMode Mode
        {
            get { Update(); return (SpeakerMode)_Mode; }
            set { dsp.Result = dsp.DSPInternal.setChannelFormat((CHANNELMASK)Mask, ChannelCount, (SPEAKERMODE)value); }
        }

        private void Update(){dsp.Result = dsp.DSPInternal.getChannelFormat(out _Mask, out _ChannelCount, out _Mode);}
        

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("ChannelCount", ChannelCount);
            s.SetValue("Mask", new SettingsData(Mask.ToString(), false));
            s.SetValue("Mode", new SettingsData(Mode.ToString(), false));
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("ChannelCount")) ChannelCount = Convert.ToInt32(Setting["ChannelCount"]);
            if (Setting.Exist("Mask")) Mask = (ChannelMask)Enum.Parse(typeof(ChannelMask), Setting["Mask"]);
            if (Setting.Exist("Mode")) Mode = (SpeakerMode)Enum.Parse(typeof(SpeakerMode), Setting["Mode"]);
            return true;
        }
        /*
        public bool LoadSetting(XmlNode Setting)
        {
            if (Setting.Name.ToLower() == "speaker")
            {
                for (int i = 0; i < Setting.Attributes.Count; i++)
                {
                    if ("ChannelCount".Equals(Setting.Attributes[i].Name)) ChannelCount = Convert.ToInt32(Setting.Attributes[i].Value);
                }

                for (int i = 0; i < Setting.ChildNodes.Count; i++)
                {
                    if ("Mask".Equals(Setting.ChildNodes[i].Name)) Mask = (CHANNELMASK)Enum.Parse(typeof(CHANNELMASK), Setting.ChildNodes[i].Value);
                    else if ("Mode".Equals(Setting.ChildNodes[i].Name)) Mode = (SPEAKERMODE)Enum.Parse(typeof(SPEAKERMODE), Setting.ChildNodes[i].Value);
                }
                return true;
            }
            else return false;
        }
        */

    }
}
