﻿namespace HSAudio.Utility.DSP
{
    public partial class HSDSP
    {
        #region 믹싱 알고리즘
        public static float SetMix(float OriginalValue, float Value, float Mix)
        {
            //return SetMix(OriginalValue, Value, Mix, 1, 0, Mix<0);
            return Mix >= 0 ?
                SetMix(OriginalValue, Value, Mix, 1, 0, Mix < 0) :
                SetMix(Value, OriginalValue, -Mix, 1, 0, Mix < 0);
        }
        public static float SetMix(float OriginalValue, float Value, float Mix, float MaxMix, float MinMix, bool Substract = false)
        {
            if (Mix == MinMix || Mix < MinMix) return OriginalValue;
            else if (Mix == MaxMix || Mix > MaxMix) return Value;
            return Substract ?
                ((Mix * Value) - ((MaxMix - Mix) * OriginalValue)) :
                ((Mix * Value) + ((MaxMix - Mix) * OriginalValue));
            //Mix > MaxMix ? (Mix * Value) + ((MaxMix - Mix) * OriginalValue) : ((WetMix) * Value) + ((1 + MaxMix) * OriginalValue);
            //return a;
        }
        public static float SetMix(float OriginalValue, float Value, float WetMix, float DryMix)
        {
            float y = SetMix(0, OriginalValue, DryMix, 1, 0, false);//SetMix(0, OriginalValue, DryMix, 1, 0, false);
            float x = SetMix(0, Value, WetMix, 1, 0, false);//SetMix(0, Value, WetMix, 1, 0, false);
            return x + y;//SetMix(x + y, x + OriginalValue, DryMix);
        }
        public static float SetMix(float OriginalValue, float Value, float WetMix, float DryMix, float MaxMix = 1, float MinMix = 0)
        {
            float y = SetMix(0, OriginalValue, DryMix, MaxMix, MinMix, false);
            float x = SetMix(0, Value, WetMix, MaxMix, MinMix, false);
            return x + y;//SetMix(x + y, x + OriginalValue, DryMix, MaxMix, MinMix, false);
        }
        public static float SetMix2Value(float OriginalValue, float Value, float Value1, float Mix)
        {
            if (Mix == 0) return OriginalValue;
            return Mix > 0 ? (Mix * Value) + ((1 - Mix) * OriginalValue) :
                                ((Mix) * Value1) + ((1 + Mix) * OriginalValue);
        }
        public static float SetMix2Value(float OriginalValue, float Value, float Value1, float Mix, float MaxMix = 1, float MinMix = 0)
        {
            if (Mix == MinMix) return OriginalValue;
            return Mix > MinMix ? (Mix * Value) + ((MaxMix - Mix) * OriginalValue) :
                                  (Mix * Value1) + ((MaxMix + Mix) * OriginalValue);
            //float a = (WetMix * Value) + ((1 - WetMix) * OriginalValue);
            //float b = SetWetMix(a, a, DryMix);
            //return a;
        }

        public static float Mixing(params float[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return (float)a;
        }
        public static double Mixing(params double[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return a;
        }
        public static float MixingAverage(params float[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return (float)(a / Value.Length);
        }
        public static double MixingAverage(params double[] Value)
        {
            double a = 0;
            for (int i = 0; i < Value.Length; i++) a += Value[i];
            return a / Value.Length;
        }
        #endregion
    }
}
