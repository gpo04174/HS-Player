﻿using Xamarin.Forms;

namespace HSPlayer.Controls
{
    public delegate void LongPressedEventHandler(object sender);
    public delegate void ValueChangedEventHandler(object sender, double Value);
    public delegate void TextChangedEventHandler(object sender, string Text);
    public delegate void ColorChangedEventHandler(object sender, Color Color);
}
