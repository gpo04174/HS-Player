﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.Core.Sound._3D
{
    public class Distance3D : ISettingManager
    {
        FD_Sound sound;
        float _Minimum;
        float _Maximum;
        internal Distance3D(FD_Sound sound) { this.sound = sound; }

        public float Minimum { get { Update(); return _Minimum; } set { sound.set3DMinMaxDistance(value, Maximum); } }
        public float Maximum { get { Update(); return _Maximum; } set { sound.set3DMinMaxDistance(Minimum, value); } }

        void Update() { sound.get3DMinMaxDistance(out _Minimum, out _Maximum); }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            try { if (Setting.Exist("Minimum")) Minimum = Convert.ToSingle(Setting["Minimum"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Maximum")) Maximum = Convert.ToSingle(Setting["Maximum"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Minimum", Minimum);
            s.SetValue("Maximum", Maximum);
            return s;
        }
    }
}
