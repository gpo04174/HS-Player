﻿using HSAudio.PlugIn.DSP;
#if FUNC_PLUGIN
namespace HSAudio.PlugIn
{
    public interface IHSAudioDSPPlugin : IHSAudioPlugin
    {
        /// <summary>
        ///  Cannot be null.
        /// </summary>
        /// <returns></returns>
        Description GetDescription();
    }
}
#endif