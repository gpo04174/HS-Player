﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using HSAudio.Tag;
using System.IO;

namespace HSAudio.Core.Sound
{
    public class SoundTag
    {
        internal Dictionary<string, List<TagData>> dic;
        internal SoundTag() { }
        //string Path;
        internal SoundTag(Dictionary<string, List<TagData>> dic)
        {
            //this.Path = Path;
            this.dic = dic;
            IEnumerator<KeyValuePair<string, List<TagData>>> a = dic.GetEnumerator();
            Tags = new List<TagData[]>();
            while (a.MoveNext())Tags.Add(a.Current.Value.ToArray());
            Parse();
        }
        //public SoundTag(TagLib tag) { }

        public string Title { get; internal set; }
        public string Artist { get; internal set; }
        public string Album { get; internal set; }
        //public int Bitrate { get; internal set; }
        public byte[][] AlbumArt { get; internal set; }

        //public int Bitrate { get; private set; }

        public List<TagData[]> Tags { get; private set; }

        public Dictionary<string, List<TagData>> GetDicnary() { return new Dictionary<string, List<TagData>>(dic); }

#if DEBUG
        public static SoundTag GetTag(FD_Sound sound)
#else
        internal static SoundTag GetTag(FD_Sound sound)
#endif
        {
            int num = 0, updated = 0;
            sound.getNumTags(out num, out updated);

            if (num == 0) return null;
            else
            {
                TagData data = null;

                //string Title = null, Artist = null, Album = null; Bitrate = null;
                List<byte[]> AlbumArt = new List<byte[]>();

                TAG tag;
                Dictionary<string, List<TagData>> dic = new Dictionary<string, List<TagData>>();
                for (int i = 0; i < num; i++)
                {
                    sound.getTag(null, i, out tag);

                    if (dic.ContainsKey(tag.name))
                    {
                        List<TagData> list = dic[tag.name];

                        if (tag.datatype == TAGDATATYPE.STRING_UTF8 || tag.datatype == TAGDATATYPE.STRING_UTF16 || tag.datatype == TAGDATATYPE.STRING_UTF16BE) { data = new TagData(tag.name, Marshal.PtrToStringUni(tag.data, (int)tag.datalen).Trim()); list.Add(data); }
                        else if (tag.datatype == TAGDATATYPE.STRING) { data = new TagData(tag.name, Marshal.PtrToStringAnsi(tag.data, (int)tag.datalen).Trim()); list.Add(data); }
                        else
                        {
                            byte[] a = new byte[tag.datalen];
                            Marshal.Copy(tag.data, a, 0, (int)tag.datalen);
                            if (tag.datatype == TAGDATATYPE.FLOAT) { data = new TagData(tag.name, BitConverter.ToSingle(a, 0)); list.Add(data); }
                            else if (tag.datatype == TAGDATATYPE.INT) { data = new TagData(tag.name, BitConverter.ToInt32(a, 0)); list.Add(data); }
                            else list.Add(new TagData(tag.name, a));
                        }
                    }
                    else
                    {
                        List<TagData> list = new List<TagData>();

                        if (tag.datatype == TAGDATATYPE.STRING_UTF8 || tag.datatype == TAGDATATYPE.STRING_UTF16 || tag.datatype == TAGDATATYPE.STRING_UTF16BE) { data = new TagData(tag.name, Marshal.PtrToStringUni(tag.data, (int)tag.datalen).Trim()); list.Add(data); }
                        else if (tag.datatype == TAGDATATYPE.STRING) { data = new TagData(tag.name, Marshal.PtrToStringAnsi(tag.data, (int)tag.datalen).Trim()); list.Add(data); }
                        else
                        {
                            byte[] a = new byte[tag.datalen];
                            Marshal.Copy(tag.data, a, 0, (int)tag.datalen);
                            if (tag.datatype == TAGDATATYPE.FLOAT) { data = new TagData(tag.name, BitConverter.ToSingle(a, 0)); list.Add(data); }
                            else if (tag.datatype == TAGDATATYPE.INT) { data = new TagData(tag.name, BitConverter.ToInt32(a, 0)); list.Add(data); }
                            else list.Add(new TagData(tag.name, a));
                        }

                        dic.Add(tag.name, list);
                    }

                    /*
                    string name = data.Name.ToUpper();
                    if (Title == null && (name.IndexOf("TIT2") >= 0 || name.IndexOf("TITLE") >= 0)) Title = data.Data.ToString();
                    else if (Artist == null && (name.IndexOf("TPE1") >= 0 || name.IndexOf("ARTIST") >= 0)) Artist = data.Data.ToString();
                    else if (Album == null && (name.IndexOf("TALB") >= 0 || name.IndexOf("ALBUM") >= 0)) Artist = data.Data.ToString();
                    else if (name.IndexOf("APIC") >= 0) AlbumArt.Add((byte[])data.Data);
                    else if(Bitrate == null && name == "ICY-BR") Bitrate = data.Data.ToString();
                    */
                }
                return new SoundTag(dic);// { Title = Title, Album = Album, Artist = Artist, AlbumArt = AlbumArt.ToArray() };//, Bitrate = Convert.ToInt32(Bitrate)
            }
        }
        public void Parse()
        {
            if (dic.ContainsKey("TIT2")) Title = dic["TIT2"][0];
            else if(dic.ContainsKey("TITLE")) Title = dic["TITLE"][0];
            if (dic.ContainsKey("TPE1")) Artist = dic["TPE1"][0];
            else if (dic.ContainsKey("ARTIST")) Artist = dic["ARTIST"][0];
            if (dic.ContainsKey("TALB")) Album = dic["TALB"][0];
            else if (dic.ContainsKey("ALBUM")) Album = dic["ALBUM"][0];
            if (dic.ContainsKey("APIC"))
            {
                List<TagData> pic = dic["APIC"];
                AlbumArt = new byte[pic.Count][];
                for (int i = 0; i < AlbumArt.Length; i++)
                    AlbumArt[i] = pic[i];
            }
        }
    }
}
