﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class Normalize : IDSP
    {
        private Parameter[] param = new Parameter[3];
        public Normalize(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.NORMALIZE, "Normalize", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Time to ramp the silence to full in ms.  0.0 to 20000.0. Default = 5000.0
        /// </summary>
        public float FadeTime
        {
            get { return DSP.GetParameter((int)DSP_NORMALIZE.FADETIME, 0f); }
            set { DSP.SetParameter((int)DSP_NORMALIZE.FADETIME, value); }
        }
        /// <summary>
        /// Lower volume range threshold to ignore.  0.0 to 1.0.  Default = 0.1.  Raise higher to stop amplification of very quiet signals
        /// </summary>
        public float Threshold
        {
            get { return DSP.GetParameter((int)DSP_NORMALIZE.THRESHOLD, 0f); }
            set { DSP.SetParameter((int)DSP_NORMALIZE.THRESHOLD, value); }
        }
        /// <summary>
        /// Maximum amplification allowed.  1.0 to 100000.0.  Default = 20.0.  1.0 = no amplifaction, higher values allow more boost
        /// </summary>
        public float MaxAmp
        {
            get { return DSP.GetParameter((int)DSP_NORMALIZE.MAXAMP, 0f); }
            set { DSP.SetParameter((int)DSP_NORMALIZE.MAXAMP, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("FadeTime")) try { FadeTime = float.Parse(Setting["FadeTime"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Threshold")) try { Threshold = float.Parse(Setting["Threshold"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("MaxAmp")) try { MaxAmp = float.Parse(Setting["MaxAmp"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("FadeTime", FadeTime);
            s.SetValue("Threshold", Threshold);
            s.SetValue("MaxAmp", MaxAmp);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
