﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound
{
    public class SoundStatus
    {
        internal uint bufferpercent;
        internal bool starving;
        internal bool diskbusy;
        internal SoundStatus() { }
        /// <summary>
        /// Gets Buffering Percent <para/>
        /// 버퍼링 백분율을 가져옵니다.
        /// </summary>
        public uint BufferingPercent { get { return bufferpercent; } }
        /// <summary>
        /// 
        /// </summary>
        public bool Starving { get { return starving; } }
        /// <summary>
        /// 
        /// </summary>
        public bool DiskBusy { get { return diskbusy; } }

        internal void Clear()
        {
            bufferpercent = 0;
            starving = false;
            diskbusy = false;
        }

        internal static SoundState GetStatus(OPENSTATE state)
        {
            if (state == OPENSTATE.READY) return SoundState.OK;
            else if (state == OPENSTATE.BUFFERING) return SoundState.Buffering;
            else if (state == OPENSTATE.LOADING) return SoundState.Loading;
            else if (state == OPENSTATE.CONNECTING) return SoundState.Connecting;
            else if (state == OPENSTATE.PLAYING || state == OPENSTATE.SEEKING || state == OPENSTATE.SETPOSITION) return SoundState.OK;
            else return SoundState.Unknown;
        }
    }
}
