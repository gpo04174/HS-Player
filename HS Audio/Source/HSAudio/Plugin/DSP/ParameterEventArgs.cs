﻿using HSAudio.DSP;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.PlugIn.DSP
{
    public class ParameterEventArgs : EventArgs
    {
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, float value) { Value_Float = value; Length = 4; Init(ref state, index, value.ToString()); }
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, bool value) { Value_Bool = value; Length = 1; Init(ref state, index, value.ToString()); }
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, int value) { Value_Int = value; Length = 4; Init(ref state, index, value.ToString()); }
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, IntPtr value, uint length) { Value_Data = value; Length = Value_Data_Length = length; Init(ref state, index, null); }
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, float value, IntPtr valuestr)
        {
            Init(ref state, index, valuestr);
            //this.Value = value;
            Value_Float = value;
            Length = 4;
            Kind = DSPParameterType.Float;
        }
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, bool value, IntPtr valuestr)
        {
            Init(ref state, index, valuestr);
            //this.Value = value;
            Value_Bool = value;
            Length = 1;
            Kind = DSPParameterType.Bool;
        }
        protected internal ParameterEventArgs(ref DSP_STATE state, int index, int value, IntPtr valuestr)
        {
            Init(ref state, index, valuestr);
            //this.Value = value;
            Value_Int = value;
            Length = 4;
            Kind = DSPParameterType.Int;
        }
        protected internal unsafe ParameterEventArgs(ref DSP_STATE state, int index, IntPtr value, uint length, IntPtr valuestr)
        {
            Init(ref state, index, valuestr);
            Length = length;
            /*
            byte[] Value = new byte[length];
            if (length > int.MaxValue)
            {
                Marshal.Copy(value, Value, 0, int.MaxValue);
                uint len = length - int.MaxValue;
                byte* d = (byte*)value.ToPointer();
                for (uint i = int.MaxValue; i < len; i++) Value[i] = d[i];
            }
            else Marshal.Copy(value, Value, 0, (int)length);l
            */
            Kind = DSPParameterType.Data;
            Value_Data = value;
            Value_Data_Length = length;
        }
        protected void Init(ref DSP_STATE state, int index, IntPtr valuestr)
        {
            this.State = state;
            this.Index = index;
            char[] tmp = new char[16];
            Marshal.Copy(valuestr, tmp, 0, 16);
            ValueString = new string(tmp);
        }
        protected void Init(ref DSP_STATE state, int index, string valuestr)
        {
            this.State = state;
            this.Index = index;
            ValueString = valuestr;
        }

        IntPtr valuestr = IntPtr.Zero;

        public DSP_STATE State { get; set; }

        /// <summary>
        /// Max Length is 16 byte and ASCII Only.
        /// </summary>
        public string ValueString { get; set; }
        public int Index { get; set; }

        //public object Value { get; set; }
        public float Value_Float { get; set; }
        public int Value_Int { get; set; }
        public bool Value_Bool { get; set; }

        public IntPtr Value_Data { get; set; }
        public uint Value_Data_Length { get; set; }

        public uint Length { get; set; }
        public DSPParameterType Kind { get; set; }

        protected internal unsafe void setValueString(ref IntPtr strptr)
        {
            if (ValueString == null) strptr = IntPtr.Zero;
            char[] a = ValueString.ToCharArray();
            byte* str = (byte*)strptr.ToPointer();
            for (int i = 0; i < Math.Min(a.Length, 16); i++) str[i] = (byte)a[i];
        }


        public static implicit operator IntPtr(ParameterEventArgs param) { return param.Value_Data; }
        public static implicit operator float(ParameterEventArgs param) { return param.Value_Float; }
        public static implicit operator int(ParameterEventArgs param) { return param.Value_Int; }
        public static implicit operator bool(ParameterEventArgs param) { return param.Value_Bool; }
    }
}
