﻿using HSAudio.Lib.FMOD;
using HSAudio.Wave.Device;
using HS.Setting;
using System;

namespace HSAudio.Output
{
    public abstract class OutputMethod : ICloneable, ISettingManager
    {
        public virtual int ID { get; protected set; }
        public virtual string Name { get; protected set; }
        public WaveDevice Device { get; protected set; }
        public abstract Output Parent { get; internal set; }
        public  OutputFormat Format { get; protected set; }

        public abstract object Clone();

        public abstract bool LoadSetting(Settings Setting, bool ThrowException = false);
        public abstract Settings SaveSetting();

        public override string ToString() { return Name; }
        internal abstract protected void Dispose();
        public new abstract bool Equals(object obj);

        public OutputType Type { get; internal set; }
    }
}
