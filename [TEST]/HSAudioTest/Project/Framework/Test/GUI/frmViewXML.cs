﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace HSPlayerCoreTest.GUI
{
    public partial class frmViewXML : Form
    {
        public frmViewXML()
        {
            InitializeComponent();
        }
        public frmViewXML(XmlDocument xml)
        {
            InitializeComponent();
            SetXML(xml);
        }

        XmlDocument XML;
        public void SetXML(XmlDocument xml)
        {
            this.XML = xml;

            StringWriter sw = new StringWriter();
            xml.Save(sw);
            this.txtXML.Text = brToNewLineToolStripMenuItem.Checked ? sw.ToString().Replace("&lt;br&gt;", "\r\n") : sw.ToString();
            sw.Close();

            treeXML.Nodes.Clear();
            ConvertXmlNodeToTreeNode(xml, treeXML.Nodes);
            treeXML.Nodes[0].ExpandAll();
        }
        private void ConvertXmlNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "ATTRIBUTE: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = brToNewLineToolStripMenuItem.Checked ? xmlNode.Value.Replace("<br>", "\r\n") : xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                }
            }
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
            }
        }

        private void wordWrapWToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            txtXML.WordWrap = wordWrapWToolStripMenuItem.Checked;
        }

        private void brToNewLineToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            SetXML(XML);
        }

        private void saveXMLFileSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(XML != null)
            {
                if(saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileStream fs = null;
                    try
                    {
                        fs = new FileStream(saveFileDialog1.FileName, FileMode.Create, FileAccess.Write, FileShare.Write);
                        XML.Save(fs);
                        fs.Close();
                    }
                    catch(Exception ex){ MessageBox.Show("Un error has occured!!\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                    finally { if (fs != null) fs.Close(); }
                }
            }
        }

        private void loadXMLFileOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(openFileDialog1.FileName);
                    SetXML(doc);
                }
                catch (Exception ex) { MessageBox.Show("Un error has occured!!\n\n" + ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }
    }
}
