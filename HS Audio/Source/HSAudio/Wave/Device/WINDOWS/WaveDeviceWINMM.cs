﻿#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
using HSAudio.Control.Device;
using HSAudio.Lib.Windows.WaveLib.AudioMixer;
using Microsoft.Win32;
using HSAudio.Lib.Windows.NAudio;
using HSAudio.Lib.Windows.NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace HSAudio.Wave.Device
{
    public class WaveDeviceWINMM : WaveDevice
    {
        public static WaveDeviceWINMM[] GetDevices(bool InputDevice)
        {
            int cnt = InputDevice ? WaveIn.DeviceCount : WaveOut.DeviceCount;
            WaveDeviceWINMM[] w = new WaveDeviceWINMM[cnt];
            for (int i = 0; i < cnt; i++)
                if (InputDevice) w[i] = new WaveDeviceWINMM(WaveIn.GetCapabilities(i), i);
                else w[i] = new WaveDeviceWINMM(WaveOut.GetCapabilities(i), i);
            return w;
        }

        internal protected WaveDeviceWINMM(WaveInCapabilities WaveInCap, int DeviceID)
        {
            Kind = DeviceKind.Input;
            capin = WaveInCap;
            ProductID = WaveInCap.ProductID;

            int mixerId = -1;
            MmResult result = mixerGetID((IntPtr)DeviceID, out mixerId, Lib.Windows.NAudio.Mixer.MixerFlags.WaveIn);
            if (result == MmResult.NoError)
            {
                Mixer Mixer = new Mixer(MixerType.Recording, mixerId);
                if (Mixer != null)
                {
                    this.Mixer = Mixer;
                    MasterControl = new DeviceControlWINMM(this, Mixer.Lines[0]);
                    SubControls = DeviceControlWINMM.GetInstance(this, Mixer.Lines, 1, Mixer.Lines.Count);

                    Mixer.MixerLineChanged += (m, l) =>
                    {
                        DeviceControlWINMM w = MasterControl as DeviceControlWINMM;
                        if (w != null && w._Control.HMixer == l.HMixer)
                            if (DevicePropertyChanged != null) try { DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(-1, w)); }
                                catch { }

                        for (int i = 0; i < SubControls.Length; i++)
                        {
                            DeviceControlWINMM w1 = SubControls[i] as DeviceControlWINMM;
                            if (w1 != null && w1._Control.HMixer == l.HMixer)
                                if (DevicePropertyChanged != null) try { DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(i, w1)); }
                                    catch { }
                        }
                        /*
                        if (MasterControl is DeviceControlWINMM)
                        {
                            DeviceControlWINMM w = (DeviceControlWINMM)MasterControl;
                            if(DevicePropertyChanged != null)try{DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(-1, w));}catch{}
                        }
                        for (int i = 0; i < SubControls.Length; i++)
                            if(SubControls[i] is DeviceControlWINMM)
                            {
                                DeviceControlWINMM w1 = (DeviceControlWINMM)SubControls[i];
                                if(DevicePropertyChanged != null)try{DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(i, w1));}catch{}
                            }
                       */
                    };
                }
                else throw new Exception("Mixer cannot be null.");
                //Name = new string(WaveInCap.szPname, 0, WaveInCap.szPname.Length);
                //Controls[0].Name = WaveInCap.NameGuid.Equals(new Guid()) ? WaveInCap.ProductName : GetNameFromGuid(WaveInCap.NameGuid);
                DeviceName = WaveInCap.NameGuid.Equals(new Guid()) ? WaveInCap.ProductName : GetNameFromGuid(WaveInCap.NameGuid);
                ManufacturerID = WaveInCap.ManufacturerID;

                List<WaveFormat> list = new List<WaveFormat>();
                string[] names = Enum.GetNames(typeof(SupportedWaveFormat));

                for (int i = 0; i < names.Length; i++)
                {
                    //SupportedWaveFormat supportedFormats = (SupportedWaveFormat)Enum.ToObject(typeof(SupportedWaveFormat), WaveInCap.dwFormats);
                    //SupportedWaveFormat waveFormat = (SupportedWaveFormat)Enum.Parse(typeof(SupportedWaveFormat), names[i]);
                    //if ((supportedFormats & waveFormat) == waveFormat)
                    if (WaveInCap.SupportsWaveFormat((SupportedWaveFormat)Enum.Parse(typeof(SupportedWaveFormat), names[i])))
                    {
                        string tail = names[i].Substring(names[i].LastIndexOf("_") + 1);

                        int bit = Convert.ToInt32(tail.Substring(tail.Length - 2));
                        tail = tail.Remove(tail.Length - 2);

                        int chan = Convert.ToInt32(tail.Substring(tail.Length - 1) == "M" ? 1 : 2);
                        tail = tail.Remove(tail.Length - 1);

                        int rate = 0;
                        if (tail == "1") rate = 11025;
                        else if (tail == "2") rate = 22050;
                        else if (tail == "44") rate = 44100;
                        else if (tail == "48") rate = 48000;
                        else rate = 96000;

                        WaveFormat wf = new WaveFormat(rate, (short)chan, bit);
                        list.Add(wf);
                    }
                }
                Formats = list.ToArray();
            }
            else throw new Exception("Init Failed!!");
        }
        internal protected WaveDeviceWINMM(WaveOutCapabilities WaveOutCap, int DeviceID)
        {
            Kind = DeviceKind.Output;
            capout = WaveOutCap;
            ProductID = -1;

            int mixerId = -1;
            MmResult result = mixerGetID((IntPtr)DeviceID, out mixerId, Lib.Windows.NAudio.Mixer.MixerFlags.WaveOut);
            if (result == MmResult.NoError)
            {
                Mixer Mixer = new Mixer(MixerType.Playback, mixerId);
                if (Mixer != null)
                {
                    this.Mixer = Mixer;
                    MasterControl = new DeviceControlWINMM(this, Mixer.Lines[0]);
                    SubControls = DeviceControlWINMM.GetInstance(this, Mixer.Lines, 1, Mixer.Lines.Count);

                    Mixer.MixerLineChanged += (m, l) =>
                    {
                        DeviceControlWINMM w = MasterControl as DeviceControlWINMM;
                        if (w != null && w._Control.HMixer == l.HMixer)
                            if (DevicePropertyChanged != null) try { DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(-1, w)); } catch { }

                        for (int i = 0; i < SubControls.Length; i++)
                        {
                            DeviceControlWINMM w1 = SubControls[i] as DeviceControlWINMM;
                            if (w1 != null && w1._Control.HMixer == l.HMixer)
                                if (DevicePropertyChanged != null) try { DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(i, w1)); } catch { }
                        }
                    };
                }
                else throw new Exception("Mixer cannot be null.");
                //Name = new string(WaveInCap.szPname, 0, WaveInCap.szPname.Length);
                //Controls[0].Name = WaveInCap.NameGuid.Equals(new Guid()) ? WaveInCap.ProductName : GetNameFromGuid(WaveInCap.NameGuid);
                DeviceName = WaveOutCap.NameGuid.Equals(new Guid()) ? WaveOutCap.ProductName : GetNameFromGuid(WaveOutCap.NameGuid);
                ManufacturerID = -1;

                List<WaveFormat> list = new List<WaveFormat>();
                string[] names = Enum.GetNames(typeof(SupportedWaveFormat));

                for (int i = 0; i < names.Length; i++)
                {
                    //SupportedWaveFormat supportedFormats = (SupportedWaveFormat)Enum.ToObject(typeof(SupportedWaveFormat), WaveInCap.dwFormats);
                    //SupportedWaveFormat waveFormat = (SupportedWaveFormat)Enum.Parse(typeof(SupportedWaveFormat), names[i]);
                    //if ((supportedFormats & waveFormat) == waveFormat)
                    if (WaveOutCap.SupportsWaveFormat((SupportedWaveFormat)Enum.Parse(typeof(SupportedWaveFormat), names[i])))
                    {
                        string tail = names[i].Substring(names[i].LastIndexOf("_") + 1);

                        int bit = Convert.ToInt32(tail.Substring(tail.Length - 2));
                        tail = tail.Remove(tail.Length - 2);

                        int chan = Convert.ToInt32(tail.Substring(tail.Length - 1) == "M" ? 1 : 2);
                        tail = tail.Remove(tail.Length - 1);

                        int rate = 0;
                        if (tail == "1") rate = 11025;
                        else if (tail == "2") rate = 22050;
                        else if (tail == "44") rate = 44100;
                        else if (tail == "48") rate = 48000;
                        else rate = 96000;

                        WaveFormat wf = new WaveFormat(rate, (short)chan, bit);
                        list.Add(wf);
                    }
                }
                Formats = list.ToArray();
            }
            else throw new Exception("Init Failed!!");
        }

        WaveInCapabilities capin;
        WaveOutCapabilities capout;
        internal Mixer Mixer;

        public event DevicePropertyChangedEventHandler DevicePropertyChanged;

        public int ProductID { get; internal set; }
        public int ManufacturerID { get; internal set; }


        public string DeviceName { get; private set; }
        public object Device { get { return Mixer; } }
        public DeviceKind Kind { get; private set; }
        public WaveFormat[] Formats { get; private set; }

        public DeviceControl MasterControl { get; private set; }
        public DeviceControl[] SubControls { get; private set; }

        public override string ToString()
        {
            return DeviceName;
        }
        public override bool Equals(object obj)
        {
            WaveDeviceWINMM w = obj as WaveDeviceWINMM;
            if (w != null)return w.ProductID.Equals(ProductID);
            return false;
        }
        public override int GetHashCode() { return base.GetHashCode(); }

        public void Dispose()
        {
            Mixer.Dispose();
            Mixer = null;
            SubControls = null;
            GC.Collect();
            IsDisposed = true;
        }
        public bool IsDisposed { get; private set; }

        #region GetDevice
        public static string GetNameFromGuid(Guid guid)
        {
            // n.b it seems many audio drivers just return the default values, which won't be in the registry
            // http://www.tech-archive.net/Archive/Development/microsoft.public.win32.programmer.mmedia/2006-08/msg00102.html
            string name = null;
            using (var namesKey = Registry.LocalMachine.OpenSubKey(@"System\CurrentControlSet\Control\MediaCategories"))
            using (var nameKey = namesKey.OpenSubKey(guid.ToString("B")))
            {
                if (nameKey != null) name = nameKey.GetValue("Name") as string;
            }
            return name;
        }

        [DllImport("winmm.dll")]
        static extern int waveInGetNumDevs();
        [DllImport("winmm.dll", EntryPoint = "waveInGetDevCaps", CharSet = CharSet.Auto)]
        static extern int waveInGetDevCapsA(int uDeviceID, ref WaveInCaps lpCaps, int uSize);

        /*
        [DllImport("winmm.dll")]
        static extern Int32 waveOutGetNumDevs();
        // http://msdn.microsoft.com/en-us/library/dd743857%28VS.85%29.aspx
        [DllImport("winmm.dll", EntryPoint = "waveOutGetDevCaps",  CharSet = CharSet.Auto)]
        static extern int waveOutGetDevCaps(IntPtr deviceID, out WaveOutCaps waveOutCaps, int waveOutCapsSize);
        */
        public static MmResult CloseMixer(IntPtr MixerHandle) { return Lib.Windows.NAudio.Mixer.MixerInterop.mixerClose(MixerHandle); }
        // http://msdn.microsoft.com/en-us/library/dd757301%28VS.85%29.aspx
        [DllImport("winmm.dll", CharSet = CharSet.Ansi)]
        public static extern MmResult mixerGetID(IntPtr hMixer, out Int32 mixerID, Lib.Windows.NAudio.Mixer.MixerFlags dwMixerIDFlags);

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct WaveInCaps
        {
            public short wMid;
            public short wPid;
            public int vDriverVersion;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public char[] szPname;
            public uint dwFormats;
            public short wChannels;
            public short wReserved1;
        }
        #endregion
    }
}
#endif