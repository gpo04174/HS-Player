﻿using HSAudio.Core;
using HSAudio.Core.Audio;
using HSAudio.Core.Sound;
using HSAudio.Tag;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HSAudioTest.Test
{
    public class PlayTest
    {
        public static void Test(string MusicPath)
        {
            Console.Title = "HSAudio Play Test";

            HSAudio.HSAudio h = new HSAudio.HSAudio();
            Sound s = Sound.FromFile(h, MusicPath);
            //try { TagManager manage = s.GetTagManager(false); } catch(Exception ex) { }
            Audio a = h.Audio.Add(s);
            a.StatusChanged += Program.A_StatusChanged;
            a.Volume = 0.5f;
            h.UpdateClock = false;

            a.Play();

            a.Loop = LoopKind.Normal;
            //Equalizer3Band eq = new Equalizer3Band(h) { HighGain = 10f };
            //Flanger f = new Flanger(h);
            //FFT fft = new FFT(h);
            //a.DSP.Add(eq);

            //a1.Play();

            //h.Reverb = Reverb.SewerPipe;
            h.Reverb.Enable3D = true;
            h.Reverb.Reverb = Reverb.SewerPipe;
            //Thread.Sleep(2000);
            //h.Reverb3D.Enable = false;
            
            a.CurrentPosition = s.TotalPosition - 10000;
            //a.Pitch = 1;
            //a.Loop = LoopKind.Normal;
            uint before = 0;
            int curL = Console.CursorLeft, curT = Console.CursorTop;
            while (a.Status != AudioState.Stopped)
            {
                Dictionary<string, List<TagData>> dic = s.Tag == null ? null : s.Tag.GetDicnary();
                if (before != a.CurrentPosition)
                {
                    Console.SetCursorPosition(curL, curT);
                    Console.Write("\n[{0} / {1}] {2} / {3} -> \n[Buffer: {4}% ({10}), {5} {6}]\n{7} Kbps / {8} - {9}",
                        Program.GetTime(a.CurrentPosition),
                        Program.GetTime(s.TotalPosition),
                        a.CurrentPosition,
                        s.TotalPosition,
                        s.Status.BufferingPercent,
                        s.Type.Type,
                        "{" + s.Format.ToString() + "}",
                        s.Tag != null ? (dic.ContainsKey("icy-br") ? dic["icy-br"][0] : "?") : "",
                        s.Tag != null ? s.Tag.Title : "",
                        s.Tag != null ? s.Tag.Artist : "",
                        s.Status.Starving);
                    before = a.CurrentPosition;
                    /*
                    if (before > (3000 * Count) && before < (3050 * Count))
                    {
                        Count++;
                        //TagManager tm = s.GetTagManager();
                        //Random rd = new Random();
                        //tm.Tag.Title = "アスノヨゾラ哨戒班_" + rd.Next();
                        //tm.Write();
                    }
                    */
                }
                Thread.Sleep(10);
            }

            h.Dispose();
            Console.WriteLine();
        }
    }
}
