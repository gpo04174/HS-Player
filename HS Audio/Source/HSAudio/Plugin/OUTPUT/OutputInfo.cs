﻿using System;
using System.Collections.Generic;
using System.Text;
using HSAudio.Lib.FMOD;
using HSAudio.Speakers;

namespace HSAudio.PlugIn.Output
{
    public class OutputInfo
    {
        public OutputInfo(string Name, GUID Guid, int Samplerate, SpeakerMode Mode)
        {
            this.Name = Name;
            this.Samplerate = Samplerate;
            this.Mode = Mode;
        }
        public OutputInfo(string Name, GUID Guid, int Samplerate, int Channel)
        {
            this.Name = Name;
            this.Samplerate = Samplerate;
            Mode = SpeakerMode.Custom;
            this.Channel = Channel;
        }
        internal OUTPUT_STATE State { get; set; }
        public string Name { get; set; }
        public GUID Guid { get; set; }
        public int Samplerate { get; set; }
        public SpeakerMode Mode { get; set; }
        public int Channel { get; set; }
    }
}
