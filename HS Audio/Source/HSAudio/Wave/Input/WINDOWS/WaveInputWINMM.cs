﻿#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
using HSAudio.Wave.Device;
using HSAudio.Lib.Windows.NAudio.Wave;
using System;
using System.Collections.Generic;

namespace HSAudio.Wave.Input
{
    public class WaveInputWINMM : WaveInput
    {
        public static WaveInputWINMM[] GetDevices()
        {
            List<WaveInputWINMM> list = new List<WaveInputWINMM>();

            int waveInDevices = WaveIn.DeviceCount;
            for (int i = 0; i < waveInDevices; i++)
            {
                WaveInCapabilities cap = WaveIn.GetCapabilities(i);
                try { list.Add(new WaveInputWINMM(cap, i)); } catch (Exception ex) { }
            }

            return list.ToArray();
        }

        public WaveInputWINMM(WaveInCapabilities WaveInCap, int DeviceID)
        {
            Kind = RecordKind.WAVEIN;
            this.WaveInCap = WaveInCap;
            Init(DeviceID);
        }
        void Init(int DeviceID)
        {
            if (waveIn != null) waveIn.Dispose();
            waveIn = new WaveIn();
            Latency = 50;
            waveIn.DeviceNumber = DeviceID;

            Device = new WaveDeviceWINMM(WaveInCap, DeviceID);

            waveIn.DataAvailable += waveIn_DataAvailable;
            waveIn.RecordingStopped += waveIn_RecordingStopped;
        }

        //private IntPtr WaveHandle;
        internal WaveIn waveIn = null;
        //WaveDataInEventArgs args = null;
        public WaveInCapabilities WaveInCap { get; private set; }

        public WaveFormat Format { get { return waveIn.WaveFormat; } set { waveIn.WaveFormat = value; } }//{ get { return args.Format; } set { args.Format = value; } }
        public RecordKind Kind { get; private set; }

        public WaveDevice Device { get; private set; }

        public bool IsBusy { get { return waveIn.Recording; } }
        public int Latency { get { return waveIn.BufferMilliseconds; } set { waveIn.BufferMilliseconds = value; } }
        public bool LatencySupport { get { return true; } }

        public event DataInEventHandler WaveDataIn;
        public event DataInStoppedEventHandler WaveDataInStopped;

        public void Start()
        {
            waveIn.StartRecording();
        }

        public void Stop()
        {
            waveIn.StopRecording();
        }

        public void Dispose()
        {
            if (waveIn != null) try { Stop(); waveIn.Dispose(); } catch { }
            if (Device != null) try { Device.Dispose(); } catch { }
            waveIn = null;
            WaveDataIn = null;
            WaveDataInStopped = null;
            Device = null;
        }

        public override string ToString()
        {
            return "[WaveIn] " + (Device == null ? WaveInCap.ToString() : Device.ToString());
        }

#region Event
        void waveIn_DataAvailable(object o, WaveInEventArgs e) { if (WaveDataIn != null)WaveDataIn.Invoke(this, new DataInEventArgs(Format, e.Buffer, e.BytesRecorded)); }
        void waveIn_RecordingStopped(object o, StoppedEventArgs e) { if (WaveDataInStopped != null)WaveDataInStopped.Invoke(this, e); }
#endregion
    }
}
#endif