﻿using System;

namespace HS
{
    public interface IHSPluginInfo
    {
        string Version { get; }
        string Name { get; }
        string Description { get; }
        string Author { get; }
        object Tag { get; set; }
    }
}
