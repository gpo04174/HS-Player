﻿#if WIN_VISTA || WIN_10 || WIN_10_1703 || WIN_METRO
using HSAudio.Control.Device;
using HSAudio.Lib.Windows.CoreAudioApi;
using HSAudio.Lib.Windows.CoreAudioApi.Interfaces;
using HSAudio.Lib.Windows.NAudio.Wave;
using System;
using System.Collections.Generic;

namespace HSAudio.Wave.Device
{
    public class WaveDeviceWASAPI : WaveDevice
    {
        public static WaveDeviceWASAPI[] GetDevices(bool InputDevice)
        {
            MMDeviceEnumerator mm = new MMDeviceEnumerator();
            MMDeviceCollection collect = mm.EnumerateAudioEndPoints(InputDevice ? DataFlow.Capture : DataFlow.Render, DeviceState.Active);

            WaveDeviceWASAPI[] list = new WaveDeviceWASAPI[collect.Count];
            for (int i = 0; i < collect.Count; i++)
                try { list[i] = new WaveDeviceWASAPI(collect[i], InputDevice); } catch (Exception ex) { }

            return list;
        }

        MMDevice _Device;
        internal protected WaveDeviceWASAPI(MMDevice Device, bool IsInput)
        {
            Kind = IsInput ? DeviceKind.Input : DeviceKind.Output;
            Init(Device);
        }
        /*
        internal protected WaveDeviceWASAPI(Guid guid)
        {
            MMDevice Device = new MMDeviceEnumerator().GetDevice(guid.ToString());
            Kind = Device.
            Init(Device);
        }
        */
        void Init(MMDevice Device)
        {
            this._Device = Device;
            Device.AudioSessionManager.OnSessionCreated += AudioSessionManager_OnSessionCreated;
            Device.AudioEndpointVolume.OnVolumeNotification += AudioEndpointVolume_OnVolumeNotification;

            MasterControl = new DeviceControlWASAPI(this, _Device.AudioEndpointVolume, _Device.AudioMeterInformation, 0);
            RefreshContols();

            //사용가능한 채널 가져오기
            List<PropVariant> key = new List<PropVariant>();
            for (int i = 0; i < Device.Properties.Count; i++)
                key.Add(Device.Properties.GetValue(i));

            Lib.Windows.NAudio.Wave.WaveFormat wf = Device.AudioClient.MixFormat;
            Formats = new WaveFormat[] { new WaveFormat(wf.SampleRate, (byte)wf.Channels, wf.BitsPerSample, wf.IsIeeeFloat() ? WaveType.FLOAT : WaveType.PCM) };

            try
            {
                string guid = Device.ID.Substring(Device.ID.IndexOf("}") + 2);
                Guid = new Guid(guid);
            }
            catch { }
        }

        public event DevicePropertyChangedEventHandler DevicePropertyChanged;

        //public float Volume { get => throw new System.NotSupportedException(); set => throw new System.NotSupportedException(); }
        //public bool Mute { get => throw new System.NotSupportedException(); set => throw new System.NotSupportedException(); }

        public object Device { get { return _Device; } }
        public string DeviceName { get { return _Device.DeviceFriendlyName; } }
        public DeviceKind Kind { get; private set; }
        public WaveFormat[] Formats { get; private set; }

        public DeviceControl MasterControl { get; private set; }
        public DeviceControl[] SubControls { get; private set; }

        public Guid Guid { get; private set; }

        public override string ToString()
        {
            return _Device.FriendlyName;//string.Format("{0} ({1})", DeviceName, MasterControl.Name);
        }

        public void Dispose()
        {
            _Device.Dispose();
            for (int i = 0; i < SubControls.Length; i++)
            {
                DeviceControlWASAPI w = SubControls[i] as DeviceControlWASAPI;
                if (w!= null) w.Dispose();
            }

            SubControls = null;
            Formats = null;
            GC.Collect();
            IsDisposed = true;
        }
        public bool IsDisposed { get; private set; }


#region Event
        private void AudioSessionManager_OnSessionCreated(object sender, IAudioSessionControl newSession)
        {
            RefreshContols();
        }

        private void AudioEndpointVolume_OnVolumeNotification(AudioVolumeNotificationData data) { RaiseVolumeEvent(-1, MasterControl); }
        protected internal void RaiseVolumeEvent(int index, DeviceControl control) { if (DevicePropertyChanged != null) DevicePropertyChanged.Invoke(this, new WaveDevicePropertyChangedEventArgs(index, control)); }

        void RefreshContols()
        {
            for (int i = 0; SubControls != null && i < SubControls.Length; i++)
            {
                DeviceControlWASAPI w = SubControls[i] as DeviceControlWASAPI;
                if (w != null) w.Dispose();
            }

            int cnt = _Device.AudioSessionManager.Sessions.Count;
            DeviceControl[] controls = new DeviceControl[cnt];
            for (int i = 0; i < cnt; i++)
                try { controls[i] = new DeviceControlWASAPI(this, _Device.AudioSessionManager.Sessions[i], i); } catch (Exception ex) { }
            SubControls = controls;
        }
#endregion
    }
}
#endif
