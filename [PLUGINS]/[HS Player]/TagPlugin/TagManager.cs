﻿using HSAudio.Core.Sound;
using System;
using System.IO;
using TagLib;

namespace TagPlugin
{
    public class TagManager : IDisposable
    {
        Sound sound;
        Stream Stream;
        protected internal TagManager(string MusicPath) { this.MusicPath = MusicPath; Read(true); }
        protected internal TagManager(Sound sound, string MusicPath, bool Writeable)
        {
            this.sound = sound;
            this.MusicPath = MusicPath;
            this.Writeable = Writeable;
            Read(true);
        }
        protected internal TagManager(Sound sound, string Path, Stream Stream, bool Writeable, string MimeType = null, bool Close = true)
        {
            this.sound = sound;
            this.Writeable = Writeable;
            this.Stream = Stream;
            this.MusicPath = Path;
            Read(Close, MimeType);
        }
        protected internal TagManager(TagLib.File file, string MusicPath, bool Writeable)
        {
            this.file = file;
            this.Tag = file.Tag;
            this.MusicPath = MusicPath;
            this.Writeable = Writeable;
            Parse();
        }

        private TagLib.File file;
        public Tag Tag { get; set; }
        public TagTypes TagType { get; private set; }


        public bool Writeable { get; private set; }
        public string MusicPath { get; private set; }

        public bool Write(bool ThrowException = false)
        {
            if (Writeable && file != null)
            {
                if (sound != null) sound.Unload();
                try
                {
                    if (Stream != null) { if (!Stream.CanWrite) { if (sound != null) sound.Unload(); return false; } }
                    else
                    {
                        FileStream stream = new FileStream(MusicPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
                        file.RegesterWriteStream(stream);
                    }
                    file.Save();
                }
                catch(Exception ex) { if (ThrowException) throw ex; }
                if (sound != null) sound.Load();
                return true;
            }
            return false;
        }

        private void Read(bool Close, string mimetype = null)
        {
            if (Stream != null)
            {
                StreamFileAbstraction ab = new StreamFileAbstraction(MusicPath, Stream, null);
                file = mimetype == null ? TagLib.File.Create(ab): TagLib.File.Create(ab, mimetype, ReadStyle.Average);
                if(Close) Stream.Close();
            }
            else
            {
                FileStream stream = new FileStream(MusicPath, FileMode.Open, FileAccess.Read, FileShare.Read);
                StreamFileAbstraction ab = new StreamFileAbstraction(MusicPath, stream, null);
                file = mimetype == null ? TagLib.File.Create(ab) : TagLib.File.Create(ab, mimetype, ReadStyle.Average);
                if (Close) stream.Close(); 
            }
            Parse();
        }
        internal void Parse()
        {
            if (file != null)
            {
                this.Tag = file.Tag;
                this.TagType = file.TagTypes;
            }
        }

        //public static Lib.TagLib.Tag Read(string MusicPath, bool ThrowException = false) { }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            IsDisposed = true;
            if(Stream != null) Stream.Dispose();
        }
    }
}
