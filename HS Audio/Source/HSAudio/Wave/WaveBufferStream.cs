﻿using System;
using System.Collections.Generic;
using System.IO;

namespace HSAudio.Wave
{
    public class WaveBufferQueueStream : MemoryQueueBufferStream
    {
        public WaveBufferQueueStream(WaveFormat Format, int Latency) { this.Format = Format; this.Latency = Latency; MaximumLength = MinimumLength * 2; }
        public WaveBufferQueueStream(WaveFormat Format, int Latency, int MaximumLength) { this.Format = Format; this.Latency = Latency; this.MaximumLength = MaximumLength; }

        public WaveFormat Format { get; private set; }
        public long MinimumLength { get { return Format.ConvertLatencyToByteSize(Latency); } }
        public long MaximumLength { get; set; }
        public int Latency { get; set; }

        protected new void SetLength(long value) { base.SetLength(value); }
        public new void Flush() { base.SetLength(0); }
        public new void Write(byte[] buffer, int offset, int length)
        {
            if(Length + length <= MaximumLength)base.Write(buffer, offset, length);
        }

        public void ReadFloat(float[] buffer, int offset, int length)
        {
            if (Format.Bits == Bits.PCM8)
            {
                byte[] buf = new byte[length * 1];
                Read(buf, 0, buf.Length);
                int i = 0;
                try
                {
                    for (i = 0; i < buf.Length; i++)
                        buffer[offset + i] = buf[i] / (float)byte.MaxValue; //255f;
                }
                catch (Exception ex) { }
            }
            else if (Format.Bits == Bits.PCM16)
            {
                byte[] buf = new byte[length * 2];
                Read(buf, 0, buf.Length);
                int i = 0;
                try
                {
                    for (i = 0; i < buf.Length; i += 2)
                    {
                        short sample = (short)(buf[i + 1] << 8 | buf[i + 0]);
                        buffer[offset + (i / 2)] = sample / (float)short.MaxValue; //32768f;
                    }
                }
                catch (Exception ex) { }
            }
            else if (Format.Bits == Bits.PCM24)
            {
                byte[] buf = new byte[length * 3];
                Read(buf, 0, buf.Length);
                int i = 0;
                try
                {
                    for (i = 0; i < buf.Length; i += 3)
                    {
                        int sample = buf[i + 2] << 16 | buf[i + 1] << 8 | buf[i + 0];
                        buffer[offset + (i / 3)] = sample / (float)0xFFFFFF; //32768f;
                    }
                }
                catch (Exception ex) { }
            }
            else if (Format.Bits == Bits.PCM32)
            {
                byte[] buf = new byte[length * 4];
                Read(buf, 0, buf.Length); int i = 0;
                try
                {
                    for (i = 0; i < buf.Length; i += 4)
                    {
                        int sample = buf[i + 3] << 24 | buf[i + 2] << 16 | buf[i + 1] << 8 | buf[i + 0];
                        buffer[offset + (i / 4)] = sample / (float)int.MaxValue;
                    }
                }
                catch (Exception ex) { }
            }
            else if(Format.Bits == Bits.PCMFLOAT)
            {
                byte[] buf = new byte[length * 4];
                Read(buf, 0, buf.Length);

                byte[] tmpFloatBuffer = new byte[4];
                int i = 0;
                try
                {
                    for (i = 0; i < buf.Length; i += 4)
                    {
                        tmpFloatBuffer[0] = buf[i + 0];
                        tmpFloatBuffer[1] = buf[i + 1];
                        tmpFloatBuffer[2] = buf[i + 2];
                        tmpFloatBuffer[3] = buf[i + 3];
                        buffer[offset + (i / 4)] = BitConverter.ToSingle(tmpFloatBuffer, 0);
                    }
                }
                catch (Exception ex) { }
                /*
               int sample = (int)(tmpSample[i + 3] << 24 | tmpSample[i + 2] << 16 | tmpSample[i + 1] << 8 | tmpSample[i + 0]);
               unsafe { aa = (*(float*)sample); }*/
            }
        }
    }
    /// <summary>
    /// This stream maintains data only until the data is read, then it is purged from the stream.
    /// </summary>
    public class MemoryQueueBufferStream : Stream, IDisposable
    {
        //Maintains the streams data.  The Queue object provides an easy and efficient way to add and remove data
        //Each item in the queue represents each write to the stream.  Every call to write translates to an item in the queue
        private Queue<byte[]> lstBuffers_m;

        //If the item on the top of the queue is read, but the amount read is less then the size of the data in the queue,
        //the remaining data is put in this buffer because the item from the queue is removed.  Subsequent read calls will first read
        //from this buffer until it is depleted, then the queue will be read again.
        private byte[] binLeftOverFromFirstBuffer_m = null;

        private bool bIsOpen_m;

        public MemoryQueueBufferStream()
        {
            this.bIsOpen_m = true;
            this.lstBuffers_m = new Queue<byte[]>();
        }

        /// <summary>
        /// Reads up to count bytes from the stream, and removes the read data from the stream.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            this.ValidateBufferArgs(buffer, offset, count);
            if (!this.bIsOpen_m)
            {
                throw new ObjectDisposedException(null, this.GetType().Name + " is closed");
            }

            int iRemainingBytesToRead = count;

            //If there was data left over return it now before reading from the queue
            if (binLeftOverFromFirstBuffer_m != null)
            {
                int iBytesRead = ReadFromBufferAndTrim(ref binLeftOverFromFirstBuffer_m, buffer, offset, iRemainingBytesToRead);
                offset += iBytesRead;
                iRemainingBytesToRead -= iBytesRead;
            }

            while ((iRemainingBytesToRead > 0) && (lstBuffers_m.Count > 0))
            {
                //Get data from the queue, and remove it from the queue, and put it in a buffer
                byte[] binNextBuffer = this.lstBuffers_m.Dequeue();

                int bytesRead = ReadFromBufferAndTrim(ref binNextBuffer, buffer, offset, iRemainingBytesToRead);
                offset += bytesRead;
                iRemainingBytesToRead -= bytesRead;

                if (binNextBuffer != null)
                {
                    // If the buffer contains more data than requested, we need to hold it for subsequent reads.
                    // ReadFromBufferAndTrim has removed the part of the buffer that was already read. The remaining part will
                    // be read at the next call (and possibly trimmed again)
                    // before going to the next buffer in the queue.
                    this.binLeftOverFromFirstBuffer_m = binNextBuffer;
                }
            }

            int totalBytesRead = count - iRemainingBytesToRead;
            return totalBytesRead;
        }

        private static int ReadFromBufferAndTrim(ref byte[] srcBuffer, byte[] destBuffer, int destOffset, int remainingBytesToRead)
        {
            int bytesToRead = Math.Min(srcBuffer.Length, remainingBytesToRead);
            Buffer.BlockCopy(srcBuffer, 0, destBuffer, destOffset, bytesToRead);

            if (bytesToRead < srcBuffer.Length)
            {
                srcBuffer = TrimBufferStart(srcBuffer, bytesToRead);
            }
            else
            {
                srcBuffer = null;
            }

            return bytesToRead;
        }

        private static byte[] TrimBufferStart(byte[] buffer, int firstByteToKeep)
        {
            int trimmedLength = buffer.Length - firstByteToKeep;
            byte[] trimmedBuffer = new byte[trimmedLength];

            Buffer.BlockCopy(buffer, firstByteToKeep, trimmedBuffer, 0, trimmedLength);
            return trimmedBuffer;
        }

        private void ValidateBufferArgs(byte[] buffer, int offset, int count)
        {
            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset", "offset must be non-negative");
            }
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException("count", "count must be non-negative");
            }
            if ((buffer.Length - offset) < count)
            {
                throw new ArgumentException("requested count exceeds available size");
            }
        }

        /// <summary>
        /// Writes data to the stream
        /// </summary>
        /// <param name="buffer">Data to copy into the stream</param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            this.ValidateBufferArgs(buffer, offset, count);
            if (!bIsOpen_m)
            {
                throw new ObjectDisposedException(null, this.GetType().Name + " is closed");
            }

            byte[] newBuffer = new byte[count];
            Buffer.BlockCopy(buffer, offset, newBuffer, 0, count);

            //Add the data to the queue
            this.lstBuffers_m.Enqueue(newBuffer);
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override long Position
        {
            get
            {
                throw new NotSupportedException(this.GetType().Name + " is not seekable");
            }
            set
            {
                throw new NotSupportedException(this.GetType().Name + " is not seekable");
            }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException(this.GetType().Name + " is not seekable");
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException(this.GetType().Name + " length can not be changed");
        }

        public override bool CanRead
        {
            get { return true; }
        }

        public override long Length
        {
            get
            {
                return lstBuffers_m.Count > 0 ? lstBuffers_m.Peek().Length * lstBuffers_m.Count : 0;

                //- Peek 시 0 이면 Dequeue();
                //- Max 보다 크면 Write  안되게 하기
                /*
                int len = 0;
                for (int i = 0; i < lstBuffers_m.Count; i++) len += lstBuffers_m.Peek;
                return len;
                */
                //return this.lstBuffers_m.Sum(b => b.Length) + this.binLeftOverFromFirstBuffer_m.Length;
            }
        }

        public override void Flush()
        {
            lstBuffers_m.Clear();
        }

        #region IDisposable Members

        void IDisposable.Dispose()
        {
            this.lstBuffers_m.Clear();
            this.lstBuffers_m = null;
            this.binLeftOverFromFirstBuffer_m = null;
            this.bIsOpen_m = false;
            this.Close();
        }

        #endregion
    }
}
