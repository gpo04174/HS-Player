﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class RAW : ISoundFormat, ISettingManager
    {
        WaveFormat _Format;
        internal RAW() : base(SoundType.RAW, MIME, Extension, Description)
        {
            info.defaultfrequency = (int)Format.SampleRate;
            info.numchannels = Format.Channel;
            if (Format.Type == Wave.WaveType.FLOAT) info.format = SOUND_FORMAT.PCMFLOAT;
            else if (Format.Type == Wave.WaveType.BIT) info.format = SOUND_FORMAT.BITSTREAM;
            else
            {
                if (Format.Bit == 8) info.format = SOUND_FORMAT.PCM8;
                else if (Format.Bit == 16) info.format = SOUND_FORMAT.PCM16;
                else if (Format.Bit == 24) info.format = SOUND_FORMAT.PCM24;
                else if (Format.Bit == 32) info.format = SOUND_FORMAT.PCM32;
                else info.format = SOUND_FORMAT.MAX;
            }
        }
        //public RAW(ChannelOrder Order = ChannelOrder.Surround) : this(null, Order) { }
        public RAW(WaveFormat Format, ChannelOrder Order = ChannelOrder.Surround) : base(SoundType.RAW, MIME, Extension, Description)
        {
            //info.
            this.Format = Format;
            this.Order = Order;
        }

        public ChannelOrder Order { get { return (ChannelOrder)info.channelorder; } private set { info.channelorder = (CHANNELORDER)value; } }
        public WaveFormat Format
        {
            get { return _Format; }
            private set
            {
                _Format = (WaveFormat)value.Clone();

                info.defaultfrequency = (int)Format.SampleRate;
                info.numchannels = Format.Channel;
                if (Format.Type == Wave.WaveType.FLOAT) info.format = SOUND_FORMAT.PCMFLOAT;
                else if (Format.Type == Wave.WaveType.BIT) info.format = SOUND_FORMAT.BITSTREAM;
                else
                {
                    if (Format.Bit == 8) info.format = SOUND_FORMAT.PCM8;
                    else if (Format.Bit == 16) info.format = SOUND_FORMAT.PCM16;
                    else if (Format.Bit == 24) info.format = SOUND_FORMAT.PCM24;
                    else if (Format.Bit == 32) info.format = SOUND_FORMAT.PCM32;
                    else info.format = SOUND_FORMAT.MAX;
                }
            }
        }

        public static string[] MIME { get { return new string[] { "audio/raw" }; } }
        public static string[] Extension { get { return new string[] { "raw" }; } }
        public static string[] Description { get { return new string[] { "Raw Audio" }; } }

        public override string ToString() { return Description[0]; }


        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("ChannelOrder", Order.ToString());
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }
        public bool LoadSetting(Settings setting, bool ThrowException = false)
        {
            try { if (setting.Exist("ChannelOrder")) Order = (ChannelOrder)Enum.Parse(typeof(ChannelOrder), setting["ChannelOrder"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.SubSetting.Exist("Format")) Format.LoadSetting(setting.SubSetting["Format"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("Length")) Length = Convert.ToUInt32(setting["Length"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("DecodeSize")) DecodeSize = Convert.ToUInt32(setting["DecodeSize"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (setting.Exist("FileOffset")) FileOffset = Convert.ToUInt32(setting["FileOffset"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }
    }
    /// <summary>
    /// Order to processing the channels. [채널을 처리할 순서 입니다. (Left: 좌, Right: 우, Center: 중앙, LFE: 우퍼, Back Left: 외쪽 뒤, Back Right: 오른쪽 뒤, Surround Left: 서라운드 좌, Surround Right: 서라운드 우)]
    /// </summary>
    public enum ChannelOrder
    {
        /// <summary>
        /// Mono, Mono, Mono, Mono, Mono, Mono, ... (each channel all the way up to 32 channels are treated as if they were mono) [각 채널은 최대 32 채널까지 모노로 처리됩니다.]
        /// </summary>
        Mono = CHANNELORDER.ALLMONO,
        /// <summary>
        /// Left, Right, Left, Right, ... (each pair of channels is treated as stereo all the way up to 32 channels) [각 채널은 최대 32 채널까지 스테레오로 처리됩니다.]
        /// </summary>
        Stereo = CHANNELORDER.ALLSTEREO,
        /// <summary>
        /// Left, Right, Center, LFE, Back Left, Back Right, Surround Left, Surround Right (as per Microsoft .wav WAVEFORMAT structure master order) [마이크로소프트 Wave 구조로 처리합니다.]
        /// </summary>
        MSWave = CHANNELORDER.WAVEFORMAT,
        /// <summary>
        /// Left, Right, Center, LFE, Surround Left, Surround Right, Back Left, Back Right
        /// </summary>
        Surround = CHANNELORDER.DEFAULT,
        /// <summary>
        /// Left, Center, Right, Surround Left, Surround Right, LFE
        /// </summary>
        ProTools = CHANNELORDER.PROTOOLS,
        /// <summary>
        /// Left, Right, Surround Left, Surround Right, Center, LFE (as per Linux ALSA channel order) [리눅스 ALSA 채널로 처리합니다.]
        /// </summary>
        ALSA = CHANNELORDER.ALSA,
    }
}
