﻿using HSAudio.DSP.Inner;
using HSAudio.DSP.OTHER;
using HSAudio.Lib.FMOD;
using HSAudio.PlugIn;
using HSAudio.PlugIn.DSP;
using HS.Setting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using HS;

namespace HSAudio.DSP
{
    public enum DSPRegistKind { Unknown, MainOutput, Channel }
    public enum DSPParameterType { Float = 0, Int, Bool, Data }

#if !SIMPLE
    [Serializable]
    public partial class DSP : IDisposable, ISettingManager
    {
        internal static Dictionary<int, DSP> list = new Dictionary<int, DSP>();

        readonly internal int ID = IDGenerator.Generate();
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        internal FD_DSP DSPInternal;


        private RESULT _result;
        public RESULT Result { get { return _result; } internal set { _result = value; /*HSAudio.ERRCHECK(value);*/ } }


        #region Private Properties
        DSP_TYPE _DSPType = DSP_TYPE.UNKNOWN;

        #endregion

        #region Public Properties
        #region Get
        public DSP ParentDSP { get; internal set; }
        public HSAudio Audio { get; internal set; }

        //public bool IsRegist { get { int i = 0; if(DSPInternal != null) DSPInternal.getNumInputs(ref i); return i > 0; } }
        public bool Lock { get; internal set; }

        public bool IsHidden { get; internal set; }
        public bool IsConnect
        {
            get
            {
                int input = 0, output = 0;
                DSPInternal.getNumInputs(out input);
                DSPInternal.getNumOutputs(out output);
                return input > 0 && output > 0;
            }
        }

        public DSP_TYPE DSPType { get { return _DSPType; } private set { _DSPType = value; } }

        public Peak Meter { get; private set; }
        public Information Information { get; private set; }

        public int Count
        {
            get
            {
                int num = 0;
                DSPInternal.getNumParameters(out num);
                return num;
            }
        }
        #endregion

        #region Get / Set
        public Parameter this[int index]
        {
            get { return GetParameter(index); }
            set { SetParameter(index, value); }
        }

        public object Tag { get; set; }
        public string Name { get; set; }

        public bool LastBypass;
        public bool Bypass
        {
            get { bool _Bypass; Result = DSPInternal.getBypass(out _Bypass); return _Bypass; }
            set { LastBypass = value; Result = DSPInternal.setBypass(value); }
        }

        public float LastGain = 1;
        public float Gain
        {
            get { float Mix = 0; Result = DSPConnection.getMix(out Mix); return Mix; }
            set { LastGain = value; try { Result = DSPConnection.setMix(value); } catch (NullReferenceException) { Result = RESULT.ERR_UNINITIALIZED; } }
        }

        public Mix Mix { get; private set; }
        public DSPSpeaker Speaker { get; private set; }
        #endregion
        #endregion

        //internal DSPDescription description;
        internal FD_DSPConnection _DSPConnection = null;
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        public FD_DSPConnection DSPConnection { get { return _DSPConnection; } }

        #region Initalizer
        //internal DSP() { }
        /// <summary>
        /// Custom DSP
        /// </summary>
        /// <param name="Audio"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Audio, Description desc, string Name, object Tag)
        {
            list.Add(ID, this);
            this.Audio = Audio;
            this.Name = Name;
            this.Tag = Tag;
            //description = desc;

            if (this.Audio == null || this.Audio.system == null) throw new HSException();
            else
            {
                Result = this.Audio.system.createDSP(ref desc._Description, out DSPInternal);
                INIT();
            }
        }
        /// <summary>
        /// Type DSP
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Value, DSP_TYPE DSPType, string Name, object Tag)
        {
            list.Add(ID, this);
            Audio = Value;
            this.Tag = Tag;
            this.Name = Name;
            this.DSPType = DSPType;
            if (HSAudio.IsEmpty(Value)) throw new HSException(new NullReferenceException());
            else
            {
                // || DSPType == DSP_TYPE.WINAMPPLUGIN || DSPType == DSP_TYPE.VSTPLUGIN || DSPType == DSP_TYPE.LADSPAPLUGIN
                //if (DSPType == DSP_TYPE.UNKNOWN) throw new NotSupportedException("This mode is not support to this initalizer");
                Result = Audio.system.createDSPByType(DSPType, out DSPInternal);
                INIT();
            }
        }
        /// <summary>
        /// Type DSP
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Value, uint PluginHandle, string Name, object Tag)
        {
            Audio = Value;
            this.Tag = Tag;
            this.Name = Name;
            this.DSPType = DSP_TYPE.VSTPLUGIN;
            if (!HSAudio.IsEmpty(Value))
            {
                // || DSPType == DSP_TYPE.WINAMPPLUGIN || DSPType == DSP_TYPE.VSTPLUGIN || DSPType == DSP_TYPE.LADSPAPLUGIN
                //if (DSPType == DSP_TYPE.UNKNOWN) throw new NotSupportedException("This mode is not support to this initalizer");
                Result = Audio.system.createDSPByPlugin(PluginHandle, out DSPInternal);
                if (Result != RESULT.OK) throw new HSAudioException(Result);
                INIT();
            }
            else throw new HSAudioException(RESULT.ERR_FILE_NOTFOUND);
        }

        private void INIT()
        {
            DSPInternal.setMeteringEnabled(true, true);
            Mix = new Mix(this);
            Information = new Information(this);
            Meter = new Peak(this);
            Speaker = new DSPSpeaker(this);
        }
        #endregion

        public void Dispose()
        {
            //Disconnect();
            try
            {
                //for (int i = SubDSP.Count - 1; i >= 0; i++) { try { SubDSP[i].Dispose(); SubDSP.RemoveAt(i); } catch { } }
                //IntPtr pt = DSP.getRaw();
                //Marshal.FreeCoTaskMem(pt);
                if (DSPInternal != null) DSPInternal.release();
                DSPInternal = null;
                Audio = null;
                GC.Collect();
            }
            catch { }
        }

        public RESULT Reset() { return DSPInternal.reset(); }

        /// <summary>
        /// DSP를 설정합니다.
        /// </summary>
        /// <param name="index">인덱스 입니다.</param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public bool SetParameter(int index, Parameter Parameter)
        {
            if (Parameter.Value.Type == DSPParameterType.Float) return ThrowOrNot(DSPInternal.setParameterFloat(index, Parameter.Value.Value_Float));
            else if (Parameter.Value.Type == DSPParameterType.Int) return ThrowOrNot(DSPInternal.setParameterInt(index, Parameter.Value.Value_Int));
            else if (Parameter.Value.Type == DSPParameterType.Bool) return ThrowOrNot(DSPInternal.setParameterBool(index, Parameter.Value.Value_Bool));
            else return ThrowOrNot(DSPInternal.setParameterData(index, Parameter.Value.Value_Data, Parameter.Value.Value_Data.Length));
        }
        internal protected bool SetParameter(int index, float Value) { return ThrowOrNot(DSPInternal.setParameterFloat(index, Value)); }
        internal protected bool SetParameter(int index, int Value) { return ThrowOrNot(DSPInternal.setParameterInt(index, Value)); }
        internal protected bool SetParameter(int index, bool Value) { return ThrowOrNot(DSPInternal.setParameterBool(index, Value)); }
        internal protected bool SetParameter(int index, IntPtr Value, uint Length) { return ThrowOrNot(DSPInternal.setParameterData(index, Value, Length)); }
        internal protected bool[] SetParameter(int offset, params Parameter[] Parameter)
        {
            bool[] result = new bool[Parameter.Length];
            for (int i = offset; i < Parameter.Length; i++) result[i] = SetParameter(i, Parameter[i]);
            return result;
        }

        /// <summary>
        /// DSP 파라미터를 가져옵니다.
        /// </summary>
        /// <param name="index">DSP 파라미터 값에 대한 인덱스 입니다.</param>
        /// <returns></returns>
        public Parameter GetParameter(int index)
        {
            DSP_PARAMETER_DESC desc;
            Result = DSPInternal.getParameterInfo(index, out desc);
            switch (desc.type)
            {
                case DSP_PARAMETER_TYPE.FLOAT: float f; DSPInternal.getParameterFloat(index, out f); return new Parameter(f, desc);
                case DSP_PARAMETER_TYPE.INT: int i; DSPInternal.getParameterInt(index, out i); return new Parameter(i, desc);
                case DSP_PARAMETER_TYPE.BOOL: bool b; Result = DSPInternal.getParameterBool(index, out b); return new Parameter(b, desc);
                case DSP_PARAMETER_TYPE.DATA:
                    IntPtr data; uint len;
                    DSPInternal.getParameterData(index, out data, out len);
                    return new Parameter(data, len, desc);
            }
            return null;
        }
        internal protected float GetParameter(int index, float dummy = 0) { float a; DSPInternal.getParameterFloat(index, out a); return a; }
        internal protected int GetParameter(int index, int dummy = 0) { int a; DSPInternal.getParameterInt(index, out a); return a; }
        internal protected bool GetParameter(int index, bool dummy = true) { bool a; DSPInternal.getParameterBool(index, out a); return a; }
        internal protected IntPtr GetParameter(int index, out uint length) { IntPtr data; uint len; DSPInternal.getParameterData(index, out data, out len); length = len; return data; }

        public Settings SaveSetting() { return SaveSetting(true); }
        public Settings SaveSetting(bool SaveParameter)
        {
            Settings s = new Settings();
            s.SetValue("Name", new SettingsData(Bypass.ToString(), false));
            s.SetValue("Gain", new SettingsData(Gain.ToString()));
            s.SetValue("Bypass", new SettingsData(Bypass.ToString()));

            s.SubSetting.SetValue("Mix", Mix.SaveSetting());
            s.SubSetting.SetValue("Speaker", Speaker.SaveSetting());

            if(SaveParameter) for (int i = 0; i < Count; i++) s.SubSetting.SetValue("Parameter"+i, this[i].SaveSetting());

            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Name")) try { Name = Setting["Name"]; } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Bypass")) try { Bypass = bool.Parse(Setting["Bypass"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Gain")) try { Gain = float.Parse(Setting["Gain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Mix")) try { Mix.LoadSetting(Setting.SubSetting["Mix"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Speaker")) try { Speaker.LoadSetting(Setting.SubSetting["Speaker"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            for (int i = 0; i < int.MaxValue; i++)
                if (Setting.SubSetting.Exist("Parameter" + i)) try { this[i].LoadSetting(Setting.SubSetting["Parameter" + i], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
                else break;
            return true;
        }

        public override bool Equals(object obj)
        {
            DSP dsp = obj as DSP;
            if (dsp == null) return false;
            else
            {
                if (dsp.DSPInternal == null) return Count == dsp.Count && dsp.Gain == Gain && dsp.Bypass == Bypass;
                else return dsp.DSPInternal.getRaw() == DSPInternal.getRaw();
            }
        }
        public override int GetHashCode() { return base.GetHashCode(); }

        bool ThrowOrNot(RESULT Result)
        {
            this.Result = Result;
            return HSAudio.ThrowOrNot(Result);
        }
        /*
        internal XmlNode SaveSetting(XmlDocument doc)
        {
            XmlNode root = doc.CreateElement("DSP");

            XmlAttribute attr = doc.CreateAttribute("Bypass"); attr.Value = Bypass.ToString();
            root.Attributes.Append(attr);
            attr = doc.CreateAttribute("Gain"); attr.Value = Gain.ToString();
            root.Attributes.Append(attr);

            XmlNode node = doc.CreateElement("Name"); node.Value = Name;
            root.AppendChild(node);
            root.AppendChild(Mix.SaveSetting(doc));
            root.AppendChild(Speaker.SaveSetting(doc));

            XmlNode node_param = doc.CreateElement("Parameter");
            Parameter[] param = Parameters;
            for(int i = 0; i < param.Length; i++)node_param.AppendChild(param[i].SaveSetting(doc));
            return root.AppendChild(node_param);
        }

        internal bool LoadSetting(XmlNode Setting)
        {
            if (Setting.Name.ToLower() == "dsp")
            {
                for (int i = 0; i < Setting.Attributes.Count; i++)
                {
                    if ("Bypass".Equals(Setting.Attributes[i].Name)) Bypass = Convert.ToBoolean(Setting.Attributes[i].Value);
                    else if ("Gain".Equals(Setting.Attributes[i].Name)) Gain = Convert.ToSingle(Setting.Attributes[i].Value);
                }

                for (int i = 0; i < Setting.ChildNodes.Count; i++)
                {
                    if ("Name".Equals(Setting.ChildNodes[i].Name)) Name = Setting.ChildNodes[i].Value;
                    else if ("Mix".Equals(Setting.ChildNodes[i].Name)) Mix.LoadSetting(Setting.ChildNodes[i]);
                    else if ("Speaker".Equals(Setting.ChildNodes[i].Name)) Speaker.LoadSetting(Setting.ChildNodes[i]);
                    if ("Parameter".Equals(Setting.ChildNodes[i].Name))
                    {
                        for (int j = 0; j < Setting.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            Parameter param = new Parameter(Setting.ChildNodes[i].ChildNodes[j]);
                            SetParameter(param);
                        }
                    }
                }
                return true;
            }
            else return false;
        }
        */

        ~DSP() { Dispose(); list.Remove(ID); }
    }
#else
    [Serializable]
    public sealed partial class DSP : IDisposable, ISettingManager
    {
        internal static Dictionary<int, DSP> list = new Dictionary<int, DSP>();

        internal int ID = DSPIDGenerator.Generate();
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        internal FD_DSP DSPInternal;


        private RESULT _result;
        public RESULT Result { get { return _result; } internal set { _result = value; /*HSAudio.ERRCHECK(value);*/ } }


        #region Private Properties
        DSP_TYPE _DSPType = DSP_TYPE.UNKNOWN;

        #endregion

        #region Public Properties
        #region Get
        public DSP ParentDSP { get; internal set; }
        public HSAudio Audio { get; internal set; }

        //public bool IsRegist { get { int i = 0; if(DSPInternal != null) DSPInternal.getNumInputs(ref i); return i > 0; } }
        public bool Lock { get; internal set; }

        public bool IsHidden { get; internal set; }
        public bool IsConnect
        {
            get
            {
                int input = 0, output = 0;
                DSPInternal.getNumInputs(out input);
                DSPInternal.getNumOutputs(out output);
                return input > 0 && output > 0;
            }
        }

        public DSP_TYPE DSPType { get { return _DSPType; } private set { _DSPType = value; } }

        public Peak Meter { get; private set; }
        public Information Information { get; private set; }


        /*
        /// <summary>
        /// DSP 파라미터들을 가져옵니다.
        /// </summary>
        public Parameter[] Parameters
        {
            get
            {
                int cnt = GetParameterCount();
                Parameter[] param = new Parameter[cnt];
                for (int i = 0; i < cnt; i++) param[i] = GetParameter(i);
                return param;
            }
        }
        */

        public int Count { get { return GetParameterCount(); } }
        #endregion

        #region Get / Set
        public ParameterEx this[int index]
        {
            get { return GetParameter(index); }
            set { Result = SetParameter(index, value); }
        }

        public object Tag { get; set; }
        public string Name { get; set; }

        public bool LastBypass;
        public bool Bypass
        {
            get { bool _Bypass; Result = DSPInternal.getBypass(out _Bypass); return _Bypass; }
            set { LastBypass = value; Result = DSPInternal.setBypass(value); }
        }

        public float LastGain = 1;
        public float Gain
        {
            get { float Mix = 0; Result = DSPConnection.getMix(out Mix); return Mix; }
            set { LastGain = value; try { Result = DSPConnection.setMix(value); } catch (NullReferenceException) { Result = RESULT.ERR_UNINITIALIZED; } }
        }

        public Mix Mix { get; private set; }
        public DSPSpeaker Speaker { get; private set; }
        #endregion
        #endregion

        //internal DSPDescription description;
        internal FD_DSPConnection _DSPConnection;
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        public FD_DSPConnection DSPConnection { get { return DSPConnection; } }

        #region Initalizer
        //internal DSP() { }
        /// <summary>
        /// Custom DSP
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Value, Description desc, string Name, object Tag)
        {
            list.Add(ID, this);
            Audio = Value;
            this.Name = Name;
            this.Tag = Tag;
            //description = desc;

            if (Audio != null || Audio.system != null)
            {
                Result = Audio.system.createDSP(ref desc._DSPDescription, out DSPInternal);
                INIT();
            }
        }
        /// <summary>
        /// Type DSP
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Value, DSP_TYPE DSPType, string Name, object Tag)
        {
            list.Add(ID, this);
            Audio = Value;
            this.Tag = Tag;
            this.Name = Name;
            this.DSPType = DSPType;
            if (HSAudio.IsEmpty(Value)) throw new HSException(new NullReferenceException());
            else
            {
                // || DSPType == DSP_TYPE.WINAMPPLUGIN || DSPType == DSP_TYPE.VSTPLUGIN || DSPType == DSP_TYPE.LADSPAPLUGIN
                //if (DSPType == DSP_TYPE.UNKNOWN) throw new NotSupportedException("This mode is not support to this initalizer");
                Result = Audio.system.createDSPByType(DSPType, out DSPInternal);
                INIT();
            }
        }
        /// <summary>
        /// Type DSP
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Value, uint PluginHandle, string Name, object Tag)
        {
            Audio = Value;
            this.Tag = Tag;
            this.Name = Name;
            this.DSPType = DSP_TYPE.VSTPLUGIN;
            if (!HSAudio.IsEmpty(Value))
            {
                // || DSPType == DSP_TYPE.WINAMPPLUGIN || DSPType == DSP_TYPE.VSTPLUGIN || DSPType == DSP_TYPE.LADSPAPLUGIN
                //if (DSPType == DSP_TYPE.UNKNOWN) throw new NotSupportedException("This mode is not support to this initalizer");
                Result = Audio.system.createDSPByPlugin(PluginHandle, out DSPInternal);
                if (Result != RESULT.OK) throw new HSAudioException(Result);
                INIT();
            }
            else throw new HSAudioException(RESULT.ERR_FILE_NOTFOUND);
        }
        /// <summary>
        /// Type DSP
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Name"></param>
        internal DSP(HSAudio Value, IP PluginHandle, string Name, object Tag)
        {
            Audio = Value;
            this.Tag = Tag;
            this.Name = Name;
            this.DSPType = DSP_TYPE.VSTPLUGIN;
            if (!HSAudio.IsEmpty(Value))
            {
                // || DSPType == DSP_TYPE.WINAMPPLUGIN || DSPType == DSP_TYPE.VSTPLUGIN || DSPType == DSP_TYPE.LADSPAPLUGIN
                //if (DSPType == DSP_TYPE.UNKNOWN) throw new NotSupportedException("This mode is not support to this initalizer");
                Result = Audio.system.createDSPByPlugin(PluginHandle, out DSPInternal);
                if (Result != RESULT.OK) throw new HSAudioException(Result);
                INIT();
            }
            else throw new HSAudioException(RESULT.ERR_FILE_NOTFOUND);
        }


        private void INIT()
        {
            DSPInternal.setMeteringEnabled(true, true);
            Mix = new Mix(this);
            Information = new Information(this);
            Meter = new Peak(this);
            Speaker = new DSPSpeaker(this);
        }
        #endregion

        /*
        public void Disconnect()
        {
            if (Regist == DSPRegistKind.MainOutput) Audio.DSPMainOutput.Remove(this);
            else if (Regist == DSPRegistKind.Channel) Audio.DSPChannel.Remove(this);
            Regist = DSPRegistKind.Unknown;
        }
        */

        public void Dispose()
        {
            //Disconnect();
            try
            {
                //for (int i = SubDSP.Count - 1; i >= 0; i++) { try { SubDSP[i].Dispose(); SubDSP.RemoveAt(i); } catch { } }
                //IntPtr pt = DSP.getRaw();
                //Marshal.FreeCoTaskMem(pt);
                DSPInternal = null;
                Audio = null;
                GC.Collect();
            }
            catch { }
        }

        public RESULT Reset() { return DSPInternal.reset(); }

        /// <summary>
        /// DSP를 설정합니다.
        /// </summary>
        /// <param name="index">인덱스 입니다.</param>
        /// <param name="Value"></param>
        /// <returns></returns>
        internal RESULT SetParameter(DSP_PARAMETER_TYPE Type, int index, object Value)
        {
            switch (Type)
            {
                case DSP_PARAMETER_TYPE.BOOL: return DSPInternal.setParameterBool(index, (bool)Value);
                case DSP_PARAMETER_TYPE.FLOAT: return DSPInternal.setParameterFloat(index, (float)Value);
                case DSP_PARAMETER_TYPE.INT: return DSPInternal.setParameterInt(index, (int)Value);
                default: return DSPInternal.setParameterData(index, (byte[])Value);
            }
        }
        public RESULT SetParameter(ParameterEx Parameter) { return SetParameter((DSP_PARAMETER_TYPE)Parameter.Type, Parameter.Index, Parameter.Value); }
        internal RESULT[] SetParameter(params ParameterEx[] Parameter)
        {
            RESULT[] result = new RESULT[Parameter.Length];
            for (int i = 0; i < Parameter.Length; i++) result[i] = SetParameter(Parameter[i]);
            return result;
        }
        internal RESULT SetParameter(int index, float Value) { return DSPInternal.setParameterFloat(index, Value); }
        internal RESULT SetParameter(int index, int Value) { return DSPInternal.setParameterInt(index, Value); }
        internal RESULT SetParameter(int index, bool Value) { return DSPInternal.setParameterBool(index, Value); }
        internal RESULT SetParameter(int index, byte[] Value) { return DSPInternal.setParameterData(index, Value); }

        public int GetParameterCount()
        {
            int num = 0;
            DSPInternal.getNumParameters(out num);
            return num;
        }
        /// <summary>
        /// DSP 파라미터를 가져옵니다.
        /// </summary>
        /// <param name="index">DSP 파라미터 값에 대한 인덱스 입니다.</param>
        /// <returns></returns>
        public ParameterEx GetParameter(int index)
        {
            DSP_PARAMETER_DESC desc;
            Result = DSPInternal.getParameterInfo(index, out desc);
            ParameterEx dsp = new ParameterEx(index, desc);
            switch (desc.type)
            {
                case DSP_PARAMETER_TYPE.BOOL: bool b; dsp.Value = DSPInternal.getParameterBool(index, out b); break;
                case DSP_PARAMETER_TYPE.FLOAT: float f; dsp.Value = DSPInternal.getParameterFloat(index, out f); break;
                case DSP_PARAMETER_TYPE.INT: int i; dsp.Value = DSPInternal.getParameterInt(index, out i); break;
                case DSP_PARAMETER_TYPE.DATA:
                    IntPtr data; uint len;
                    DSPInternal.getParameterData(index, out data, out len);
                    byte[] br = new byte[len];
                    Marshal.Copy(data, br, 0, (int)len);
                    dsp.Value = br;
                    break;
            }
            return dsp;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Bypass", new SettingsData(Bypass.ToString()));
            s.SetValue("Gain", new SettingsData(Gain.ToString()));
            s.SetValue("Bypass", new SettingsData(Bypass.ToString()));
            s.SetValue("Name", new SettingsData(Bypass.ToString(), false));

            s.SubSetting.SetValue("Mix", Mix.SaveSetting());
            s.SubSetting.SetValue("Speaker", Speaker.SaveSetting());

            ParameterEx[] param = Parameters;
            for (int i = 0; i < param.Length; i++) s.SubSetting.SetValue("Parameter", param[i].SaveSetting());


            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new NotImplementedException();
        }


        /*
        internal XmlNode SaveSetting(XmlDocument doc)
        {
            XmlNode root = doc.CreateElement("DSP");

            XmlAttribute attr = doc.CreateAttribute("Bypass"); attr.Value = Bypass.ToString();
            root.Attributes.Append(attr);
            attr = doc.CreateAttribute("Gain"); attr.Value = Gain.ToString();
            root.Attributes.Append(attr);

            XmlNode node = doc.CreateElement("Name"); node.Value = Name;
            root.AppendChild(node);
            root.AppendChild(Mix.SaveSetting(doc));
            root.AppendChild(Speaker.SaveSetting(doc));

            XmlNode node_param = doc.CreateElement("Parameter");
            Parameter[] param = Parameters;
            for(int i = 0; i < param.Length; i++)node_param.AppendChild(param[i].SaveSetting(doc));
            return root.AppendChild(node_param);
        }

        internal bool LoadSetting(XmlNode Setting)
        {
            if (Setting.Name.ToLower() == "dsp")
            {
                for (int i = 0; i < Setting.Attributes.Count; i++)
                {
                    if ("Bypass".Equals(Setting.Attributes[i].Name)) Bypass = Convert.ToBoolean(Setting.Attributes[i].Value);
                    else if ("Gain".Equals(Setting.Attributes[i].Name)) Gain = Convert.ToSingle(Setting.Attributes[i].Value);
                }

                for (int i = 0; i < Setting.ChildNodes.Count; i++)
                {
                    if ("Name".Equals(Setting.ChildNodes[i].Name)) Name = Setting.ChildNodes[i].Value;
                    else if ("Mix".Equals(Setting.ChildNodes[i].Name)) Mix.LoadSetting(Setting.ChildNodes[i]);
                    else if ("Speaker".Equals(Setting.ChildNodes[i].Name)) Speaker.LoadSetting(Setting.ChildNodes[i]);
                    if ("Parameter".Equals(Setting.ChildNodes[i].Name))
                    {
                        for (int j = 0; j < Setting.ChildNodes[i].ChildNodes.Count; j++)
                        {
                            Parameter param = new Parameter(Setting.ChildNodes[i].ChildNodes[j]);
                            SetParameter(param);
                        }
                    }
                }
                return true;
            }
            else return false;
        }
        */

        ~DSP() { Dispose(); }
    }
#endif
}
