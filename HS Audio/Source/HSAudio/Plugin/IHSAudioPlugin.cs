﻿#if FUNC_PLUGIN
using HS.PlugIn;
using System;

namespace HSAudio.PlugIn
{
    public interface IHSAudioPlugin : IDisposable
    {
        PluginKind Kind { get; }
        IPluginInformation Information { get; }
    }
}
#endif