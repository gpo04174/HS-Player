﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

//https://stackoverflow.com/questions/36545896/universal-windows-uwp-range-slider
// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HSPlayer.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HSSlider : ContentView
    {
        public event ValueChangedEventHandler ValueChanged;
        public event ValueChangedEventHandler Scrolled;
        public event MouseButtonEventHandler SliderPointerPressed;
        public event MouseButtonEventHandler SliderPointerReleased;

        SolidColorBrush brush_disabled = new SolidColorBrush(Color.FromArgb(255, 100, 100, 100));
        SolidColorBrush brush_disabled_dark = new SolidColorBrush(Color.FromArgb(255, 80, 80, 80));
        public HSSlider()
        {
            this.InitializeComponent();
            this.Loaded += HSSlider_Loaded;
            value_thumb.ApplyTemplate();
            grid_slider.AddHandler(MouseDownEvent, new MouseButtonEventHandler((object o, MouseButtonEventArgs e) =>
            {
                SliderPointerPressed?.Invoke(this, e);
                Point pt = e.GetPosition(this);
                double odd = ButtonShape != null && ButtonShape.Visibility == Visibility.Visible ? value_thumb.ActualWidth / 2 : 0;
                Value = GetValToPos(pt.X - odd);
                Scrolled?.Invoke(this, Value);
            }), true);
            grid_slider.AddHandler(MouseUpEvent, new MouseButtonEventHandler((object o, MouseButtonEventArgs e) => { SliderPointerReleased?.Invoke(this, e); }), true);
            value_thumb.AddHandler(MouseDownEvent, new MouseButtonEventHandler((object o, MouseButtonEventArgs e) => { SliderPointerPressed?.Invoke(this, e); }), true);
            value_thumb.AddHandler(MouseUpEvent, new MouseButtonEventHandler((object o, MouseButtonEventArgs e) => { SliderPointerReleased?.Invoke(this, e); }), true);

            this.SizeChanged += (object sender, SizeChangedEventArgs e) =>
            {
                value_thumb.Width = value_thumb.Height = e.NewSize.Height;
                UpdateMargin(e.NewSize.Width, e.NewSize.Height);
                _SliderHeight = grid_slider.Height = Math.Max(Math.Min(_SliderHeight, e.NewSize.Height), 0);
            };

            Background = _Background;
            Foreground = _Foreground;
        }

        private void HSSlider_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void HSSlider_Loaded(object sender, RoutedEventArgs e)
        {
            double pos = GetPosToVal(Value);
            UpdatePos(pos);
        }

        string Lock_ValueChange = "Lock_ValueChange";
        public double Value
        {
            get { return _Value; }
            set
            {
                lock (Lock_ValueChange)
                {
                    UpdateValue(value, true);
                    double pos = GetPosToVal(Value);
                    UpdatePos(pos);
                }
            }
        }
        public double Minimum
        {
            get { return _Minimum; }
            set { if (Minimum != value) { _Minimum = value; UpdateValue(Value, false); } }
        }

        public double Maximum
        {
            get { return _Maximum; }
            set { if (Maximum != value) { _Maximum = value; UpdateValue(Value, false); } }
        }

        public double SliderHeight
        {
            get { return _SliderHeight; }
            set
            {
                _SliderHeight = value;
                if (Height > 0)
                {
                    grid_slider.Height = Math.Max(Math.Min(value, Height), 1);
                    UpdateMargin(Width, Height);
                }
            }
        }
        public double SliderMargin
        {
            get { return _SliderMargin; }
            set
            {
                value = Math.Max(value, 0);
                _SliderMargin = value;
                UpdateMargin(Width, Height);
            }
        }

        public new bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set
            {
                SetValue(IsEnabledProperty, value);
                if (value)
                {
                    ActiveRectangle.SetValue(Shape.FillProperty, Foreground);
                    rec_slider.SetValue(Shape.FillProperty, Background);
                }
                else
                {
                    //btn_slider.SetValue(Shape.FillProperty, brush_disabled);
                    rec_slider.SetValue(Shape.FillProperty, brush_disabled);
                    ActiveRectangle.SetValue(Shape.FillProperty, brush_disabled);
                }
                UpdateButton(ButtonShape);
            }
        }

        public bool SliderAutoMargin
        {
            get { return _SliderAutoMargin; }
            set
            {
                _SliderAutoMargin = value;
                this.Width = this.Width;
            }
        }

        //(Color)rec_slider.Fill.GetValue(SolidColorBrush.ColorProperty))
        public new Brush Background { get { return _Background; } set { _Background = value; rec_slider.Fill = value; } }
        public new Brush Foreground { get { return _Foreground; } set { _Foreground = value; ActiveRectangle.Fill = value; } }

        public Shape ButtonShape { get { return _ButtonShape; } set { _ButtonShape = value; UpdateButton(value);} }

        Shape _ButtonShape = new Ellipse() { Opacity = 0.95, RenderTransformOrigin = new Point(0.5, 0.5), StrokeThickness = 1, Stroke = new SolidColorBrush(Color.FromArgb(255, 80, 80, 80)), Fill = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)) };
        Brush _Background = new SolidColorBrush(Color.FromArgb(255, 210, 210, 210));
        Brush _Foreground = new SolidColorBrush(Color.FromArgb(255, 105, 160, 204));
        bool _SliderAutoMargin = true;
        double _Value = 0;
        double _SliderMargin = 5.0;
        double _SliderHeight = 10.0;
        double _Maximum = 100;
        double _Minimum = 0;
        /*
        public static new readonly DependencyProperty BackgroundProperty = DependencyProperty.Register("Background", typeof(Brush), typeof(HSSlider), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 210, 210, 210))));
        public static new readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground", typeof(Brush), typeof(HSSlider), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 105, 160, 204))));
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(double), typeof(HSSlider), new PropertyMetadata(0.0));
        public static readonly DependencyProperty MinimumProperty = DependencyProperty.Register("Minimum", typeof(double), typeof(HSSlider), new PropertyMetadata(0.0));
        public static readonly DependencyProperty MaximumProperty = DependencyProperty.Register("Maximum", typeof(double), typeof(HSSlider), new PropertyMetadata(100.0));
        public static readonly DependencyProperty SliderAutoMarginProperty = DependencyProperty.Register("SliderAutoMargin", typeof(double), typeof(HSSlider), new PropertyMetadata(new bool()));//true
        public static readonly DependencyProperty SliderHeightProperty = DependencyProperty.Register("SliderHeight", typeof(double), typeof(HSSlider), new PropertyMetadata(new double()));//10.0
        public static readonly DependencyProperty SliderMarginProperty = DependencyProperty.Register("SliderMargin", typeof(double), typeof(HSSlider), new PropertyMetadata(5.0));
        */

        public static Shape GetDefaultButtonShape()
        {
            Ellipse ee = new Ellipse();
            ee.Fill = new SolidColorBrush(Color.FromRgb(255, 255, 255));
            ee.Opacity = 0.95;
            ee.RenderTransformOrigin = new Point(0.5, 0.5);
            ee.Stroke = new SolidColorBrush(Color.FromArgb(255, 80, 80, 80));
            ee.StrokeThickness = 1;
            return ee;
        }

        #region Update
        void UpdateButton(Shape shape)
        {
            try
            {
                StringBuilder sb = new StringBuilder("<ControlTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\">");
                string kind = shape.GetType().Name;
                sb.Append("<").Append(kind);

                try
                {
                    Color c = IsEnabled ? ((SolidColorBrush)shape.Fill).Color : brush_disabled_dark.Color;
                    sb.Append(" Fill=\"#").Append(c.A.ToString("X2")).Append(c.R.ToString("X2")).Append(c.G.ToString("X2")).Append(c.B.ToString("X2")).Append("\" ");
                }
                catch { }
                try
                {
                    Color s = IsEnabled ? ((SolidColorBrush)shape.Stroke).Color : brush_disabled_dark.Color;
                    sb.Append("Stroke=\"#").Append(s.A.ToString("X2")).Append(s.R.ToString("X2")).Append(s.G.ToString("X2")).Append(s.B.ToString("X2")).Append("\" ");
                }
                catch { }
                try { sb.Append(" StrokeThickness=\"").Append(shape.StrokeThickness).Append("\" "); } catch { }
                try { sb.Append("Opacity=\"").Append(shape.Opacity).Append("\" "); } catch { }
                try { sb.Append("Name=\"").Append(shape.Name == "" ? "Ellipse_" + (new Random().Next(0, int.MaxValue)).ToString() : shape.Name).Append("\" "); } catch { }
                try { sb.Append("Width=\"").Append(shape.Width).Append("\" "); } catch { }
                try { sb.Append("Height=\"").Append(shape.Height).Append("\" "); } catch { }
                try { sb.Append("Visibility=\"").Append(shape.Visibility).Append("\" "); } catch { }

                try { sb.Append("StrokeDashCap=\"").Append(shape.StrokeDashCap).Append("\" "); } catch { }
                try { sb.Append("StrokeDashOffset=\"").Append(shape.StrokeDashOffset).Append("\" "); } catch { }
                try { sb.Append("StrokeEndLineCap=\"").Append(shape.StrokeEndLineCap).Append("\" "); } catch { }
                try { sb.Append("StrokeLineJoin=\"").Append(shape.StrokeLineJoin).Append("\" "); } catch { }
                try { sb.Append("StrokeMiterLimit=\"").Append(shape.StrokeMiterLimit).Append("\" "); } catch { }
                try { sb.Append("StrokeStartLineCap=\"").Append(shape.StrokeStartLineCap).Append("\" "); } catch { }
                try { sb.Append("RenderTransformOrigin=\"").Append(shape.RenderTransformOrigin.X).Append(" ").Append(shape.RenderTransformOrigin.Y).Append("\" "); } catch { }
                try { sb.Append("Stretch=\"").Append(shape.Stretch).Append("\" "); } catch { }
                try { sb.Append("AllowDrop=\"").Append(shape.AllowDrop).Append("\" "); } catch { }
                //try { sb.Append("MaxWidth=\"").Append(shape.MaxWidth).Append("\" "); } catch { }
                //try { sb.Append("MaxHeight=\"").Append(shape.MaxHeight).Append("\" "); } catch { }

                sb.Append("/></ControlTemplate>");
                string a = sb.ToString();
                ControlTemplate ct = (ControlTemplate)XamlReader.Load(System.Xml.XmlReader.Create(new StringReader(a)));

                Style asd = new Style(typeof(Thumb));//(Style)grid_main.Resources.ToArray()[0].Value;
                asd.Setters.Add(new Setter(TemplateProperty, ct));
                value_thumb.Style = asd;
            }
            catch (Exception ex) { }
        }
        public void UpdateMargin(double newWidth, double newHeight)
        {
            rec_slider.Width = Math.Max(newWidth - (SliderAutoMargin ? value_thumb.ActualWidth : SliderMargin * 2), 0);
            double W_M = SliderAutoMargin ? (value_thumb.ActualWidth / 2) : SliderMargin;
            double H_M = (newHeight - SliderHeight) / 2;
            grid_slider.Margin = new Thickness(W_M, H_M, W_M, 0);
        }
        void UpdatePos(double pos)
        {
            if (Name.IndexOf("vol") >= 0) ;
            pos = Math.Min(Math.Max(pos, 0), ActualWidth - value_thumb.ActualWidth);
            Canvas.SetLeft(value_thumb, pos);
            //if (!SliderAutoMargin) Math.Max(pos - (SliderMargin * 2), 0);
            ActiveRectangle.Width = Math.Max((Math.Abs(Value) / (Math.Abs(Minimum) + Math.Abs(Maximum))) * rec_slider.ActualWidth, 0);
            //if(ButtonShape != null && ButtonShape.Visibility == Visibility.Visible) ActiveRectangle.Width = pos;
            //else ActiveRectangle.Width = Math.Max((Math.Abs(Value) / (Math.Abs(Minimum) + Math.Abs(Maximum))) * rec_slider.ActualWidth, 0);
        }
        void UpdateValue(double value, bool RaiseEvent)
        {
            double _Value = Math.Min(Math.Max(value, Minimum), Maximum);
            bool Changed = Value != _Value;
            //SetValue(ValueProperty, _Value);
            this._Value = value;
            if (RaiseEvent && Changed) ValueChanged?.Invoke(this, value);
        }
        #endregion

        #region 알고리즘
        private void value_thumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var currentPos = Canvas.GetLeft(value_thumb);
            var nextPos = (currentPos + e.HorizontalChange);
            UpdatePos(nextPos);
            UpdateValue(GetValToPos(nextPos), false);
            Scrolled?.Invoke(this, Value);
        }

        double GetValToPos(double pos)
        {
            double w = 0;
            if (ButtonShape != null && ButtonShape.Visibility == Visibility.Visible) w = Math.Max(ActualWidth - value_thumb.Width, 0);
            else w = rec_slider.ActualWidth;
            pos = pos / w;
            //pos = Math.Min(Math.Max(pos, 0), 1);
            double dif = Maximum - Minimum;
            double mid = dif * pos;
            return mid + Minimum;
        }
        double GetPosToVal(double Val)
        {
            double percent = (Math.Abs(Minimum) + Math.Abs(Val)) / (Math.Abs(Minimum) + Math.Abs(Maximum));
            //percent = Math.Min(Math.Max(percent, 0), 1);
            double w = Math.Max(ActualWidth - value_thumb.Width, 0);
            return w * percent;
        }
        #endregion

        #region 사용되지 않음
        private static IEnumerable<T> FindChildrenVisual<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindChildrenVisual<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
        public List<UIElement> FindChildren(DependencyObject parent)
        {
            var _List = new List<UIElement>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var _Child = VisualTreeHelper.GetChild(parent, i);
                if (_Child is UIElement)
                    _List.Add(_Child as UIElement);
                _List.AddRange(FindChildren(_Child));
            }
            return _List;
        }
        #endregion
    }
}