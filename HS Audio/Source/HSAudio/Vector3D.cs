﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;

namespace HSAudio
{
    public struct Vector3D : ISettingManager
    {
        public Vector3D(float X, float Y, float Z) { x = X; y = Y; z = Z; }
        internal Vector3D(VECTOR vector) { x = vector.x; y = vector.y; z = vector.z; }

        private float x;
        private float y;
        private float z;

        public float X { get { return x; }set { x = value; } }
        public float Y { get { return y; } set { y = value; } }
        public float Z { get { return z; } set { z = value; } }

        public static implicit operator VECTOR(Vector3D vec) { return new VECTOR() { x = vec.X, y = vec.Y, z = vec.Z }; }
        public static implicit operator Vector3D(VECTOR vec) { return new Vector3D() { x = vec.x, y = vec.y, z = vec.z }; }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("X", X); s.SetValue("Y", Y); s.SetValue("Z", Z);
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("X")) try { X = float.Parse(Setting["X"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Y")) try { Y = float.Parse(Setting["Y"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Z")) try { X = float.Parse(Setting["Z"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }
    }
}
