﻿using Android.App;
using Android.Content;
using Android.Util;
using Android.Widget;
using CrossCallback;
using Plugin.FilePicker;
using System;
using System.Threading;
using System.Threading.Tasks;
using static CrossCallback.FunctionManager;

namespace AndroidTest.Droid
{
    public class FunctionManagerAndroid
    {
        public static FunctionManagerAndroid Instance { get; private set; }
        public static FunctionManagerAndroid Init(Context context)
        {
            if (Instance == null) Instance = new FunctionManagerAndroid(context);
            else Instance.context = context;
            return Instance;
        }

        Context context;
        private FunctionManagerAndroid(Context context)
        {
            this.context = context;

            //manager = new FunctionManager();
            FunctionManager.ShowMessageBox = ShowMessageBox_Callback;
            FunctionManager.ShowAlert = ShowAlert_Callback;
            FunctionManager.ShowFileDialog = ShowFileDialog_Callback;
        }

        private async Task<DialogResult> ShowMessageBox_Callback(string Message, string Title, MessageBoxButton Button)
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(context);
            dlgAlert.SetMessage(Message);
            dlgAlert.SetTitle(Title);

            DialogInterface dialog = new DialogInterface(Button == MessageBoxButton.OK);
            if (Button == MessageBoxButton.OK)
            {
                dlgAlert.SetCancelable(false);
                dlgAlert.SetPositiveButton("OK", dialog);
            }
            else
            {
                dlgAlert.SetCancelable(Button == MessageBoxButton.YesNoCancel);
                dlgAlert.SetPositiveButton("Yes", dialog);
                dlgAlert.SetNegativeButton("No", dialog);
                dlgAlert.SetOnCancelListener(dialog);
            }
            dlgAlert.Create().Show();
            return await dialog.GetResult();
        }

        private void ShowAlert_Callback(string Message, bool ShowLong)
        {
            Toast.MakeText(context, Message, ShowLong ? ToastLength.Long : ToastLength.Short).Show();
        }

        private async Task<string> ShowFileDialog_Callback(string[] Type)
        {
            try
            {
                var pickedFile = await CrossFilePicker.Current.PickFile(Type);
                //pickedFile.GetStream()
                return pickedFile.FilePath;
            }
            catch (Exception ex)
            {
                Log.Debug("FunctionManagerAndroid", ex.Message);
                return null;
            }
        }

        private class DialogInterface : Java.Lang.Object, IDialogInterfaceOnClickListener, IDialogInterfaceOnCancelListener
        {
            bool OnlyOK;
            public DialogInterface(bool OnlyOK) { this.OnlyOK = OnlyOK; }

            bool IsConfirm;
            DialogResult Result = DialogResult.Cancel;
            public async Task<DialogResult> GetResult() {return await Task.Run(() => { while (!IsConfirm) { Thread.Sleep(10); } IsConfirm = false; return Result; });  }

            public void OnClick(IDialogInterface dialog, int which)
            {
                if (which == -1) Result = OnlyOK ? DialogResult.OK : DialogResult.Yes;
                else Result = DialogResult.No;
                IsConfirm = true;
            }

            public void OnCancel(IDialogInterface dialog)
            {
                Result = DialogResult.Cancel;
                IsConfirm = true;
            }
        }
    }
}