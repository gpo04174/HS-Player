﻿#if NETSTANDARD1x
using System;
using System.Reflection;

namespace HSAudio
{
    class Serializable : Attribute{}
    
    interface ICloneable
    {
        object Clone();
    }

    public interface ICustomMarshaler
    {
        void CleanUpManagedData(object ManagedObj);
        void CleanUpNativeData(IntPtr pNativeData);
        int GetNativeDataSize();
        IntPtr MarshalManagedToNative(object ManagedObj);
        object MarshalNativeToManaged(IntPtr pNativeData);
    }

    public static class Encoding
    {
        //https://ko.wikipedia.org/wiki/%EC%BD%94%EB%93%9C_%ED%8E%98%EC%9D%B4%EC%A7%80
        public static System.Text.Encoding Default
        {
            get
            {
                string cul = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                switch(cul.ToLower())
                {
                    case "kor": return System.Text.Encoding.GetEncoding("euc-kr"); //949
                    case "jpn": return System.Text.Encoding.GetEncoding("euc-jp"); //932
                    default: return System.Text.Encoding.UTF8;
                }
            }
        }

        public static System.Text.Encoding UTF8 { get { return System.Text.Encoding.UTF8; } }
    }

#if NETSTANDARD1x
    public static class TypeInfoExtension
    {
        public static Type GetInterface(this TypeInfo info, string Name){return info.GetInterface(Name); }
    }
#endif

}
#endif