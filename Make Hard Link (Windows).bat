@echo off
@rem Get batchfile name.
@rem More: https://ss64.com/nt/syntax-args.html
@rem echo "%~nx0"

rem Get Administrator Privilege
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

if '%errorlevel%' NEQ '0' (

    goto UACPrompt

) else ( goto gotAdmin )

:UACPrompt

    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"

    echo UAC.ShellExecute "%~s0", "", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"

    exit /B
	
:gotAdmin
    mklink /d "Z:\HS Player" "%~dp0"