﻿namespace HSAudio.Core.Audio
{
    public delegate void AudioStatusChangedEventHandler(Audio sender, AudioState status);

    public delegate void SoundChangeEventHandler(object sender, Sound.Sound sound);
    public delegate void AudioChangeEventHandler(object sender, CollectionChange Type, int index, Audio audio);
}
