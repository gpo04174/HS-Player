﻿using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;
using HSAudio.Lib.FMOD;

namespace HSAudio.DSP.Type
{
    public class Equalizer3Band : IDSP
    {
        public enum Slope { Crossover_12dB, Crossover_24dB, Crossover_48dB }

        private Parameter[] param = new Parameter[6];
        public Equalizer3Band(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.THREE_EQ, "Equalizer-3Band", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Low frequency gain in dB. -80.0 to 10.0. Default = 0
        /// </summary>
        public float LowGain
        {
            get { return DSP.GetParameter((int)DSP_THREE_EQ.LOWGAIN, 0f); }
            set { DSP.SetParameter((int)DSP_THREE_EQ.LOWGAIN, value); }
        }
        /// <summary>
        /// Mid frequency gain in dB. -80.0 to 10.0. Default = 0
        /// </summary>
        public float MidGain
        {
            get { return DSP.GetParameter((int)DSP_THREE_EQ.MIDGAIN, 0f); }
            set { DSP.SetParameter((int)DSP_THREE_EQ.MIDGAIN, value); }
        }
        /// <summary>
        /// High frequency gain in dB. -80.0 to 10.0. Default = 0
        /// </summary>
        public float HighGain
        {
            get { return DSP.GetParameter((int)DSP_THREE_EQ.HIGHGAIN, 0f); }
            set { DSP.SetParameter((int)DSP_THREE_EQ.HIGHGAIN, value); }
        }
        /// <summary>
        /// Low-to-mid crossover frequency in Hz. 10.0 to 22000.0. Default = 400.0
        /// </summary>
        public float LowCrossover
        {
            get { return DSP.GetParameter((int)DSP_THREE_EQ.LOWCROSSOVER, 0f); }
            set { DSP.SetParameter((int)DSP_THREE_EQ.LOWCROSSOVER, value); }
        }
        /// <summary>
        /// Mid-to-high crossover frequency in Hz. 10.0 to 22000.0. Default = 4000.0
        /// </summary>
        public float HighCrossover
        {
            get { return DSP.GetParameter((int)DSP_THREE_EQ.HIGHCROSSOVER, 0f); }
            set { DSP.SetParameter((int)DSP_THREE_EQ.HIGHCROSSOVER, value); }
        }
        /// <summary>
        /// Crossover Slope
        /// </summary>
        public Slope CrossoverSlope
        {
            get { return (Slope)DSP.GetParameter((int)DSP_THREE_EQ.CROSSOVERSLOPE, 0); }
            set { DSP.SetParameter((int)DSP_THREE_EQ.CROSSOVERSLOPE, (int)value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("LowGain")) try { LowGain = float.Parse(Setting["LowGain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("MidGain")) try { MidGain = float.Parse(Setting["MidGain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("HighGain")) try { HighGain = float.Parse(Setting["HighGain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("LowCrossover")) try { LowCrossover = float.Parse(Setting["LowCrossover"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("HighCrossover")) try { HighCrossover = float.Parse(Setting["HighCrossover"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("CrossoverSlope")) try { CrossoverSlope = (Slope)Enum.Parse(typeof(Slope), Setting["CrossoverSlope"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("LowGain", LowGain);
            s.SetValue("MidGain", MidGain);
            s.SetValue("HighGain", HighGain);
            s.SetValue("LowCrossover", LowCrossover);
            s.SetValue("HighCrossover", HighCrossover);
            s.SetValue("CrossoverSlope", CrossoverSlope.ToString());
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
