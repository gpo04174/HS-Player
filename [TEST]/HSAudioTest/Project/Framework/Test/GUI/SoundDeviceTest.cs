﻿using HS.Setting;
using HS.Setting.IO;
using HSAudio.Output;
using HSAudio.Output.Devices;
using HSAudio.Resource.Language;
using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace HSAudioTest.Test.GUI
{
    public partial class SoundDeviceTest : Form
    {
        HSAudio.HSAudio Audio = new HSAudio.HSAudio(new AutoOutput(), new OutputFormat(44100, HSAudio.Speakers.SpeakerMode.Default), OutputResampler.Cubic);
        public SoundDeviceTest()
        {
            InitializeComponent();
            Audio.UpdateClock = true;
            deviceControl1.Audio = Audio;

            //cbDeviceKind.SelectedIndex = 0;
        }

        private void loadMusic1_Loaded(object sender, EventArgs e)
        {
            playcontrol1.Audio = Audio;
            try { playcontrol1.Load(loadMusic1.MusicPath); }
            catch (Exception ex) { MessageBox.Show(ex.Message, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void SoundDeviceTest_Load(object sender, EventArgs e)
        {

        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolStripComboBox1.SelectedIndex == 1)
            {
                string[] lng = Properties.Resources.ko_kr.Split('\n');
                HSAudio.Resource.StringResource.setStringResource(new Custom(lng));
            }
            else HSAudio.Resource.StringResource.setStringResource(new Default());

            playcontrol1.RefreshReverb();
        }

        private void saveSettingsXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Settings s = Audio.SaveSetting();
                XmlDocument doc = SettingXML.Save(s);
                doc.Save(saveFileDialog1.FileName);
            }
        }
    }
}
