﻿using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;

namespace HSAudio.Core.Sound.Kind
{
    public abstract class SoundFile : Sound
    {
        protected bool _CanReverse;
        protected WaveFormat _Format;
        protected ISoundFormat _SoundType;
        protected SoundTag _Tag;
        protected CREATESOUNDEXINFO info = new CREATESOUNDEXINFO();
        internal SoundFile(HSAudio Audio, string MusicPath, bool LoadMemory = false, bool Compress = false) : this(Audio, MusicPath, null, LoadMemory, Compress){}
        internal SoundFile(HSAudio Audio, string MusicPath, ISoundFormat Type, bool LoadMemory = false, bool Compress = false) : base(Audio)
        {
            this.Path = MusicPath;
            this.LoadMemory = LoadMemory;
            this.Compress = Compress;
            _Format = Format;
            _SoundType = Type;
            _CanReverse = LoadMemory && !Compress;

            if (Type is RAW) InitFormat(ref info, (Type as RAW).Format);
            if (Type != null) InitType(ref info, Type.Type);
            Load();
        }


        public override void Unload()
        {
            _Tag = null;
            base.Unload();
        }

        #region Private Properties
        #endregion

        public string Path { get; protected set; }
        public bool LoadMemory { get; protected set; }
        public bool Compress { get; protected set; }

        public override WaveFormat Format { get { return _Format; } }
        public override ISoundFormat Type { get { return _SoundType; } }
        public override SoundTag Tag { get { return _Tag; } }

        public override bool CanPosition {get { return false; } }
        public override bool CanPitch {get { return true; } }
        public override bool CanReverse { get {return _CanReverse;} }
    }
}
