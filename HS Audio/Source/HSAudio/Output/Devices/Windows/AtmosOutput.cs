﻿#if WIN_10_1703 || XBOX
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Dolby Atmos output [Windows 10 build 1703 Higher or XBOX] (돌비 애트모스 출력 [윈도우 10 빌드 1703 이상 또는 XBOX])
    /// </summary>
    public class AtmosOutput : DeviceOutput
    {
        public static AtmosOutput[] GetOutputs(HSAudio Audio)
        {
#if !NETSTANDARD
            if (System.Environment.OSVersion.Version.Major < 6) return new AtmosOutput[0];
#else
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return new AtmosOutput[0];
#endif
            Audio.system_sub.setOutput(OUTPUTTYPE.ATMOS);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<AtmosOutput> device = new List<AtmosOutput>();
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                device.Add(new AtmosOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Dolby Atmos" : "Dolby Atmos " + i) : sb.ToString()});
            }
            return device.ToArray();
        }
        public static AtmosOutput[] GetOutputs()
        {
#if !NETSTANDARD
            if (System.Environment.OSVersion.Version.Major < 6) return new AtmosOutput[0];
#else
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return new AtmosOutput[0];
#endif
            Lib.FMOD.FD_System system_sub;
            Lib.FMOD.Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.ATMOS);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<AtmosOutput> device = new List<AtmosOutput>();
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        device.Add(new AtmosOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Dolby Atmos" : "Dolby Atmos " + i) : sb.ToString() });
                    }
                    return device.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }

        internal AtmosOutput(int index, OutputFormat Format, Guid guid)
        {
            Type = OutputType.DolbyAtmos;
            ID = index;
            this.Format = Format;
            //this.Guid = Guid;
        }
        private AtmosOutput(AtmosOutput before)
        {
            Type = OutputType.DolbyAtmos;
            ID = before.ID;
            Name = before.Name;
            Parent = before.Parent;
            Format = before.Format;
        }

        public override Output Parent { get; internal set; }

        public override object Clone() { return new AtmosOutput(this); }
        
        public override bool Equals(DeviceOutput other){ try { return other is AtmosOutput? ((AtmosOutput)other).Name.Equals(Name) : false; } catch { return false; } }
        public override bool Equals(object obj) { try { return obj is AtmosOutput ? ((AtmosOutput)obj).Name.Equals(Name) : false; } catch { return false; } }
        //public override int GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return Name; }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose()
        {
            try { Device.Dispose(); } catch (Exception ex) { new HSAudioLog(ex.Message, "AtmosOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }
        }
    }
}
#endif
