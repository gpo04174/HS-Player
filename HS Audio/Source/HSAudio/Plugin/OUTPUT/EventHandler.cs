﻿using System;

namespace HSAudio.PlugIn.Output
{
    public delegate bool Init(object sender, InitInfo info);
    public delegate void Closed(object sender);
    public delegate bool PositionChanged(object sender, ref uint PCM);
    public delegate bool Lock(object sender, LockInfo info);
    public delegate bool UnLock(object sender, UnLockInfo info);
    public delegate bool GetHandle(object sender, ref IntPtr handle);

    public delegate bool PortOpen(object sender, PortOpenInfo info);
    public delegate bool PortClose(object sender, int PortId);

    public delegate bool Object3DGetInfo(object sender, ref int maxhardwareobjects);
    public delegate bool Object3DAlloc(object sender, ref IntPtr object3d);
    public delegate bool Object3DFree(object sender, IntPtr object3d);
    public delegate bool Object3DUpdate(object sender, Object3DUpdateInfo info);
}
