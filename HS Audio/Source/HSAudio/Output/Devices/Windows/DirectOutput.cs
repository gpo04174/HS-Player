﻿#if WIN_XP || WIN_VISTA || WIN_10_1703
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HSAudio.Wave.Device;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// WIndows DirectSound output. (윈도우 DirectSound 출력)
    /// </summary>
    public class DirectOutput : DeviceOutput
    {
        public static DirectOutput[] GetOutputs(HSAudio Audio)
        {
            Audio.system_sub.setOutput(OUTPUTTYPE.DSOUND);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<DirectOutput> output = new List<DirectOutput>();
#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
            OperatingSystem os = Environment.OSVersion;
            WaveDevice[] device;
            if (os.Version.Major < 6) device = WaveDeviceWINMM.GetDevices(false);
            else device = WaveDeviceWASAPI.GetDevices(false);
#elif WIN_VISTA || WIN_10_1703
            WaveDeviceWASAPI[] device = WaveDeviceWASAPI.GetDevices(false);
#endif
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                output.Add(new DirectOutput(i, format, guid, sb.ToString()));
                for (int j = 0; j < device.Length; j++)
                {
#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
                    if (os.Version.Major < 6) { if (output[i].Name.IndexOf(device[i].DeviceName) == 0) { output[i].Device = device[i]; break; } }
                    else if (guid.Equals(((WaveDeviceWASAPI)device[j]).Guid)) { output[i].Device = device[j]; break; }
#elif WIN_VISTA || WIN_10_1703
                    if (guid.Equals(device[j].Guid)) { output[i].Device = device[j]; break; }
#else
                    if (output[i].Name.IndexOf(device[i].DeviceName) == 0){ output[i].Device = device[i]; break; }
#endif
                }
            }
            return output.ToArray();
        }
        public static DirectOutput[] GetOutputs()
        {
            FD_System system_sub;
            Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.DSOUND);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<DirectOutput> output = new List<DirectOutput>();
#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
                    OperatingSystem os = Environment.OSVersion;
                    WaveDevice[] device;
                    if (os.Version.Major < 6) device = WaveDeviceWINMM.GetDevices(false);
                    else device = WaveDeviceWASAPI.GetDevices(false);
#elif WIN_VISTA || WIN_10_1703
                    WaveDeviceWASAPI[] device = WaveDeviceWASAPI.GetDevices(false);
#endif
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        output.Add(new DirectOutput(i, format, guid, sb.ToString()));
                        for (int j = 0; j < device.Length; j++)
                        {
#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
                            if (os.Version.Major < 6) { if (output[i].Name.IndexOf(device[i].DeviceName) == 0) { output[i].Device = device[i]; break; } }
                            else if (guid.Equals(((WaveDeviceWASAPI)device[j]).Guid)) { output[i].Device = device[j]; break; }
#elif WIN_VISTA || WIN_10_1703
                            if (guid.Equals(device[j].Guid)) { output[i].Device = device[j]; break; }
#else
                            if (output[i].Name.IndexOf(device[i].DeviceName) == 0){ output[i].Device = device[i]; break; }
#endif
                        }
                    }
                    return output.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }

        OutputFormat format;
#if !NETSTANDARD
        OperatingSystem os;
#endif
        internal DirectOutput(int index, OutputFormat Format, Guid guid, string Name)
        {
            Type = OutputType.DirectSound;
            ID = index;
            this.Format = Format;
            Guid = guid;
            base.Name = Name;
#if !NETSTANDARD
            os = Environment.OSVersion;
#endif
        }
        private DirectOutput(DirectOutput before)
        {
            Type = OutputType.DirectSound;
            ID = before.ID;
            Name = before.Name;
            Parent = before.Parent;
            Device = before.Device;
            Format = before.Format;
            Guid = before.Guid;
#if !NETSTANDARD
            os = Environment.OSVersion;
#endif
        }

        public override Output Parent { get; internal set; }
        public Guid Guid { get; private set; }

        public override object Clone() { return new DirectOutput(this); }

#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
        public override bool Equals(DeviceOutput other)
        {
            try
            {
                if (os.Version.Major < 6) return other is DirectOutput ? ((DirectOutput)other).Name.Equals(Name) : false;
                else return other is DirectOutput ? Guid.Equals(((DirectOutput)other).Guid) : false;
            }
            catch { return false; }
        }
        public override bool Equals(object obj)
        {
            try
            {
                if (os.Version.Major < 6) return obj is DirectOutput ? ((DirectOutput)obj).Name.Equals(Name) : false;
                else return obj is DirectOutput ? Guid.Equals(((DirectOutput)obj).Guid) : false;
            }
            catch { return false; }
        }
#elif WIN_XP
        public override bool Equals(object obj) { try { return obj is DirectOutput ? ((DirectOutput)obj).Name.Equals(Name) : false; } catch { return false; } }
        public override bool Equals(DeviceOutput other){ try { return other is DirectOutput ? ((DirectOutput)other).Name.Equals(Name) : false; } catch { return false; } }
#else
        public override bool Equals(object obj) { try { return obj is DirectOutput ? Guid.Equals(((DirectOutput)obj).Guid) : false; } catch (Exception ex) { return false; } }
        public override bool Equals(DeviceOutput other) { try { return other is DirectOutput ? Guid.Equals(((DirectOutput)other).Guid) : false; } catch (Exception ex) { return false; } }
#endif
        //public override int GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return Name; }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
#if WIN_XP
            s.SetValue("Guid", Guid.ToString());
#endif
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose()
        {
            try { Device.Dispose(); } catch (Exception ex) { new HSAudioLog(ex.Message, "DirectOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }
        }
    }
}
#endif