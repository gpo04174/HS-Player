﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public abstract class ISoundFormat
    {
        internal CREATESOUNDEXINFO info = new CREATESOUNDEXINFO();
        public ISoundFormat(SoundType Type, string[] MIME, string[] Description, string[] Extension)
        {
            this.Type = Type;
            this.DecodeSize = DecodeSize;
            this.MIMEs = MIME;
            this.Extensions = Extension;
            this.Descriptions = Description;
        }

        public SoundType Type { get; internal set; }

        //public WaveFormat Format { get; }
        /// <summary>
        /// Decode buffer size. [디코딩 버퍼 크기]
        /// </summary>
        public uint DecodeSize { get { return info.decodebuffersize; } set { info.decodebuffersize = value; } }
        public uint FileOffset { get { return info.fileoffset; } set { info.fileoffset = value; } }
        /// <summary>
        /// Number of bytes to load starting at 'fileoffset',
        /// </summary>
        public uint Length { get { return info.length; } set { info.length = value; } }

        public string[] MIMEs { get; protected set; }
        public string[] Descriptions { get; protected set; }
        public string[] Extensions { get; protected set; }

        public override abstract string ToString();

        public virtual Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("SoundType", Type.ToString());
            s.SetValue("Length", Length);
            s.SetValue("DecodeSize", DecodeSize);
            s.SetValue("FileOffset", FileOffset);
            return s;
        }
        public static ISoundFormat FromSetting(Settings setting, bool ThrowException = false)
        {
            try
            {
                if (setting.Exist("SoundType"))
                {
                    ISoundFormat snd = null;
                    SoundType type = (SoundType)Enum.Parse(typeof(SoundType), setting["SoundType"]);
                    switch (type)
                    {
                        case SoundType.AIFF: snd = new AIFF(); break;
                        case SoundType.ASF: snd = new ASF(); break;
                        case SoundType.FLAC: snd = new FLAC(); break;
                        case SoundType.OGG: snd = new OGG(); break;
                        case SoundType.MPEG: snd = new MPEG(); break;
                        case SoundType.Wave: snd = new WAVE(); break;
                        case SoundType.XM: snd = new XM(); break;
                        case SoundType.MOD: snd = new MOD(); break;
                        case SoundType.IT: snd = new IT(); break;
                        case SoundType.MIDI: MIDI midi = new MIDI(); midi.LoadSetting(setting, ThrowException); return midi;
                        case SoundType.RAW: RAW raw = new RAW(); raw.LoadSetting(setting, ThrowException); return raw;
                        //case SoundType.Codec: CODEC cod = new CODEC(); cod.LoadSetting(setting, ThrowException); return cod;
#if XBOX
                        case SoundType.XMA: snd = new XMA(); break;
#endif
                        default: return null;
                    }

                    try { if (setting.Exist("Length")) snd.Length = Convert.ToUInt32(setting["Length"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                    try { if (setting.Exist("DecodeSize")) snd.DecodeSize = Convert.ToUInt32(setting["DecodeSize"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                    try { if (setting.Exist("FileOffset")) snd.FileOffset = Convert.ToUInt32(setting["FileOffset"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                    return snd;
                }
            }
            catch (Exception ex) { if (ThrowException) throw ex; }
            return null;
        }
        public static string GetTaglibMime(SoundType type)
        {
            switch (type)
            {
                case SoundType.AIFF: return "taglib/aiff";
                case SoundType.ASF: return "taglib/asf";
                default: return null;
            }
        }
    }
}
