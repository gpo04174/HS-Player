﻿namespace HSAudio.Lib.FFmpegSharp
{
    public static class AVCodecLib
    {
#if WIN_XP || WIN_VISTA || WIN_10_1703 || WIN_METRO || XBOX 
        public const string lib = "avcodec-51.dll";
#elif LINUX || ANDROID
        public const string lib = "libavcodec51.so";
#endif
    }
    public static class AVFormatLib
    {
#if WIN_XP || WIN_VISTA || WIN_10_1703 || WIN_METRO || XBOX 
        public const string lib = "avcodec-51.dll";
#elif LINUX || ANDROID
        public const string lib = "libavcodec51.so";
#endif
    }
}
