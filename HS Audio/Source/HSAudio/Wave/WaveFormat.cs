﻿using HS;
using HS.Setting;
using HSAudio.Lib.Windows.NAudio.Wave;
using HSAudio.Resource;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.Wave
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 2)]
    public class WaveFormat : ICloneable, ISettingManager
    {
        internal static readonly int[] Samples = { 8000, 11025, 16000, 22050, 32000, 44100, 48000, 88200, 96000, 176400, 192000, 352800, 384000 };
        internal static readonly int[] Channels = { 16, 24, 32 };

        internal short channels;
        internal float sampleRate;
        //internal Bits bits;
        internal int bit;
        internal WaveType type;
        //internal bool ispcm = true;
        //public WaveFormat() { sampleRate = 44100; channels = 2; bits = Bits.PCM16; }
        //public WaveFormat(float Samplate, short Channel, Bits Bit) { sampleRate = Samplate; channels = Channel; bits = Bit; }
        //public WaveFormat(float Samplate, short Channel, Bits Bits, int Bit) { sampleRate = Samplate; channels = Channel; bits = Bits; bit = Bit; }
        public WaveFormat() { sampleRate = 44100; channels = 2; bit = 16; type = WaveType.PCM; }
        public WaveFormat(float Samplate = 44100, short Channel = 2, int Bit = 16, WaveType Type = WaveType.PCM) { sampleRate = Samplate; channels = Channel; bit = Bit; type = Type; }
        public WaveFormat(WaveFormat format) { bit = format.bit; bit = format.bit; channels = format.channels; sampleRate = format.sampleRate;  type = format.type; }
        /// <summary>
        /// Returns the number of channels (1=mono,2=stereo etc)
        /// </summary>
        public short Channel
        {
            get { return channels; }
            set { channels = value; }
        }

        /// <summary>
        /// Returns the sample rate (samples per second)
        /// </summary>
        public float SampleRate
        {
            get { return sampleRate; }
            set { sampleRate = value; }
        }

        /*
        /// <summary>
        /// Returns the sample rate (samples per second)
        /// </summary>
        public Endien Endien
        {
            get { return sampleRate; }
            set { sampleRate = value; }
        }
        */
        /*
        /// <summary>
        /// Bit (8, 16, 24, 32)
        /// </summary>
        public Bits Bits
        {
            get { return bits; }
            set { bits = value; bit = GetBit(); }
        }
        */

        public WaveType Type { get { return type; } }

        public int Bit { get { return bit;  } }

        public short BlockAlign { get { return (short)(channels * (Bit / 8)); } }
        public int AverageBytesPerSecond { get { return (int)SampleRate * BlockAlign; } }

        /// <summary>
        /// Gets the size of a wave buffer equivalent to the latency in milliseconds.
        /// </summary>
        /// <param name="milliseconds">The milliseconds.</param>
        /// <returns></returns>
        public int ConvertLatencyToByteSize(int milliseconds)
        {
            int bytes = (int)((AverageBytesPerSecond / 1000.0) * milliseconds);
            if ((bytes % BlockAlign) != 0)
            {
                // Return the upper BlockAligned
                bytes = bytes + BlockAlign - (bytes % BlockAlign);
            }
            return bytes;
        }
        public int ConvertByteSizeToLatency(int Length)
        {
            if (Length == 0) return 0;
            int bytes = (int)(Length / (AverageBytesPerSecond / 1000f));
            return bytes;
        }

#if NETSTANDARD
        public string ToString()
#else
        public override string ToString()
#endif
        {
            return string.Format("[{0}] {1:#,#} Hz, {2} ch, {3} bit", Type, SampleRate, Channel, Bit);
        }

        public object Clone()
        {
            return new WaveFormat(this);
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("SampleRate", SampleRate);
            s.SetValue("Channel", Channel);
            s.SetValue("Bit", Bit);
            s.SetValue("WaveType", Type.ToString());
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            try { if (Setting.Exist("SampleRate")) SampleRate = Convert.ToSingle(Setting["SampleRate"]); } catch(Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Channel")) Channel = Convert.ToInt16(Setting["Channel"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Bit")) SampleRate = Convert.ToInt32(Setting["Bit"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("WaveType")) type = (WaveType)Enum.Parse(typeof(WaveType), Setting["WaveType"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }
        public static WaveFormat Load(Settings Setting, bool ThrowException = false)
        {
            WaveFormat format = new WaveFormat();
            try { if (Setting.Exist("SampleRate")) format.SampleRate = Convert.ToSingle(Setting["SampleRate"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Channel")) format.Channel = Convert.ToInt16(Setting["Channel"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Bit")) format.SampleRate = Convert.ToInt32(Setting["Bit"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("WaveType")) format.type = (WaveType)Enum.Parse(typeof(WaveType), Setting["WaveType"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return format;
        }

        /*
        int GetBit()
        {
            switch (Bits)
            {
                //case Bits.DSD: return 1;
                case Bits.PCM8: return 8;
                case Bits.PCM16: return 16;
                case Bits.PCM24: return 24;
                case Bits.PCM32: return 32;
                case Bits.PCM64: return 64;
                case Bits.PCMFLOAT: return 32;
                case Bits.PCMFLOAT64: return 64;
                default: return 0;
            }
        }
        */

        #region Static
        /// <summary>
        /// Helper function to retrieve a WaveFormat structure from a pointer
        /// </summary>
        /// <param name="pointer">WaveFormat structure</param>
        /// <returns></returns>
        public static WaveFormat MarshalFromPtr(IntPtr pointer)
        {
#if NETSTANDARD || NETCORE || NETFX
            WaveFormat waveFormat = Marshal.PtrToStructure<WaveFormat>(pointer);
#else
            WaveFormat waveFormat = (WaveFormat)Marshal.PtrToStructure(pointer, typeof(WaveFormat));
#endif
            return waveFormat;
        }

        /// <summary>
        /// Helper function to marshal WaveFormat to an IntPtr
        /// </summary>
        /// <param name="format">WaveFormat</param>
        /// <returns>IntPtr to WaveFormat structure (needs to be freed by callee)</returns>
        public static IntPtr MarshalToPtr(WaveFormat format)
        {
            int formatSize = Marshal.SizeOf(format);
            IntPtr formatPointer = Marshal.AllocHGlobal(formatSize);
            Marshal.StructureToPtr(format, formatPointer, false);
            return formatPointer;
        }
        #endregion

        public static implicit operator WaveFormat(Lib.Windows.NAudio.Wave.WaveFormat format)
        {
            /*
            if (format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.Pcm && format.Channels == 8) {b = Bits.PCM8; bit = 8; }
            //if (format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.Pcm && format.Channels == 16) {}
            else if(format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.Pcm && format.Channels == 24) { b = Bits.PCM24; bit = 24; }
            else if (format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.Pcm && format.Channels == 32) { b = Bits.PCM32; bit = 32; }
            else if (format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.Pcm && format.Channels == 64) { b = Bits.PCM32; bit = 64; }
            else if (format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.IeeeFloat && format.Channels == 32) { b = Bits.PCMFLOAT; bit = 32; }
            else if (format.Encoding == Lib.Windows.NAudio.Wave.WaveFormatEncoding.IeeeFloat && format.Channels == 64) { b = Bits.PCMFLOAT64; bit = 64; }
            */

            WaveType type = WaveType.PCM;
            if (format.Encoding == WaveFormatEncoding.Pcm || format.Encoding == WaveFormatEncoding.IeeeFloat)
                type = format.Encoding == WaveFormatEncoding.Pcm ? WaveType.PCM : WaveType.FLOAT;
            else throw new HSException(StringResource.Resource["STR_ERR_WAVE_UNSUPPORT_FORMAT"]);

            return new WaveFormat(format.SampleRate, format.Channels, format.BitsPerSample, type);
        }
        public static implicit operator Lib.Windows.NAudio.Wave.WaveFormat(WaveFormat format)
        {
            WaveFormatEncoding enc = WaveFormatEncoding.Pcm;
            //if(format.Bits == Bits.PCMFLOAT || format.Bits == Bits.PCMFLOAT64)enc = Lib.Windows.NAudio.Wave.WaveFormatEncoding.IeeeFloat;
            if (format.Type == WaveType.FLOAT || format.Type == WaveType.PCM)
                enc = format.Type == WaveType.FLOAT ? WaveFormatEncoding.IeeeFloat : WaveFormatEncoding.Pcm;
            else throw new HSException(StringResource.Resource["STR_ERR_WAVE_UNSUPPORT_FORMAT"]);

            return new Lib.Windows.NAudio.Wave.WaveFormat((int)format.SampleRate, format.Bit, format.Channel) { Encoding = enc };
        }
    }
}
