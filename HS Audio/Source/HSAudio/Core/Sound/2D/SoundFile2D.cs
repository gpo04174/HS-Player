﻿using HS;
using HS.Setting;
using HSAudio.Core.Sound.Kind;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using HSAudio.Wave;
using System;

namespace HSAudio.Core.Sound._2D
{
    public class SoundFile2D : SoundFile, Sound2D
    {
        public SoundFile2D(HSAudio Audio, string MusicPath, bool LoadMemory = false, bool Compress = false) : this(Audio, MusicPath, null, LoadMemory, Compress){}
        public SoundFile2D(HSAudio Audio, string MusicPath, ISoundFormat Type, bool LoadMemory = false, bool Compress = false) : base(Audio, MusicPath, Type, LoadMemory, Compress) { }
        
        public override bool Load(bool ThrowException = true)
        {
            if (IsValid) return true;
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(Path)) throw new HSException(string.Format(StringResource.Resource["STR_ENGINE_ERR_NOT_EMPTY"], StringResource.Resource["STR_PATH"]));
                    else
                    {
                        MODE mode_comp = Compress ? MODE.CREATECOMPRESSEDSAMPLE | MODE.ACCURATETIME : MODE.ACCURATETIME;
                        RESULT result = RESULT.OK;
                        if (Type is RAW)
                        {
                            result = LoadMemory ?
                                Audio.system.createSound(Path, MODE._2D | MODE.OPENRAW | mode_comp | MODE.LOOP_OFF, out sound) :
                                Audio.system.createStream(Path, MODE._2D | MODE.OPENRAW | MODE.ACCURATETIME | MODE.LOOP_OFF, out sound);
                        }
                        else
                        {
                            result = LoadMemory ?
                                Audio.system.createSound(Path, MODE._2D | mode_comp | MODE.LOOP_OFF, out sound) :
                                Audio.system.createStream(Path, MODE._2D | MODE.ACCURATETIME | MODE.LOOP_OFF, out sound);
                        }

                        if (result != RESULT.OK) { sound.release(); throw new HSAudioException(result); }
                        else { _Format = getFormat(); _SoundType = Type == null ? getType() : Type; }

                        _Tag = SoundTag.GetTag(sound);
                        base.OnLoaded();
                        return true;
                    }
                }
                catch (Exception ex) { if (ThrowException) throw ex; }
                return false;
            }
        }

        #region Private Properties
        float LastVolume;
        #endregion

        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Type", "File");
            s.SetValue("Dimension", "2D");
            s.SetValue("Path", new SettingsData(Path, false));
            s.SetValue("LoadMemory", LoadMemory);
            s.SetValue("Compress", Compress);
            return s;
        }
        public static SoundFile2D LoadSetting(HSAudio Audio, Settings Setting, bool ThrowException = false)
        {
            string Path = null;
            ISoundFormat type = null;
            WaveFormat format = null;
            bool Compress = true, LoadMemory = false;

            try { if (Setting.Exist("Path")) Path = Setting["Path"]; } catch (Exception ex) { if (ThrowException) throw ex; return null; }
            try { if (Setting.SubSetting.Exist("Type")) type = ISoundFormat.FromSetting(Setting.SubSetting["Type"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.SubSetting.Exist("Format")) format = WaveFormat.Load(Setting.SubSetting["Format"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Compress")) Compress = Convert.ToBoolean(Setting["Compress"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("LoadMemory")) LoadMemory = Convert.ToBoolean(Setting["LoadMemory"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return new SoundFile2D(Audio, Path, type, LoadMemory);
        }
    }
}
