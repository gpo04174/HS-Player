﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.DSP.Type
{
    public class Equalizer5Band
    {
        public enum Band { Band1 = 0, Band2 = 1, Band3 = 2, Band4 = 3, Band5 = 4 }
        public enum Filter
        {
            /// <summary>
            /// Disabled, no processing.<para/>
            /// 비활성화, 처리하지 않음
            /// </summary>
            Disable = DSP_MULTIBAND_EQ_FILTER_TYPE.DISABLED,
            /// <summary>
            /// All-pass filter, allows all frequencies to pass, but changes the phase response at a given point (with specified sharpness). <para/>
            /// All-pass 필터, 모든 주파수를 통과할 수 있지만 특정 지점에서 위상 응답을 변경합니다. (지정된 sharpness 포함)
            /// </summary>
            AllPass = DSP_MULTIBAND_EQ_FILTER_TYPE.ALLPASS,
            BandPass = DSP_MULTIBAND_EQ_FILTER_TYPE.BANDPASS,
            Notch = DSP_MULTIBAND_EQ_FILTER_TYPE.NOTCH,
            Peaking = DSP_MULTIBAND_EQ_FILTER_TYPE.PEAKING,
            LowShelf = DSP_MULTIBAND_EQ_FILTER_TYPE.LOWSHELF,
            HighShelf = DSP_MULTIBAND_EQ_FILTER_TYPE.HIGHSHELF,
            LowPass_12dB = DSP_MULTIBAND_EQ_FILTER_TYPE.LOWPASS_12DB,
            LowPass_24dB = DSP_MULTIBAND_EQ_FILTER_TYPE.LOWPASS_24DB,
            LowPass_48dB = DSP_MULTIBAND_EQ_FILTER_TYPE.LOWPASS_48DB,
            HighPass_12dB = DSP_MULTIBAND_EQ_FILTER_TYPE.HIGHPASS_12DB,
            HighPass_24dB = DSP_MULTIBAND_EQ_FILTER_TYPE.HIGHPASS_24DB,
            HighPass_48dB = DSP_MULTIBAND_EQ_FILTER_TYPE.HIGHPASS_48DB,
        }

        private Value[] param = new Value[5];
        public Equalizer5Band(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.MULTIBAND_EQ, "Equalizer-5Band", null);
            param[0] = new Equalizer5Band.Value(this, Band.Band1);
            param[1] = new Equalizer5Band.Value(this, Band.Band2);
            param[2] = new Equalizer5Band.Value(this, Band.Band3);
            param[3] = new Equalizer5Band.Value(this, Band.Band4);
            param[4] = new Equalizer5Band.Value(this, Band.Band5);
        }
        public DSP DSP { get; private set; }

        public Value this[Band Band] { get { return param[(int)Band]; } }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }
        
        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.SubSetting.Exist("Band1")) try { this[Band.Band1].LoadSetting(Setting.SubSetting["Band1"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Band2")) try { this[Band.Band2].LoadSetting(Setting.SubSetting["Band2"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Band3")) try { this[Band.Band3].LoadSetting(Setting.SubSetting["Band3"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Band4")) try { this[Band.Band4].LoadSetting(Setting.SubSetting["Band4"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Band5")) try { this[Band.Band5].LoadSetting(Setting.SubSetting["Band5"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SubSetting.SetValue("Band1", this[Band.Band1].SaveSetting());
            s.SubSetting.SetValue("Band2", this[Band.Band2].SaveSetting());
            s.SubSetting.SetValue("Band3", this[Band.Band3].SaveSetting());
            s.SubSetting.SetValue("Band4", this[Band.Band4].SaveSetting());
            s.SubSetting.SetValue("Band5", this[Band.Band5].SaveSetting());
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }


        public class Value : ISettingManager
        {
            private Band band;
            private Parameter param;
            private Equalizer5Band eq;
            internal Value(Equalizer5Band eq, Band band)
            {
                this.eq = eq;
                this.band = band;
            }

            public float Frequency
            {
                get
                {
                    if (band == Band.Band1) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.A_FREQUENCY, 0f);
                    else if (band == Band.Band2) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.B_FREQUENCY, 0f);
                    else if (band == Band.Band3) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.C_FREQUENCY, 0f);
                    else if (band == Band.Band4) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.D_FREQUENCY, 0f);
                    else if (band == Band.Band5) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.E_FREQUENCY, 0f);
                    return 0;
                }
                set
                {
                    if (band == Band.Band1) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.A_FREQUENCY, value);
                    else if (band == Band.Band2) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.B_FREQUENCY, value);
                    else if (band == Band.Band3) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.C_FREQUENCY, value);
                    else if (band == Band.Band4) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.D_FREQUENCY, value);
                    else if (band == Band.Band5) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.E_FREQUENCY, value);
                }
            }
            public float Gain
            {
                get
                {
                    if (band == Band.Band1) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.A_GAIN, 0f);
                    else if (band == Band.Band2) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.B_GAIN, 0f);
                    else if (band == Band.Band3) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.C_GAIN, 0f);
                    else if (band == Band.Band4) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.D_GAIN, 0f);
                    else if (band == Band.Band5) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.E_GAIN, 0f);
                    return 0;
                }
                set
                {
                    if (band == Band.Band1) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.A_GAIN, value);
                    else if (band == Band.Band2) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.B_GAIN, value);
                    else if (band == Band.Band3) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.C_GAIN, value);
                    else if (band == Band.Band4) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.D_GAIN, value);
                    else if (band == Band.Band5) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.E_GAIN, value);
                }
            }
            public float Q
            {
                get
                {
                    if (band == Band.Band1) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.A_Q, 0f);
                    else if (band == Band.Band2) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.B_Q, 0f);
                    else if (band == Band.Band3) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.C_Q, 0f);
                    else if (band == Band.Band4) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.D_Q, 0f);
                    else if (band == Band.Band5) return eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.E_Q, 0f);
                    return 0;
                }
                set
                {
                    if (band == Band.Band1) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.A_Q, value);
                    else if (band == Band.Band2) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.B_Q, value);
                    else if (band == Band.Band3) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.C_Q, value);
                    else if (band == Band.Band4) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.D_Q, value);
                    else if (band == Band.Band5) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.E_Q, value);
                }
            }
            public Filter Filter
            {
                get
                {
                    if (band == Band.Band1) return (Filter)eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.A_FILTER, 0);
                    else if (band == Band.Band2) return (Filter)eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.B_FILTER, 0);
                    else if (band == Band.Band3) return (Filter)eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.C_FILTER, 0);
                    else if (band == Band.Band4) return (Filter)eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.D_FILTER, 0);
                    else if (band == Band.Band5) return (Filter)eq.DSP.GetParameter((int)DSP_MULTIBAND_EQ.E_FILTER, 0);
                    return 0;
                }
                set
                {
                    if (band == Band.Band1) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.A_FILTER, (int)value);
                    else if (band == Band.Band2) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.A_FILTER, (int)value);
                    else if (band == Band.Band3) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.C_FILTER, (int)value);
                    else if (band == Band.Band4) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.D_FILTER, (int)value);
                    else if (band == Band.Band5) eq.DSP.SetParameter((int)DSP_MULTIBAND_EQ.E_FILTER, (int)value);
                }
            }

            public bool LoadSetting(Settings Setting, bool ThrowException = false)
            {
                if (Setting.Exist("Frequency")) try { Frequency = float.Parse(Setting["Frequency"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Q")) try { Q = float.Parse(Setting["Q"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Gain")) try { Gain = float.Parse(Setting["Gain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Filter")) try { Filter = (Filter)Enum.Parse(typeof(Filter), Setting["Filter"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                return true;
            }

            public Settings SaveSetting()
            {
                Settings s = new Settings();
                s.SetValue("Frequency", Frequency);
                s.SetValue("Q", Q);
                s.SetValue("Gain", Gain);
                s.SetValue("Filter", Filter.ToString());
                return s;
            }
        }
    }
}
