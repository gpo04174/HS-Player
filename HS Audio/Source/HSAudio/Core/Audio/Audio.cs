﻿using HS.Attributes;
using HS.Setting;
using HSAudio.DSP;
using HSAudio.DSP.OTHER;
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Utility;
using System;
using System.Text;

namespace HSAudio.Core.Audio
{
    public abstract partial class Audio
    {
        internal readonly int ID = IDGenerator.Generate();
        //Matrix 구현하기
        protected internal AudioState LastStatus = AudioState.Unknown;
        protected internal FD_Channel channel;
        protected internal FD_ChannelGroup group;
        protected internal FD_ChannelGroup group_parent;
        protected internal FD_DSPConnection conn;
        protected HSAudio audio;
        //MixMatrix _Matrix;
        protected AudioRepeat _Repeat;
        //Thread LoopThread;

        public bool Is3D;

        protected CHANNEL_CALLBACK _CHANNEL_CALLBACK;
        internal Audio(HSAudio audio, bool Is3D, FD_ChannelGroup parent)
        {
            this.Is3D = Is3D;
            this.audio = audio;
            this.group_parent = parent;
            Result = audio.system.createChannelGroup(string.Format("Audio Session ({0})", ID), out group);
            Result = parent.addGroup(group, false, out conn);

            //try { }
            if (Result == RESULT.OK)
            {
                _CHANNEL_CALLBACK = new CHANNEL_CALLBACK(CHANNEL_CALLBACK_);
                DSP = new DSPCollection(group);
                _Repeat = new AudioRepeat(this);
            }
            else { group = null; throw new HSAudioException(Result); }
            //finally { if (group != null) group.release(); }
            LogCheck(Result, "Init()");

        }
        internal Audio(Sound.Sound Sound, bool Is3D, FD_ChannelGroup parent) : this(Sound.Audio, Is3D, parent) {if(group != null)setSound(Sound, Is3D);}
        protected void OnSetStatus(AudioState Status) { StatusChanged.Invoke(this, Status); }

        #region Method
        public void setStatus(AudioState Status)
        {
            if (Status == AudioState.Playing) Play();
            else if (Status == AudioState.Pausing) Pause();
            else if (Status == AudioState.Stopped) Stop();
        }
        public virtual void Play()
        {
            bool play = getIsPlaying();
            if (play) Result = channel.setPaused(false);
            else Result = Init(true);

            LogCheck(Result, "Play()");
            Status = AudioState.Playing;
        }
        public virtual void Pause()
        {
            RESULT Result = channel.setPaused(true);
            LogCheck(Result, "Pause()");

            Status = AudioState.Pausing;
        }
        public virtual void Stop()
        {
            RESULT Result = RESULT.OK;
            if (getIsPlaying())
            {
                Result = channel.setPaused(true);
                Result = channel.setPosition(0, TIMEUNIT.MS);
            }
            else
            {
                Result = channel.stop();
                Result = Init(false);
            }

            this.Result = Result;
            LogCheck(Result, "Stop()");
            Status = AudioState.Stopped;
        }

        public bool setPosition(uint position, TimeUnit unit) { return channel.setPosition(position, (TIMEUNIT)unit) == RESULT.OK; }
        public uint getPosition(TimeUnit unit) { if (channel == null) return 0; else { uint output; Result = channel.getPosition(out output, (TIMEUNIT)unit); return output; } }

        public bool getIsPlaying() { bool Isplaying = false; if(channel != null)channel.isPlaying(out Isplaying); return Isplaying; }

        public virtual bool setSound(Sound.Sound Sound, bool ThrowException = true)
        {
            if (!IsDisposed)
            {
                if (SoundChanging != null)try { SoundChanging.Invoke(this, Sound); } catch { } 
                try
                {
                    //if (Sound == null) { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_NULL"]); }
                    //else
                    if (Sound != null)
                    {
                        FD_Channel channel = null;
                        RESULT Result = audio.system.playSound(Sound.sound, group, true, out channel);
                        if (Result == RESULT.OK)
                        {
                            DSP.Unload();
                            if (this.channel != null) this.channel.stop();
                            if (this.Sound != null)
                            {
                                this.Sound.Unloading -= OnUnloading;
                                this.Sound.Loaded -= OnLoaded;
                            }
                            this.Sound = Sound;
                            Sound.Unloading += OnUnloading;
                            Sound.Loaded += OnLoaded;
                            this.channel = channel;
                            channel.setMode(Is3D ? MODE._3D : MODE._2D);
                            channel.setCallback(_CHANNEL_CALLBACK);
                            Loop = _Loop;
                            Status = AudioState.Stopped;
                            Matrix = ArrayConveterN<float>.Merge(GetMatrixDefault(), Matrix);
                            DSP.Reload(group);
                            HSAudioLogManager.Logging(new HSAudioLog("Sound Attached!!", string.Format("Audio ({0})", ID), LogKind.Engine, HS.Log.LogLevel.Info));
                            return true;
                        }
                        else
                        {
                            Status = AudioState.Unknown;
                            if (ThrowException) throw new HSAudioException(Result);
                            else LogCheck(Result, "setSound()");
                        }
                    }
                    else
                    {
                        if (channel != null) channel.stop();
                        if (Sound != null)
                        {
                            Sound.Unloading += OnUnloading;
                            Sound.Loaded += OnLoaded;
                        }
                        this.Sound = null;
                        Status = AudioState.Dettached;
                        HSAudioLogManager.Logging(new HSAudioLog("Sound Dettached!!", string.Format("Audio ({0})", ID), LogKind.Engine, HS.Log.LogLevel.Warning));
                    }
                }
                finally { if (SoundChanged != null)try { SoundChanged.Invoke(this, Sound); } catch { } }
            }
            return false;
        }

        protected bool IsValidChannel() { return channel != null && channel.isValid(); }
        protected bool IsValidGroup() { return group != null && group.isValid(); }

        protected uint _CurrentPosition;
        protected AudioState _State;
        internal virtual void Unload()
        {
            if (channel != null && channel.isValid())
            {
                _CurrentPosition = CurrentPosition;
                _State = Status;
                Reverse = _Reverse;
            }
            if (group != null && group.isValid())
            {
                _Volume = Volume;
                _Pitch = Pitch;
                _Mute = Mute;
            }
            Status = AudioState.Unknown;
            OnSetStatus(Status);
            DSP.Unload();
            if (this.channel != null) { /*Result = this.channel.stop();*/ channel = null; }
            if (this.group != null) { /*Result = this.group.release();*/ this.group = null; }
            Sound.Unload();
            //Sound.Unload();
        }
        internal virtual void Reload(FD_ChannelGroup group_parent)
        {
            Volume = _Volume;
            Pitch = _Pitch;
            Mute = _Mute;
            ReverbLevel = _ReverbLevel;
            Matrix = ArrayConveterN<float>.Merge(GetMatrixDefault(), Matrix);
            DSP.Reload(group);

            Result = audio.system.playSound(Sound.sound, group, true, out channel);
            if (Result == RESULT.OK)
            {
                Result = channel.setMode(Is3D ? MODE._3D : MODE._2D);
                Result = channel.setCallback(_CHANNEL_CALLBACK);
                Result = channel.setMode((MODE)_Loop);
                Result = channel.setPosition(_CurrentPosition, TIMEUNIT.MS);
                Repeat.Reload(channel);

                AudioState state = _State;
                if (state == AudioState.Playing) Play();
                else if (state == AudioState.Pausing) Pause();
                else if (Status != AudioState.Unknown || Status != AudioState.Unknown) Status = state;
                OnSetStatus(Status);
            }
        }
        #endregion

        #region Event
        public event FloatValueChangedEventHandler VolumeChanged;
        public event FloatValueChangedEventHandler PitchChanged;
        public event FloatValueChangedEventHandler ReverbLevelChanged;
        public event AudioStatusChangedEventHandler StatusChanged;
        public event UintChangedEventHandler PositionChanged;
        public event BoolChangedEventHandler ReverseChanged;

        public event SoundChangeEventHandler SoundChanging;
        public event SoundChangeEventHandler SoundChanged;
        #endregion

        #region Properties
        protected RESULT Result { get; set; }
        
        public Sound.Sound Sound { get; private set; }
        [NotNull]
        public DSPCollection DSP { get; private set; }
        //[NotNull]
        //public MixMatrix Matrix { get { _Matrix.Update(true); return _Matrix; } }
        [NotNull]
        public AudioRepeat Repeat { get { _Repeat.Update(true); return _Repeat; } }
        public AudioState Status
        {
            get
            {
                if (Sound == null || channel == null || !channel.isValid()) return AudioState.Unknown;
                else
                {
                    if (Result == RESULT.OK && getIsPlaying()) return LastStatus;
                    else return AudioState.Stopped;
                }
            }
            protected set
            {
                LastStatus = value;
                if (StatusChanged != null) try { StatusChanged.Invoke(this, value); } catch { }
            }
        }

        float _Volume;
        public float Volume
        {
            get
            {
                if (IsValidGroup()) { float vol; group.getVolume(out vol); return vol; }
                else return _Volume;
            }
            set
            {
                _Volume = value;
                if (IsValidGroup()) LogCheck(group.setVolume(value), "Volume");
                else LogCheck(RESULT.ERR_INVALID_HANDLE, "Volume");
                try { if (VolumeChanged != null) VolumeChanged.Invoke(this, Volume); } catch (Exception ex) { }
            }
        }

        float _Pitch;
        public float Pitch
        {
            get
            {
                if (IsValidGroup()) { float pit; group.getPitch(out pit); return pit; }
                else return _Pitch;
            }
            set
            {
                _Pitch = value;
                if (IsValidGroup()) LogCheck(group.setPitch(value), "Pitch");
                else LogCheck(RESULT.ERR_INVALID_HANDLE, "Pitch");
                try { if (PitchChanged != null) PitchChanged.Invoke(this, Pitch); } catch (Exception ex) { }
            }
        }

        protected bool _Reverse;
        public bool Reverse
        {
            get {return Frequency < 0; }
            set
            {
                _Reverse = value;
                LogCheck(channel.setFrequency(value ? -Math.Abs(Frequency) : Math.Abs(Frequency)), "Reverse");
                try { if (ReverseChanged != null) ReverseChanged.Invoke(this, Reverse); } catch (Exception ex) { }
            }
        }

        bool _Mute;
        public bool Mute
        {
            get
            {
                if (IsValidGroup()) { bool mute; group.getMute(out mute); return mute; }
                else return _Mute;
            }
            set { _Mute = value; if (IsValidGroup()) Result = group.setMute(value); }
        }
        //public bool RampVolume { get { bool mute; channel.loop return mute; } set { Result = group.setMute(value); } }

        float _ReverbLevel;
        public float ReverbLevel
        {
            get
            {
                if (IsValidGroup()) { float rev; group.getReverbProperties(0, out rev); return rev; }
                else return _ReverbLevel;
            }
            set
            {
                _ReverbLevel = Math.Max(value, -1);
                if (IsValidGroup()) LogCheck(group.setReverbProperties(0, value), "Reverb Level");
                else LogCheck(RESULT.ERR_INVALID_HANDLE, "Level");
                try { if (ReverbLevelChanged != null) ReverbLevelChanged.Invoke(this, ReverbLevel); }catch (Exception ex) { }
            }
        }

        public float Frequency { get { float freq; channel.getFrequency(out freq); return freq; } }

        public uint CurrentPosition
        {
            get { return IsValidChannel() ? getPosition(TimeUnit.MilliSecond) : _CurrentPosition; }
            set
            {
                _CurrentPosition = value;
                if (IsValidChannel())
                {
                    RESULT result = channel.setPosition(value, TIMEUNIT.MS);
                    LogCheck(result, "CurrentPosition");
                }
                try { if (PositionChanged != null) PositionChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }

        protected LoopKind _Loop;
        public LoopKind Loop
        {
            get
            {
                if (channel == null) return _Loop;
                else
                {
                    MODE mode; channel.getMode(out mode);
                    if ((int)(mode | ~MODE.LOOP_BIDI) == -1) return LoopKind.BIDI;
                    else if ((int)(mode | ~MODE.LOOP_NORMAL) == -1) return LoopKind.Normal;
                    else return LoopKind.Off;
                }
            }
            set
            {
                _Loop = value;
                MODE mode; channel.getMode(out mode);
                //mode &= ~(MODE.LOOP_BIDI | MODE.LOOP_NORMAL | MODE.LOOP_OFF);
                if (value == LoopKind.BIDI) Result = channel.setMode(MODE.LOOP_BIDI);//mode |= MODE.LOOP_BIDI;
                else if (value == LoopKind.Normal) Result = channel.setMode(MODE.LOOP_NORMAL);//mode |= MODE.LOOP_NORMAL;
                else Result = channel.setMode(MODE.LOOP_OFF);
            }
        }
        public uint LoopCount { get; set; }//{ get { int count; channel.getLoopCount(out count); return count; } set {Result = channel.setLoopCount(value); } }
        public uint LoopCountCurrent { get; private set; }

        protected float[][] _Matrix;
        public float[][] Matrix
        {
            get
            {
                /*
                if (group != null && group.isValid())
                {
                    float[] matrix; int input, output;
                    Result = group.getMixMatrix(out matrix, out input, out output, 0);
                    if (matrix == null || matrix.Length >= 0)
                    {
                        float[][] a = new float[12][];
                        for (int i = 0; i < a.Length; i++)
                        {
                            a[i] = new float[12];
                            a[i][i] = 1;
                        }
                        return a;
                    }
                    else return ArrayConveterN<float>.ArrayToMatrix(matrix, input, output, true);
                }
                else 
                */
                return _Matrix;
            }
            set
            {
                _Matrix = value;
                if (value != null && value.Length > 0)
                {
                    float[] matrix = ArrayConveterN<float>.MatrixToArray(value, true);
                    Result = group.setMixMatrix(matrix, value.Length, value[0].Length, 0);
                }
                else Result = group.setMixMatrix(null, 0, 0, 0); 
            }
        }

        public float[][] GetMatrixDefault()
        {
            float[][] a = new float[Sound.Format.Channel][];
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = new float[Sound.Format.Channel];
                a[i][i] = 1;
            }
            return a;
        }
        public float[][] GetMatrix()
        {
            float[] matrix;
            int inch, outch;
            Result = group.getMixMatrix(out matrix, out inch, out outch, 0);
            float[][] arr = ArrayConveterN<float>.ArrayToMatrix(matrix, inch, outch, true);
            return arr;
        }
        #endregion

        #region Setting IO
        public virtual Settings SaveSetting()
        {
            if (IsDisposed) return null;

            Settings s = new Settings();
            s.SetValue("Is3D", Is3D);
            s.SetValue("Volume", Volume);
            s.SetValue("Mute", Mute);
            s.SetValue("Position", CurrentPosition);
            s.SetValue("Pitch", Pitch);
            s.SetValue("ReverbLevel", ReverbLevel);
            s.SetValue("Status", new SettingsData(Status.ToString()));
            
            if (_Matrix != null && _Matrix.Length > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0},{1}", _Matrix.Length, _Matrix[0].Length);

                for (int i = 0; i < _Matrix.Length; i++)
                    for (int j = 0; j < _Matrix[i].Length; j++)
                        sb.Append((" " + _Matrix[i][j]));

                s.SetValue("Matrix", new SettingsData(sb.ToString(), false));
            }

            s.SubSetting.SetValue("Repeat", Repeat.SaveSetting());
            s.SubSetting.SetValue("DSP", DSP.SaveSetting());
            s.SubSetting.SetValue("Sound", Sound.SaveSetting());
            /*
            if (Sound is SoundFile2D) s.SubSetting.SetValue("2D_File", Sound.SaveSetting());
            else if (Sound is SoundNetwork2D) s.SubSetting.SetValue("2D_Network", Sound.SaveSetting());
            else if (Sound is SoundBuffer2D) s.SubSetting.SetValue("2D_Buffer", Sound.SaveSetting());
            */

            return s;
        }
        public virtual bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            bool suc = false;
            /*
            if (Setting.SubSetting.Exist("2D_File")) suc = setSound(SoundFile2D.LoadSetting(audio, Setting.SubSetting["2D_File"], ThrowException));
            else if (Setting.SubSetting.Exist("2D_Network")) suc = setSound(SoundNetwork2D.LoadSetting(audio, Setting.SubSetting["2D_Network"], ThrowException));
            else if (Setting.SubSetting.Exist("2D_Buffer")) suc = setSound(SoundBuffer2D.LoadSetting(audio, Setting.SubSetting["2D_Buffer"], ThrowException));
            else return false;
            */
            if (Setting.Exist("Is3D")) try { Is3D = bool.Parse(Setting["Is3D"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("Sound")) suc = setSound(Core.Sound.Sound.LoadSetting(audio, Setting.SubSetting["Sound"], ThrowException));

            if(suc)
            {
                if (Setting.Exist("Volume")) try { Volume = float.Parse(Setting["Volume"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Mute")) try { Mute = bool.Parse(Setting["Volume"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Position")) try { CurrentPosition = uint.Parse(Setting["Position"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Pitch")) try { Pitch = float.Parse(Setting["Pitch"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("ReverbLevel")) try { ReverbLevel = float.Parse(Setting["ReverbLevel"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.Exist("Status")) try { Status = (AudioState)Enum.Parse(typeof(AudioState), Setting["Status"]); } catch (Exception ex) { if (ThrowException) throw ex; }
                
                if (Setting.Exist("Matrix"))
                {
                    try
                    {
                        string[] s = Setting["Matrix"].Value.Split(' ');
                        string[] size = s[0].Split(',');

                        int x = Convert.ToInt32(size[0]),
                            y = Convert.ToInt32(size[1]);

                        float[][] matrix = new float[x][];

                        int k = 0;
                        for (int i = 0; i < x; i++)
                        {
                            matrix[i] = new float[y];
                            for (int j = 0; j < y && k < s.Length; j++)
                                matrix[i][j] = Convert.ToSingle(s[k++]);
                        }

                        Matrix = Matrix == null ?
                            ArrayConveterN<float>.Merge(GetMatrixDefault(), matrix) :
                            ArrayConveterN<float>.Merge(Matrix, matrix);
                    }
                    catch (Exception ex) { if (ThrowException) throw ex; }
                }

                if (Setting.SubSetting.Exist("Repeat")) try { Repeat.LoadSetting(Setting.SubSetting["Repeat"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
                if (Setting.SubSetting.Exist("Sound")) try { Sound.LoadSetting(Setting.SubSetting["Sound"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            }
            return suc;
        }
        #endregion

        #region Method (Private)
        //protected void LogCheck(RESULT Result, string Func) { HSAudioLog.LogCheck(Result, "Audio::" + Func);}
        protected void LogCheck(RESULT Result, string Func){ HSAudioLog.LogCheck(Result, (Is3D ? "Audio3D::" : "Audio2D::") + Func);}

        string LOCK_Init = "LOCK_Init";
        private RESULT Init(bool Play)
        {
            lock (LOCK_Init)
            {
                if (channel != null) channel.stop();
                if (Sound != null)
                {
                    RESULT Result = audio.system.playSound(Sound.sound, group, !Play, out channel);
                    channel.setCallback(_CHANNEL_CALLBACK);
                    //group.setCallback(_CHANNEL_CALLBACK);
                    return Result;
                }
                return RESULT.ERR_INVALID_HANDLE;
            }
        }
        /*
        private void Loop_Loop()
        {
            while(_Loop == LoopKind.Normal && (LoopCountCurrent == 0 || LoopCount >= LoopCountCurrent))
            {
                if (channel != null && channel.isValid())
                {
                    bool play = true;
                    while (play) { channel.isPlaying(out play); Thread.Sleep(5); }
                    LoopCountCurrent++;
                    Play();
                }
            }
        }
        */
        #endregion

        #region Method (Event)
            //protected void OnStatusChanged(SoundStatus Status) { if (StatusChanged != null) try { StatusChanged(this, Status); } catch { } }
        private RESULT CHANNEL_CALLBACK_(IntPtr channelraw, CHANNELCONTROL_TYPE controltype, CHANNELCONTROL_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            if(type == CHANNELCONTROL_CALLBACK_TYPE.END && Loop == LoopKind.Off)
            {
                Init(false);
                if (Loop == LoopKind.Normal) Play();
                else if (StatusChanged != null) try { StatusChanged.Invoke(this, AudioState.Stopped); } catch { }
            }
            return RESULT.OK;
        }
        
        uint _tmp_pos; AudioState _tmp_stat;
        protected virtual void OnUnloading(object sender)
        {
            _tmp_pos = CurrentPosition;
            _tmp_stat = Status;
        }
        protected virtual void OnLoaded(object sender)
        {
            if (Sound.State == Core.Sound.SoundState.Unload)
            {
                RESULT Result = audio.system.playSound(Sound.sound, group, true, out channel);
                //this.channel = channel;
                channel.setCallback(_CHANNEL_CALLBACK);
                CurrentPosition = _tmp_pos;

                if (_tmp_stat == AudioState.Playing) Play();
                else if (_tmp_stat == AudioState.Pausing) Pause();
            }
        }
        #endregion

        public bool IsDisposed { get; private set; }
        protected internal virtual void Dispose() { Dispose(true); }
        protected internal virtual void Dispose(bool DisposeSound)
        {
            LastStatus = AudioState.Unknown;
            //try { LoopThread.Abort(); LoopThread = null; } catch { }
            IsDisposed = true;
            if (DisposeSound && Sound != null) Sound.Dispose(); Sound = null;
            try { if (channel != null) channel.stop(); channel = null; } catch { }
            try { if (group != null) group.release(); group = null; } catch { }
            DSP.Dispose();
        }
        ~Audio() { if (!IsDisposed) Dispose(); }
    }
}
