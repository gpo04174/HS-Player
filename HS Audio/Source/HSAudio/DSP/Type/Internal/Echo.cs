﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class Echo : IDSP
    {
        private Parameter[] param = new Parameter[4];
        public Echo(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.ECHO, "Echo", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Echo delay in ms.  10  to 5000.  Default = 500.
        /// </summary>
        public float Delay
        {
            get { return DSP.GetParameter((int)DSP_ECHO.DELAY, 0f); }
            set { DSP.SetParameter((int)DSP_ECHO.DELAY, value); }
        }
        /// <summary>
        ///  Echo decay per delay.  0 to 100.  100.0 = No decay, 0.0 = total decay (ie simple 1 line delay).  Default = 50.0
        /// </summary>
        public float Feedback
        {
            get { return DSP.GetParameter((int)DSP_ECHO.FEEDBACK, 0f); }
            set { DSP.SetParameter((int)DSP_ECHO.FEEDBACK, value); }
        }
        /// <summary>
        /// Original sound volume in dB.  -80.0 to 10.0.  Default = 0
        /// </summary>
        public float WetLevel
        {
            get { return DSP.GetParameter((int)DSP_ECHO.WETLEVEL, 0f); }
            set { DSP.SetParameter((int)DSP_ECHO.WETLEVEL, value); }
        }
        /// <summary>
        /// Original sound volume in dB.  -80.0 to 10.0.  Default = 0
        /// </summary>
        public float DryLevel
        {
            get { return DSP.GetParameter((int)DSP_ECHO.DRYLEVEL, 0f); }
            set { DSP.SetParameter((int)DSP_ECHO.DRYLEVEL, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Delay")) try { Delay = float.Parse(Setting["Delay"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Feedback")) try { Feedback = float.Parse(Setting["Feedback"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("WetLevel")) try { WetLevel = float.Parse(Setting["WetLevel"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("DryLevel")) try { DryLevel = float.Parse(Setting["DryLevel"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Delay", Delay);
            s.SetValue("Feedback", Feedback);
            s.SetValue("WetLevel", WetLevel);
            s.SetValue("DryLevel", DryLevel);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
