﻿using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.PlugIn.Output
{
    public class LockInfo
    {
        internal LockInfo(ref OUTPUT_STATE state, uint offset, uint length, ref IntPtr ptr1, ref IntPtr ptr2, ref uint len1, ref uint len2)
        {
            State = state;
            Offset = offset;
            Length = length;
            this.ptr1 = ptr1;
            this.ptr2 = ptr2;
            this.len1 = len1;
            this.len2 = len2;
        }

        internal OUTPUT_STATE State { get; set; }
        /// <summary>
        /// Read only
        /// </summary>
        public uint Offset { get; private set; }
        /// <summary>
        /// Read only
        /// </summary>
        public uint Length { get; private set; }

        public IntPtr ptr1 { get; set; }
        public IntPtr ptr2 { get; set; }
        public uint len1 { get; set; }
        public uint len2 { get; set; }
    }
}
