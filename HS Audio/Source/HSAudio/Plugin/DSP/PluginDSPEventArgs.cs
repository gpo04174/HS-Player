﻿using System;

namespace HSAudio.PlugIn.DSP
{
    public class PluginDSPEventArgs : EventArgs
    {
        protected PluginDSPEventArgs() { }
        /// <summary>
        /// True is don't processing.
        /// </summary>
        public bool Cancel { get; set; }
    }
}
