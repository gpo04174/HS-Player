﻿using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using HS.Log;

namespace HSAudio.Log
{
    public static class HSAudioLogManager
    {
        public static void Logging(this HSAudioLog log) { LogManager.Log.Write(log);}
    }
    public class HSAudioLog : HS.Log.Log
    {
        internal HSAudioLog(string Message, string Source, LogKind Kind, LogLevel Level)
            : base(Message, Source, Level) { this.Kind = Kind;}

        public LogKind Kind { get; private set; }
        //Example: [15:11:05.34] : [Engine][Info] (Sound) - The fatal error has occured!!
        public override string ToString()
        {
            return string.Format("[{0}:{1}:{2}.{3}] : [{4}][{5}] ({6}) - {7}",
                Timestamp.Hour, Timestamp.Minute, Timestamp.Second, Timestamp.Millisecond,
                Kind.ToString(), Level.ToString(), Source, Message);
        }

        internal static void LogCheck(RESULT Result, string Source, bool LogOK = false, LogKind Kind = LogKind.Engine, LogLevel Level = LogLevel.Debug)
        {
            if (Result != RESULT.OK || LogOK) HSAudioLogManager.Logging(new HSAudioLog(StringResource.Resource["STR_ENGINE_" + Result.ToString()], Source, Kind, Level));
        }
        internal static void LogCheck(HSAudioException ex, string Source, LogKind Kind = LogKind.Engine, LogLevel Level = LogLevel.Error)
        {
            HSAudioLogManager.Logging(new HSAudioLog(StringResource.Resource["STR_ENGINE_" + ex.ErrorCode.ToString()], Source, Kind, Level));
        }
    }
}
