﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS.Setting
{
    class SettingManagerINI : Setting<string>
    {
        public override bool LoadSetting(string Setting) { return Load(Setting, "="); }
        public bool Load(string Setting, string Separator)
        {
            string[] Text = Setting.Split('\r', '\n');//File.ReadAllLines(path);

            Dictionary<string, string> a = GetDictionary();
            string[] b = new string[2];
            int LrCf = 0;
            for (int i = 0; i < Text.Length; i++)
            {
                if (Text[i] == null || Text[i] == "") { b[0] = "\n" + (LrCf++); b[1] = null; }
                else if (Text[i][0] == '/' || Text[i][0] == ';') { b[0] = Text[i]; b[1] = null; }
                else b = ConvertStringToArray_Index(Text[i], Separator == null ? "=" : Separator);
                //b = b == null ? b = new string[2] : b;if (b != null)
                if (b != null)
                {
                    try
                    {
                        //object o = b[1];
                        //object nul = null;
                        //(T)Convert.ChangeType(readData, typeof(T));
                        if (b.Length == 2) a.Add(b[0], b[1]);
                        else if (b.Length == 1) a.Add(b[0], null);
                        else if (Text[i] != null && Text[i] != "") a.Add(Text[i], null);
                    }
                    catch { }
                }
            }
            return true;
        }
        public override string SaveSetting()
        {
            return SaveSettingWithoutFilter();
        }

        #region Internal
        private string[] SaveSettingWithoutFilter_Array(string Separator = "=")
        {
            string[] a = new string[Count];
            int i = 0;
            foreach (KeyValuePair<string, object> s in GetDictionary())
                a[i++] = s.Key[0] != '\n' ?
                    (s.Key + (Separator == null ? "=" : Separator) + s.Value) : "";
            return a;
        }
        private string SaveSettingWithoutFilter(string Separator = "=")
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, object> s in GetDictionary())
                sb.AppendLine(s.Key[0] != '\n' ?
                    (s.Key + (Separator == null ? "=" : Separator) + s.Value) : "");
            return sb.ToString();
        }
        private static string[] ConvertStringToArray_Index(string Text, string Seperater)
        {
            if (Seperater != "" || Seperater != null && Text != null | Seperater != "")
            {
                int a = Text.IndexOf(Seperater);
                if (a != -1)
                {
                    string a1 = Text.Remove(a); string a2 = Text.Substring(a + 1);
                    return new string[] { a1, a2 };
                }
                else { return null; }//throw new Exception("분리할 문자열이 내용에 없습니다"); }
            }
            else { return null; }
        }
        #endregion
    }
}
