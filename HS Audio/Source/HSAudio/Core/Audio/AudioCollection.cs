﻿using HS.Setting;
using HSAudio.Core.Audio._2D;
using HSAudio.Core.Audio._3D;
using HSAudio.Core.Sound._2D;
using HSAudio.Core.Sound._3D;
using HSAudio.Core.Sound.Kind;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Threading;

namespace HSAudio.Core.Audio
{
    public class AudioCollection : ISettingManager
    {
        HSAudio Audio;
        RESULT Result;
        List<Audio> list = new List<Audio>();

        //FD_SoundGroup sg;
        internal FD_ChannelGroup cg;
        internal FD_DSPConnection conn;
        internal AudioCollection(HSAudio Audio)
        {
            this.Audio = Audio;
            //Audio.system.createSoundGroup("AudioCollection", out sg);
            //Result = Audio.system.createChannelGroup("AudioCollection", out this.cg);
            //FD_ChannelGroup cg;
            Result = Audio.system.getMasterChannelGroup(out cg);
            //Result = cg.addGroup(this.cg, true, out conn);
            Audio.Output.OutputOpened += Output_OutputOpened;
            Audio.Output.OutputClosing += Output_OutputClosing;

            //Rising StackOverflowException!! 
            /*
            float[] matrix;
            int inch, outch;
            if (Audio != null && cg != null)
            {
                Result = this.cg.getMixMatrix(out matrix, out inch, out outch, 0);
            }
            */
        }

        internal AudioCollection(HSAudio Audio, FD_ChannelGroup cg, FD_DSPConnection conn)
        {
            this.Audio = Audio;
            //Audio.system.createSoundGroup("SoundCollection", out sg);
            this.cg = cg;
            this.conn = conn;
            Audio.Output.OutputOpened += Output_OutputOpened;
            Audio.Output.OutputClosing += Output_OutputClosing;
        }

        private bool Output_OutputOpened(global::HSAudio.Output.Output sender, bool Opened)
        {
            //Result = Audio.system.createChannelGroup("AudioCollection", out this.cg);
            //FD_ChannelGroup cg;
            Result = Audio.system.getMasterChannelGroup(out cg);
            //Result = cg.addGroup(this.cg, true, out conn);

            if (Opened)
                for (int i = 0; i < Count; i++) { this[i].Reload(this.cg); } //this[i].Sound.Load(); Add(this[i].Sound); RemoveAt(i, false); this[i].Play();
            return false;
        }

        private void Output_OutputClosing(global::HSAudio.Output.Output sender)
        {
            //if (cg != null && cg.isValid()) Result = cg.release();
            for (int i = 0; i < Count; i++) this[i].Unload();
            cg = null;
        }

        public Audio this[int Index]
        {
            get { return list[Index]; }
            private set { list[Index].Pause(); list[Index] = value; }
        }

        public float[][] GetMatrix()
        {
            float[] matrix;
            int inch, outch;
            if (Audio != null && cg != null)
            {
                Result = cg.getMixMatrix(out matrix, out inch, out outch, 0);
            }
            else
            {
                //FD_ChannelGroup cg;
                //Result = system.getMasterChannelGroup(out cg);
                Result = cg.getMixMatrix(out matrix, out inch, out outch, 0);
            }
            return null;
        }

        public int Count { get { return list.Count; } }

        #region Add
        public Audio Add(Audio Audio)
        {
            if (cg != null) Result = Audio.channel.setChannelGroup(cg);
            if(AudioChanging != null)try { AudioChanging.Invoke(this, CollectionChange.Add, list.Count, Audio); }catch{}

            //Result = Audio.sound.setSoundGroup(sg);
            list.Add(Audio);

            if(AudioChanged != null)try { AudioChanged.Invoke(this, CollectionChange.Add, list.Count, Audio); }catch{}
            return Audio;
        }
        public Audio Add(Sound.Sound Sound)
        {
            Audio audio = cg == null ? 
                (Sound is Sound2D ? (Audio)new Audio2D((Sound2D)Sound) : new Audio3D((Sound3D)Sound)):
                (Sound is Sound2D ? (Audio)new Audio2D((Sound2D)Sound, cg) : new Audio3D((Sound3D)Sound, cg));
            if (AudioChanging != null) try { AudioChanging.Invoke(this, CollectionChange.Add, list.Count, audio); }catch { }

            list.Add(audio);

            if(AudioChanged != null)try { AudioChanged.Invoke(this, CollectionChange.Add, list.Count, audio); }catch{}
            return audio;
        }

        void AddCommon()
        {

        }
        #endregion

        #region Remove
        internal void Remove(Audio Sound)
        {
            for (int i = Count - 1; i >= 0; i--)
                if (this[i].ID == Sound.ID) { list.RemoveAt(i); break; }
        }
        public void Remove(Audio Sound, bool DisposeSound = true)
        {
            for (int i = Count - 1; i >= 0; i--)
                if (this[i].ID == Sound.ID) { RemoveAt(i); break; }
        }
        public void RemoveAt(int Index, bool DisposeSound = true)
        {
            this[Index].Dispose(DisposeSound);
            list.RemoveAt(Index);
        }

        public void RemoveAll() {for (int i = Count - 1; i >= 0; i--) RemoveAt(i);  }
        #endregion

        #region Event
        public event AudioChangeEventHandler AudioChanging;
        public event AudioChangeEventHandler AudioChanged;
        #endregion

        #region Settings
        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Count", s.Count);
            for(int i = 0; i < Count; i++) if(!(this[i].Sound is SoundStream)) s.SubSetting.SetValue("Audio_" + (i + 1), this[i].SaveSetting());
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            int cnt = -1;
            if (Setting.Exist("Count")) try { cnt = int.Parse(Setting["Count"]); } catch {  }

            if(cnt < 0)
            {
                IEnumerator<KeyValuePair<string, Settings>> e = Setting.SubSetting.GetEnumerator();
                while (e.MoveNext())
                {
                    KeyValuePair<string, Settings> k = e.Current;
                    if (k.Key.ToUpper().IndexOf("Audio_") == 0)
                        try {  LoadAudio(k.Value); } catch (Exception ex) { if (ThrowException) throw ex; }
                }
            }
            else
            {
                for (int i = 1; i <= Count; i++)
                {
                    if (Setting.SubSetting.Exist("Audio_" + i))
                        try { LoadAudio(Setting.SubSetting["Audio_" + i]); } catch (Exception ex) { if (ThrowException) throw ex; }
                }
            }
            return true;
        }

        Audio LoadAudio(Settings Setting)
        {
            bool Is3D = false;
            if (Setting.Exist("Is3D")) Is3D = bool.Parse(Setting["Is3D"]);
            if (Is3D)
            {
                Audio3D audio = new Audio3D(Audio, cg);
                audio.LoadSetting(Setting, true);
                return audio;
            }
            else
            {
                Audio2D audio = new Audio2D(Audio, cg);
                audio.LoadSetting(Setting, true);
                return audio;
            }
        }
        #endregion
        
        void Update()
        {
            while (true)
            {
                for(int i = 0; i < Count; i++)
                {
                    RESULT Result = RESULT.OK;
                    if (Audio.system != null) Result = Audio.system.update();
                    Thread.Sleep(1);
                }
            }
        }

        internal void Dispose()
        {
            RemoveAll();
        }
    }
}
