﻿using System;
using System.Collections.Generic;
using System.Text;
using HSAudio.Wave.Device;
using HSAudio.Lib.Windows.NAudio.Wave;

namespace HSAudio.Control.Device
{
    public class WaveDeviceControlASIO : DeviceControl
    {
        AsioOut asio;
        internal protected WaveDeviceControlASIO(AsioOut asio, WaveDeviceASIO device)
        {
            Device = device;
            this.asio = asio;
            Name = asio.DriverName;
        }
        public WaveDevice Device { get; private set; }

        public float VolumeMax { get { return 1; } }

        public float VolumeMin { get { return 0; } }

        public float Volume { get { return asio.Volume; } set { asio.Volume = Math.Max(Math.Min(value, VolumeMax), VolumeMin); } }
        public float VolumePercent { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public float VolumeScalar { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public bool Mute { get { return false; }set { } }

        public string Name { get; private set; }

        public object Control { get { return asio; } }
    }
}
