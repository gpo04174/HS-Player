﻿using HSAudio.Resource.Language;

namespace HSAudio.Resource
{
    public abstract class StringResource
    {
        public static void setStringResource(StringResource Resource)
        {
            if (Resource != null) StringResource.Resource = Resource;
        }
        internal static StringResource Resource = new Default();

        public abstract string this[string Key] { get; }
        public abstract bool Exist(string Key);
        public abstract int Count { get; }
    }
}
