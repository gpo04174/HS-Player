﻿using System;

#if FUNC_PLUGIN
namespace HSAudio.PlugIn
{
#if NETSTANDARD1x
    internal class PluginPack : IDisposable
    {
        public PluginPack(IHSAudioPlugin Plugin) { Base = Plugin; }
        public IHSAudioPlugin Base { get; private set; }

        public void Dispose()
        {
            Base = null;
            Base.Dispose();
        }
    }
#else
    internal class PluginPack : IDisposable
    {
        public PluginPack(IHSAudioPlugin Base, AppDomain Domain) { this.Base = Base; this.Domain = Domain; }
        public IHSAudioPlugin Base { get; private set; }
        public AppDomain Domain { get; private set; }

        public void Dispose()
        {
            AppDomain.Unload(Domain);
            Domain = null;
            Base = null;
            Base.Dispose();
        }
    }
#endif
}
#endif