﻿namespace HS.Log
{
    public enum LogLevel { Debug, Info = 0, Warning, Error, Fatal }
}
