﻿using System;
using System.IO;

namespace HSAudio.Streams
{

    public class FixBufferStream : Stream
    {
        //List<byte> list;
        byte[] buffer;
        public FixBufferStream(Stream Source) : this(Source, -1, 10240, 512) { }
        public FixBufferStream(Stream Source, int BufferKB, int InitBufferKB) : this(Source, -1, BufferKB, InitBufferKB) { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Source"></param>
        /// <param name="Length"> -1 is SourceStream.Length</param>
        /// <param name="BufferKB"></param>
        public FixBufferStream(Stream Source, long Length, int BufferKB, int InitBufferKB) // 512 KB
        {
            _Length = Length;
            //list = new List<byte>(Buffer)
            BaseStream = Source;
            buffer = new byte[1024 * BufferKB];
            byte[] buf = new byte[1024];
            for (int i = 0; i < InitBufferKB; i++, Fill += buf.Length)
            {
                int read = BaseStream.Read(buf, 0, buf.Length);
                Buffer.BlockCopy(buf, 0, buffer, Fill, read);
                //if (read == buf.Length) 
                //else for (int j = 0; j < read; j++) list.Add(buf[j]);
            }
        }

        public Stream BaseStream { get; private set; }
        public int MaxBuffer
        {
            get { return buffer.Length; }
        }
        public int Fill { get; private set; }

        string LOCK = "LOCK";

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override void Flush()
        {
            Position = 0;
        }

        long _Length = -1;
        public override long Length
        {
            get { return _Length < 0 ? long.MaxValue : (_Length == 0 ? BaseStream.Length : _Length); }
        }

        long _Position = 0;
        int pos;
        public override long Position
        {
            get { return _Position; }
            set
            {
                if (value > MaxBuffer) pos = (int)(_Position - value); // && _Position <= MaxBuffer
                _Position = value;
            }
        }

        bool IsOver = false;
        public override int Read(byte[] buffer, int offset, int count)
        {
            //Position = 800
            //Length = 1000
            //Count = 300

            int len_read = 0;

            if (IsOver)
            {
                int first = MaxBuffer - pos;
                //int second = 0;
                if (pos > 0)
                {
                    Buffer.BlockCopy(this.buffer, first, buffer, offset, pos);
                    //second = offset + pos;
                }
                //else second = offset;

                int off = Math.Max(MaxBuffer + (pos - count), 0);
                int posc = count - pos;
                Array.Copy(this.buffer, posc, this.buffer, 0, off);
                len_read = BaseStream.Read(this.buffer, off, posc);
                Buffer.BlockCopy(this.buffer, off, buffer, offset, len_read);
                _Position += (len_read + pos);
            }
            else if (count + Position > MaxBuffer)
            {
                int first = (int)(MaxBuffer - Position); //200
                int second = count - first; //100
                int posc = MaxBuffer - second;
                Array.Copy(this.buffer, second, this.buffer, 0, posc);
                int read = BaseStream.Read(this.buffer, posc, second);
                Buffer.BlockCopy(this.buffer, MaxBuffer - count, buffer, offset, count);//Math.Min(first, count)
                IsOver = true;
                _Position += first + read;
                pos = 0;
                return first + read;
            }
            else
            {
                if (count + Position > Fill)
                {
                    int cnt = (count + (int)Position) - Fill;
                    int read = BaseStream.Read(this.buffer, Fill, cnt);
                    Buffer.BlockCopy(this.buffer, (int)_Position, buffer, offset, count);//Math.Min(first, count)

                    len_read = read + (Fill - (int)Position);
                    Fill += read;
                    _Position = Fill;
                }
                else
                {
                    Buffer.BlockCopy(this.buffer, (int)Position, buffer, offset, count);
                    len_read = count;
                    _Position += count;
                }
            }

            return len_read + pos;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            if (_Position <= MaxBuffer && origin != SeekOrigin.End)
            {
                if (origin == SeekOrigin.Begin) return _Position = offset;
                else if (_Position + offset < MaxBuffer) return _Position = _Position + offset;
                else throw new NotSupportedException();
            }
            else throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new InvalidOperationException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException();
        }

        public void TakeLock() { }
        public void ReleaseLock() { }
        public void DiscardThrough(long offset) { }
    }
}
