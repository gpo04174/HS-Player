﻿using System;
using System.Reflection;

namespace HSPlayerCore
{
    /*
    class Serializable : Attribute
    {
    }
    */

    interface ICloneable
    {
        object Clone();
    }

    public interface ICustomMarshaler
    {
        void CleanUpManagedData(object ManagedObj);
        void CleanUpNativeData(IntPtr pNativeData);
        int GetNativeDataSize();
        IntPtr MarshalManagedToNative(object ManagedObj);
        object MarshalNativeToManaged(IntPtr pNativeData);
    }

    /*
    public class Encoding : System.Text.Encoding
    {
        System.Text.Encoding encoding;
        public Encoding(System.Text.Encoding encoding) { this.encoding = encoding; }
        //https://ko.wikipedia.org/wiki/%EC%BD%94%EB%93%9C_%ED%8E%98%EC%9D%B4%EC%A7%80
        public static Encoding Default
        {
            get
            {
                string cul = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                switch(cul.ToLower())
                {
                    case "kor": return new Encoding(GetEncoding(949));
                    case "jpn": return new Encoding(GetEncoding(932));
                    default: return new Encoding(UTF8);
                }
            }
        }

        //public static Encoding UTF8 { get { return new Encoding(System.Text.Encoding.UTF8); } }

        public override int GetByteCount(char[] chars, int index, int count) { return encoding.GetByteCount(chars, index, count); }

        public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex) { return encoding.GetBytes(chars, charIndex, charCount, bytes, byteIndex); }

        public override int GetCharCount(byte[] bytes, int index, int count) { return encoding.GetCharCount(bytes, index, count); }

        public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex) { return encoding.GetChars(bytes, byteIndex, byteCount, chars, charIndex); }

        public override int GetMaxByteCount(int charCount) { return encoding.GetMaxCharCount(charCount); }

        public override int GetMaxCharCount(int byteCount) { return encoding.GetMaxCharCount(byteCount); }
    }
    */
}
