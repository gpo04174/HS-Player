﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HS.Attributes
{
    public class HSAttribute : System.Attribute
    {
        public string Description;
        public HSAttribute() { }
        public HSAttribute(string Description) { this.Description = Description; }
    }
}
