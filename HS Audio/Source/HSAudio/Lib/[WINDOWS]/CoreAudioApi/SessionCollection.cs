﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using HSAudio.Lib.Windows.CoreAudioApi.Interfaces;
using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace HSAudio.Lib.Windows.CoreAudioApi
{
    /// <summary>
    /// Collection of sessions.
    /// </summary>
    public class SessionCollection
    {
        readonly IAudioSessionEnumerator audioSessionEnumerator;

        internal SessionCollection(IAudioSessionEnumerator realEnumerator)
        {
            audioSessionEnumerator = realEnumerator;
        }

        /// <summary>
        /// Returns session at index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public AudioSessionControl this[int index]
        {
            get
            {
                IAudioSessionControl session;
                Marshal.ThrowExceptionForHR(audioSessionEnumerator.GetSession(index, out session));
                return new AudioSessionControl(session);
            }
        }

        /// <summary>
        /// Number of current sessions.
        /// </summary>
        public int Count
        {
            get
            {
                int scount;
                Marshal.ThrowExceptionForHR(audioSessionEnumerator.GetCount(out scount));
                return scount;
            }
        }
    }
}
#endif