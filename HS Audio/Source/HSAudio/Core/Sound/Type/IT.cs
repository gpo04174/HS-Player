﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    /// <summary>
    /// Impulse Tracker
    /// </summary>
    public class IT : ISoundFormat
    {
        public IT() : base(SoundType.IT, MIME, Extension, Description) {  }

        public static string[] MIME { get { return new string[] { "audio/it" }; } }
        public static string[] Extension { get { return new string[] { "it" }; } }
        public static string[] Description { get { return new string[] { "Impulse Tracker" }; } }

        public override string ToString() { return Description[0]; }
    }
}
