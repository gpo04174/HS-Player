﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using HSAudio.Output;
using HSAudio.Speakers;
using System;

namespace HSAudio.PlugIn.Output
{
    public class InitInfo : OutputFormat, IEquatable<InitInfo>
    {
        internal int bit;
        internal bool ispcm = true;
        internal InitInfo(int index, int samplerate, SpeakerMode mode, int ch, SOUND_FORMAT format, int dspbufferlength, int dspnumbuffers) : base(samplerate, mode, ch)
        {
            switch (format)
            {
                case SOUND_FORMAT.PCM8: bit = 8; break;
                case SOUND_FORMAT.PCM16: bit = 16; break;
                case SOUND_FORMAT.PCM24: bit = 24; break;
                case SOUND_FORMAT.PCM32: bit = 32; break;
                case SOUND_FORMAT.PCMFLOAT: bit = 32; ispcm = false; break;
                case SOUND_FORMAT.BITSTREAM: bit = 1; break;
                case SOUND_FORMAT.NONE: bit = 0; break;
                default: bit = -1; break;// throw new HSException(StringResource.Resource["STR_ERR_OUTPUT_UNSUPPORT_FORMAT"]);
            }

            Index = index;
            DSPBufferLength = dspbufferlength;
            DSPBufferNum = dspnumbuffers;
        }

        public int Index { get; private set; }
        public int DSPBufferLength { get; private set; }
        public int DSPBufferNum { get; private set; }


        /// <summary>
        /// Bit (8, 16, 24, 32)
        /// </summary>
        public int Bit
        {
            get { return bit; }
            private set { bit = value; }
        }

        public bool IsPCM { get { return ispcm; } }


        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Bit", Bit.ToString());
            s.SetValue("IsPCM", IsPCM.ToString());
            return s;
        }

        public bool Equals(InitInfo other)
        {
            return
                other.Bit == Bit && other.SampleRate == SampleRate && other.IsPCM == IsPCM &&
                other.MaxInputChannel == MaxInputChannel && other.Channel == Channel;
        }
    }
}
