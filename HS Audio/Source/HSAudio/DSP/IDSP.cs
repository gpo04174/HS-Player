﻿using HS.Setting;
using System;

namespace HSAudio.DSP
{
    public interface IDSP : ISettingManager, IDisposable
    {
        bool IsDisposed { get; }
        DSP DSP { get; }
    }
}
