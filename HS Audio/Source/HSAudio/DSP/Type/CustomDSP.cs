﻿using HSAudio.Lib.FMOD;
using HSAudio.PlugIn.DSP;
using HS.Setting;
using System;

namespace HSAudio.DSP.Type
{
    public abstract class CustomDSP : IDSP
    {
        //public enum ParamType {Float, Int, Bool, String, Byte  }
        //public delegate void DSPCallbackHandler(object sender, DSPCallBackEventArgs e);

        //public event DSPCallbackHandler Callback;

        DSP dsp;
        internal CustomDSP(HSAudio Audio, int ParamCount, string Name, uint Version = 0, object Tag = null)
        {
            _DSPDescription = new Description(OnReadCallback, ParamCount, Name, Version);
            dsp = new DSP(Audio, _DSPDescription, Name, Tag);
            //DSP Regester 될떄 호출
            //DSPDescription = new DSPDescription(READCALLBACK, "Custom DSPDescription (ID: " + dsp.ID + ")");
        }

        public DSP DSP { get; private set; }
        
        Description _DSPDescription;
        /// <summary>
        /// 전문가 외에는 손대지 말것.
        /// </summary>
        protected Description Description { get { return _DSPDescription; } }
        
        public abstract Settings SaveSetting();
        public abstract bool LoadSetting(Settings Setting, bool ThrowException = false);

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        #region Callback
        DSPCallBackEventArgs e = new DSPCallBackEventArgs();
        protected virtual RESULT OnReadCallback(ref DSP_STATE dsp_state, IntPtr inbuf, IntPtr outbuf, uint length, int inchannels, ref int outchannels)
        {
            e.Set(dsp_state, inbuf, outbuf, length, inchannels, ref outchannels);
            return ReadCallback(this, e) ? RESULT.OK : RESULT.ERR_DSP_DONTPROCESS;
            /*
            if (Callback == null) return RESULT.ERR_DSP_DONTPROCESS;
            else
            {
                e.Set(dsp_state, inbuf, outbuf, length, inchannels, outchannels);
                try { Callback(this, e); } catch { }
                outchannels = e.OutChannel;
                dsp_state = e.State;

                uint count = 0;
                int count2 = 0;
                unsafe
                {
                    float* inbuffer = (float*)inbuf.ToPointer();
                    float* outbuffer = (float*)outbuf.ToPointer();
                    for (count = 0; count < length; count++)
                    {
                        for (count2 = 0; count2 < outchannels; count2++)
                        {
                            int* block = stackalloc int[100];
                            float data = inbuffer[(count * outchannels) + count2];
                            float* data1 = &outbuffer[(count * outchannels) + count2];
                            if (Callback == null) *data1 = data;
                        }
                    }
                }
            }
            return RESULT.OK;
            */
        }
        protected abstract bool ReadCallback(object sender, DSPCallBackEventArgs e);

        public class DSPCallBackEventArgs : EventArgs
        {
            internal DSPCallBackEventArgs() { }
            internal void Set(DSP_STATE State, IntPtr InBuffer, IntPtr OutBuffer, uint Length, int InChannel, ref int OutChannel)
            {
                this.State = State;
                this.InBuffer = InBuffer;
                this.OutBuffer = OutBuffer;
                this.Length = Length;
                this.InChannel = InChannel;
                this.OutChannel = OutChannel;
            }
            public DSP_STATE State { get; private set; }
            public IntPtr InBuffer { get; private set; }
            public IntPtr OutBuffer { get; private set; }
            public uint Length { get; private set; }
            public int InChannel { get; private set; }
            public int OutChannel { get; set; }
        }
        #endregion
    }
}
