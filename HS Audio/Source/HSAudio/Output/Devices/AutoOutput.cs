﻿using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;
using HSAudio.Resource;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Auto output [Default] (자동 출력 [기본값])
    /// </summary>
    public class AutoOutput : OutputMethod
    {
        public AutoOutput(){ ID = 0; }
        
        public override string Name
        {
            get { return StringResource.Resource["STR_DEVICE_AUTOOUTPUT"]; }
            protected set {}
        }

        public override Output Parent { get; internal set; }

        public override object Clone(){return new object();}

        public override bool LoadSetting(Settings Setting, bool ThrowException = false) {return true;}

        public override Settings SaveSetting() { return null; }

        protected internal override void Dispose(){}

        public override bool Equals(object obj) { return obj is AutoOutput; }

        public override string ToString() { return Name; }
        //public override int GetHashCode() { return base.GetHashCode(); }
    }
}
