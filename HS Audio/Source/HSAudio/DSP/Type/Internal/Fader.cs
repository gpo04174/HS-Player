﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.DSP.Type
{
    public class Fader : IDSP
    {
        private Parameter[] param = new Parameter[3];
        public Fader(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.FADER, "Fader", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Signal gain in dB. -80.0 to 10.0. Default = 0.0
        /// </summary>
        public float Gain
        {
            get { return DSP[(int)DSP_FADER.GAIN].Value; }
            set { int index = (int)DSP_FADER.GAIN; DSP[index] = param[index].SetValue(value); }
        }
        /// <summary>
        /// Overall gain. For information only, not set by user. Data of type FMOD_DSP_PARAMETER_DATA_TYPE_OVERALLGAIN to provide to FMOD, to allow FMOD to know the DSP is scaling the signal for virtualization purposes.
        /// </summary>
        public float GainOverAll
        {
            get { return DSP[(int)DSP_FADER.OVERALL_GAIN].Value; }
            set { int index = (int)DSP_FADER.OVERALL_GAIN; DSP[index] = param[index].SetValue(value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Mix")) try { Mix = float.Parse(Setting["Mix"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Depth")) try { Depth = float.Parse(Setting["Depth"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Rate")) try { Rate = float.Parse(Setting["Rate"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Mix", Mix);
            s.SetValue("Depth", Depth);
            s.SetValue("Rate", Rate);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
