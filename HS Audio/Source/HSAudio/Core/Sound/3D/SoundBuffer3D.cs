﻿using HS.Setting;
using HSAudio.Core.Sound.Kind;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;
using System.IO;

namespace HSAudio.Core.Sound._3D
{
    public class SoundBuffer3D : SoundBuffer, Sound3D
    {
        Distance3D _Distance;
        Cone3D _Cone;
        public SoundBuffer3D(HSAudio Audio, byte[] Buffer, bool Compress = true, bool LoadMemory = true) : this(Audio, Buffer, null, null, Compress, LoadMemory) { }
        public SoundBuffer3D(HSAudio Audio, byte[] Buffer, ISoundFormat Type, WaveFormat Format, bool Compress, bool LoadMemory = true) : base(Audio, Buffer, Type, Format, Compress, LoadMemory)
        {
            _Distance = new Distance3D(sound);
            _Cone = new Cone3D(sound);
        }
        
        public override bool Load(bool ThrowException = true)
        {
            if (IsValid) return true;
            else
            {
                try
                {
                    MODE mode_comp = Compress ? MODE.CREATECOMPRESSEDSAMPLE | MODE.ACCURATETIME : MODE.ACCURATETIME;
                    RESULT result = LoadMemory ?
                        Audio.system.createSound(Buffer, MODE._3D | mode_comp, ref info, out sound) :
                        Audio.system.createStream(Buffer, MODE._3D | MODE.ACCURATETIME, ref info, out sound);

                    if (result != RESULT.OK) { sound.release(); throw new HSAudioException(result); }
                    else { _Format = getFormat(); if (_SoundType == null) getType(); }

                    _Tag = SoundTag.GetTag(sound);
                    ms = new MemoryStream(Buffer);
                    GC.Collect();
                    base.OnLoaded();
                    return true;
                }
                catch (Exception ex) { if (ThrowException) throw ex; }
                return false;
            }
        }

        public Distance3D Distance { get { return _Distance; } }
        public Cone3D Cone { get { return _Cone; } }

        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Type", "Buffer");
            s.SetValue("Dimension", "3D");
            s.SetValue("Data", new SettingsData(Convert.ToBase64String(ms.ToArray())));
            s.SetValue("LoadMemory", LoadMemory);
            s.SetValue("Compress", Compress);
            s.SubSetting.SetValue("Distance", Distance.SaveSetting());
            s.SubSetting.SetValue("Cone", Cone.SaveSetting());
            return s;
        }

        public static SoundBuffer3D LoadSetting(HSAudio Audio, Settings Setting, bool ThrowException = false)
        {
            byte[] Data = null;
            ISoundFormat type = null;
            WaveFormat format = null;
            bool Compress = true, LoadMemory = true;

            try { if (Setting.Exist("Data")) Data = Convert.FromBase64String(Setting["Data"]); } catch (Exception ex) { if (ThrowException) throw ex; return null; }
            try { if (Setting.SubSetting.Exist("Type")) type = ISoundFormat.FromSetting(Setting.SubSetting["Type"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.SubSetting.Exist("Format")) format = WaveFormat.Load(Setting.SubSetting["Format"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            try { if (Setting.Exist("Compress")) Compress = Convert.ToBoolean(Setting["Compress"]); } catch (Exception ex) { if (ThrowException) throw ex;}
            try { if (Setting.Exist("LoadMemory")) LoadMemory = Convert.ToBoolean(Setting["LoadMemory"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            SoundBuffer3D sound = new SoundBuffer3D(Audio, Data, type, format, Compress, LoadMemory);

            if (Setting.SubSetting.Exist("Distance")) sound.Distance.LoadSetting(Setting.SubSetting["Distance"], ThrowException); 
            if (Setting.SubSetting.Exist("Cone")) sound.Cone.LoadSetting(Setting.SubSetting["Cone"], ThrowException);
            return sound;
        }
    }
}
