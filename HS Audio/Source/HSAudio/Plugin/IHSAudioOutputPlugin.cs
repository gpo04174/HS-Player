﻿using System;
#if FUNC_PLUGIN
using System.Collections.Generic;
using System.Text;
using HSAudio.PlugIn.Output;

namespace HSAudio.PlugIn
{
    public interface IHSAudioOutputPlugin : IHSAudioPlugin
    {
        /// <summary>
        ///  Cannot be null.
        /// </summary>
        /// <returns></returns>
        Description GetDescription();
    }
}
#endif