﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using HSAudio.Lib.Windows.CoreAudioApi;
using HSAudio.Wave.Device;
using HSAudio.Lib.Windows.NAudio.Wave;
using System;
using System.Collections.Generic;

namespace HSAudio.Wave.Input
{
    public class WaveInputWASAPI : WaveInput
    {
        public static WaveInputWASAPI[] GetInputs()
        {
            List<WaveInputWASAPI> list = new List<WaveInputWASAPI>();

            MMDeviceEnumerator mm = new MMDeviceEnumerator();
            MMDeviceCollection collect = mm.EnumerateAudioEndPoints(DataFlow.Capture, DeviceState.Active);
            for (int i = 0; i < collect.Count; i++)
                try { list.Add(new WaveInputWASAPI(collect[i], true)); } catch(Exception ex) { }

            return list.ToArray();
        }

        internal WasapiCapture waveIn;
        //WaveDataInEventArgs args;

        internal protected WaveInputWASAPI(MMDevice Device, bool IsInput)
        {
            Kind = RecordKind.WASAPI;
            this.Device = new WaveDeviceWASAPI(Device, IsInput);
            //args = new WaveDataInEventArgs();
            Format = this.Device.Formats[0];

            this.waveIn = new WasapiCapture(Device);
            this.waveIn.DataAvailable += DataAvailable_Event;
            this.waveIn.RecordingStopped += WaveDataInStopped_Event;
        }

        public event DataInEventHandler WaveDataIn;
        public event DataInStoppedEventHandler WaveDataInStopped;

        public WaveFormat Format { get; set; }//{ get { return args.Format; }set { args.Format = value; } }
        public WaveDevice Device { get; private set; }
        public RecordKind Kind { get; private set; }

        public bool IsBusy { get; private set; }
        public int Latency { get { return 10; } set { } }
        public bool LatencySupport { get { return false; } }

        public void Start() { IsBusy = true; try { waveIn.StartRecording(); } finally { IsBusy = false; } }
        public void Stop() { IsBusy = true; try { waveIn.StopRecording(); } finally { IsBusy = false; } }

        public void Dispose()
        {
            if (waveIn != null) try{ Stop(); waveIn.Dispose(); } catch { }
            if (Device != null) try{ Device.Dispose(); } catch { }
            //for (int i = 0; SubDevice != null && i < SubDevice.Length; i++) { SubDevice[i].Dispose(); }
            waveIn = null;
            WaveDataIn = null;
            WaveDataInStopped = null;
            Device = null;
        }

        public override string ToString()
        {
            return "[WASAPI] " + Device.ToString();
        }

        #region Event Method
        private void DataAvailable_Event(object o, WaveInEventArgs e)
        {
            if (WaveDataIn != null)
                try { WaveDataIn.Invoke(o, new DataInEventArgs((WaveFormat)Format.Clone(), e.Buffer, e.BytesRecorded)); }catch { }
        }
        private void WaveDataInStopped_Event(object o, StoppedEventArgs e)
        {
            if (WaveDataInStopped != null) WaveDataInStopped.Invoke(this, e);
        }
        #endregion
    }
}
#endif
