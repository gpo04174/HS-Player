﻿using HSAudio.PlugIn.DSP;

namespace HSAudio.Lib.FMOD
{
    public delegate RESULT DSP_PROCESS_CALLBACK(ref DSP_STATE dsp_state, uint length, ref BufferArray inbufferarray, ref BufferArray outbufferarray, bool inputsidle, DSP_PROCESS_OPERATION op);
}
