﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Output
{
    public enum OutputResampler
    {
        Not = DSP_RESAMPLER.NOINTERP,
        Linear = DSP_RESAMPLER.LINEAR,
        Spline = DSP_RESAMPLER.SPLINE,
        Cubic = DSP_RESAMPLER.CUBIC,
        Other = DSP_RESAMPLER.MAX
    }
}
