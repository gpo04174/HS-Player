﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSAudio.DSP
{
    public enum DataType : int
    {
        TypeCustom = 0,
        TypeOverAllGain = -1,
        Type3DAttribute = -2,
        TypeSideChain = -3,
        TypeFFT = -4,
        Type3DAttributeMulti = -5
    }
}
