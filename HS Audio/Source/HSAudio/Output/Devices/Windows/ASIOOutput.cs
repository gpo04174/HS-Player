﻿#if WIN_XP || WIN_VISTA || WIN_10_1703
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Windows Low latency ASIO 2.0 output (윈도우 저 지연 ASIO 2.0 출력)
    /// </summary>
    public class ASIOOutput : DeviceOutput
    {
        public static ASIOOutput[] GetOutputs(HSAudio Audio)
        {
            if (Audio != null)
            {
                Audio.system_sub.setOutput(OUTPUTTYPE.ASIO);
                int num;
                Audio.system_sub.getNumDrivers(out num);

                List<ASIOOutput> device = new List<ASIOOutput>();
                for (int i = 0; i < num; i++)
                {
                    StringBuilder sb = new StringBuilder(256);
                    OutputFormat format = new OutputFormat();

                    Guid guid; SPEAKERMODE spk; int ch;
                    Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                    Audio.system_sub.setDriver(i);
                    ADVANCEDSETTINGS setting = new ADVANCEDSETTINGS();
                    Audio.system_sub.getAdvancedSettings(ref setting);

                    device.Add(new ASIOOutput(i, format, guid, setting) { Name = sb.ToString() });//string.IsNullOrEmpty(sb.ToString()) ? GetName(guid.ToString()) : sb.ToString()
                }
                return device.ToArray();
            }
            else return null;
        }
        public static ASIOOutput[] GetOutputs()
        {
            Lib.FMOD.FD_System system_sub;
            Lib.FMOD.Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.ASIO);
                    int num;
                    system_sub.getNumDrivers(out num);

                    List<ASIOOutput> device = new List<ASIOOutput>();
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        system_sub.setDriver(i);
                        ADVANCEDSETTINGS setting = new ADVANCEDSETTINGS();
                        system_sub.getAdvancedSettings(ref setting);

                        device.Add(new ASIOOutput(i, format, guid, setting) { Name = sb.ToString() });//string.IsNullOrEmpty(sb.ToString()) ? GetName(guid.ToString()) : sb.ToString()
                    }
                    return device.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }
        /*
        private static string GetName(string Guid)
        {
            RegistryKey reg = Registry.LocalMachine.OpenSubKey("SOFTWARE\\ASIO", false);
            string[] names = reg.GetSubKeyNames();
            for (int i = 0; i < names.Length; i++)
            {
                RegistryKey reg1 = reg.OpenSubKey(names[i], false);
                if (Guid.ToUpper().IndexOf(reg1.GetValue("CLSID", "").ToString().ToUpper()) >= 0) { reg1.Close(); reg.Close(); return (string)names[i].Clone(); }
                reg1.Close();
            }
            reg.Close();
            return null;
        }
        */

        internal ADVANCEDSETTINGS setting;
        OutputFormat format;
        internal ASIOOutput(int index, OutputFormat Format, Guid guid, ADVANCEDSETTINGS setting)
        {
            Type = OutputType.ASIO;
            ID = index;
            this.Format = Format;
            this.Guid = guid;
            this.setting = setting;
        }
        private ASIOOutput(ASIOOutput before)
        {
            Type = OutputType.ASIO;
            ID = before.ID;
            Name = before.Name;
            Guid = before.Guid;
            Parent = before.Parent;
            Format = before.Format;
        }

        public override Output Parent { get; internal set; }

        public Guid Guid { get; private set; }

        public override object Clone() { return new ASIOOutput(ID, Format, Guid, setting); }

        public override bool Equals(object obj) { try { return obj is ASIOOutput ? Guid.Equals(((ASIOOutput)obj).Guid) : false; } catch (Exception ex) { return false; } }
        public override bool Equals(DeviceOutput other) { try { return other is ASIOOutput ? Guid.Equals(((ASIOOutput)other).Guid) : false; } catch (Exception ex) { return false; } }
        //public override int GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return Name; }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SetValue("Guid", Guid.ToString());
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose()
        {
            try { Device.Dispose(); } catch (Exception ex) { new HSAudioLog(ex.Message, "ASIOOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }
        }
    }
}
#endif