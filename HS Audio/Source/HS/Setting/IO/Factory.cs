﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS.Setting.IO
{
    public class SettingFactory<T> where T : SettingIO<T>, new()
    {
        public static Settings Load(T Data)
        {
            T setting = new T();
            return setting.Load(Data);
        }
        public static T Save(Settings Data)
        {
            T setting = new T();
            return setting.Save(Data);
        }
    }
}
