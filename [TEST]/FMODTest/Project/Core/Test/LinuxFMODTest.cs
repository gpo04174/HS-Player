﻿using System;
using System.Threading;
using HSAudio.Lib.FMOD;

namespace FMODTest.Test
{
    class LinuxFMODTest
    {
        public static void Test(string MusicPath)
        {
            RESULT result = RESULT.OK;
            FD_System system;
            FD_Sound sound;
            FD_Channel channel;

            result = Factory.System_Create(out system);
            Console.WriteLine(system.getRaw().ToString());

            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            Console.WriteLine(result.ToString());
            result = system.createSound(MusicPath, MODE.DEFAULT, out sound);
            Console.WriteLine(result.ToString());
            result = system.playSound(sound, null, false, out channel);

            Console.WriteLine(result.ToString());

            bool play = true;
            while (play)
            {
                channel.isPlaying(out play);
                uint pos; channel.getPosition(out pos, TIMEUNIT.MS);
                Console.WriteLine(pos);

                //system.update();
                Thread.Sleep(10);
            }
        }
    }
}