﻿namespace HS.PlugIn
{
    public interface IPluginInformation
    {
        string Version { get; }
        string Name { get; }
        string Description { get; }
        string Author { get; }
        object Tag { get; set; }
    }
}
