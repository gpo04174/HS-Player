﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace HSAudio.Utility.Security
{
    public class Crc32
    {
        private readonly uint[] m_checksumTable;
        private const uint s_generator = 0xedb88320;

        public Crc32()
        {
            this.m_checksumTable = Enumerable.Range(0, 0x100).Select<int, uint>(delegate (int i) {
                uint num = (uint)i;
                for (int k = 0; k < 8; k++)
                {
                    num = ((num & 1) != 0) ? (0xedb88320 ^ (num >> 1)) : (num >> 1);
                }
                return num;
            }).ToArray<uint>();
        }

        public uint Get<T>(IEnumerable<T> byteStream)
        {
            try
            {
                return ~byteStream.Aggregate<T, uint>(uint.MaxValue, delegate (uint checksumRegister, T currentByte) {
                    return (this.m_checksumTable[(checksumRegister & 0xff) ^ Convert.ToByte(currentByte)] ^ (checksumRegister >> 8));
                });
            }
            catch (FormatException)
            {
                return 0;
            }
        }
    }
}
