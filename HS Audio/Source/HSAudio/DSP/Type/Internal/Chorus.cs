﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class Chorus : IDSP
    {
        private Parameter[] param = new Parameter[3];
        public Chorus(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.CHORUS, "Chorus", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Volume of original signal to pass to output.  0.0 to 100.0. Default = 50.0
        /// </summary>
        public float Mix
        {
            get { return DSP.GetParameter((int)DSP_CHORUS.MIX, 0f); }
            set { DSP.SetParameter((int)DSP_CHORUS.MIX, value); }
        }
        /// <summary>
        /// Chorus modulation rate in Hz.  0.0 to 20.0.  Default = 0.8 Hz
        /// </summary>
        public float Rate
        {
            get { return DSP.GetParameter((int)DSP_CHORUS.RATE, 0f); }
            set { DSP.SetParameter((int)DSP_CHORUS.RATE, value); }
        }
        /// <summary>
        /// Chorus modulation depth.  0.0 to 100.0.  Default = 3.0
        /// </summary>
        public float Depth
        {
            get { return DSP.GetParameter((int)DSP_CHORUS.DEPTH, 0f); }
            set { DSP.SetParameter((int)DSP_CHORUS.DEPTH, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Mix")) try { Mix = float.Parse(Setting["Mix"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Rate")) try { Rate = float.Parse(Setting["Rate"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Depth")) try { Depth = float.Parse(Setting["Depth"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Mix", Mix);
            s.SetValue("Rate", Rate);
            s.SetValue("Depth", Depth);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
