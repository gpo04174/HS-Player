﻿#if FUNC_PLUGIN
using HS;
using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;

namespace HSAudio.PlugIn
{
#if !NETSTANDARD1x
    internal class PlugInHelper
    {
        public static PluginKind PluginCheck(string Path)
        {
            PluginPack pack = null;
            try
            {
                pack = PluginLoader(Path);
                if (pack is IHSAudioOutputPlugin) return PluginKind.Output;
#if FUNC_PLUGIN_CODEC
                else if (pack is IHSAudioCodecPlugin) return PluginKind.Codec;
#endif
                else if (pack is IHSAudioDSPPlugin) return PluginKind.DSP;
            }
            catch (Exception ex) { }
            finally { if(pack != null) pack.Dispose(); }
            return PluginKind.Unknown;
        }
        public static PluginPack PluginLoader(string Path)
        {
            AppDomain dom = null;
            try
            {
                DateTime dt = DateTime.Now;
                dom = AppDomain.CreateDomain(string.Format("Plugin [{0}-{1}-{2} {3}:{4}:{5}.{6}]", dt.Year, dt.Month, dt.Date, dt.Hour, dt.Millisecond, dt.Second, dt.Millisecond));
                AssemblyName assemblyName = new AssemblyName();
                assemblyName.CodeBase = Path;
                Assembly assembly = dom.Load(assemblyName);

                if (assembly != null)
                {
                    Type[] pluginTypes = new Type[]
                    {
                        typeof(IHSAudioOutputPlugin),
#if FUNC_PLUGIN_CODEC
                        typeof(IHSAudioCodecPlugin),
#endif
                        typeof(IHSAudioDSPPlugin)
                    };
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsInterface || type.IsAbstract) continue;
                        else if (type.GetInterface(pluginTypes[0].FullName) != null)
                        {
                            IHSAudioOutputPlugin plugin = (IHSAudioOutputPlugin)Activator.CreateInstance(type);
                            return new PluginPack(plugin, dom);
                        }
#if FUNC_PLUGIN_CODEC
                        else if (type.GetInterface(pluginTypes[1].FullName) != null)
                        {
                            IHSAudioCodecPlugin plugin = (IHSAudioCodecPlugin)Activator.CreateInstance(type);
                            return new PluginPack(plugin, dom);
                        }
#endif
                        else if (type.GetInterface(pluginTypes[2].FullName) != null)
                        {
                            IHSAudioDSPPlugin plugin = (IHSAudioDSPPlugin)Activator.CreateInstance(type);
                            return new PluginPack(plugin, dom);
                        }
                    }
                }
            }
            catch (Exception ex) { }

            if (dom != null) AppDomain.Unload(dom);
            return null;
        }
        public static IHSAudioOutputPlugin PluginLoaderOutput(string Path, out AppDomain domain, bool ThrowException = true)
        {
            AppDomain dom = null;
            try
            {
                DateTime dt = DateTime.Now;
                dom = AppDomain.CreateDomain(string.Format("OutputPlugin [{0}-{1}-{2} {3}:{4}:{5}.{6}]", dt.Year, dt.Month, dt.Date, dt.Hour, dt.Millisecond, dt.Second, dt.Millisecond));
                AssemblyName assemblyName = new AssemblyName();
                assemblyName.CodeBase = Path;
                Assembly assembly = dom.Load(assemblyName);

                if (assembly != null)
                {
                    Type pluginType = typeof(IHSAudioOutputPlugin);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsInterface || type.IsAbstract) continue;
                        else if (type.GetInterface(pluginType.FullName) != null)
                        {
                            IHSAudioOutputPlugin plugin = (IHSAudioOutputPlugin)Activator.CreateInstance(type);
                            domain = dom;
                            return plugin;
                        }
                    }
                    if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_OUTPUT"].Replace("{0}", Path));
                }
                else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN"].Replace("{0}", Path));}

            }
            catch (Exception ex) { if (ThrowException) throw ex; }

            if(dom != null) AppDomain.Unload(dom);
            domain = null;
            return null;
        }
        public static IHSAudioDSPPlugin PluginLoaderDSP(string Path, out AppDomain domain, bool ThrowException = true)
        {
            AppDomain dom = null;
            try
            {
                DateTime dt = DateTime.Now;
                dom = AppDomain.CreateDomain(string.Format("DSPPlugin [{0}-{1}-{2} {3}:{4}:{5}.{6}]", dt.Year, dt.Month, dt.Date, dt.Hour, dt.Millisecond, dt.Second, dt.Millisecond));
                AssemblyName assemblyName = new AssemblyName();
                assemblyName.CodeBase = Path;
                Assembly assembly = dom.Load(assemblyName);

                if (assembly != null)
                {
                    Type pluginType = typeof(IHSAudioDSPPlugin);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsInterface || type.IsAbstract) continue;
                        else if (type.GetInterface(pluginType.FullName) != null)
                        {
                            IHSAudioDSPPlugin plugin = (IHSAudioDSPPlugin)Activator.CreateInstance(type);
                            domain = dom;
                            return plugin;
                        }
                    }
                    if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_DSP"].Replace("{0}", Path));
                }
                else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN"].Replace("{0}", Path)); }

            }
            catch (Exception ex) { if (ThrowException) throw ex; }

            if (dom != null) AppDomain.Unload(dom);
            domain = null;
            return null;
        }

#if FUNC_PLUGIN_CODEC
        public static IHSAudioCodecPlugin PluginLoaderCodec(string Path, out AppDomain domain, bool ThrowException = true)
        {
            AppDomain dom = null;
            try
            {
                DateTime dt = DateTime.Now;
                dom = AppDomain.CreateDomain(string.Format("CodecPlugin [{0}-{1}-{2} {3}:{4}:{5}.{6}]", dt.Year, dt.Month, dt.Date, dt.Hour, dt.Millisecond, dt.Second, dt.Millisecond));
                AssemblyName assemblyName = new AssemblyName();
                assemblyName.CodeBase = Path;
                Assembly assembly = dom.Load(assemblyName);

                if (assembly != null)
                {
                    Type pluginType = typeof(IHSAudioCodecPlugin);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        if (type.IsInterface || type.IsAbstract) continue;
                        else if (type.GetInterface(pluginType.FullName) != null)
                        {
                            IHSAudioCodecPlugin plugin = (IHSAudioCodecPlugin)Activator.CreateInstance(type);
                            domain = dom;
                            return plugin;
                        }
                    }
                    if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_CODEC"].Replace("{0}", Path));
                }
                else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN"].Replace("{0}", Path)); }

            }
            catch (Exception ex) { if (ThrowException) throw ex; }

            if (dom != null) AppDomain.Unload(dom);
            domain = null;
            return null;
        }
#endif
    }
#else
    internal class PlugInHelper
    {
        public static PluginKind PluginCheck(string Path)
        {
            PluginPack pack = null;
            try
            {
                pack = PluginLoader(Path);
                if (pack is IHSAudioOutputPlugin) return PluginKind.Output;
#if FUNC_PLUGIN_CODEC
                else if (pack is IHSAudioCodecPlugin) return PluginKind.Codec;
#endif
                else if (pack is IHSAudioDSPPlugin) return PluginKind.DSP;
            }
            catch (Exception ex) { }
            finally { if (pack != null) pack.Dispose(); }
            return PluginKind.Unknown;
        }
        public static PluginPack PluginLoader(string Path)
        {
            try
            {
                DateTime dt = DateTime.Now;
                AssemblyName assemblyName = AssemblyLoadContext.GetAssemblyName(Path);
                Assembly assembly = Assembly.Load(assemblyName);

                if (assembly != null)
                {
                    Type[] pluginTypes = new Type[]
                    {
                        typeof(IHSAudioOutputPlugin),
#if FUNC_PLUGIN_CODEC
                        typeof(IHSAudioCodecPlugin),
#endif
                        typeof(IHSAudioDSPPlugin)
                    };
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        TypeInfo info = type.GetTypeInfo();
                        if (info.IsInterface || info.IsAbstract) continue;
                        else if (info.GetInterface(pluginTypes[0].FullName) != null)
                        {
                            IHSAudioOutputPlugin plugin = (IHSAudioOutputPlugin)Activator.CreateInstance(type);
                            return new PluginPack(plugin);
                        }
#if FUNC_PLUGIN_CODEC
                        else if (type.GetInterface(pluginTypes[1].FullName) != null)
                        {
                            IHSAudioCodecPlugin plugin = (IHSAudioCodecPlugin)Activator.CreateInstance(type);
                            return new PluginPack(plugin, dom);
                        }
#endif
                        else if (info.GetInterface(pluginTypes[2].FullName) != null)
                        {
                            IHSAudioDSPPlugin plugin = (IHSAudioDSPPlugin)Activator.CreateInstance(type);
                            return new PluginPack(plugin);
                        }
                    }
                }
            }
            catch (Exception ex) { }
            
            return null;
        }
        public static IHSAudioOutputPlugin PluginLoaderOutput(string Path, bool ThrowException = true)
        {
            try
            {
                DateTime dt = DateTime.Now;
                AssemblyName assemblyName = AssemblyLoadContext.GetAssemblyName(Path);
                Assembly assembly = Assembly.Load(assemblyName);

                if (assembly != null)
                {
                    Type pluginType = typeof(IHSAudioOutputPlugin);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        TypeInfo info = type.GetTypeInfo();
                        if (info.IsInterface || info.IsAbstract) continue;
                        else if (info.GetInterface(pluginType.FullName) != null)
                        {
                            IHSAudioOutputPlugin plugin = (IHSAudioOutputPlugin)Activator.CreateInstance(type);
                            return plugin;
                        }
                    }
                    if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_OUTPUT"].Replace("{0}", Path));
                }
                else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN"].Replace("{0}", Path)); }

            }
            catch (Exception ex) { if (ThrowException) throw ex; }
            
            return null;
        }
        public static IHSAudioDSPPlugin PluginLoaderDSP(string Path, bool ThrowException = true)
        {
            try
            {
                DateTime dt = DateTime.Now;
                AssemblyName assemblyName = AssemblyLoadContext.GetAssemblyName(Path);
                Assembly assembly = Assembly.Load(assemblyName);

                if (assembly != null)
                {
                    Type pluginType = typeof(IHSAudioDSPPlugin);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        TypeInfo info = type.GetTypeInfo();
                        if (info.IsInterface || info.IsAbstract) continue;
                        else if (info.GetInterface(pluginType.FullName) != null)
                        {
                            IHSAudioDSPPlugin plugin = (IHSAudioDSPPlugin)Activator.CreateInstance(type);
                            return plugin;
                        }
                    }
                    if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_OUTPUT"].Replace("{0}", Path));
                }
                else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN"].Replace("{0}", Path)); }

            }
            catch (Exception ex) { if (ThrowException) throw ex; }

            return null;
        }

#if FUNC_PLUGIN_CODEC
        public static IHSAudioCodecPlugin PluginLoaderCodec(string Path, bool ThrowException = true)
        {
            try
            {
                DateTime dt = DateTime.Now;
                AssemblyName assemblyName = AssemblyLoadContext.GetAssemblyName(Path);
                Assembly assembly = Assembly.Load(assemblyName);

                if (assembly != null)
                {
                    Type pluginType = typeof(IHSAudioCodecPlugin);
                    Type[] types = assembly.GetTypes();

                    foreach (Type type in types)
                    {
                        TypeInfo info = type.GetTypeInfo();
                        if (info.IsInterface || info.IsAbstract) continue;
                        else if (info.GetInterface(pluginType.FullName) != null)
                        {
                            IHSAudioCodecPlugin plugin = (IHSAudioCodecPlugin)Activator.CreateInstance(type);
                            return plugin;
                        }
                    }
                    if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN_OUTPUT"].Replace("{0}", Path));
                }
                else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_NOT_PLUGIN"].Replace("{0}", Path)); }

            }
            catch (Exception ex) { if (ThrowException) throw ex; }

            return null;
        }
#endif
    }
#endif
}
#endif