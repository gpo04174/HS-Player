﻿#if WIN_XP || WIN_VISTA || WIN_10_1703
using HSAudio.Wave.Device;
using HSAudio.Lib.Windows.NAudio.Wave;
using HSAudio.Lib.Windows.NAudio.Wave.Asio;

namespace HSAudio.Wave.Input
{
    public class WaveInputASIO : WaveInput
    {
        public static WaveInputASIO[] GetInputs()
        {
            string[] names = AsioDriver.GetAsioDriverNames();
            WaveInputASIO[] asio = new WaveInputASIO[names.Length];
            for (int i = 0; i < names.Length; i++)
                asio[i] = new WaveInputASIO(AsioDriver.GetAsioDriverByName(names[i]));
            return asio;
        }
        AsioOut asio;
        AsioDriver driver;
        public WaveInputASIO(AsioDriver driver)
        {
            this.driver = driver;
            asio.AudioAvailable += Asio_AudioAvailable;

            int inch, outch;
            driver.GetChannels(out inch, out outch);
            Format = GetFormat((int)driver.GetSampleRate(), (short)inch, driver.GetChannelInfo(0, true).type);
        }

        public WaveFormat Format { get; set; }

        public RecordKind Kind { get { return RecordKind.ASIO; } }

        public WaveDevice Device { get; private set; }

        public bool IsBusy { get; private set; }

        public int Latency { get{ return Format.ConvertByteSizeToLatency(Buffer); }set { } }
        public int Buffer { get { int inl, outl; driver.GetLatencies(out inl, out outl); return inl; } }

        public bool LatencySupport { get { return false; } }

        #region More ASIO
        public int ChannelOffset { get { return asio.InputChannelOffset; } set { asio.InputChannelOffset = value; } }
        #endregion

        public event DataInEventHandler WaveDataIn;
        public event DataInStoppedEventHandler WaveDataInStopped { add { } remove { } }

        public bool IsDisposed { get; private set; }

        public void Start() { init(); asio.Play(); IsBusy = true; }

        public void Stop() { asio.Stop(); IsBusy = false; }
        public void Dispose()
        {
            Device.Dispose();
            try { if (asio != null) asio.Dispose(); } catch { }
            asio = null;
            IsDisposed = true;
        }


        private unsafe void Asio_AudioAvailable(object sender, AsioAudioAvailableEventArgs e)
        {
            if (WaveDataIn != null)
            {
                WaveFormat format = GetFormat((int)Format.SampleRate, e.InputBuffers.Length, e.AsioSampleType);
                int bps = format.Bit / 8;
                int len = e.SamplesPerBuffer;
                int ch = e.InputBuffers.Length;

                byte[] data = new byte[ch * len * bps];
                for (int i = 0; i <= ch; i++)
                {
                    byte* a = (byte*)e.InputBuffers[i].ToPointer();
                    for (int j = 0; j < len; j+=bps)
                        for(int k = 0; k < bps; k++)
                            data[ch * (i + j) + k] = a[j+k];
                }
                WaveDataIn.Invoke(this, new DataInEventArgs(format, data, data.Length));
            }
        }

        internal static WaveFormat GetFormat(int sample, int channel, AsioSampleType type)
        {
            if (type == AsioSampleType.Int16LSB) return new WaveFormat(sample, (short)channel, 16);
            else if (type == AsioSampleType.Int24LSB) return new WaveFormat(sample, (short)channel, 24);
            else if (type == AsioSampleType.Int32LSB) return new WaveFormat(sample, (short)channel, 32);
            else if (type == AsioSampleType.Float32LSB) return new WaveFormat(sample, (short)channel, 32, WaveType.FLOAT);
            else return null;
        }
        private void init()
        {
            asio = new AsioOut(driver);
        }
    }
}
#endif