﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.Core.Sound
{
    public class SoundInfo
    {
        internal CREATESOUNDEXINFO info;
        public SoundInfo() {info = new CREATESOUNDEXINFO();}
        internal SoundInfo(ref CREATESOUNDEXINFO info)
        {
            this.info = info;
        }


        private static int GetSize(ref CREATESOUNDEXINFO info) { return Marshal.SizeOf(info); }

        public static implicit operator CREATESOUNDEXINFO(SoundInfo info)
        { int size = GetSize(ref info.info); info.info.cbsize = size; return info.info; }
        public static implicit operator SoundInfo(CREATESOUNDEXINFO info) { return new SoundInfo(ref info); }
    }
}
