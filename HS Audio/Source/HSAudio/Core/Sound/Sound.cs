﻿using HS.Attributes;
using HSAudio.Core.Audio;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Resource;
using HSAudio.Wave;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using HSAudio.Core.Sound._2D;
using HSAudio.Tag;
using HS;
using HSAudio.Core.Sound._3D;
using HSAudio.Core.Sound.Kind;

namespace HSAudio.Core.Sound
{
    public abstract class Sound : ISettingManager, IDisposable
    {
        protected internal HSAudio Audio;
#if DEBUG
        public FD_Sound sound;
#else
        protected internal FD_Sound sound;
#endif
        protected internal RESULT Result;
        internal OPENSTATE state;
        SoundStatus status = new SoundStatus();

        public Sound(HSAudio Audio)
        {
            this.Audio = Audio;
        }

        #region Properties
        #region Implement
        public SoundStatus Status
        {
            get
            {
                if (sound == null || !sound.isValid()) { status.Clear(); return status; }
                else Result = sound.getOpenState(out state, out status.bufferpercent, out status.starving, out status.diskbusy); return status;
            }
        }

        public virtual uint TotalPosition { get { return getLength(TimeUnit.MilliSecond); } }
        

        /// <summary>
        /// Gets Sound State <para/>
        /// 사운드 상태를 가져옵니다.
        /// </summary>
        public SoundState State
        {
            get
            {
                if (sound == null) return SoundState.Unknown;
                else if (!sound.isValid()) return SoundState.Unload;
                else return SoundStatus.GetStatus(state);
            }
        }

        public bool IsValid { get { return sound != null && sound.isValid(); } }
        #endregion

        #region Abstruct
        [NotNull]
        public abstract WaveFormat Format { get; }
        [NotNull]
        public abstract ISoundFormat Type { get; }
        public abstract SoundTag Tag { get; }
        
        public abstract bool CanReverse { get; }
        public abstract bool CanPosition { get; }
        public abstract bool CanPitch { get; }
        #endregion
        #endregion

        #region Method
        #region Abstruct
        #endregion

        #region Implement
        public uint getLength(TimeUnit unit) { uint output; Result = sound.getLength(out output, (TIMEUNIT)unit); return output; }
        public virtual void Unload()
        {
            if(sound != null) sound.release();
            OnUnloading();
        }
        public abstract bool Load(bool ThrowException = true);
        #endregion

        #region Setting I/O
        /// <summary>
        /// You can use <Sound type>.LoadSetting or Sound.LoadSetting() 
        /// </summary>
        /// <param name="Setting"></param>
        /// <param name="ThrowException"></param>
        /// <returns></returns>
        public virtual bool LoadSetting(Settings Setting, bool ThrowException = false) { if(ThrowException) throw new NotSupportedException(); return false; }
        public virtual Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            s.SubSetting.SetValue("Type", Type.SaveSetting());
            return s;
        }
        #endregion
        #endregion

        public event EventHandler Unloading;
        public event EventHandler Loaded;

        public virtual void Dispose() { try { if (sound != null) sound.release(); sound = null; } catch { } }
        ~Sound() { Dispose(); }

        protected internal WaveFormat getFormat()
        {
            SOUND_TYPE type; SOUND_FORMAT format; int ch; int bit; float freq;
            sound.getFormat(out type, out format, out ch, out bit);
            int priority;
            sound.getDefaults(out freq, out priority);

            WaveType wave = WaveType.PCM;
            if (format == SOUND_FORMAT.PCMFLOAT) wave = WaveType.FLOAT;
            else if (format >= SOUND_FORMAT.PCM8 && format <= SOUND_FORMAT.PCM32) wave = WaveType.PCM;
            else if (format == SOUND_FORMAT.BITSTREAM) wave = WaveType.BIT;
            return new WaveFormat(freq, (short)ch, bit, wave);
        }
        protected internal ISoundFormat getType()
        {
            SOUND_TYPE type; SOUND_FORMAT format; int ch; int bit; float freq;
            sound.getFormat(out type, out format, out ch, out bit);
            switch (type)
            {
                case SOUND_TYPE.MPEG: return new MPEG();
                case SOUND_TYPE.OGGVORBIS: return new OGG();
                case SOUND_TYPE.FLAC: return new FLAC();
                case SOUND_TYPE.ASF: return new ASF();
                case SOUND_TYPE.AIFF: return new AIFF();
                case SOUND_TYPE.MIDI: return new MIDI();
                case SOUND_TYPE.WAV: return new WAVE();
                case SOUND_TYPE.XM: return new XM();
                case SOUND_TYPE.MOD: return new MOD();
                case SOUND_TYPE.IT: return new IT();
                case SOUND_TYPE.FSB: return new FSB();
#if XBOX
                case SOUND_TYPE.XMA: return new XMA();
#endif
                default: return null;
            }
        }
        //public bool getPlayEnable() { return Status == AudioStatus.Buffering || Status == AudioStatus.Loading || Status == AudioStatus.Unknown; }
        //public bool getPauseEnable() { return Status == AudioStatus.Buffering || Status == AudioStatus.Loading || Status == AudioStatus.Unknown; }
        //public bool getStopEnable() { return Status != AudioStatus.Loading || Status != AudioStatus.Unknown; }

        #region Method (Internal)
        protected virtual void OnUnloading()
        {
            if (Unloading != null) try { Unloading(this); } catch { }
            //if (TagChanging != null && TagChanging.Target != null) try { TagChanging.Invoke(this); } catch { }
        }
        protected virtual void OnLoaded()
        {
            if (Loaded != null) try { Loaded(this); } catch { }
        }
        #endregion

        #region Method (Static)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Audio"></param>
        /// <param name="MusicPath"></param>
        /// <param name="LoadMemory"></param>
        /// <param name="Compress"></param>
        /// <param name="Is3D"></param>
        /// <returns></returns>
        public static SoundFile FromFile(HSAudio Audio, string MusicPath, bool Is3D = false) {
            return Is3D ? (SoundFile)new SoundFile3D(Audio, MusicPath) : new SoundFile2D(Audio, MusicPath);}
        public static SoundFile FromFileMemory(HSAudio Audio, string MusicPath, bool Compress = false, bool Is3D = false) {
            return Is3D ? (SoundFile)new SoundFile3D(Audio, MusicPath, true, Compress) : new SoundFile2D(Audio, MusicPath, true, Compress);}
        public static SoundFile FromFile(HSAudio Audio, string MusicPath, ISoundFormat Type, bool Is3D = false) {
            return Is3D ? (SoundFile)new SoundFile3D(Audio, MusicPath, Type) : new SoundFile2D(Audio, MusicPath, Type); }
        public static SoundFile FromFile(HSAudio Audio, string MusicPath, ISoundFormat Type, bool LoadMemory, bool Compress, bool Is3D = false) {
            return Is3D ? (SoundFile)new SoundFile3D(Audio, MusicPath, Type, LoadMemory, Compress) : new SoundFile2D(Audio, MusicPath, Type, LoadMemory, Compress); }

        public static SoundNetwork FromNetwork(HSAudio Audio, string NetworkURL, bool Is3D = false, uint BufferLength = 1024 * 64){
            return Is3D ? (SoundNetwork)new SoundNetwork3D(Audio, NetworkURL, BufferLength) : new SoundNetwork2D(Audio, NetworkURL, BufferLength);}
        public static SoundNetwork FromNetwork(HSAudio Audio, string NetworkURL, ISoundFormat Type, bool Is3D = false, uint BufferLength = 1024 * 64) {
            return Is3D ? (SoundNetwork)new SoundNetwork3D(Audio, NetworkURL, Type, BufferLength) : new SoundNetwork2D(Audio, NetworkURL, Type, BufferLength);}
        
        public static SoundStream FromStream(HSAudio Audio, Stream Stream, RAW raw, bool Is3D){
            bool seek = false;try { seek = Stream.CanSeek; } catch { }
            return Is3D ? (SoundStream)new SoundStream3D(Audio, Stream, raw, seek, false) : new SoundStream2D(Audio, Stream, raw, seek, false); }
        public static SoundStream FromStreamMemory(HSAudio Audio, Stream Stream, RAW raw, bool Is3D){
            bool seek = false; try { seek = Stream.CanSeek; } catch { }
            return Is3D ? (SoundStream)new SoundStream3D(Audio, Stream, raw, seek, true) : new SoundStream2D(Audio, Stream, raw, seek, true); }
        public static SoundStream FromStream(HSAudio Audio, Stream Stream, RAW raw, bool Seekable, bool Is3D){
            return Is3D ? (SoundStream)new SoundStream3D(Audio, Stream, raw, Seekable) : new SoundStream2D(Audio, Stream, raw, Seekable); }
        public static SoundStream FromStreamMemory(HSAudio Audio, Stream Stream, RAW raw, bool Seekable, bool Is3D){
            return Is3D ? (SoundStream)new SoundStream3D(Audio, Stream, raw, true) : new SoundStream2D(Audio, Stream, raw, true); }
        public static SoundStream FromStream(HSAudio Audio, Stream Stream, RAW raw, uint PCMLength, bool Seekable, bool Is3D) {
            return Is3D ? (SoundStream)new SoundStream3D(Audio, Stream, raw, PCMLength, Seekable, false) : new SoundStream2D(Audio, Stream, raw, PCMLength, Seekable, false); }
        public static SoundStream FromStreamMemory(HSAudio Audio, Stream Stream, RAW raw, uint PCMLength, bool Seekable, bool Is3D) {
            return Is3D ? (SoundStream)new SoundStream3D(Audio, Stream, raw, PCMLength, Seekable, true) : new SoundStream2D(Audio, Stream, raw, PCMLength, Seekable, true); }

        public static Sound LoadSetting(HSAudio Audio, Settings Setting, bool ThrowException = false)
        {
            bool Is2D = true;
            if (Setting.Exist("Dimension") && Setting["Dimension"].Value.ToUpper() == "3D") Is2D = false;

            if (Setting.Exist("Type"))
            {
                string type = Setting["Type"];
                if (type == "File") return Is2D ? (Sound)SoundFile2D.LoadSetting(Audio, Setting, ThrowException) : SoundFile3D.LoadSetting(Audio, Setting, ThrowException);
                else if (type == "Network") return Is2D ? (Sound)SoundNetwork2D.LoadSetting(Audio, Setting, ThrowException) : SoundNetwork3D.LoadSetting(Audio, Setting, ThrowException);
                else if (type == "Buffer") return Is2D ? (Sound)SoundBuffer2D.LoadSetting(Audio, Setting, ThrowException) : SoundBuffer3D.LoadSetting(Audio, Setting, ThrowException);
            }
            else if (ThrowException) { throw new HSException(StringResource.Resource["STR_ERR_SETTING_NOTFOUND_TYPE"]); }
            return null;
        }


        internal static void InitFormat(ref CREATESOUNDEXINFO info, WaveFormat format)
        {
            //unsafe { info.cbsize = sizeof(CREATESOUNDEXINFO); }

            if (format.Type == WaveType.FLOAT) info.format = SOUND_FORMAT.PCMFLOAT;
            else if (format.Type == WaveType.BIT) info.format = SOUND_FORMAT.BITSTREAM;
            else if (format.Bit == 8) info.format = SOUND_FORMAT.PCM8;
            else if (format.Bit == 16) info.format = SOUND_FORMAT.PCM16;
            else if (format.Bit == 24) info.format = SOUND_FORMAT.PCM24;
            else if (format.Bit == 32) info.format = SOUND_FORMAT.PCM32;
            else throw new HSException(StringResource.Resource["STR_ERR_WAVE_UNSUPPORT_FORMAT"]);

            info.defaultfrequency = (int)format.SampleRate;
            info.numchannels = format.Channel;
        }
        internal static void InitType(ref CREATESOUNDEXINFO info, SoundType StreamType)
        {
            switch (StreamType)
            {
                case SoundType.MPEG: info.suggestedsoundtype = SOUND_TYPE.MPEG; break;
                case SoundType.OGG: info.suggestedsoundtype = SOUND_TYPE.OGGVORBIS; break;
                case SoundType.MIDI: info.suggestedsoundtype = SOUND_TYPE.MIDI; break;
                case SoundType.FLAC: info.suggestedsoundtype = SOUND_TYPE.FLAC; break;
                case SoundType.AIFF: info.suggestedsoundtype = SOUND_TYPE.AIFF; break;
                case SoundType.ASF: info.suggestedsoundtype = SOUND_TYPE.ASF; break;
                case SoundType.Wave: info.suggestedsoundtype = SOUND_TYPE.WAV; break;
                case SoundType.MOD: info.suggestedsoundtype = SOUND_TYPE.MOD; break;
                case SoundType.XM: info.suggestedsoundtype = SOUND_TYPE.XM; break;
                case SoundType.FSB: info.suggestedsoundtype = SOUND_TYPE.FSB; break;
                case SoundType.Codec: info.suggestedsoundtype = SOUND_TYPE.USER; break;
                case SoundType.RAW: info.suggestedsoundtype = SOUND_TYPE.RAW; break;
                default: info.suggestedsoundtype = SOUND_TYPE.UNKNOWN; break;
            }
        }
        #endregion
    }
}
