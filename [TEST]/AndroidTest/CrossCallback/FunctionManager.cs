﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CrossCallback
{
    public enum DialogResult { OK, Yes, No, Cancel }
    public class FunctionManager
    {
        #region MessageBox
        public enum MessageBoxButton { OK, YesNo, YesNoCancel }
        public delegate Task<DialogResult> ShowMessageBoxCallback(string Message, string Title, MessageBoxButton Button);
        public static ShowMessageBoxCallback ShowMessageBox;
        /*
        public static async Task<DialogResult> OnShowMessageBox(string Message, string Title, MessageBoxButton Button)
        {
            if (OnShowMessageBox == null) return await Task.Run(() => { return DialogResult.Cancel; });
            return await OnShowMessageBox(Message, Title, Button);
        }
        */
        #endregion

        #region ShowAlert
        public delegate void ShowAlertCallback(string Message, bool ShowLong);
        public static ShowAlertCallback ShowAlert;
        //public static void OnShowAlert(string Message, bool ShowLong) { OnShowAlert?.Invoke(Message, ShowLong); }
        #endregion

        #region ShowFileDialog
        public delegate Task<string> ShowFileDialogCallback(params string[] Type);
        public static ShowFileDialogCallback ShowFileDialog;
        //public static async Task<string> OnShowFileDialog(params string[] Type) { return await OnShowFileDialog?.Invoke(Type); }
        #endregion
    }
}
