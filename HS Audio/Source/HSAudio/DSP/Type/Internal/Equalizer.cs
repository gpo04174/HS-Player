﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace HSAudio.DSP.Type
{
    public class Equalizer : IDSP
    {
        private Parameter[] param = new Parameter[3];
        public Equalizer(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.PARAMEQ, "Equalizer", null);//"ParamEqualizer"
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Frequency center.  20.0 to 22000.0.  Default = 8000.0
        /// </summary>
        public float Center
        {
            get { return DSP.GetParameter((int)DSP_PARAMEQ.CENTER, 0f); }
            set { DSP.SetParameter((int)DSP_PARAMEQ.CENTER, value); }
        }
        /// <summary>
        /// Octave range around the center frequency to filter.  0.2 to 5.0.  Default = 1.0
        /// </summary>
        public float BandWidth
        {
            get { return DSP.GetParameter((int)DSP_PARAMEQ.BANDWIDTH, 0f); }
            set { DSP.SetParameter((int)DSP_PARAMEQ.BANDWIDTH, value); }
        }
        /// <summary>
        /// Frequency Gain.  0.05 to 3.0.  Default = 1.0.
        /// </summary>
        public float Gain
        {
            get { return DSP.GetParameter((int)DSP_PARAMEQ.GAIN, 0f); }
            set { DSP.SetParameter((int)DSP_PARAMEQ.GAIN, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Center")) try { Center = float.Parse(Setting["Center"]); } catch(Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("BandWidth")) try { BandWidth = float.Parse(Setting["BandWidth"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Gain")) try { Gain = float.Parse(Setting["Gain"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Center", Center);
            s.SetValue("BandWidth", BandWidth);
            s.SetValue("Gain", Gain);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
