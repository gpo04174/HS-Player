﻿#if PLG_Codec
namespace HSAudio.PlugIn.Codec
{
    public delegate bool OpenCallback();
    public delegate bool CloseCallback();
    public delegate CodecFormat GetFormatCallback(object sender);
}
#endif
