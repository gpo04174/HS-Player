﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class XM : ISoundFormat
    {
        public XM() : base(SoundType.XM, MIME, Extension, Description) {  }

        public static string[] MIME { get { return new string[] { "audio/xm" }; } }
        public static string[] Extension { get { return new string[] { "xm" }; } }
        public static string[] Description { get { return new string[] { "FastTracker 2" }; } }

        public override string ToString() { return Description[0]; }
    }
}
