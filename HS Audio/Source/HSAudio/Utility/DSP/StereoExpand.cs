﻿namespace HSAudio.Utility.DSP
{
    public partial class HSDSP
    {
        #region 스테레오 확장 (StereoExpand)
        static float SQRT_2 = 1.414213562373095f;
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, float Width = 0.5f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - Width);
                S *= 2 * Width;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, float SideWidth = 0.5f, float MidWidth = 0.5f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / 2;   // obtain mid-signal from left and right
                float S = (L - R) / 2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                Output[i] = (M + S) / 2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / 2;   // obtain right signal from mid and side
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, uint N, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0, float Mix = 1)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / 2;   // obtain mid-signal from left and right
                float S = (L - R) / 2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                Output[i] = SetMix(Input[i], (M + S) / 2, Mix);   // obtain left signal from mid and side
                Output[i + 1] = SetMix(Input[i + 1], (M - S) / 2, Mix);   // obtain right signal from mid and side
            }
        }
        internal static unsafe void StereoExpand(float* Input, float* Output, uint N, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0, float WetMix = 1, float DryMix = 1, float Method = 1.414213562373095f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float y = SetMix(0, Input[i], DryMix),
                      y1 = SetMix(0, Input[i + 1], DryMix);
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / Method;   // obtain mid-signal from left and right
                float S = (L - R) / Method;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                float L1 = (M + S) / Method;
                float R1 = (M - S) / Method;
                float x = SetMix(0, L1, WetMix),
                      x1 = SetMix(0, R1, WetMix);
                Output[i] = SetMix(x + y, x + L, DryMix);   // obtain left signal from mid and side
                //Output[i] = SetWetMix2Value(Output[i], L1, R1, 1-DryMix);
                Output[i + 1] = SetMix(x1 + y1, x1 + R, DryMix);   // obtain right signal from mid and side
                //Output[i + 1] = SetWetMix2Value(Output[i + 1], R1, L1, 1-DryMix);
            }
        }
        internal static unsafe void StereoExpand(float* Input, float* Output, uint N, float SideWidth = 0.5f, float MidWidth = 0.5f, float Gain = 0, float Mix = 1, float Method = 1.414213562373095f)
        {
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = (float)Volume(Input[i], Gain);
                float R = (float)Volume(Input[i + 1], Gain);
                float M = (L + R) / Method;   // obtain mid-signal from left and right
                float S = (L - R) / Method;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                S *= 2 * SideWidth;

                float L1 = (M + S) / Method;
                float R1 = (M - S) / Method;

                Output[i] = SetMix(Input[i], L1, Mix);   // obtain left signal from mid and side
                Output[i + 1] = SetMix(Input[i + 1], R1, Mix); // obtain right signal from mid and side
                //Output[i + 1] = SetWetMix2Value(Output[i + 1], R1, L1, 1-DryMix);
            }
        }
        public static unsafe void StereoExpand(float* Input, float* Output, out float[] SideValue, out float[] MidValue, uint N, float Width = 0.5f)
        {
            float[] _MidValue = new float[(N / 2)];
            float[] _SideValue = new float[(N / 2)];
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - Width);
                if (_MidValue.Length < (i / 2)) _MidValue[i / 2] = M;
                S *= 2 * Width;
                if (_SideValue.Length < (i / 2)) _SideValue[i / 2] = S;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
            SideValue = _SideValue;
            MidValue = _MidValue;
        }
        public static unsafe void StereoExpand(float* Input, float* Output, out float[] SideValue, out float[] MidValue, uint N, float SideWidth = 0.5f, float MidWidth = 0.5f)
        {
            float[] _MidValue = new float[(N / 2)];
            float[] _SideValue = new float[(N / 2)];
            //float SQRT_2 = (float)Math.Sqrt(2);
            for (int i = 0; i < N; i += 2)
            {
                float L = Input[i];
                float R = Input[i + 1];
                float M = (L + R) / SQRT_2;   // obtain mid-signal from left and right
                float S = (L - R) / SQRT_2;   // obtain side-signal from left and right

                // amplify mid and side signal seperately:
                M *= 2 * (1 - MidWidth);
                if (_MidValue.Length < (i / 2)) _MidValue[i / 2] = M;
                S *= 2 * SideWidth;
                if (_SideValue.Length < (i / 2)) _SideValue[i / 2] = S;

                Output[i] = (M + S) / SQRT_2;   // obtain left signal from mid and side
                Output[i + 1] = (M - S) / SQRT_2;   // obtain right signal from mid and side
            }
            SideValue = _SideValue;
            MidValue = _MidValue;
        }
        #endregion
    }
}
