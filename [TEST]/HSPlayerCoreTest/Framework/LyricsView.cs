﻿using System;
using System.Drawing;
using System.Windows.Forms;
using HSPlayerCore.Lyrics;

namespace HSPlayerCoreTest
{
    public partial class LyricsView : UserControl
    {
        #region Designer
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLyricsBefore = new System.Windows.Forms.Label();
            this.lblLyricsNext = new System.Windows.Forms.Label();
            this.lblLyricsCurrent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblLyricsBefore
            // 
            this.lblLyricsBefore.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLyricsBefore.Location = new System.Drawing.Point(3, 0);
            this.lblLyricsBefore.Name = "lblLyricsBefore";
            this.lblLyricsBefore.Size = new System.Drawing.Size(311, 23);
            this.lblLyricsBefore.TabIndex = 0;
            this.lblLyricsBefore.Text = "<Before Lyrics>";
            this.lblLyricsBefore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLyricsNext
            // 
            this.lblLyricsNext.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLyricsNext.Location = new System.Drawing.Point(3, 46);
            this.lblLyricsNext.Name = "lblLyricsNext";
            this.lblLyricsNext.Size = new System.Drawing.Size(311, 23);
            this.lblLyricsNext.TabIndex = 1;
            this.lblLyricsNext.Text = "<Next Lyrics>";
            this.lblLyricsNext.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblLyricsCurrent
            // 
            this.lblLyricsCurrent.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblLyricsCurrent.Location = new System.Drawing.Point(3, 23);
            this.lblLyricsCurrent.Name = "lblLyricsCurrent";
            this.lblLyricsCurrent.Size = new System.Drawing.Size(311, 23);
            this.lblLyricsCurrent.TabIndex = 2;
            this.lblLyricsCurrent.Text = "<Current Lyrics>";
            this.lblLyricsCurrent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LyricsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblLyricsCurrent);
            this.Controls.Add(this.lblLyricsNext);
            this.Controls.Add(this.lblLyricsBefore);
            this.Name = "LyricsView";
            this.Size = new System.Drawing.Size(317, 180);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblLyricsBefore;
        private System.Windows.Forms.Label lblLyricsNext;
        private System.Windows.Forms.Label lblLyricsCurrent;
        #endregion

        public LyricsManager Manager { get; set; }
        public LyricsView()
        {
            InitializeComponent();
            Resize += UpdateSize;
            Load += LyricsView_Load;
        }

        public LyricsView(LyricsManager Manager) : this()
        {
            this.Manager = Manager;
        }

        private void LyricsView_Load(object sender, EventArgs e) { UpdateSize(sender, e); }
        public void UpdateSize(object sender, EventArgs e)
        {
            int h = Height / 3;
            lblLyricsBefore.Location = new Point(0, 0);
            lblLyricsBefore.Size = new Size(this.Width, h);
            lblLyricsCurrent.Location = new Point(0, h);
            lblLyricsCurrent.Size = new Size(this.Width, h);
            lblLyricsNext.Location = new Point(0, h * 2);
            lblLyricsNext.Size = new Size(this.Width, h);
        }
        public void RefreshTime(double time)
        {
            if (Manager == null) lblLyricsBefore.Text = lblLyricsCurrent.Text = lblLyricsNext.Text = "";
            else
            {
                Manager.Refresh(time);
                lblLyricsBefore.Text = Manager.PreviousLyrics;
                lblLyricsCurrent.Text = Manager.CurrentLyrics;
                lblLyricsNext.Text = Manager.NextLyrics;
            }
        }
    }
}
