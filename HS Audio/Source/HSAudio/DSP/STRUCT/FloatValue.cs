﻿using System;
using HSAudio.Lib.FMOD;
using HS.Setting;

namespace HSAudio.DSP
{
    public class FloatValue : ISettingManager
    {
        internal FloatValue(DSP_PARAMETER_DESC_FLOAT desc) { this.Value = desc.defaultval; Maximum = desc.max; Minimum = desc.min; }
        public FloatValue(Settings setting) { LoadSetting(setting); }
        public FloatValue(float Value, float Min, float Max) { this.Value = Value; this.Minimum = Min; this.Maximum = Max; }
        public float Value { get; private set; }

        public float Maximum { get; private set; }
        public float Minimum { get; private set; }

        public static implicit operator float(FloatValue param) { return param.Value; }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Value", Value);
            s.SetValue("Minimum", Minimum);
            s.SetValue("Maximum", Maximum);
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Value")) Value = Convert.ToSingle(Setting["Value"]);
            if (Setting.Exist("Maximum")) Maximum = Convert.ToSingle(Setting["Maximum"]);
            if (Setting.Exist("Minimum")) Value = Convert.ToSingle(Setting["Minimum"]);
            return true;
        }
    }
}
