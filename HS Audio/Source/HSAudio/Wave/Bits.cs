﻿namespace HSAudio.Wave
{
    //public enum Bits : int { Unknown = 0, PCM8 = 8, PCM16 = 16, PCM24 = 24, PCM32 = 32, PCM64 = 64, PCMFLOAT = 33, PCMFLOAT64 = 65 }//, DSD = 1
    public enum Endien {Little, Middle, Big }
    public enum Aligment : int{ A8 = 8, A16 = 16, A18 = 18, A24 = 24, A32 = 32  }
}
