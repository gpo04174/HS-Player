﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml;

namespace HSAudio.DSP
{
    /// <summary>
    /// 파라미터 값
    /// </summary>
#if !SIMPLE
    [Serializable]
    public class Parameter : ISettingManager
    {
        internal Parameter(Settings Setting) { LoadSetting(Setting); }

        /// <summary>
        /// DSP구조체의 인스턴스를 초기화합니다.
        /// </summary>
        /// <param name="Index">DSP_TYPE에 대한 각 값의 인덱스 입니다. (만약 DSP_TYPE 를 PARAMEQ 로 줬다면 60HZ의 설정은 (DSP_PARAMEQ.BANDWIDTH, 60) 형태로 입력하면 됩니다.)</param>
        /// <param name="Value">인덱스에 대한 값 입니다.</param>=
        //internal Parameter(float Value, string Name, string Label, string Description) { this.Type = Type; this.Value_Float = Value; }
        //internal Parameter(int Value, string Name, string Label, string Description) { this.Type = Type; this.Value_Int = Value; }
        //internal Parameter(bool Value, string Name, string Label, string Description) { this.Type = Type; this.Value_Bool = Value; }
        //internal Parameter(IntPtr Value, uint Length, string Name, string Label, string Description) { this.Type = Type; this.Value_Data = Value; Value_Data_Length = Length; }
        internal Parameter(DSP_PARAMETER_DESC desc)
        {
            this.desc = desc;
            Name = new string(desc.name);
            Label = new string(desc.label);
            Description = desc.description;
            Type = (DSPParameterType) desc.type;
        }

        public float Value_Float;
        public int Value_Int;
        public bool Value_Bool;

        public DataValue Value_Data;

        public DSPParameterType Type { get; internal set; }
        public string Name { get; internal set; }
        public string Label { get; internal set; }
        public string Description { get; internal set; }

        
        private DSP_PARAMETER_DESC desc;

        internal DSP_PARAMETER_DESC Desc
        {
            get { return desc; }
        }

        #region Setter
        public Parameter SetValue(float Value) { Value_Float = Value; return this; }
        public Parameter SetValue(int Value) { Value_Int = Value; return this; }
        public Parameter SetValue(bool Value) { Value_Bool = Value; return this; }
        public Parameter SetValue(IntPtr Value, uint Length) { Value_Data = Value; Value_Data_Length = Length; return this; }
        #endregion

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Type", new SettingsData(Type.ToString()));
            s.SetValue("Name", new SettingsData(Name, false));
            s.SetValue("Label", new SettingsData(Label, false));
            s.SetValue("Description", new SettingsData(Description, false));
            if(Type == DSPParameterType.Data) s.SubSetting.SetValue("Value", Value_Data.SaveSetting());
            else s.SetValue("Value", new SettingsData(convert(), false));
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            DSPParameterType t = DSPParameterType.Data;
            if (Setting.Exist("Type")) t = (DSPParameterType)Enum.Parse(typeof(DSPParameterType), Setting["Type"]);
            if (Setting.Exist("Name")) Name = Setting["Name"];
            if (Setting.Exist("Label")) Name = Setting["Label"];
            if (Setting.Exist("Description")) Name = Setting["Description"];
            if (Setting.Exist("Value")) parse(t, Setting["Value"]);
            else if (Setting.SubSetting.Exist("Value")) Value_Data = new DataValue(Setting.SubSetting["Value"]);
            return true;
        }
        
        unsafe void parse(DSPParameterType Type, string value)
        {
            if (Type == DSPParameterType.Float) Value_Float = Convert.ToSingle(value);
            else if (Type == DSPParameterType.Int) Value_Int = Convert.ToInt32(value);
            else if (Type == DSPParameterType.Bool) Value_Bool = Convert.ToBoolean(value);
        }
        unsafe string convert()
        {
            if (Type == DSPParameterType.Float) return Value_Float.ToString();
            else if (Type == DSPParameterType.Int) return Value_Int.ToString();
            else return null;
        }

        ~Parameter() { if (Value_Data != null) Value_Data.Dispose(); }

        public static implicit operator DataValue(Parameter param) { return param.Value_Data; }
        public static implicit operator float(Parameter param) { return param.Value_Float; }
        public static implicit operator int(Parameter param) { return param.Value_Int; }
        public static implicit operator bool(Parameter param) { return param.Value_Bool; }
    }
#else
    [Serializable]
    public class Parameter : ISettingManager
    {
        internal Parameter(Settings Setting) { LoadSetting(Setting); }
        /// <summary>
        /// DSP구조체의 인스턴스를 초기화합니다.
        /// </summary>
        /// <param name="Index">DSP_TYPE에 대한 각 값의 인덱스 입니다. (만약 DSP_TYPE 를 PARAMEQ 로 줬다면 60HZ의 설정은 (DSP_PARAMEQ.BANDWIDTH, 60) 형태로 입력하면 됩니다.)</param>
        /// <param name="Value">인덱스에 대한 값 입니다.</param>
        public Parameter(DSPParameterType Type, int Index, object Value) { this.Type = Type; this.Index = Index; this.Value = Value; }
        public Parameter(DSPParameterType Type, int Index, object Value, string Name, string Label, string Description) { this.Type = Type; this.Index = Index; this.Value = Value; }
        internal Parameter(int Index, DSP_PARAMETER_DESC desc) { this.Index = Index; Name = new string(desc.name); Label = new string(desc.label); Description = desc.description; Type = (DSPParameterType)desc.type; }
        /// <summary>
        /// DSP_TYPE에 대한 각 값의 인덱스 입니다.
        /// </summary>
        public int Index;
        /// <summary>
        /// 인덱스에 대한 값 입니다.
        /// </summary>
        public object Value;

        public DSPParameterType Type { get; internal set; }
        public string Name { get; internal set; }
        public string Label { get; internal set; }
        public string Description { get; internal set; }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Index", new SettingsData(Index.ToString()));
            s.SetValue("Type", new SettingsData(Type.ToString()));
            s.SetValue("Name", new SettingsData(Name, false));
            s.SetValue("Label", new SettingsData(Label, false));
            s.SetValue("Description", new SettingsData(Description, false));
            s.SetValue("Value", new SettingsData(Forge(Type, Value), false));
            return s;
        }

        public bool LoadSetting(Setting.Settings Setting, bool ThrowException = false)
        {
            DSPParameterType t = DSPParameterType.Data;
            if (Setting.Exist("Index")) Index = Convert.ToInt32(Setting["Index"].Value);
            if (Setting.Exist("Type")) t = (DSPParameterType)Enum.Parse(typeof(DSPParameterType), Setting["Type"]);
            if (Setting.Exist("Name")) Name = Setting["Name"];
            if (Setting.Exist("Label")) Name = Setting["Label"];
            if (Setting.Exist("Description")) Name = Setting["Description"];
            if (Setting.Exist("Value")) Value = convert(t, Setting["Value"]);
            return true;
        }

        static object convert(DSPParameterType Type, string value)
        {
            if (Type == DSPParameterType.Float) return Convert.ToSingle(value);
            else if (Type == DSPParameterType.Int) return Convert.ToInt32(value);
            else if (Type == DSPParameterType.Bool) return Convert.ToBoolean(value);
            else
            {
                string[] val = value.Split(' ');
                byte[] data = new byte[val.Length];
                for (int i = 0; i < val.Length; i++)
                    if (!string.IsNullOrEmpty(val[i])) data[i] = Convert.ToByte(val[i]);
                return data;
            }
        }
        static string Forge(DSPParameterType Type, object value)
        {
            if (Type == DSPParameterType.Data)
            {
                byte[] data = (byte[])value;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                    sb.AppendFormat("{0:X2} ", data[i]);
                return sb.ToString();
            }
            else return value.ToString();
        }
    }
#endif
}
