﻿using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.PlugIn.DSP
{
    public class Should_I_ProcesEventArgs : PluginDSPEventArgs
    {
        protected internal Should_I_ProcesEventArgs(ref DSP_STATE dsp_state, bool inputsidle, uint length, CHANNELMASK inmask, int inchannels, SPEAKERMODE speakermode)
        {
            State = dsp_state;
            InputsIdle = inputsidle;
            Length = length;
            InMask = inmask;
            SpeakerMode = speakermode;
        }
        protected internal DSP_STATE State { get; private set; }
        public bool InputsIdle { get; private set; }
        public uint Length { get; private set; }
        public CHANNELMASK InMask { get; private set; }
        public int InChannels { get; private set; }
        public SPEAKERMODE SpeakerMode { get; private set; }
    }
}
