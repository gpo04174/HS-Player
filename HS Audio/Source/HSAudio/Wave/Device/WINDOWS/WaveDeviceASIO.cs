﻿#if WIN_XP || WIN_VISTA || WIN_10_1703
using HSAudio.Control.Device;
using HSAudio.Lib.Windows.NAudio.Wave.Asio;
using System;
using System.Collections.Generic;

namespace HSAudio.Wave.Device
{
    public class WaveDeviceASIO : WaveDevice
    {
        AsioDriver driver;
        internal WaveDeviceASIO(AsioDriver driver, DeviceKind Kind, bool PreLoad = false)
        {
            Device = driver;
            DeviceName = driver.GetDriverName();
            this.Kind = Kind;

            int inch, outch;
            driver.GetChannels(out inch, out outch);
            short ch = (short)(Kind == DeviceKind.Input ? inch : outch);
            
            List<WaveFormat> fm = new List<WaveFormat>();
            for (int i = 0; i < WaveFormat.Samples.Length; i++)
                if (driver.CanSampleRate(WaveFormat.Samples[i])){ fm.Add(Input.WaveInputASIO.GetFormat(WaveFormat.Samples[i], ch, driver.GetChannelInfo(0, true).type));}
            //if (PreLoad) GetAvailableFormat();
            //else Formats = new WaveFormat[] { new WaveFormat((int)driver.GetSampleRate(), ch, Bits.PCM16) };
        }

        public string DeviceName { get; private set; }

        public object Device { get; private set; }

        public DeviceKind Kind { get; private set; }

        public WaveFormat[] Formats { get; private set; }

        public DeviceControl MasterControl { get { return null; } }

        public DeviceControl[] SubControls { get { return null; } }

        public bool IsDisposed { get; internal set; }

        [Obsolete]
        public event DevicePropertyChangedEventHandler DevicePropertyChanged { add { } remove { } }

        public void showControlPanel() { driver.ControlPanel(); }
        public void GetAvailableFormat()
        {
            int inch, outch;
            driver.GetChannels(out inch, out outch);
            short ch = (short)(Kind == DeviceKind.Input ? inch : outch);
            int sample = (int)driver.GetSampleRate();

            List<WaveFormat> f = new List<WaveFormat>();
            for (int i = 0; i < WaveFormat.Samples.Length; i++) try
            {
                if (Kind == DeviceKind.Input) driver.CanSampleRate(WaveFormat.Samples[i]);
            }
            catch { }
            //finally { asio.Dispose(); }
        }
        public void Dispose()
        {
            IsDisposed = true;
            driver.DisposeBuffers();
            driver.ReleaseComAsioDriver();
        }
    }
}
#endif