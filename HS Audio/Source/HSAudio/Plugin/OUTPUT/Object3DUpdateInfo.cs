﻿using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.PlugIn.Output
{
    public class Object3DUpdateInfo
    {
        internal Object3DUpdateInfo(ref OUTPUT_STATE state, IntPtr object3d, ref OUTPUT_OBJECT3DINFO info)
        {
            this.State = state;
            this.Object3D = object3d;

            this.Buffer = info.buffer;
            this.BufferLength = info.bufferlength;
            this.Position = info.position;
            this.Gain = info.gain;
            this.Spread = info.spread;
            this.Priority = info.priority;
        }

        internal OUTPUT_STATE State { get; set; }
        /// <summary>
        /// Read Only - Pointer to native 3D audio object.
        /// </summary>
        public IntPtr Object3D { get; private set; }

        #region OUTPUT_OBJECT3DINFO
        /// <summary>
        /// Read Only - Mono PCM floating point buffer. This buffer needs to be scaled by the gain value to get distance attenuation.
        /// </summary>
        public IntPtr Buffer { get; private set; }
        /// <summary>
        /// Read Only - Length in PCM samples of buffer.
        /// </summary>
        public uint BufferLength { get; private set; }
        /// <summary>
        /// Read Only - Vector3D relative between object and listener.
        /// </summary>
        public Vector3D Position { get; private set; }
        /// <summary>
        /// Read Only - 0.0 to 1.0 - 1 = 'buffer' is not attenuated, 0 = 'buffer' is fully attenuated.
        /// </summary>
        public float Gain { get; private set; }
        /// <summary>
        /// Read Only - 0 - 360 degrees.  0 = point source, 360 = sound is spread around all speakers
        /// </summary>
        public float Spread { get; private set; }
        /// <summary>
        /// Read Only - 0.0 to 1.0 - 0 = most important, 1 = least important. Based on height and distance (height is more important).
        /// </summary>
        public float Priority { get; private set; }
        #endregion
    }
}
