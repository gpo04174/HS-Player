﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;
using HS.Setting;

namespace HSAudio.DSP.Type
{
    public class Flanger : IDSP
    {
        private Parameter[] param = new Parameter[3];
        public Flanger(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.FLANGE, "Flanger", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Percentage of wet signal in mix.  0 to 100. Default = 50.
        /// </summary>
        public float Mix
        {
            get { return DSP.GetParameter((int)DSP_FLANGE.MIX, 0f); }
            set { DSP.SetParameter((int)DSP_FLANGE.MIX, value); }
        }
        /// <summary>
        /// lange depth (percentage of 40ms delay).  0.01 to 1.0.  Default = 1.0
        /// </summary>
        public float Depth
        {
            get { return DSP.GetParameter((int)DSP_FLANGE.DEPTH, 0f); }
            set { DSP.SetParameter((int)DSP_FLANGE.DEPTH, value); }
        }
        /// <summary>
        /// Flange speed in hz.  0.0 to 20.0.  Default = 0.1
        /// </summary>
        public float Rate
        {
            get { return DSP.GetParameter((int)DSP_FLANGE.RATE, 0f); }
            set { DSP.SetParameter((int)DSP_FLANGE.RATE, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Mix")) try { Mix = float.Parse(Setting["Mix"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Depth")) try { Depth = float.Parse(Setting["Depth"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.Exist("Rate")) try { Rate = float.Parse(Setting["Rate"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Mix", Mix);
            s.SetValue("Depth", Depth);
            s.SetValue("Rate", Rate);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
