﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Tag
{
    public class TagData
    {
        public TagData(string Name, string Data) { this.Name = Name; this.Data = Data; if (Data != null) Length = Data.Length; Type = TagDataType.String; }
        public TagData(string Name, byte[] Data) { this.Name = Name; this.Data = Data; if (Data != null) Length = Data.Length; Type = TagDataType.ByteArray; }
        public TagData(string Name, int Data) : this(Name, Data, 1, TagDataType.Decimal) { }
        public TagData(string Name, float Data) : this(Name, Data, 1, TagDataType.Decimal) { }
        public TagData(string Name, double Data) : this(Name, Data, 1, TagDataType.Decimal) { }
        public TagData(string Name, decimal Data) : this(Name, Data, 1, TagDataType.Decimal) { }
        public TagData(string Name, bool Data) : this(Name, Data, 1, TagDataType.Boolean) { }
        public TagData(string Name, object Data, int Length, TagDataType Type) { this.Name = Name; this.Data = Data; this.Length = Length; this.Type = Type; }

        public string Name { get; set; }
        public object Data { get; set; }
        public int Length { get; set; }
        public TagDataType Type { get; set; }

        /*
        public static implicit operator TagData(string Data) { return new TagData(Data); }
        public static implicit operator TagData(byte[] Data) { return new TagData(Data); }
        public static implicit operator TagData(int Data) { return new TagData(Data); }
        public static implicit operator TagData(float Data) { return new TagData(Data); }
        public static implicit operator TagData(double Data) { return new TagData(Data); }
        public static implicit operator TagData(decimal Data) { return new TagData(Data); }
        public static implicit operator TagData(bool Data) { return new TagData(Data); }
        */

        public static implicit operator string(TagData Data) { return (string)Data.Data; }
        public static implicit operator byte[] (TagData Data) { return (byte[])Data.Data; }
        public static implicit operator int(TagData Data) { return Convert.ToInt32(Data.Data); }
        public static implicit operator float(TagData Data) { return Convert.ToSingle(Data.Data); }
        public static implicit operator double(TagData Data) { return Convert.ToInt32(Data.Data); }
        public static implicit operator decimal(TagData Data) { return Convert.ToDecimal(Data.Data); }
        public static implicit operator bool(TagData Data) { return Convert.ToBoolean(Data.Data); }
    }
}
