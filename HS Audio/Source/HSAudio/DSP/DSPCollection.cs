﻿using HSAudio.Core.Sound;
using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using System.Collections;
using System.Collections.Generic;
using HSAudio.Core.Audio;

namespace HSAudio.DSP
{
    public class DSPCollection : ISettingManager, IEnumerable, IList<IDSP>
    {
        internal List<IDSP> list = new List<IDSP>();
        internal RESULT Result;
        FD_ChannelGroup group;
        FD_Channel channel;
        //Audio audio;
        //AudioCollection audiocol;
        internal DSPCollection(FD_ChannelGroup group) { this.group = group; }
        internal void Reload(FD_ChannelGroup group)
        {
            this.group = group;
            for(int i = 0; i < Count; i++)
                Result = group == null ? channel.addDSP(i, list[i].DSP.DSPInternal) : group.addDSP(i, list[i].DSP.DSPInternal);
        }
        internal void Unload()
        {
            for (int i = 0; i < Count; i++)
                Result = group == null ? channel.removeDSP(list[i].DSP.DSPInternal) : group.removeDSP(list[i].DSP.DSPInternal);
        }

        public int Count { get { return list.Count; } }

        public bool IsReadOnly { get { return false; } }

        public IDSP this[int index]
        {
            get { return list[index]; }
            set { Insert(index, value); }
        }

        public void Add(IDSP DSP)
        {
            if (IndexOf(DSP) < 0)
            {
                Result = group == null ? channel.addDSP(Count, DSP.DSP.DSPInternal) : group.addDSP(Count, DSP.DSP.DSPInternal);
                list.Add(DSP);
            }
        }
        public void Remove(IDSP DSP)
        {
            if (IndexOf(DSP) < 0)
            {
                Result = group == null ? channel.removeDSP(DSP.DSP.DSPInternal) : group.removeDSP(DSP.DSP.DSPInternal);
                list.Remove(DSP);
            }
        }
        private void Remove(IDSP DSP, bool RemoveDSP)
        {
            if (IndexOf(DSP) < 0)
            {
                if (RemoveDSP) Result = group == null ? channel.removeDSP(DSP.DSP.DSPInternal) : group.removeDSP(DSP.DSP.DSPInternal);
                list.Remove(DSP);
            }
        }
        public void RemoveAt(int index)
        {
            Result = group == null ? channel.removeDSP(list[index].DSP.DSPInternal) : group.removeDSP(list[index].DSP.DSPInternal);
            list.RemoveAt(index);
        }

        public void Insert(int index, IDSP item)
        {
            Result = group == null ? channel.setDSPIndex(item.DSP.DSPInternal, index) : group.setDSPIndex(item.DSP.DSPInternal, index);
            Remove(item, false);
            list.Insert(index, item);
        }

        public IEnumerator GetEnumerator() {return new DSPEnumerator(this); }

        public int IndexOf(IDSP item) {return list.IndexOf(item); }

        public void Clear(){ for (int i = Count - 1; i >= 0; i--) RemoveAt(i);}

        public bool Contains(IDSP item) {return list.Contains(item); }

        public void CopyTo(IDSP[] array, int arrayIndex) {list.CopyTo(array, arrayIndex);}

        bool ICollection<IDSP>.Remove(IDSP item)
        {
            Result = group == null ? channel.removeDSP(item.DSP.DSPInternal) : group.removeDSP(item.DSP.DSPInternal);
            list.Remove(item);
            return Result == RESULT.OK;
        }

        IEnumerator<IDSP> IEnumerable<IDSP>.GetEnumerator() { return list.GetEnumerator();}



        [Obsolete]
        public IDSP[] GetDSP()
        {
            int num;
            if(group == null) channel.getNumDSPs(out num);
            else group.getNumDSPs(out num);

            IDSP[] dsps = new IDSP[num];
            for (int i = 0; i < num; i++)
            {
                for (int j = 0; j < list.Count; i++)
                {
                    FD_DSP dsp;
                    if (group == null) channel.getDSP(i, out dsp);
                    else group.getDSP(i, out dsp);

                    if (list[i].DSP.DSPInternal.Equals(dsp)) dsps[i] = list[j];
                }
            }
            return dsps;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            for (int i = 0; i < Count; i++)
            {
                Settings s1 = list[i].SaveSetting();
                s1.SetValue("Type", list[i].GetType().ToString());
                s.SubSetting.SetValue((i + 1).ToString(), s1);
            }
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            int cnt = Setting.SubSetting.Count;
            for (int i = 1; i <= cnt; i++)
            {
                Settings sub = Setting.SubSetting[(i + 1).ToString()];
                if(sub.Exist("Type"))
                {
                    string type = sub["Type"];
                    if (type == "ParamEqualizer")
                    {

                    }
                }
            }
            return true;
        }

        string[] DSPList = { "DSPPlugin" };

        internal void Dispose()
        {
            for(int i = Count - 1; i >= 0; i--)
            {
                try
                {
                    list[i].Dispose();
                    RemoveAt(i);
                } catch { }
            }
        }
        ~DSPCollection() { Dispose(); }
    }

    public class DSPEnumerator : IEnumerator, IEnumerator<IDSP>
    {
        DSPCollection col;
        internal DSPEnumerator(DSPCollection col) {this.col = col; }

        int now = -1;

        public object Current { get { return col[now]; } }

        IDSP IEnumerator<IDSP>.Current { get { return col[now]; } }

        public bool MoveNext()
        {
            if(now < col.Count) { now++; return true; }
            else return false;
        }

        public void Reset() {now = -1; }

        public void Dispose() { Reset(); }
    }
}
