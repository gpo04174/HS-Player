﻿using HSAudio.Core;
using HSAudio.Core.Audio;
using HSAudio.Core.Audio._2D;
using HSAudio.Core.Sound;
using System;
using System.Windows.Forms;

namespace HSAudioTest.Test.GUI
{
    public delegate void PlayingProgressEventHandler(object sender, uint Millisecond);
    public partial class PlayControl : UserControl
    {
        #region Designer        
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblMusicVol = new System.Windows.Forms.Label();
            this.tbMusicVolume = new System.Windows.Forms.TrackBar();
            this.chkLoop = new System.Windows.Forms.CheckBox();
            this.lblMusicPos = new System.Windows.Forms.Label();
            this.tbMusicPos = new System.Windows.Forms.TrackBar();
            this.btnMusicStop = new System.Windows.Forms.Button();
            this.btnMusicPlay = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cbReverb = new System.Windows.Forms.ComboBox();
            this.chkReverb3D = new System.Windows.Forms.CheckBox();
            this.tbReverbLevel = new System.Windows.Forms.TrackBar();
            this.lblReverbLevel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tbMusicVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMusicPos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbReverbLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMusicVol
            // 
            this.lblMusicVol.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMusicVol.AutoSize = true;
            this.lblMusicVol.Location = new System.Drawing.Point(344, 72);
            this.lblMusicVol.Name = "lblMusicVol";
            this.lblMusicVol.Size = new System.Drawing.Size(23, 12);
            this.lblMusicVol.TabIndex = 16;
            this.lblMusicVol.Text = "100";
            // 
            // tbMusicVolume
            // 
            this.tbMusicVolume.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMusicVolume.AutoSize = false;
            this.tbMusicVolume.Location = new System.Drawing.Point(236, 68);
            this.tbMusicVolume.Maximum = 150;
            this.tbMusicVolume.Name = "tbMusicVolume";
            this.tbMusicVolume.Size = new System.Drawing.Size(104, 20);
            this.tbMusicVolume.TabIndex = 15;
            this.tbMusicVolume.TickFrequency = 10;
            this.tbMusicVolume.Value = 100;
            this.tbMusicVolume.Scroll += new System.EventHandler(this.tbMusicVolume_Scroll);
            // 
            // chkLoop
            // 
            this.chkLoop.AutoSize = true;
            this.chkLoop.Location = new System.Drawing.Point(107, 7);
            this.chkLoop.Name = "chkLoop";
            this.chkLoop.Size = new System.Drawing.Size(52, 16);
            this.chkLoop.TabIndex = 14;
            this.chkLoop.Text = "Loop";
            this.chkLoop.UseVisualStyleBackColor = true;
            this.chkLoop.CheckedChanged += new System.EventHandler(this.chkLoop_CheckedChanged);
            // 
            // lblMusicPos
            // 
            this.lblMusicPos.AutoSize = true;
            this.lblMusicPos.Location = new System.Drawing.Point(3, 73);
            this.lblMusicPos.Name = "lblMusicPos";
            this.lblMusicPos.Size = new System.Drawing.Size(151, 12);
            this.lblMusicPos.TabIndex = 13;
            this.lblMusicPos.Text = "00:00:00.000 / 00:00:00.000";
            // 
            // tbMusicPos
            // 
            this.tbMusicPos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMusicPos.AutoSize = false;
            this.tbMusicPos.Location = new System.Drawing.Point(3, 34);
            this.tbMusicPos.Name = "tbMusicPos";
            this.tbMusicPos.Size = new System.Drawing.Size(360, 31);
            this.tbMusicPos.TabIndex = 12;
            this.tbMusicPos.Scroll += new System.EventHandler(this.tbMusicPos_Scroll);
            // 
            // btnMusicStop
            // 
            this.btnMusicStop.Location = new System.Drawing.Point(59, 3);
            this.btnMusicStop.Name = "btnMusicStop";
            this.btnMusicStop.Size = new System.Drawing.Size(42, 23);
            this.btnMusicStop.TabIndex = 11;
            this.btnMusicStop.Text = "Stop";
            this.btnMusicStop.UseVisualStyleBackColor = true;
            this.btnMusicStop.Click += new System.EventHandler(this.btnMusicStop_Click);
            // 
            // btnMusicPlay
            // 
            this.btnMusicPlay.Location = new System.Drawing.Point(3, 3);
            this.btnMusicPlay.Name = "btnMusicPlay";
            this.btnMusicPlay.Size = new System.Drawing.Size(50, 23);
            this.btnMusicPlay.TabIndex = 10;
            this.btnMusicPlay.Text = "Play";
            this.btnMusicPlay.UseVisualStyleBackColor = true;
            this.btnMusicPlay.Click += new System.EventHandler(this.btnMusicPlay_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cbReverb
            // 
            this.cbReverb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbReverb.FormattingEnabled = true;
            this.cbReverb.Location = new System.Drawing.Point(3, 97);
            this.cbReverb.Name = "cbReverb";
            this.cbReverb.Size = new System.Drawing.Size(186, 20);
            this.cbReverb.TabIndex = 17;
            this.cbReverb.SelectedIndexChanged += new System.EventHandler(this.cbReverb_SelectedIndexChanged);
            // 
            // chkReverb3D
            // 
            this.chkReverb3D.AutoSize = true;
            this.chkReverb3D.Location = new System.Drawing.Point(195, 99);
            this.chkReverb3D.Name = "chkReverb3D";
            this.chkReverb3D.Size = new System.Drawing.Size(38, 16);
            this.chkReverb3D.TabIndex = 18;
            this.chkReverb3D.Text = "3D";
            this.chkReverb3D.UseVisualStyleBackColor = true;
            this.chkReverb3D.CheckedChanged += new System.EventHandler(this.chkReverb3D_CheckedChanged);
            // 
            // tbReverbLevel
            // 
            this.tbReverbLevel.AutoSize = false;
            this.tbReverbLevel.Location = new System.Drawing.Point(236, 97);
            this.tbReverbLevel.Maximum = 200;
            this.tbReverbLevel.Minimum = -100;
            this.tbReverbLevel.Name = "tbReverbLevel";
            this.tbReverbLevel.Size = new System.Drawing.Size(104, 20);
            this.tbReverbLevel.TabIndex = 19;
            this.tbReverbLevel.TickFrequency = 10;
            this.tbReverbLevel.Scroll += new System.EventHandler(this.tbReverbLevel_Scroll);
            // 
            // lblReverbLevel
            // 
            this.lblReverbLevel.AutoSize = true;
            this.lblReverbLevel.Location = new System.Drawing.Point(344, 100);
            this.lblReverbLevel.Name = "lblReverbLevel";
            this.lblReverbLevel.Size = new System.Drawing.Size(11, 12);
            this.lblReverbLevel.TabIndex = 20;
            this.lblReverbLevel.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(269, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(98, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "Matrix (Sound)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // PlayControl
            // 
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblReverbLevel);
            this.Controls.Add(this.tbReverbLevel);
            this.Controls.Add(this.chkReverb3D);
            this.Controls.Add(this.cbReverb);
            this.Controls.Add(this.lblMusicVol);
            this.Controls.Add(this.tbMusicVolume);
            this.Controls.Add(this.chkLoop);
            this.Controls.Add(this.lblMusicPos);
            this.Controls.Add(this.tbMusicPos);
            this.Controls.Add(this.btnMusicStop);
            this.Controls.Add(this.btnMusicPlay);
            this.Name = "PlayControl";
            this.Size = new System.Drawing.Size(372, 125);
            ((System.ComponentModel.ISupportInitialize)(this.tbMusicVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMusicPos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbReverbLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMusicVol;
        private System.Windows.Forms.TrackBar tbMusicVolume;
        private System.Windows.Forms.CheckBox chkLoop;
        private System.Windows.Forms.Label lblMusicPos;
        private System.Windows.Forms.TrackBar tbMusicPos;
        private System.Windows.Forms.Button btnMusicStop;
        private System.Windows.Forms.Button btnMusicPlay;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cbReverb;
        private System.Windows.Forms.CheckBox chkReverb3D;
        private System.Windows.Forms.TrackBar tbReverbLevel;
        private System.Windows.Forms.Label lblReverbLevel;
        private System.Windows.Forms.Button button1;
        #endregion

        public event PlayingProgressEventHandler PlayingProgress;
        public PlayControl()
        {
            InitializeComponent();
            RefreshReverb();
        }
        public PlayControl(HSAudio.HSAudio Audio)
        {
            InitializeComponent();
            this.Audio = Audio;
            RefreshReverb();
        }

        HSAudio.HSAudio _Audio;
        public HSAudio.HSAudio Audio { get { return _Audio; } set { _Audio = value; cbReverb_SelectedIndexChanged(null, null); } }
        public Audio audio { get; set; }
        
        public static string GetTime(uint tick)
        {
            uint h, m, s, ms;
            ms = tick % 1000;
            tick = tick / 1000;
            h = tick / 3600;
            tick = tick % 3600;
            m = tick / 60;
            s = tick % 60;
            return string.Format("{0:00}:{1:00}:{2:00}.{3:000}", h, m, s, ms);
        }

        public new void Load(string MusicPath)
        {
            Sound s = Sound.FromFile(Audio, MusicPath);
            if(audio != null)
            {
                Sound s1 = audio.Sound;
                try { if (audio.setSound(s, true)) s1.Unload(); }
                catch(Exception ex) { MessageBox.Show(ex.Message); }
            }
            else
            {
                if (Audio.Audio.Count > 0) Audio.Audio.Remove(audio);
                audio = Audio.Audio.Add(s) as Audio2D;
            }
            //else audio.setSound(s);

            audio.Loop = chkLoop.Checked ? LoopKind.Normal : LoopKind.Off;
            audio.Volume = tbMusicVolume.Value / 100f;
            audio.StatusChanged += Audio_StatusChanged;
            tbMusicPos.Maximum = (int)s.TotalPosition;
            tbMusicPos.Value = 0;
            lblMusicPos.Text = string.Format("00:00:00.000 / {0}", GetTime(s.TotalPosition));

            timer1.Start();
        }

        bool Play_Click;
        private void btnMusicPlay_Click(object sender, EventArgs e) { Play(); }
        public void Play()
        {
            if (audio == null) { MessageBox.Show("Open file first!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning); }
            else
            {
                if (Play_Click) { audio.Pause(); Play_Click = false; btnMusicPlay.Text = "Play"; timer1.Stop(); }
                else { audio.Play(); Play_Click = true; btnMusicPlay.Text = "Pause"; timer1.Start(); }
            }
        }

        private void btnMusicStop_Click(object sender, EventArgs e) { Stop(); }
        public void Stop()
        {
            if (audio == null) MessageBox.Show("Open file first!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else { audio.Stop(); Play_Click = false; btnMusicPlay.Text = "Play"; timer1.Stop(); }
        }

        private void tbMusicVolume_Scroll(object sender, EventArgs e)
        {
            if (audio != null) audio.Volume = tbMusicVolume.Value / 100f;
            lblMusicVol.Text = tbMusicVolume.Value.ToString();
        }

        private void chkLoop_CheckedChanged(object sender, EventArgs e)
        {
            if (audio == null) MessageBox.Show("Open first!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else audio.Loop = chkLoop.Checked ? LoopKind.Normal : LoopKind.Off;
        }

        private void tbMusicPos_Scroll(object sender, EventArgs e)
        {
            if (audio != null) audio.CurrentPosition = (uint)tbMusicPos.Value;
        }


        private void Audio_StatusChanged(Audio sender, AudioState status)
        {
            if (status == AudioState.Playing) { timer1.Start(); return; }
            else if (status == AudioState.Stopped) { Play_Click = false; btnMusicPlay.Text = "Play";}
            timer1.Stop();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (audio.Status != AudioState.Dettached && audio.Status != AudioState.Unknown)
                {
                    tbMusicPos.Value = (int)audio.CurrentPosition;
                    lblMusicPos.Text = string.Format("{0} / {1}", GetTime(audio.CurrentPosition), GetTime(audio.Sound.TotalPosition));
                    if (PlayingProgress != null) try { PlayingProgress(this, audio.CurrentPosition); } catch { }
                }
                //if()
            }
            catch { tbMusicPos.Value = 0; lblMusicPos.Text = "00:00:00.000 / 00:00:00.000"; }
        }

        private void cbReverb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Audio != null) Audio.Reverb.Reverb = (Reverb)cbReverb.Items[cbReverb.SelectedIndex];
        }

        int reverb_index  = 0;
        public void RefreshReverb()
        {
            if(cbReverb.SelectedIndex >= 0) reverb_index = cbReverb.SelectedIndex;
            cbReverb.Items.Clear();
            cbReverb.Items.Add(Reverb.Off);
            cbReverb.Items.Add(Reverb.Generic);
            cbReverb.Items.Add(Reverb.PaddedCell);
            cbReverb.Items.Add(Reverb.Room);
            cbReverb.Items.Add(Reverb.Bathroom);
            cbReverb.Items.Add(Reverb.LivingRoom);
            cbReverb.Items.Add(Reverb.StoneRoom);
            cbReverb.Items.Add(Reverb.Auditorium);
            cbReverb.Items.Add(Reverb.ConcertHall);
            cbReverb.Items.Add(Reverb.Cave);
            cbReverb.Items.Add(Reverb.Arena);
            cbReverb.Items.Add(Reverb.Hangar);
            cbReverb.Items.Add(Reverb.CarpetedHallway);
            cbReverb.Items.Add(Reverb.Hallway);
            cbReverb.Items.Add(Reverb.StoneCorridor);
            cbReverb.Items.Add(Reverb.Alley);
            cbReverb.Items.Add(Reverb.Forest);
            cbReverb.Items.Add(Reverb.City);
            cbReverb.Items.Add(Reverb.Mountains);
            cbReverb.Items.Add(Reverb.Quarry);
            cbReverb.Items.Add(Reverb.Plain);
            cbReverb.Items.Add(Reverb.ParkingLot);
            cbReverb.Items.Add(Reverb.SewerPipe);
            cbReverb.Items.Add(Reverb.UnderWater);
            cbReverb.SelectedIndex = reverb_index;
        }

        private void chkReverb3D_CheckedChanged(object sender, EventArgs e)
        {
            Audio.Reverb.Enable3D = chkReverb3D.Checked;
        }

        private void tbReverbLevel_Scroll(object sender, EventArgs e)
        {
            if (audio != null) audio.ReverbLevel = tbReverbLevel.Value / 100f;
            lblReverbLevel.Text = tbReverbLevel.Value.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmSpeaker spk = new frmSpeaker(audio);
            spk.Show();
        }
    }
}
