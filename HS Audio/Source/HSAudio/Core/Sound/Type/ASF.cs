﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class ASF : ISoundFormat
    {
        public ASF() : base(SoundType.ASF, MIME, Extension, Description) { }

            public static string[] MIME { get { return new string[] { "video/x-ms-asf"}; } }
            public static string[] Extension { get { return new string[] { "asf", "wma" }; } }
            public static string[] Description { get { return new string[] { "Advanced Systems Format", "Advanced Streaming Format", "Windows Media Audio" }; } }

            public override string ToString() { return Description[0]; }
    }
}
