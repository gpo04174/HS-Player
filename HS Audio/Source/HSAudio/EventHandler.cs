﻿using HSAudio.Core.Sound;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio
{
    public delegate void EventHandler(object sender);
    public delegate void FloatValueChangedEventHandler(object sender, float Value);
    public delegate void UintChangedEventHandler(object sender, uint Value);
    public delegate void IntChangedEventHandler(object sender, int Value);
    public delegate void BoolChangedEventHandler(object sender, bool Value);

    public enum CollectionChange { Add, Remove, Change }
}
