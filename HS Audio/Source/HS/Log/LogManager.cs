﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS.Log
{
    public delegate void LogEventHandler(LogManager sender);
    public delegate void FullEventHandler(LogManager sender, ref bool Continue);
    public delegate void WritedEventHandler(LogManager sender, Log log, ref bool Save);
    public class LogManager
    {
        public static LogManager Log = new LogManager();
        public event FullEventHandler Full;
        public event LogEventHandler Clearing;
        public event LogEventHandler Cleared;
        public event WritedEventHandler Writed;

        Queue<Log> logs;
        public LogManager(int Maximum = 100000)
        {
            this.Maximum = Maximum;
            logs = new Queue<Log>(Maximum);
        }

        public int Count { get { return logs.Count; } }
        public int Maximum { get; private set; }

        public int Write(Log log)
        {
            if (log != null)
            {
                if (Count == Maximum)
                {
                    bool Continue = true;
                    if (Full != null)
                        try
                        {
                            Full.Invoke(this, ref Continue);
                            if (Continue) logs.Dequeue();
                            else return Count;
                        } catch { }
                }
                bool Save = true;
                if (Writed != null) Writed(this, log, ref Save);
                else if (Save) logs.Enqueue(log);
            }
            return Count;
        }
        public void WriteAsync(Log log)
        {
            if (log != null)
            {
                if (Count == Maximum)
                {
                    bool Continue = true;
                    if (Full != null)
                        try
                        {
                            Full.Invoke(this, ref Continue);
                            if (Continue) logs.Dequeue();
                            else return;
                        }
                        catch { }
                }
                bool Save = true;
                Writed(this, log, ref Save);
                if (Save) logs.Enqueue(log);
            }
        }
        public Log Read()
        {
            return Count > 0 ? logs.Dequeue() : null;
        }


        public void Clear()
        {
            if (Clearing != null) try { Clearing.Invoke(this); } catch { }
            logs.Clear();
            if (Cleared != null) try { Cleared.Invoke(this); } catch { }
        }
        public void Clear(int Count)
        {
            if (Clearing != null) try { Clearing.Invoke(this); } catch { }
            for (int i = 0; i < Count; i++) logs.Dequeue();
            if (Cleared != null) try { Cleared.Invoke(this); } catch { }
        }
    }
}
