#if LINUX
using HSAudio.Lib.FMOD;
using HSAudio.Log;
using HSAudio.Output.Kind;
using HSAudio.Speakers;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Linux Pulse Audio output [for Linux] (리눅스 전용)
    /// </summary>
    public class PulseAudioOutput : DeviceOutput
    {
        public static PulseAudioOutput[] GetOutputs(HSAudio Audio)
        {
            Audio.system_sub.setOutput(OUTPUTTYPE.PULSEAUDIO);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<PulseAudioOutput> device = new List<PulseAudioOutput>();
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.Channel = (short)ch; format.Mode = (SpeakerMode)spk;

                device.Add(new PulseAudioOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Linux Pulse Audio" : "Linux Pulse Audio " + i) : sb.ToString() });
            }
            return device.ToArray();
        }
        public static PulseAudioOutput[] GetOutputs()
        {
            Lib.FMOD.FD_System system_sub;
            Lib.FMOD.Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.PULSEAUDIO);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<PulseAudioOutput> device = new List<PulseAudioOutput>();
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.Channel = (short)ch; format.Mode = (SpeakerMode)spk;

                        device.Add(new PulseAudioOutput(i, format, guid) { Name = string.IsNullOrEmpty(sb.ToString()) ? (i == 0 ? "Linux Pulse Audio" : "Linux Pulse Audio " + i) : sb.ToString() });
                    }
                    return device.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }

        internal PulseAudioOutput(int index, OutputFormat Format, Guid guid)
        {
            Type = OutputType.ALSA;
            ID = index;
            this.Format = Format;
            //this.Guid = Guid;
        }
        private PulseAudioOutput(PulseAudioOutput before)
        {
            Type = OutputType.ALSA;
            ID = before.ID;
            Name = before.Name;
            Parent = before.Parent;
            Format = before.Format;
        }

        public override Output Parent { get; internal set; }

        public override object Clone() { return new PulseAudioOutput(this); }

        public override bool Equals(DeviceOutput other) { try { return other is PulseAudioOutput ? ((PulseAudioOutput)other).Name.Equals(Name) : false; } catch { return false; } }
        public override bool Equals(object obj) { try { return obj is PulseAudioOutput ? ((PulseAudioOutput)obj).Name.Equals(Name) : false; } catch { return false; } }
        //public override int GetHashCode() { return base.GetHashCode(); }
        public override string ToString() { return Name; }

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }

        protected internal override void Dispose()
        {
            try { Device.Dispose(); } catch (Exception ex) { new HSAudioLog(ex.Message, "PulseAudioOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }
        }
    }
}
#endif
