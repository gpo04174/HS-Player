﻿using HSAudio.Core;
using HSAudio.Core.Audio;
using HSAudio.Core.Sound;
using HSAudio.Tag;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace HSAudioTest.Test
{
    public class LoopTest
    {
        //static int Time = 1;
        public static void Test(string MusicPath)
        {
            Console.Title = "HSAudio Loop Test";
            Console.WriteLine("HSAudio Loop Test...\n");

            HSAudio.HSAudio h = new HSAudio.HSAudio();
            Sound s = Sound.FromFile(h, MusicPath);
            Audio a = h.Audio.Add(s);
            a.StatusChanged += Program.A_StatusChanged;

            a.Play();

            a.Loop = LoopKind.Normal;
            a.LoopCount = 10;
            h.Reverb.Enable3D = true;
            h.Reverb.Reverb = Reverb.Hangar;

            uint before = 0;
            int curL = Console.CursorLeft, curT = Console.CursorTop;
            while (a.Status != AudioState.Stopped)
            {
                //Dictionary<string, List<TagData>> dic = s.Tag == null ? null : s.Tag.GetDicnary();
                if (before != a.CurrentPosition)
                {
                    Console.SetCursorPosition(curL, curT);
                    Console.Write("{4}. [{0} / {1}] {2} / {3}]",
                        Program.GetTime(a.CurrentPosition),
                        Program.GetTime(s.TotalPosition),
                        a.CurrentPosition,
                        s.TotalPosition,
                        a.LoopCountCurrent);
                    before = a.CurrentPosition;

                    /*
                    if (before > (3000 * Count) && before < (3050 * Count))
                    {
                        Count++;
                        //TagManager tm = s.GetTagManager();
                        //Random rd = new Random();
                        //tm.Tag.Title = "アスノヨゾラ哨戒班_" + rd.Next();
                        //tm.Write();
                    }
                    */
                }
            }

            h.Dispose();
            Console.WriteLine();
        }
    }
}
