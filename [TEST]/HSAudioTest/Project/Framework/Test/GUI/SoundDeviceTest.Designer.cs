﻿namespace HSAudioTest.Test.GUI
{
    partial class SoundDeviceTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.deviceControl1 = new HSAudioTest.Test.GUI.DeviceControl();
            this.loadMusic1 = new HSAudioTest.Test.GUI.LoadMusic();
            this.playcontrol1 = new HSAudioTest.Test.GUI.PlayControl();
            this.saveSettingsXMLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileFToolStripMenuItem,
            this.settingSToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(723, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileFToolStripMenuItem
            // 
            this.fileFToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveSettingsXMLToolStripMenuItem});
            this.fileFToolStripMenuItem.Name = "fileFToolStripMenuItem";
            this.fileFToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.fileFToolStripMenuItem.Text = "File(&F)";
            // 
            // settingSToolStripMenuItem
            // 
            this.settingSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1});
            this.settingSToolStripMenuItem.Name = "settingSToolStripMenuItem";
            this.settingSToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.settingSToolStripMenuItem.Text = "Setting(&S)";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Items.AddRange(new object[] {
            "English (Default)",
            "한국어"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 23);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // deviceControl1
            // 
            this.deviceControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.deviceControl1.Audio = null;
            this.deviceControl1.Location = new System.Drawing.Point(2, 181);
            this.deviceControl1.Name = "deviceControl1";
            this.deviceControl1.Size = new System.Drawing.Size(721, 141);
            this.deviceControl1.TabIndex = 11;
            // 
            // loadMusic1
            // 
            this.loadMusic1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loadMusic1.Location = new System.Drawing.Point(2, 25);
            this.loadMusic1.MusicPath = "";
            this.loadMusic1.Name = "loadMusic1";
            this.loadMusic1.Size = new System.Drawing.Size(720, 30);
            this.loadMusic1.TabIndex = 1;
            this.loadMusic1.Loaded += new System.EventHandler(this.loadMusic1_Loaded);
            // 
            // playcontrol1
            // 
            this.playcontrol1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.playcontrol1.audio = null;
            this.playcontrol1.Audio = null;
            this.playcontrol1.Location = new System.Drawing.Point(2, 52);
            this.playcontrol1.Name = "playcontrol1";
            this.playcontrol1.Size = new System.Drawing.Size(718, 123);
            this.playcontrol1.TabIndex = 0;
            // 
            // saveSettingsXMLToolStripMenuItem
            // 
            this.saveSettingsXMLToolStripMenuItem.Name = "saveSettingsXMLToolStripMenuItem";
            this.saveSettingsXMLToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveSettingsXMLToolStripMenuItem.Text = "Save Settings XML";
            this.saveSettingsXMLToolStripMenuItem.Click += new System.EventHandler(this.saveSettingsXMLToolStripMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "*.xml";
            this.saveFileDialog1.Filter = "XML|*.xml|*.*|*.*";
            // 
            // SoundDeviceTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 325);
            this.Controls.Add(this.deviceControl1);
            this.Controls.Add(this.loadMusic1);
            this.Controls.Add(this.playcontrol1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "SoundDeviceTest";
            this.Text = "SoundDeviceTest";
            this.Load += new System.EventHandler(this.SoundDeviceTest_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private HSAudioTest.Test.GUI.PlayControl playcontrol1;
        private LoadMusic loadMusic1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingSToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private DeviceControl deviceControl1;
        private System.Windows.Forms.ToolStripMenuItem saveSettingsXMLToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}