﻿using HS.Setting;
using HSAudio.Core.Sound.Kind;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;

namespace HSAudio.Core.Sound._3D
{
    public class SoundNetwork3D : SoundNetwork, Sound3D
    {
        Distance3D _Distance;
        Cone3D _Cone;
        public SoundNetwork3D(HSAudio Audio, string NetworkURL, uint BufferLength = 1024 * 64) : this(Audio, NetworkURL, null, BufferLength) { }
        public SoundNetwork3D(HSAudio Audio, string NetworkURL, ISoundFormat Type, uint BufferLength = 1024 * 64) : base(Audio, NetworkURL, Type, BufferLength)
        {
            _Distance = new Distance3D(sound);
            _Cone = new Cone3D(sound);
        }

        public override bool Load(bool ThrowException = true)
        {
            if (IsValid) return true;
            else
            {
                try
                {
                    Result = Audio.system.createStream(NetworkURL, MODE._3D | MODE.IGNORETAGS, out sound);
                    if (Result != RESULT.OK) { sound.release(); throw new HSAudioException(Result); }
                    else
                    {
                        _Format = getFormat();
                        if(_SoundType == null) _SoundType = getType();
                        Audio.BufferSizeStream = _BufferLength;
                        _Tag = SoundTag.GetTag(sound);
                    }
                    base.OnLoaded();
                    return true;
                }
                catch (Exception ex) { if (ThrowException) throw ex; }
                return false;
            }
        }

        public Distance3D Distance { get { return _Distance; } }
        public Cone3D Cone { get { return _Cone; } }

        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Type", "Network");
            s.SetValue("Dimension", "3D");
            s.SetValue("BufferLength", _BufferLength);
            s.SetValue("NetworkURL", new SettingsData(NetworkURL, false));
            s.SubSetting.SetValue("Distance", Distance.SaveSetting());
            s.SubSetting.SetValue("Cone", Cone.SaveSetting());
            return s;
        }
        public static SoundNetwork3D LoadSetting(HSAudio Audio, Settings Setting, bool ThrowException = false)
        {
            string NetworkURL = null;
            uint BufferLength = Audio.BufferSizeStream;
            ISoundFormat type = null;

            try { if (Setting.Exist("NetworkURL")) NetworkURL = Setting["NetworkURL"]; } catch (Exception ex) { if (ThrowException) throw ex; return null; }
            try { if (Setting.Exist("BufferLength")) BufferLength = Convert.ToUInt32(Setting["BufferLength"]); } catch (Exception ex) { if (ThrowException) throw ex; return null; }
            try { if (Setting.SubSetting.Exist("Type")) type = ISoundFormat.FromSetting(Setting.SubSetting["Type"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            //try { if (Setting.SubSetting.Exist("Format")) format = WaveFormat.Load(Setting.SubSetting["Format"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            SoundNetwork3D sound = new SoundNetwork3D(Audio, NetworkURL, type, BufferLength);

            if (Setting.SubSetting.Exist("Distance")) sound.Distance.LoadSetting(Setting.SubSetting["Distance"], ThrowException);
            if (Setting.SubSetting.Exist("Cone")) sound.Cone.LoadSetting(Setting.SubSetting["Cone"], ThrowException);
            return sound;
        }
    }
}
