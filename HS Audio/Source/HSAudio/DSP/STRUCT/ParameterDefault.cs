﻿using HSAudio.Resource;
using HS.Setting;
using System;
using System.Runtime.InteropServices;
using HS;

namespace HSAudio.DSP
{
    public class ParameterDefault : ISettingManager
    {
        internal ParameterDefault(DSPParameterType Type, Lib.FMOD.DSP_PARAMETER_DESC_UNION union)
        {
            this.Type = Type;
            switch (Type)
            {
                case DSPParameterType.Float: Value_Float = new FloatValue(union.floatdesc); ValueString = GetString(union.booldesc.valuenames); break;
                case DSPParameterType.Int: Value_Int = new IntValue(union.intdesc); ValueString = GetString(union.booldesc.valuenames); break;
                case DSPParameterType.Bool: Value_Bool = union.booldesc.defaultval; ValueString = GetString(union.booldesc.valuenames); break;
                case DSPParameterType.Data: Value_Data_Type = (DataType)union.datadesc.datatype; ValueString = GetString(union.booldesc.valuenames); break;
            }
        }
        public ParameterDefault(FloatValue Default) { this.Value_Float = Default; Type = DSPParameterType.Float; }
        public ParameterDefault(IntValue Default) { this.Value_Int = Default; Type = DSPParameterType.Int; }
        public ParameterDefault(bool Default) { this.Value_Bool = Default; Type = DSPParameterType.Bool; }
        public ParameterDefault(DataType DataType) { Value_Data_Type = DataType; }
        public ParameterDefault(Settings settings) { LoadSetting(settings); }

        public DSPParameterType Type { get; private set; }

        public FloatValue Value_Float { get; private set; }
        public IntValue Value_Int { get; private set; }
        public bool Value_Bool { get; private set; }
        public DataType Value_Data_Type { get; private set; }

        public string ValueString { get; private set; }


        public static implicit operator DataType(ParameterDefault param) { return param.Value_Data_Type; }
        public static implicit operator FloatValue(ParameterDefault param) { return param.Value_Float; }
        public static implicit operator IntValue(ParameterDefault param) { return param.Value_Int; }
        public static implicit operator bool(ParameterDefault param) { return param.Value_Bool; }


        private static string GetString(IntPtr fstring)
        {
            if (fstring == IntPtr.Zero) return null;
            else return Marshal.PtrToStringUni(fstring);
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Type", Type.ToString());
            if (Type == DSPParameterType.Bool) s.SetValue("Value", Value_Bool);
            else if (Type == DSPParameterType.Data) s.SetValue("DataType", Value_Data_Type.ToString());
            else if (Type == DSPParameterType.Float) s.SubSetting.SetValue("Value", Value_Float.SaveSetting());
            else if (Type == DSPParameterType.Int) s.SubSetting.SetValue("Value", Value_Int.SaveSetting());
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Type")) Type = (DSPParameterType)Enum.Parse(typeof(DSPParameterType), Setting["Type"]);
            else { if (ThrowException) throw new HSException(StringResource.Resource["STR_ERR_SETTING_WRONG"]); return false; }

            if (Setting.Exist("DataType")) Value_Data_Type = (DataType)Enum.Parse(typeof(DataType), Setting["DataType"]);
            else if (Setting.Exist("Value")) Value_Bool = Convert.ToBoolean(Setting["Value"]);
            if (Setting.SubSetting.Exist("Value"))
            {
                if (Type == DSPParameterType.Float) Value_Float = new FloatValue(Setting.SubSetting["Value"]);
                else if (Type == DSPParameterType.Int) Value_Int = new IntValue(Setting.SubSetting["Value"]);
            }
            return true;
        }
    }
}
