﻿using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using HSAudio.Lib.FMOD;
using HSAudio.Resource;

namespace HSAudio.Core
{
    public struct Reverb : ISettingManager, IEquatable<Reverb>
    {
        private string name;
        internal REVERB_PROPERTIES reverb;
        /*
        public Reverb()
        {
            reverb = Off;
            name = "STR_REVERB_CUSTOM";
        }
        */
        public Reverb(float decayTime, float earlyDelay, float lateDelay, float hfReference,
            float hfDecayRatio, float diffusion, float density, float lowShelfFrequency, float lowShelfGain,
            float highCut, float earlyLateMix, float wetLevel, string Name = null) : this()
        {
            DecayTime = decayTime;
            EarlyDelay = earlyDelay;
            LateDelay = lateDelay;
            HFReference = hfReference;
            HFDecayRatio = hfDecayRatio;
            Diffusion = diffusion;
            Density = density;
            LowShelfFrequency = lowShelfFrequency;
            LowShelfGain = lowShelfGain;
            HighCut = highCut;
            EarlyLateMix = earlyLateMix;
            WetLevel = wetLevel;

            if (Name == null) name = "STR_REVERB_CUSTOM";
            else name = Name;
        }
        internal Reverb(REVERB_PROPERTIES reverb)
        {
            this.reverb = reverb; 
            name = "STR_REVERB_CUSTOM";
        }

        #region Properties
        /// <summary>
        /// Reverberation decay time (ms) <para/>
        /// 잔향 감쇠 시간 (ms) <para/>
        /// Range (범위): 0.0 ~ 2000.0
        /// </summary>
        public float DecayTime { get { return reverb.DecayTime; } set { reverb.DecayTime = value; } }
        /// <summary>
        /// Initial reflection delay time <para/>
        /// 초기 반사 지연시간 <para/>
        /// Range (범위): 0.0 ~ 300.0
        /// </summary>
        public float EarlyDelay { get { return reverb.EarlyDelay; } set { reverb.EarlyDelay = value; } }
        /// <summary>
        /// Late reverberation delay time relative to initial reflection <para/>
        /// 초기 반사를 기준으로 한 후반향 지연 시간 <para/>
        /// Range (범위): 0.0 ~ 100.0
        /// </summary>
        public float LateDelay { get { return reverb.LateDelay; } set { reverb.LateDelay = value; } }
        /// <summary>
        /// Reference high frequency (Hz) <para/>
        /// 고주파 기준 <para/>
        /// Range (범위): 20.0 ~ 20000.0
        /// </summary>
        public float HFReference { get { return reverb.HFReference; } set { reverb.HFReference = value; } }
        /// <summary>
        /// High-frequency to mid-frequency decay time ratio <para/>
        /// 고주파 대 중주파 감쇠 시간 비율 <para/>
        /// Range (범위): 10.0 ~ 100.0
        /// </summary>
        public float HFDecayRatio { get { return reverb.HFDecayRatio; } set { reverb.HFDecayRatio = value; } }
        /// <summary>
        /// Value that controls the echo density in the late reverberation decay <para/>
        ///  <para/>
        /// Range (범위): 0.0 ~ 100.0
        /// </summary>
        public float Diffusion { get { return reverb.Diffusion; } set { reverb.Diffusion = value; } }
        /// <summary>
        /// Value that controls the modal density in the late reverberation decay <para/>
        ///  <para/>
        /// Range (범위): 0.0 ~ 100.0
        /// </summary>
        public float Density { get { return reverb.Density; } set { reverb.Density = value; } }
        /// <summary>
        /// Reference low frequency (Hz) <para/>
        /// 저주파 기준 <para/>
        /// Range (범위): 0.0 ~ 1000.0
        /// </summary>
        public float LowShelfFrequency { get { return reverb.LowShelfFrequency; } set { reverb.LowShelfFrequency = value; } }
        /// <summary>
        /// Relative room effect level at low frequencies <para/>
        /// 저주파에서의 상대적 실내 효과 레벨 <para/>
        /// Range (범위): -36.0 ~ 12.0
        /// </summary>
        public float LowShelfGain { get { return reverb.LowShelfGain; } set { reverb.LowShelfGain = value; } }
        /// <summary>
        /// Relative room effect level at high frequencies <para/>
        /// 고주파에서의 상대적 실내 효과 레벨 <para/>
        /// Range (범위): 20.0 ~ 20000.0
        /// </summary>
        public float HighCut { get { return reverb.HighCut; } set { reverb.HighCut = value; } }
        /// <summary>
        /// Early reflections level relative to room effect <para/>
        /// 상대적 Room effect 의 초기 반사 <para/>
        /// Range (범위): 20.0 ~ 20000.0
        /// </summary>
        public float EarlyLateMix { get { return reverb.EarlyLateMix; } set { reverb.EarlyLateMix = value; } }

        /// <summary>
        /// Room effect level (at mid frequencies) <para/>
        /// Room effect 수준 (중 주파에서) <para/>
        /// Range (범위): -80.0 ~ 20.0
        /// </summary>
        public float WetLevel { get { return reverb.WetLevel; } set { reverb.WetLevel = value; } }

        public string Name
        {
            get { return StringResource.Resource[name]; }
            set { name = value; }
        }
        #endregion

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            try { if (Setting.Exist("WetLevel")) WetLevel = Convert.ToSingle(Setting["WetLevel"]); } catch { }
            try { if (Setting.Exist("DecayTime")) DecayTime = Convert.ToSingle(Setting["DecayTime"]); } catch { }
            try { if (Setting.Exist("EarlyDelay")) EarlyDelay = Convert.ToSingle(Setting["EarlyDelay"]); } catch { }
            try { if (Setting.Exist("LateDelay")) LateDelay = Convert.ToSingle(Setting["LateDelay"]); } catch { }
            try { if (Setting.Exist("HFReference")) HFReference = Convert.ToSingle(Setting["HFReference"]); } catch { }
            try { if (Setting.Exist("HFDecayRatio")) HFDecayRatio = Convert.ToSingle(Setting["HFDecayRatio"]); } catch { }
            try { if (Setting.Exist("Diffusion")) Diffusion = Convert.ToSingle(Setting["Diffusion"]); } catch { }
            try { if (Setting.Exist("Density")) Density = Convert.ToSingle(Setting["Density"]); } catch { }
            try { if (Setting.Exist("LowShelfFrequency")) LowShelfFrequency = Convert.ToSingle(Setting["LowShelfFrequency"]); } catch { }
            try { if (Setting.Exist("LowShelfGain")) LowShelfGain = Convert.ToSingle(Setting["LowShelfGain"]); } catch { }
            try { if (Setting.Exist("HighCut")) HighCut = Convert.ToSingle(Setting["HighCut"]); } catch { }
            try { if (Setting.Exist("EarlyLateMix")) EarlyLateMix = Convert.ToSingle(Setting["EarlyLateMix"]); } catch { }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("WetLevel", WetLevel);
            s.SetValue("DecayTime", new SettingsData(DecayTime, false));
            s.SetValue("EarlyDelay", new SettingsData(EarlyDelay, false));
            s.SetValue("LateDelay", new SettingsData(LateDelay, false));
            s.SetValue("HFReference", new SettingsData(HFReference, false));
            s.SetValue("HFDecayRatio", new SettingsData(HFDecayRatio, false));
            s.SetValue("Diffusion", new SettingsData(Diffusion, false));
            s.SetValue("Density", new SettingsData(Density, false));
            s.SetValue("LowShelfFrequency", new SettingsData(LowShelfFrequency, false));
            s.SetValue("LowShelfGain", new SettingsData(LowShelfGain, false));
            s.SetValue("HighCut", new SettingsData(HighCut, false));
            s.SetValue("EarlyLateMix", new SettingsData(EarlyLateMix, false));
            s.SetValue("WetLevel", new SettingsData(WetLevel, false));
            return s;
        }


        public bool Equals(Reverb other)
        {
            return DecayTime == other.DecayTime &&
                   EarlyDelay == other.EarlyDelay &&
                   LateDelay == other.LateDelay &&
                   HFReference == other.HFReference &&
                   HFDecayRatio == other.HFDecayRatio &&
                   Diffusion == other.Diffusion &&
                   Density == other.Density &&
                   LowShelfFrequency == other.LowShelfFrequency &&
                   LowShelfGain == other.LowShelfGain &&
                   HighCut == other.HighCut &&
                   EarlyLateMix == other.EarlyLateMix;
        }
        public override bool Equals(object obj) { return obj is Reverb ? Equals((Reverb)obj) : false; }
        public override int GetHashCode() { return base.GetHashCode(); }

        public override string ToString() { return Name; }

        public static implicit operator REVERB_PROPERTIES(Reverb Instance) { return Instance.reverb; }
        public static implicit operator Reverb(REVERB_PROPERTIES Instance) { return new Reverb(Instance); }

        #region Preset
        /// <summary>끄기</summary>
        public static Reverb Off { get { return new Reverb(1000, 7, 11, 5000, 100, 100, 100, 250, 0, 20, 96, -80.0f, "STR_REVERB_OFF"); } }
        /// <summary>일반</summary>
        public static Reverb Generic { get { return new Reverb(1500, 7, 11, 5000, 83, 100, 100, 250, 0, 14500, 96, -8.0f, "STR_REVERB_GENERIC"); } }
        /// <summary>벽면에 패드를 댄 방 (방음실)</summary>
        public static Reverb PaddedCell { get { return new Reverb(170, 1, 2, 5000, 10, 100, 100, 250, 0, 160, 84, -7.8f, "STR_REVERB_PADDEDCELL"); } }
        /// <summary>방</summary>
        public static Reverb Room { get { return new Reverb(400, 2, 3, 5000, 83, 100, 100, 250, 0, 6050, 88, -9.4f, "STR_REVERB_ROOM"); } }
        /// <summary>욕실</summary>
        public static Reverb Bathroom { get { return new Reverb(1500, 7, 11, 5000, 54, 100, 60, 250, 0, 2900, 83, 0.5f, "STR_REVERB_BATHROOM"); } }
        /// <summary>거실</summary>
        public static Reverb LivingRoom { get { return new Reverb(500, 3, 4, 5000, 10, 100, 100, 250, 0, 160, 58, -19.0f, "STR_REVERB_LIVINGROOM"); } }
        /// <summary>석조실</summary>
        public static Reverb StoneRoom { get { return new Reverb(2300, 12, 17, 5000, 64, 100, 100, 250, 0, 7800, 71, -8.5f, "STR_REVERB_STONEROOM"); } }
        /// <summary>강당</summary>
        public static Reverb Auditorium { get { return new Reverb(4300, 20, 30, 5000, 59, 100, 100, 250, 0, 5850, 64, -11.7f, "STR_REVERB_AUDITORIUM"); } }
        /// <summary>콘서트 홀</summary>
        public static Reverb ConcertHall { get { return new Reverb(3900, 20, 29, 5000, 70, 100, 100, 250, 0, 5650, 80, -9.8f, "STR_REVERB_CONCERTHALL"); } }
        /// <summary>동굴</summary>
        public static Reverb Cave { get { return new Reverb(2900, 15, 22, 5000, 100, 100, 100, 250, 0, 20000, 59, -11.3f, "STR_REVERB_CAVE"); } }
        /// <summary>경기장</summary>
        public static Reverb Arena { get { return new Reverb(7200, 20, 30, 5000, 33, 100, 100, 250, 0, 4500, 80, -9.6f, "STR_REVERB_ARENA"); } }
        /// <summary>격납고</summary>
        public static Reverb Hangar { get { return new Reverb(10000, 20, 30, 5000, 23, 100, 100, 250, 0, 3400, 72, -7.4f, "STR_REVERB_HANGAR"); } }
        /// <summary>카페트가 깔린 복도</summary>
        public static Reverb CarpetedHallway { get { return new Reverb(300, 2, 30, 5000, 10, 100, 100, 250, 0, 500, 56, -24.0f, "STR_REVERB_CARPETEDHALLWAY"); } }
        /// <summary>복도</summary>
        public static Reverb Hallway { get { return new Reverb(1500, 7, 11, 5000, 59, 100, 100, 250, 0, 7800, 87, -5.5f, "STR_REVERB_HALLWAY"); } }
        /// <summary>석조 회랑</summary>
        public static Reverb StoneCorridor { get { return new Reverb(500, 300, 100, 5000, 21, 27, 100, 250, 0, 1220, 82, -24.0f, "STR_REVERB_STONECORRIDOR"); } }
        /// <summary>골목길</summary>
        public static Reverb Alley { get { return new Reverb(1500, 7, 11, 5000, 86, 100, 100, 250, 0, 8300, 80, -9.8f, "STR_REVERB_ALLEY"); } }
        /// <summary>숲</summary>
        public static Reverb Forest { get { return new Reverb(1500, 162, 88, 5000, 54, 79, 100, 250, 0, 760, 94, -12.3f, "STR_REVERB_FOREST"); } }
        /// <summary>도시</summary>
        public static Reverb City { get { return new Reverb(1500, 7, 11, 5000, 67, 50, 100, 250, 0, 4050, 66, -26.0f, "STR_REVERB_CITY"); } }
        /// <summary>산</summary>
        public static Reverb Mountains { get { return new Reverb(1500, 300, 100, 5000, 21, 27, 100, 250, 0, 1220, 82, -24.0f, "STR_REVERB_MOUNTAINS"); } }
        /// <summary>채석장</summary>
        public static Reverb Quarry { get { return new Reverb(1500, 61, 25, 5000, 83, 100, 100, 250, 0, 3400, 100, -5.0f, "STR_REVERB_QUARRY"); } }
        /// <summary>플레인</summary>
        public static Reverb Plain { get { return new Reverb(1500, 179, 100, 5000, 50, 21, 100, 250, 0, 1670, 65, -28.0f, "STR_REVERB_PLAIN"); } }
        /// <summary>주차장</summary>
        public static Reverb ParkingLot { get { return new Reverb(1700, 8, 12, 5000, 100, 100, 100, 250, 0, 20000, 56, -19.5f, "STR_REVERB_PARKINGLOT"); } }
        /// <summary>하수도</summary>
        public static Reverb SewerPipe { get { return new Reverb(2800, 14, 21, 5000, 14, 80, 60, 250, 0, 3400, 66, 1.2f, "STR_REVERB_SEWERPIPE"); } }
        /// <summary>지하수</summary>
        public static Reverb UnderWater { get { return new Reverb(1000, 7, 11, 5000, 100, 100, 100, 250, 0, 20, 96, -80.0f, "STR_REVERB_UNDERWATER"); } }
        #endregion
    }
}
