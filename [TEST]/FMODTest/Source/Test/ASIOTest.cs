﻿#if !LINUX
using HSAudio.Lib.FMOD;
using System;
using System.Text;
using System.Threading;

namespace FMODTest.Test
{
    class TestASIO
    {
        public static void Test(string MusicPath)
        {
            FD_System system;
            Factory.System_Create(out system);
            //Output[] o = Program.GetOutputs(system, OUTPUTTYPE.ASIO);

            if (MusicPath != null)
            {
                FD_Sound sound;
                FD_Channel channel;
                RESULT result = RESULT.OK;
                result = system.setOutput(OUTPUTTYPE.ASIO);
                result = system.setDriver(0);
                result = system.update();
                //result = system.setSoftwareFormat(96000, SPEAKERMODE.RAW, 2);

                //result = system.setOutput
                //result = system.init(16, INITFLAGS.PREFER_DOLBY_DOWNMIX | INITFLAGS.PROFILE_METER_ALL | INITFLAGS.PROFILE_ENABLE, IntPtr.Zero);
                result = system.init(16, INITFLAGS.PROFILE_ENABLE | INITFLAGS.PROFILE_METER_ALL, IntPtr.Zero);
                if (result == RESULT.OK)
                {
                    result = system.createStream(MusicPath, MODE.LOOP_OFF | MODE.ACCURATETIME, out sound);
                    result = system.playSound(sound, null, false, out channel);

                    StringBuilder sb = new StringBuilder(256); Guid guid; int rate; SPEAKERMODE mode; int ch;
                    result = system.getDriverInfo(0, sb, 256, out guid, out rate, out mode, out ch);
                    int samplate; int rawspk;
                    system.getSoftwareFormat(out samplate, out mode, out rawspk);
                    system.getSoftwareChannels(out ch);

                    OPENSTATE state; uint percent; bool starving; bool diskbusy;
                    result = sound.getOpenState(out state, out percent, out starving, out diskbusy);
                    result = channel.setPaused(false);
                    while (true)
                    {
                        bool play;
                        channel.isPlaying(out play);
                        if (!play) result = system.playSound(sound, null, false, out channel);
                    }
                }
            }
            Thread.Sleep(5000);
            system.close();
            system.release();
        }
    }
}
#endif