﻿using System;
using HSAudio.Core.Audio;
using HSAudio.Core.Sound;
using HSAudio.Core.Sound._2D;
using HSAudio.Output.Devices;
using HSAudio.Output.Kind;
using HSAudio.DSP;
using HSAudio.DSP.Type;
using HSAudio.Core;
using System.Threading;

namespace FMODTest.Test
{
    public class LinuxPlayTest
    {
        public static void Test(string MusicPath)
        {
            DeviceOutput[] outputs = ALSAOutput.GetOutputs();
            //audio.Output.Open()
            HSAudio.HSAudio audio = new HSAudio.HSAudio(outputs[0]);
            Console.WriteLine(outputs[0]);

            if (audio.Output.Status == HSAudio.Output.OutputStatus.Open)
            {
                Sound s = new SoundFile2D(audio, MusicPath);
                Audio a = audio.Audio.Add(s);
                a.Volume = 0.7f;
                //a.StatusChanged += Change;

                audio.Reverb.Reverb = Reverb.Hallway;
                Equalizer3Band eq = new Equalizer3Band(audio);
                eq.HighGain = 5f;
                a.DSP.Add(eq);

                //a.CurrentPosition = s.TotalPosition - 10000;

                bool play = true;
                int posX = Console.CursorLeft; int posY = Console.CursorTop;
                while (play)
                {
                    Console.SetCursorPosition(posX, posY);
                    try { Console.WriteLine("{0} / {1}\n", a.CurrentPosition, s == null ? 0 : s.TotalPosition); } catch { }

                    if (a.Status == AudioState.Stopped) a.Play();
                    //system.update();
                    Thread.Sleep(10);
                }
            }
            Console.Write("Press key and continue...");
            Console.ReadKey();
        }

        static void Change(Audio sender, AudioState status)
        {
            Console.WriteLine(status.ToString());
        }
    }
}
