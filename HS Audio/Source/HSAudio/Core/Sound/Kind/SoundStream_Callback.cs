﻿using HSAudio.Lib.FMOD;
using System;
using System.Runtime.InteropServices;

namespace HSAudio.Core.Sound.Kind
{
    partial class SoundStream
    {
        private unsafe RESULT SOUND_PCMREADCALLBACK(IntPtr soundraw, IntPtr data, uint datalen)
        {
            byte[] a = new byte[datalen];
            int len = stream.Read(a, 0, (int)datalen);
            Marshal.Copy(a, 0, data, a.Length);

            /*
            Marshal.Copy(a, 0, data, len == 0 ? a.Length : len);
            if (len == 0)
            {
                try
                {
                    if (stream.Position == stream.Length) return RESULT.ERR_FILE_ENDOFDATA;
                    else return RESULT.ERR_INVALID_POSITION;
                }
                catch { return RESULT.ERR_INVALID_POSITION; }
            }
            else return RESULT.OK;
            */
            return RESULT.OK;
        }
        private RESULT SOUND_PCMSETPOSCALLBACK(IntPtr soundraw, int subsound, uint position, TIMEUNIT postype)
        {
            if(Seekable && stream.CanSeek)
            {
                int pos;
                if (postype == TIMEUNIT.MS) pos = (Format.AverageBytesPerSecond);
                else if (postype == TIMEUNIT.RAWBYTES) pos = (int)position;
                long seek = stream.Seek(position, System.IO.SeekOrigin.Begin);

                //return seek == 0 ? RESULT.ERR_INVALID_POSITION : RESULT.OK;
            }
            //else return RESULT.ERR_FILE_COULDNOTSEEK;
            return RESULT.OK;
        }
        private RESULT SOUND_NONBLOCKCALLBACK(IntPtr soundraw, RESULT result)
        {
            return RESULT.OK;
        }
    }
}
