﻿using HS.Setting;
using HSAudio.Resource;

namespace HSAudio.Output.Devices
{
    public class NoOutput : OutputMethod
    {
        public NoOutput(){Type = OutputType.No;}
        public override string Name
        {
            get { return StringResource.Resource["STR_DEVICE_NOOUTPUT"]; }
            protected set { }
        }

        public override Output Parent { get; internal set; }

        public override object Clone() { return new object(); }

        public override bool LoadSetting(Settings Setting, bool ThrowException = false) { return true; }

        public override Settings SaveSetting() { return null; }

        protected internal override void Dispose() { }

        public override bool Equals(object obj) { return obj is NoOutput; }

        public override string ToString() {return Name; }
        //public override int GetHashCode() {return base.GetHashCode();}
    }
}
