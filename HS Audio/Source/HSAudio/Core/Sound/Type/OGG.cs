﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class OGG : ISoundFormat
    {
        public OGG() : base(SoundType.OGG, MIME, Extension, Description)
        {

        }

        public static string[] MIME { get { return new string[] { "audio/ogg", "application/ogg" }; } }
        public static string[] Extension { get { return new string[] { "ogg", "oga" }; } }
        public static string[] Description { get { return new string[] { "OGG Vorbis"}; } }

        public override string ToString() { return Descriptions[0]; }
    }
}
