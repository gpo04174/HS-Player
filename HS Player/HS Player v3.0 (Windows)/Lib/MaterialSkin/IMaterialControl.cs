﻿#if WIN_XP || WIN_VISTA || WIN_10_1703
namespace HSPlayer.Lib.MaterialSkin
{
    public interface IMaterialControl
    {
        int Depth { get; set; }
        MaterialSkinManager SkinManager { get; }
        MouseState MouseState { get; set; }

    }

    public enum MouseState
    {
        HOVER,
        DOWN,
        OUT
    }
}

#endif