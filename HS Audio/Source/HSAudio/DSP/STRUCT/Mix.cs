﻿using HSAudio.Lib.FMOD;
using HS.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using HS;

namespace HSAudio.DSP
{
    [Serializable]
    public class Mix : ISettingManager
    {
        DSP dsp;
        internal Mix(DSP dsp) { this.dsp = dsp;}
        
        public float WetPre
        {
            get { Update(); return wetr; }
            set { dsp.Result = dsp.DSPInternal.setWetDryMix(value, WetPost, Dry); }
        }
        public float WetPost
        {
            get { Update(); return wetp; }
            set { dsp.Result = dsp.DSPInternal.setWetDryMix(WetPre, value, Dry); }
        }
        public float Dry
        {
            get { Update(); return dry; }
            set { dsp.Result = dsp.DSPInternal.setWetDryMix(WetPre, WetPost, value); }
        }

        private float wetr, wetp, dry;
        private void Update(){ dsp.Result = dsp.DSPInternal.getWetDryMix(out wetr, out wetp, out dry);}

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("WetPre", new SettingsData(WetPre.ToString()));
            s.SetValue("WetPost", new SettingsData(WetPost.ToString()));
            s.SetValue("Dry", new SettingsData(WetPre.ToString()));
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("WetPre")) Convert.ToSingle(Setting["WetPre"]);
            if (Setting.Exist("WetPost")) Convert.ToSingle(Setting["WetPre"]);
            if (Setting.Exist("Dry")) Convert.ToSingle(Setting["WetPre"]);
            return true;
        }
    }
}
