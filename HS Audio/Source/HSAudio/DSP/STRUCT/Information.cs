﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.DSP.Inner
{
    public class Information
    {
        DSP dsp;
        internal Information(DSP dsp) { this.dsp = dsp; }

        public string Name { get; private set; }
        public int Channel { get { return ch; } }
        /// <summary>
        /// Plugin DSP Only
        /// </summary>
        public int ConfigWidth { get { return cfw; } }
        /// <summary>
        /// Plugin DSP Only
        /// </summary>
        public int ConfigHeight { get { return cfh; } }
        
        uint version;
        int ch, cfw, cfh;
        private void Update()
        {
            StringBuilder sb = new StringBuilder(128);
            dsp.DSPInternal.getInfo(sb, out version, out ch, out cfw, out cfh);
            Name = sb.ToString();
        }
    }
}
