﻿using System.Threading.Tasks;

namespace CrossCallback
{
    public enum PermissionKind { Storage, Phone }
    public delegate Task<bool> PermissionCheckingCallback(object sender, PermissionKind Kind);
    //public delegate void PermissionCheckedCallback(object sender, PermissionKind Kind, bool Granted);
    public class PermissionManager
    {
        public static PermissionCheckingCallback PermissionCheck;
        //public static PermissionCheckedCallback PermissionChecked;
        /*
        public static async Task<bool> PermissionCheck(object sender, PermissionKind Kind)
        {
            if (PermissionChecking == null) return await Task.Run(()=> { return false; });
            return await PermissionChecking(sender, Kind);
        }
        */
        //protected static void OnPermissionChecked(object sender, PermissionKind Kind) {//return PermissionCheckedCallback == null ? false : PermissionChecking(sender, Kind);}
    }
}
