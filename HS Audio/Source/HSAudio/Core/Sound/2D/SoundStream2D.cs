﻿using HS.Setting;
using HSAudio.Core.Sound.Kind;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;
using System.IO;

namespace HSAudio.Core.Sound._2D
{
    public partial class SoundStream2D : SoundStream, Sound2D
    {
        /*
        public SoundStream2D(HSAudio Audio, Stream Stream, WaveFormat Format, bool IsEndlessStream = false, uint BufferLength = 1024 * 16) : this(Audio, Stream, new RAW(Format), (IsEndlessStream ? uint.MaxValue : (uint)Stream.Length), IsEndlessStream, false, BufferLength) { }
        public SoundStream2D(HSAudio Audio, Stream Stream, WaveFormat Format, bool IsEndlessStream = false, bool LoadMemory = false, uint BufferLength = 1024 * 16) : this(Audio, Stream, new RAW(Format), (uint)Stream.Length, LoadMemory, IsEndlessStream, BufferLength) { }
        public SoundStream2D(HSAudio Audio, Stream Stream, WaveFormat Format, uint PCMLength, bool IsEndlessStream = false, bool LoadMemory = false, uint BufferLength = 1024 * 16) : this(Audio, Stream, new RAW(Format), PCMLength, IsEndlessStream, LoadMemory, BufferLength) { }
        */
        public SoundStream2D(HSAudio Audio, Stream Stream, RAW Raw, bool Seekable, bool LoadMemory = false) : base(Audio, Stream, Raw, Seekable, LoadMemory) { }
        public SoundStream2D(HSAudio Audio, Stream Stream, RAW Raw, uint PCMLength, bool Seekable, bool LoadMemory = false) : base(Audio, Stream, Raw, PCMLength, Seekable, LoadMemory) { }

        public override bool Load(bool ThrowException = true)
        {
            if (IsValid) return true;
            else
            {
                try
                {
                    //try { Stream.Position = 0; } catch { }
                    
                    RESULT result = LoadMemory ?
                        Audio.system.createSound(MODE._2D | MODE.OPENUSER | MODE.LOOP_OFF, ref info, out sound) :
                        Audio.system.createStream(MODE._2D | MODE.OPENUSER | MODE.LOOP_OFF, ref info, out sound);
                      
                    if (result != RESULT.OK) { sound.release(); throw new HSAudioException(result); }
                    else _Format = (WaveFormat)Format.Clone();

                    _Tag = SoundTag.GetTag(sound);
                    base.OnLoaded();
                    return true;
                }
                catch (Exception ex) { if (sound != null) sound.release(); if (ThrowException) throw ex; }
                return false;
            }
        }
    }
}
