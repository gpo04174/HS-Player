﻿using HS.Setting;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;

namespace HSAudio.Core.Sound.Kind
{
    public abstract class SoundNetwork : Sound
    {
        protected WaveFormat _Format;
        protected ISoundFormat _SoundType;
        protected SoundTag _Tag;
        protected uint _BufferLength;

        protected CREATESOUNDEXINFO info = new CREATESOUNDEXINFO();
        internal SoundNetwork(HSAudio Audio, string NetworkURL, uint BufferLength = 1024 * 64) : this(Audio, NetworkURL, null, BufferLength) { }
        internal SoundNetwork(HSAudio Audio, string NetworkURL, ISoundFormat Type, uint BufferLength = 1024 * 64) : base(Audio)
        {
            this.NetworkURL = NetworkURL;
            _BufferLength = BufferLength;
            _SoundType = Type;
            _Format = Format;

            if (Type is RAW) InitFormat(ref info, (Type as RAW).Format);
            if (Type != null) InitType(ref info, Type.Type);
            Load();
        }
        public override void Unload()
        {
            _Tag = null;
            base.Unload();
        }

        public string NetworkURL { get; protected set; }

        public override WaveFormat Format { get { return _Format; } }
        public override ISoundFormat Type { get { return _SoundType; } }
        public override SoundTag Tag { get { return _Tag; } }

        public override uint TotalPosition { get { return base.TotalPosition == uint.MaxValue ? 0 : base.TotalPosition; } }

        public override bool CanPosition { get { return false; } }
        public override bool CanPitch { get { return false; } }
        public override bool CanReverse { get { return false; } }
    }
}
