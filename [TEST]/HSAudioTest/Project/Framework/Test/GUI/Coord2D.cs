﻿namespace HSAudioTest.Test.GUI
{
    public class Coord2D
    {
        public Coord2D() { }
        public Coord2D(int X, int Y) { this.X = X; this.Y = Y; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
