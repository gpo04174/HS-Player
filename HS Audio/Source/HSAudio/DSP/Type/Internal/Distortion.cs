﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.DSP.Type
{
    public class Distortion : ISettingManager
    {
        private Parameter[] param = new Parameter[1];
        public Distortion(HSAudio Value)
        {
            DSP = new DSP(Value, DSP_TYPE.DISTORTION, "Distortion", null);
        }
        public DSP DSP { get; private set; }

        /// <summary>
        /// Distortion value.  0.0 to 1.0.  Default = 0.5
        /// </summary>
        public float Level
        {
            get { return DSP.GetParameter((int)DSP_DISTORTION.LEVEL, 0f); }
            set { DSP.SetParameter((int)DSP_DISTORTION.LEVEL, value); }
        }

        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            DSP.Dispose();
            IsDisposed = true;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            if (Setting.Exist("Level")) try { Level = float.Parse(Setting["Level"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            if (Setting.SubSetting.Exist("DSP")) try { DSP.LoadSetting(Setting.SubSetting["DSP"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Level", Level);
            s.SubSetting.SetValue("DSP", DSP.SaveSetting(false));
            return s;
        }
    }
}
