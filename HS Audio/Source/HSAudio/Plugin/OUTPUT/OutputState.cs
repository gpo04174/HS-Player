﻿using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.PlugIn.Output
{
    public class OutputState
    {
        public OutputState()
        {
            
        }

        internal void Register(ref OUTPUT_STATE state)
        {

        }

        private RESULT OUTPUT_READFROMMIXER_FUNC(ref OUTPUT_STATE state, IntPtr buffer, uint length)
        {

        }
        private RESULT OUTPUT_COPYPORT_FUNC(ref OUTPUT_STATE state, int portId, IntPtr buffer, uint length)
        {

        }
        private RESULT OUTPUT_REQUESTRESET_FUNC(ref OUTPUT_STATE state)
        {

        }
        private IntPtr OUTPUT_ALLOC_FUNC(uint size, uint align, StringWrapper file, int line)
        {

        }
        private void OUTPUT_FREE_FUNC(IntPtr ptr, StringWrapper file, int line)
        {

        }
        private void OUTPUT_LOG_FUNC(DEBUG_FLAGS level, StringWrapper file, int line, StringWrapper function, StringWrapper wrapper)
        {

        }
    }
}
