﻿namespace HSAudio.Core.Sound._3D
{
    public interface Sound3D
    {
        Distance3D Distance { get; }
        Cone3D Cone { get; }
    }
}
