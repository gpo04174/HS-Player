﻿using HS.Setting;
using HSAudio.Lib.FMOD;
using HSAudio.Speakers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace HSAudio.Output
{
    public partial class Output
    {
        unsafe IntPtr CopySpeaker(SPEAKER[] speaker, int Count)
        {
            IntPtr ptr = Marshal.AllocHGlobal(sizeof(SPEAKER) * Count); //sizeof(SPEAKER) = sizeof(int)
            SPEAKER* spk = (SPEAKER*)ptr.ToPointer();
            for (int i = 0; i < Count; i++) spk[i] = speaker[i];
            return ptr;
        }


        internal static SPEAKER[] spk = { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.FRONT_CENTER, SPEAKER.LOW_FREQUENCY, SPEAKER.SURROUND_LEFT, SPEAKER.SURROUND_RIGHT, SPEAKER.BACK_LEFT, SPEAKER.BACK_RIGHT, SPEAKER.TOP_FRONT_LEFT, SPEAKER.TOP_FRONT_RIGHT, SPEAKER.TOP_BACK_LEFT, SPEAKER.TOP_BACK_RIGHT };
        internal static Dictionary<SpeakerMode, SPEAKER[]> speakers = new Dictionary<SpeakerMode, SPEAKER[]>();
        internal static void Speakers_Init()
        {
            speakers.Add(SpeakerMode.Mono, new SPEAKER[] { SPEAKER.FRONT_LEFT });
            speakers.Add(SpeakerMode.Stereo, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT });
            speakers.Add(SpeakerMode.Quad, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.SURROUND_LEFT, SPEAKER.SURROUND_RIGHT });
            //speakers.Add(SpeakerMode.Quad1, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.FRONT_CENTER, SPEAKER.LOW_FREQUENCY });
            speakers.Add(SpeakerMode.Surround, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.FRONT_CENTER, SPEAKER.SURROUND_LEFT, SPEAKER.SURROUND_RIGHT });
            speakers.Add(SpeakerMode.System5_1, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.FRONT_CENTER, SPEAKER.LOW_FREQUENCY, SPEAKER.SURROUND_LEFT, SPEAKER.SURROUND_RIGHT });
            speakers.Add(SpeakerMode.System7_1, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.FRONT_CENTER, SPEAKER.LOW_FREQUENCY, SPEAKER.SURROUND_LEFT, SPEAKER.SURROUND_RIGHT, SPEAKER.BACK_LEFT, SPEAKER.BACK_RIGHT });
            speakers.Add(SpeakerMode.System7_1_4, new SPEAKER[] { SPEAKER.FRONT_LEFT, SPEAKER.FRONT_RIGHT, SPEAKER.FRONT_CENTER, SPEAKER.LOW_FREQUENCY, SPEAKER.SURROUND_LEFT, SPEAKER.SURROUND_RIGHT, SPEAKER.BACK_LEFT, SPEAKER.BACK_RIGHT, SPEAKER.TOP_FRONT_LEFT, SPEAKER.TOP_FRONT_RIGHT, SPEAKER.TOP_BACK_LEFT, SPEAKER.TOP_BACK_RIGHT });
        }
    }
}
