﻿using HSAudio.Lib.FMOD;

namespace HSAudio.PlugIn.Output
{
    public class PortOpenInfo
    {
        internal PortOpenInfo(ref OUTPUT_STATE state, uint portType, ulong portIndex, ref int portId, ref int portRate, ref int portChannels, ref SOUND_FORMAT portFormat)
        {
            this.PortType = portType;
            this.PortIndex = PortIndex;
            this.PortId = portId;
            this.PortRate = portRate;
            this.PortChannel = portChannels;

            this.IsPCM = portFormat != SOUND_FORMAT.PCMFLOAT;
            switch (portFormat)
            {
                case SOUND_FORMAT.BITSTREAM: Bit = 1; break;
                case SOUND_FORMAT.MAX: Bit = -1; break;
                case SOUND_FORMAT.PCM8: Bit = 8; break;
                case SOUND_FORMAT.PCM16: Bit = 16; break;
                case SOUND_FORMAT.PCM24: Bit = 24; break;
                case SOUND_FORMAT.PCM32: Bit = 32; break;
                case SOUND_FORMAT.PCMFLOAT: Bit = 32; break;
                default: Bit = 0; break;
            }
        }

        /// <summary>
        /// Read only
        /// </summary>
        public uint PortType { get; private set; }
        /// <summary>
        /// Read only
        /// </summary>
        public uint PortIndex { get; private set; }

        /// <summary>
        /// Write only
        /// </summary>
        public int PortId { get; set; }
        /// <summary>
        /// Write only
        /// </summary>
        public int PortRate { get; set; }
        /// <summary>
        /// Write only
        /// </summary>
        public int PortChannel { get; set; }

        /// <summary>
        /// Write only
        /// </summary>
        public int Bit { get; set; }
        /// <summary>
        /// Write only
        /// </summary>
        public bool IsPCM { private get; set; }

        public static implicit operator SOUND_FORMAT(PortOpenInfo info)
        {
            if (info.IsPCM)
            {
                if (info.Bit == -1) return SOUND_FORMAT.MAX;
                else if (info.Bit == 0) return SOUND_FORMAT.BITSTREAM;
                else if (info.Bit == 8) return SOUND_FORMAT.PCM8;
                else if (info.Bit == 16) return SOUND_FORMAT.PCM16;
                else if (info.Bit == 24) return SOUND_FORMAT.PCM24;
                else if (info.Bit == 32) return SOUND_FORMAT.PCM32;
                else return SOUND_FORMAT.NONE;
            }
            else return SOUND_FORMAT.PCMFLOAT;
        }
    }

    internal enum SoundFormat
    {
        None = SOUND_FORMAT.NONE,
        Max = SOUND_FORMAT.MAX,
        BitStream = SOUND_FORMAT.BITSTREAM,
        PCM8 = SOUND_FORMAT.PCM8,
        PCM16 = SOUND_FORMAT.PCM16,
        PCM24 = SOUND_FORMAT.PCM24,
        PCM32 = SOUND_FORMAT.PCM32,
        Float = SOUND_FORMAT.PCMFLOAT,
    }
}
