﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class FLAC : ISoundFormat
    {
        public FLAC() : base(SoundType.FLAC, MIME, Extension, Description) { }
        
        public static string[] MIME { get { return new string[] { "audio/flac" }; } }
        public static string[] Extension { get { return new string[] { "flac" }; } }
        public static string[] Description { get { return new string[] { "Free Lossless Audio Codec" }; } }

        public override string ToString() { return Description[0]; }
    }
}
