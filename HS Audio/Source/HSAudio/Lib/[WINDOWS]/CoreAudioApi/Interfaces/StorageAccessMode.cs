﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Lib.Windows.CoreAudioApi.Interfaces
{
    /// <summary>
    /// MMDevice STGM enumeration
    /// </summary>
    enum StorageAccessMode
    {
        Read,
        Write,
        ReadWrite
    }
}
#endif