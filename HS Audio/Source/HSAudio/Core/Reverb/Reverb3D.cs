﻿using HS.Setting;
using System;

namespace HSAudio.Core
{
    public class Reverb3D : IEquatable<Reverb3D>, ISettingManager
    {
        ReverbManager Manager;
        internal Reverb3D(ReverbManager Manager) { this.Manager = Manager; }

        internal Vector3D _Vector;
        internal float _MidDistance;
        internal float _MaxDistance;

        public Vector3D Vector { get { return _Vector; } set { _Vector = value; Manager.Update(); } }
        public float MidDistance { get { return _MidDistance; } set { _MidDistance = value; Manager.Update(); } }
        public float MaxDistance { get { return _MaxDistance; } set { _MaxDistance = value; Manager.Update(); } }

        public override bool Equals(object obj) { Reverb3D rev = obj as Reverb3D; return rev == null ? false : Equals(rev); }
        public bool Equals(Reverb3D other)
        {
            return Vector.Equals(other.Vector) &&
                   MidDistance == other.MidDistance &&
                   MaxDistance == other.MaxDistance;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("MidDistance", MidDistance);
            s.SetValue("MaxDistance", MaxDistance);
            s.SubSetting.SetValue("Vector", Vector.SaveSetting());
            return s;
        }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new NotImplementedException();
        }
    }
}
