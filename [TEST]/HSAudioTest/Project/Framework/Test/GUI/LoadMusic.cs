﻿using System;
using System.Windows.Forms;

namespace HSAudioTest.Test.GUI
{
    public partial class LoadMusic : UserControl
    {
        #region Designer
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMusicOpen = new System.Windows.Forms.Button();
            this.txtMusicPath = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // btnMusicOpen
            // 
            this.btnMusicOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMusicOpen.Location = new System.Drawing.Point(385, 3);
            this.btnMusicOpen.Name = "btnMusicOpen";
            this.btnMusicOpen.Size = new System.Drawing.Size(33, 21);
            this.btnMusicOpen.TabIndex = 20;
            this.btnMusicOpen.Text = "...";
            this.btnMusicOpen.UseVisualStyleBackColor = true;
            this.btnMusicOpen.Click += new System.EventHandler(this.btnMusicOpen_Click);
            // 
            // txtMusicPath
            // 
            this.txtMusicPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMusicPath.Location = new System.Drawing.Point(4, 3);
            this.txtMusicPath.Name = "txtMusicPath";
            this.txtMusicPath.Size = new System.Drawing.Size(380, 21);
            this.txtMusicPath.TabIndex = 19;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // LoadMusic
            // 
            this.Controls.Add(this.btnMusicOpen);
            this.Controls.Add(this.txtMusicPath);
            this.Name = "LoadMusic";
            this.Size = new System.Drawing.Size(422, 27);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMusicOpen;
        private System.Windows.Forms.TextBox txtMusicPath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        #endregion

        public LoadMusic()
        {
            InitializeComponent();
        }

        public event EventHandler Loaded;

        public string MusicPath { get { return txtMusicPath.Text; }set { txtMusicPath.Text = value; } }
        private void btnMusicOpen_Click(object sender, EventArgs e)
        {
            if (Load() != null) Loaded(this, e);
        }

        public string Load()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK) { MusicPath = openFileDialog1.FileName; return txtMusicPath.Text; }
            else return null;
        }
    }
}
