﻿using HSAudio.Core.Sound.Kind;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;
using System.IO;

namespace HSAudio.Core.Sound._3D
{
    public partial class SoundStream3D : SoundStream, Sound3D
    {
        Distance3D _Distance;
        Cone3D _Cone;
        /*
        public SoundStream3D(HSAudio Audio, Stream Stream, WaveFormat Format, bool IsEndlessStream = false, uint BufferLength = 1024 * 16) : this(Audio, Stream, new RAW(Format), (IsEndlessStream ? uint.MaxValue : (uint)Stream.Length), IsEndlessStream, false, BufferLength) { }
        public SoundStream3D(HSAudio Audio, Stream Stream, WaveFormat Format, bool IsEndlessStream = false, bool LoadMemory = false, uint BufferLength = 1024 * 16) : this(Audio, Stream, new RAW(Format), (uint)Stream.Length, LoadMemory, IsEndlessStream, BufferLength) { }
        public SoundStream3D(HSAudio Audio, Stream Stream, WaveFormat Format, uint PCMLength, bool IsEndlessStream = false, bool LoadMemory = false, uint BufferLength = 1024 * 16) : this(Audio, Stream, new RAW(Format), PCMLength, IsEndlessStream, LoadMemory, BufferLength) { }
        */
        public SoundStream3D(HSAudio Audio, Stream Stream, RAW Raw, bool Seekable, bool LoadMemory = false) : base(Audio, Stream, Raw, Seekable, LoadMemory) { }
        public SoundStream3D(HSAudio Audio, Stream Stream, RAW Raw, uint PCMLength, bool Seekable, bool LoadMemory = false) : base(Audio, Stream, Raw, PCMLength, Seekable, LoadMemory)
        {
            _Distance = new Distance3D(sound);
            _Cone = new Cone3D(sound);
        }

        public Distance3D Distance { get { return _Distance; } }
        public Cone3D Cone { get { return _Cone; } }

        public override bool Load(bool ThrowException = true)
        {
            if (IsValid) return true;
            else
            {
                try
                {
                    try { Stream.Position = 0; } catch { }
                    
                    RESULT result = LoadMemory ?
                        Audio.system.createSound(MODE._3D | MODE.OPENUSER | MODE.LOOP_OFF, ref info, out sound) :
                        Audio.system.createStream(MODE._3D | MODE.OPENUSER | MODE.LOOP_OFF, ref info, out sound);

                    if (result != RESULT.OK) { sound.release(); throw new HSAudioException(result); }
                    else _Format = (WaveFormat)Format.Clone();

                    _Tag = SoundTag.GetTag(sound);
                    base.OnLoaded();
                    return true;
                }
                catch (Exception ex) { if (sound != null) sound.release(); if (ThrowException) throw ex; }
                return false;
            }
        }
    }
}
