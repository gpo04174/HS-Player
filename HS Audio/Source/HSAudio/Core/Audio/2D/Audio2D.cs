using HS.Setting;
using HSAudio.Core.Sound._2D;
using HSAudio.Lib.FMOD;
using HSAudio.Utility;
using System;

namespace HSAudio.Core.Audio._2D
{
    public partial class Audio2D : Audio
    {
        float _Pan;
        internal Audio2D(HSAudio audio, FD_ChannelGroup parent = null) : base(audio, false, parent) { }
        internal Audio2D(Sound2D Sound, FD_ChannelGroup parent = null) : base((Sound.Sound)Sound, false, parent) { }
        internal Audio2D(Sound.Sound Sound, FD_ChannelGroup parent = null) : base(Sound, false, parent) { }

        AudioState state;
        internal override void Unload()
        {
            if (group != null && group.isValid())
            {
                state = Status;
                _Pan = Pan;
            }
            base.Unload();
        }
        internal override void Reload(FD_ChannelGroup group_parent)
        {
            if (!IsDisposed && audio.Output.Status == global::HSAudio.Output.OutputStatus.Open)
            {
                RESULT Result = RESULT.OK;
                this.group_parent = group_parent;
                Result = audio.system.createChannelGroup(string.Format("Audio Session ({0})", ID), out group);
                Result = group_parent.addGroup(group, false, out conn);

                bool load = Sound != null ? Sound.Load() : false;

                if (this.Sound != null && (Sound.State != Core.Sound.SoundState.Unknown && Sound.State != Core.Sound.SoundState.Unload))
                {
                    Pan = _Pan;
                    base.Reload(group_parent);
                }
            }
        }

        #region Event
        public event FloatValueChangedEventHandler PanChanged;
        #endregion

        #region Properties
        public float Pan
        {
            get { return _Pan; }
            set
            {
                RESULT r = group.setPan(value);
                if (r == RESULT.OK) _Pan = value;
                LogCheck(r, "Pan");
                try { if (PanChanged != null) PanChanged.Invoke(this, value); } catch (Exception ex) { }
            }
        }
        #endregion

        #region Settings IO
        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Pan", Pan);
            return s;
        }
        public override bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            bool load = base.LoadSetting(Setting, ThrowException);
            if (Setting.Exist("Pan")) try { Pitch = float.Parse(Setting["Pan"]); } catch (Exception ex) { if (ThrowException) throw ex; }
            return load;
        }
        #endregion
    }
}