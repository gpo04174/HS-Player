﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HSPlayer.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MusicTitleControl : ContentView
	{
		public MusicTitleControl ()
		{
			InitializeComponent ();
            DefaultAlbumArt();
            this.LayoutChanged += MusicTitleControl_Load;
        }

        private void MusicTitleControl_Load(object sender, EventArgs e)
        {
            pic_album.WidthRequest = this.Height;
        }

        public string Title { get { return lbl_title.Text; } set { lbl_title.Text = value == null ? "" : value; } }
        public string Artist { get { return lbl_artist.Text; } set { lbl_artist.Text = value == null ? "" : value; ; } }
        public string Album { get { return lbl_album.Text; } set { lbl_album.Text = value == null ? "" : value; ; } }

        public ImageSource AlbumArt { get { return pic_album.Source; } set { pic_album.Source = value; } }

        public void DefaultAlbumArt()
        {
            AlbumArt = ImageSource.FromStream(() => new MemoryStream(Properties.Resources.default_album_orgin));
        }
    }
}