﻿using System;
using System.Collections.Generic;

namespace HS.Setting
{
    public class Setting<T> : ISetting<T>
    {
        private Dictionary<string, T> dic;
        public Setting() { Init(); SubSetting = new Setting<Settings>(0); }
        public Setting(Dictionary<string, T> dic) { this.dic = dic; SubSetting = new Setting<Settings>(0); }
        private Setting(byte dummy) {  }
        public void Init() { dic = new Dictionary<string, T>(); AutoAdd = true; }

        public T this[string Name]
        {
            get { return dic[Name]; }
            set { if (Exist(Name)) dic[Name] = value; else dic.Add(Name, value); }
        }
        public int Count { get { return dic.Count; } }

        public bool AutoAdd { get; set; }
        //public string Name { get; set; }
        public Setting<Settings> SubSetting { get; private set; }

        public T GetValue(string Name, T Default) { return Exist(Name) ? this[Name] : Default; }
        [Obsolete]
        public object GetValue(string Name, object Default) { return Exist(Name) ? this[Name] : Default; }
        public void SetValue(string Name, T Value)
        {
            if (dic == null) { Init(); dic.Add(Name, Value); }
            else if (Exist(Name)) this[Name] = Value;
            else dic.Add(Name, Value);
        }
        public bool Remove(string Name) { if (dic != null && dic.ContainsKey(Name)) { dic.Remove(Name); return true; } return false; }
        public bool Exist(string Name) { return dic == null ? false :dic.ContainsKey(Name); }
        //protected void Apply(Setting<T> Setting){dic = Setting.dic;}
        //public abstract void Flush();

        //public abstract string SaveSetting();
        //public abstract bool LoadSetting(string Setting);

        protected internal Dictionary<string, T> GetDictionary() { return dic; }
        public IEnumerator<KeyValuePair<string, T>> GetEnumerator() { if (dic == null) Init(); return dic.GetEnumerator(); }

        #region Convert Utils
        public static TYPE GetType(object obj)
        {
            if (typeof(string).Equals(obj.GetType())) return TYPE.String;
            if (typeof(string[]).Equals(obj.GetType())) return TYPE.String | TYPE.Array;
            else if (typeof(bool).Equals(obj.GetType())) return TYPE.Bool;
            else if (typeof(bool[]).Equals(obj.GetType())) return TYPE.Bool | TYPE.Array;
            else if (typeof(byte).Equals(obj.GetType())) return TYPE.Byte;
            else if (typeof(byte[]).Equals(obj.GetType())) return TYPE.Byte | TYPE.Array;
            else if (typeof(sbyte).Equals(obj.GetType())) return TYPE.SByte;
            else if (typeof(sbyte[]).Equals(obj.GetType())) return TYPE.SByte | TYPE.Array;
            else if (typeof(short).Equals(obj.GetType())) return TYPE.Short;
            else if (typeof(short[]).Equals(obj.GetType())) return TYPE.Short | TYPE.Array;
            else if (typeof(ushort).Equals(obj.GetType())) return TYPE.UShort;
            else if (typeof(ushort[]).Equals(obj.GetType())) return TYPE.UShort | TYPE.Array;
            else if (typeof(int).Equals(obj.GetType())) return TYPE.Int;
            else if (typeof(int[]).Equals(obj.GetType())) return TYPE.Int | TYPE.Array;
            else if (typeof(uint).Equals(obj.GetType())) return TYPE.UInt;
            else if (typeof(uint[]).Equals(obj.GetType())) return TYPE.UInt | TYPE.Array;
            else if (typeof(long).Equals(obj.GetType())) return TYPE.Long;
            else if (typeof(long[]).Equals(obj.GetType())) return TYPE.Long | TYPE.Array;
            else if (typeof(ulong).Equals(obj.GetType())) return TYPE.ULong;
            else if (typeof(ulong[]).Equals(obj.GetType())) return TYPE.ULong | TYPE.Array;
            else if (typeof(decimal).Equals(obj.GetType())) return TYPE.Decimal;
            else if (typeof(decimal[]).Equals(obj.GetType())) return TYPE.Decimal | TYPE.Array;
            else if (typeof(float).Equals(obj.GetType())) return TYPE.Float;
            else if (typeof(float[]).Equals(obj.GetType())) return TYPE.Float | TYPE.Array;
            else if (typeof(Single).Equals(obj.GetType())) return TYPE.Single;
            else if (typeof(Single[]).Equals(obj.GetType())) return TYPE.Single | TYPE.Array;
            else if (typeof(double).Equals(obj.GetType())) return TYPE.Double;
            else if (typeof(double[]).Equals(obj.GetType())) return TYPE.Double | TYPE.Array;
            else if (typeof(object[]).Equals(obj.GetType())) return TYPE.Object | TYPE.Array;
            else return TYPE.Object;
        }
        public static TYPE GetType<T1>(T1 obj)
        {
            if (typeof(string).Equals(typeof(T1))) return TYPE.String;
            if (typeof(string[]).Equals(typeof(T1))) return TYPE.String | TYPE.Array;
            else if (typeof(bool).Equals(typeof(T1))) return TYPE.Bool;
            else if (typeof(bool[]).Equals(typeof(T1))) return TYPE.Bool | TYPE.Array;
            else if (typeof(byte).Equals(typeof(T1))) return TYPE.Byte;
            else if (typeof(byte[]).Equals(typeof(T1))) return TYPE.Byte | TYPE.Array;
            else if (typeof(sbyte).Equals(typeof(T1))) return TYPE.SByte;
            else if (typeof(sbyte[]).Equals(typeof(T1))) return TYPE.SByte | TYPE.Array;
            else if (typeof(short).Equals(typeof(T1))) return TYPE.Short;
            else if (typeof(short[]).Equals(typeof(T1))) return TYPE.Short | TYPE.Array;
            else if (typeof(ushort).Equals(typeof(T1))) return TYPE.UShort;
            else if (typeof(ushort[]).Equals(typeof(T1))) return TYPE.UShort | TYPE.Array;
            else if (typeof(int).Equals(typeof(T1))) return TYPE.Int;
            else if (typeof(int[]).Equals(typeof(T1))) return TYPE.Int | TYPE.Array;
            else if (typeof(uint).Equals(typeof(T1))) return TYPE.UInt;
            else if (typeof(uint[]).Equals(typeof(T1))) return TYPE.UInt | TYPE.Array;
            else if (typeof(long).Equals(typeof(T1))) return TYPE.Long;
            else if (typeof(long[]).Equals(typeof(T1))) return TYPE.Long | TYPE.Array;
            else if (typeof(ulong).Equals(typeof(T1))) return TYPE.ULong;
            else if (typeof(ulong[]).Equals(typeof(T1))) return TYPE.ULong | TYPE.Array;
            else if (typeof(decimal).Equals(typeof(T1))) return TYPE.Decimal;
            else if (typeof(decimal[]).Equals(typeof(T1))) return TYPE.Decimal | TYPE.Array;
            else if (typeof(float).Equals(typeof(T1))) return TYPE.Float;
            else if (typeof(float[]).Equals(typeof(T1))) return TYPE.Float | TYPE.Array;
            else if (typeof(Single).Equals(typeof(T1))) return TYPE.Single;
            else if (typeof(Single[]).Equals(typeof(T1))) return TYPE.Single | TYPE.Array;
            else if (typeof(double).Equals(typeof(T1))) return TYPE.Double;
            else if (typeof(double[]).Equals(typeof(T1))) return TYPE.Double | TYPE.Array;
            else if (typeof(object[]).Equals(typeof(T1))) return TYPE.Object | TYPE.Array;
            else return TYPE.Object;
        }

        
        public static string GetTypeToString(TYPE Type)
        {
            bool IsArray = (Type & TYPE.Array) == TYPE.Array;//(int)(Type |~ TYPE.Array) == -1;
            TYPE t = Type & ~TYPE.Array;
            switch (t)
            {
                case TYPE.String: return IsArray ? "String,Array" : "String";
                case TYPE.Bool: return IsArray ? "Bool,Array" : "Bool";
                case TYPE.Byte: return IsArray ? "Byte,Array" : "Byte";
                case TYPE.SByte: return IsArray ? "SByte,Array" : "SByte";
                case TYPE.Short: return IsArray ? "Short,Array" : "Short";
                case TYPE.UShort: return IsArray ? "UShort,Array" : "UShort";
                case TYPE.Int: return IsArray ? "Int,Array" : "Int";
                case TYPE.UInt: return IsArray ? "UInt,Array" : "UInt";
                case TYPE.Long: return IsArray ? "Long,Array" : "Long";
                case TYPE.ULong: return IsArray ? "ULong,Array" : "ULong";
                case TYPE.Single: return IsArray ? "Float,Array" : "Float";
                case TYPE.Double: return IsArray ? "Double,Array" : "Double";
                default: return IsArray ? "Object,Array" : "Object";
            }
        }
        public static TYPE GetStringToType(string Type)
        {
            if (Type == null) return TYPE.Object;

            string[] t = Type.Split(',');
            bool IsArray = InArray(t, "Array"); 
            switch (t[0].ToLower())
            {
                case "string": return IsArray ? TYPE.String | TYPE.Array : TYPE.String;
                case "bool": return IsArray ? TYPE.Bool | TYPE.Array : TYPE.Bool;
                case "byte": return IsArray ? TYPE.Byte | TYPE.Array : TYPE.Byte;
                case "sbyte": return IsArray ? TYPE.SByte | TYPE.Array : TYPE.SByte;
                case "short": return IsArray ? TYPE.Short | TYPE.Array : TYPE.Short;
                case "ushort": return IsArray ? TYPE.UShort | TYPE.Array : TYPE.UShort;
                case "int": return IsArray ? TYPE.Int | TYPE.Array : TYPE.Int;
                case "uint": return IsArray ? TYPE.UInt | TYPE.Array : TYPE.UInt;
                case "long": return IsArray ? TYPE.Long | TYPE.Array : TYPE.Long;
                case "ulong": return IsArray ? TYPE.ULong | TYPE.Array : TYPE.ULong;
                case "single": return IsArray ? TYPE.Single | TYPE.Array : TYPE.Single;
                case "float": return IsArray ? TYPE.Float | TYPE.Array : TYPE.Float;
                case "double": return IsArray ? TYPE.Double | TYPE.Array : TYPE.Double;
                default: return IsArray ? TYPE.Object | TYPE.Array : TYPE.Object;
            }
        }

        public static bool InArray(string[] data, string word)
        {
            for (int i = 0; i < data.Length; i++)
                if (word.ToLower().Equals(data[i].ToLower())) return true;
            return false;
        }
        #endregion
    }
}
