﻿using HSAudio.Lib.FMOD;
using System;
using System.Threading;

namespace FMODTest.Test
{
    public class PlaySoundTest
    {
        public static void Test(string MusicPath)
        {
            FD_System system;
            FD_Sound sound, sound1;
            FD_Channel channel, channel1;

            Factory.System_Create(out system);

            RESULT result = RESULT.OK;
            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            result = system.createSound(MusicPath, MODE.DEFAULT, out sound);
            result = system.playSound(sound, null, false, out channel);
            channel.setCallback(new CHANNEL_CALLBACK(CHANNEL_CALLBACK));

            bool play = true;
            while (play)
            {
                channel.isPlaying(out play);
                uint pos; channel.getPosition(out pos, TIMEUNIT.MS);
                Console.WriteLine(pos);

                //system.update();
                Thread.Sleep(10);
            }
            
        }

        protected static RESULT CHANNEL_CALLBACK(IntPtr channelraw, CHANNELCONTROL_TYPE controltype, CHANNELCONTROL_CALLBACK_TYPE type, IntPtr commanddata1, IntPtr commanddata2)
        {
            if (type == CHANNELCONTROL_CALLBACK_TYPE.END)
            {

            }
            return RESULT.OK;
        }
    }
}
