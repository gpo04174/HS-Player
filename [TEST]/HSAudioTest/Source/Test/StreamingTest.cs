﻿using HSAudio.Core.Audio;
using HSAudio.Core.Sound;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Lib.NVorbis;
using HSAudio.Lib.Windows.NAudio.Wave;
using HSAudio.Streams;
using HSAudio.Utility.Stream;
using HSAudio.Wave;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using Test;

namespace HSAudioTest.Test
{
    public class StreamingTest
    {
        public static void BufferTest()
        {
            MemoryStream ms = new MemoryStream();
            for (byte i = 0; i < 50; i++) ms.WriteByte(i);
            ms.Position = 0;
            FixBufferStream fs = new FixBufferStream(ms, 2, 1);
            byte[] b = new byte[10];
            int read = fs.Read(b, 0, b.Length);
            fs.Position = 5;
            b = new byte[13];
            read = fs.Read(b, 0, b.Length);
            b = new byte[10];
            read = fs.Read(b, 0, b.Length);
            fs.Position = 25;
            b = new byte[5];
            read = fs.Read(b, 0, b.Length);
            b = new byte[5];
            read = fs.Read(b, 0, b.Length);
        }
        public static void Test(string MusicPath)
        {
            //"http://185.33.21.112/classical_128"
            //"C:\\Users\\gpo04\\OneDrive\\음악\\보컬로이드\\IA\\Orangestar\\アスノヨゾラ哨戒班 (Night Sky Patrol of Tomorrow).mp3"
            //"file:///C:/Users/gpo04/OneDrive/음악/보컬로이드/IA/Orangestar/アスノヨゾラ哨戒班%20(Night%20Sky%20Patrol%20of%20Tomorrow).mp3"
            //"file:///C:/Users/gpo04/OneDrive/음악/[Test]/シンクロナイザー.ogg"
            //"C:\\Users\\gpo04\\OneDrive\\음악\\[Test]\\シンクロナイザー.ogg"
            Console.Title = "HSAudio Streaming Test";
            Stream Stream;

            //FileStream fs = new FileStream(, FileMode.Open);
            //Stream = new Mp3FileReader(fs);

            //WebRequest webRequest = (HttpWebRequest)WebRequest.Create("http://theprodigy.ru:8000/live.ogg");
            //webRequest.Timeout = 10000;
            //WebResponse resp = webRequest.GetResponse();
            //Stream = new StreamFix(resp.GetResponseStream());//);

            //Stream = new FileStream("Z:\\asd.raw", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);//
            //FileStream fs = new FileStream("C:\\Users\\gpo04\\OneDrive\\음악\\[Test]\\シンクロナイザー.ogg", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);//


            /*
            FileStream fs = new FileStream("D:\\test.raw", FileMode.Create);
            WebRequest webRequest = (HttpWebRequest)WebRequest.Create("http://theprodigy.ru:8000/live.ogg");
            WebResponse resp = webRequest.GetResponse();
            VorbisReader vorbis = new VorbisReader(resp.GetResponseStream(), true); //System.NotSupportedException

            long testlen = 0;
            while (testlen < 100000)
            {
                float[] buf = new float[1024];
                int len = vorbis.ReadSamples(buf, 0, buf.Length);
                for (int i = 0; i < len; i++)
                {
                    byte[] four = BitConverter.GetBytes(buf[i]);
                    fs.Write(four, 0, 4);
                }

                fs.Flush();
            }
            */

            FileStream fs = new FileStream("C:\\Users\\gpo04\\OneDrive\\음악\\[Test]\\シンクロナイザー.ogg", FileMode.Open);
            //WebStreamRead web = new WebStreamRead("http://theprodigy.ru:8000/live.ogg", 3000, long.MaxValue);
            //FixBufferStream fix = new FixBufferStream(web, -1, 10240, 256);
            Stream = new VorbisReadStream(fs);

            HSAudio.HSAudio audio = new HSAudio.HSAudio();
            HSAudio.Wave.WaveFormat format = new HSAudio.Wave.WaveFormat(44100, 2, 32, WaveType.FLOAT);
            Sound s = Sound.FromStream(audio, Stream, new RAW(format), false, true);
            Audio a = audio.Audio.Add(s);
            a.Loop = LoopKind.Off;

            a.Play();

            int curL = Console.CursorLeft, curT = Console.CursorTop;
            uint before = 0;
            //a.CurrentPosition = s.TotalPosition - 5000;
            while (true)
            {
                if (before != a.CurrentPosition)
                {
                    Console.Write("\n[{0} / {1}] {2} / {3} -> [Buffer: {4}% ({5}), {6}]",
                        Program.GetTime(a.CurrentPosition),
                        Program.GetTime(s.TotalPosition),
                        a.CurrentPosition,
                        s.TotalPosition,
                        (Stream.Position / (float)Stream.Length) * 100,
                          //Stream.CurrentTime.ToString(),
                        s.Status.Starving,
                        s.Type.Type);
                }

                before = a.CurrentPosition;
                //if (a.Status == AudioState.Stopped && !a.getIsPlaying()) a.Play();
                Thread.Sleep(10);
            }
        }

        private static IMp3FrameDecompressor CreateFrameDecompressor(Mp3Frame frame)
        {
            Mp3WaveFormat waveFormat = new Mp3WaveFormat(frame.SampleRate, frame.ChannelMode == ChannelMode.Mono ? 1 : 2,
                frame.FrameLength, frame.BitRate);
            return new AcmMp3FrameDecompressor(waveFormat);
        }
    }

    class MpegStream : Stream
    {
        IMp3FrameDecompressor decompressor;
        Thread th;
        BufferedWaveProvider bufferedWaveProvider;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Mp3Stream"></param>
        /// <param name="MaxBufferLength">1 MB</param>
        /// <param name="MinBufferLength">80 KB</param>
        public MpegStream(Stream Mp3Stream)
        {
            BaseStream = Mp3Stream;
            Mp3Frame frame = Mp3Frame.LoadFromStream(BaseStream);
            Mp3WaveFormat waveFormat = new Mp3WaveFormat(frame.SampleRate, frame.ChannelMode == ChannelMode.Mono ? 1 : 2,frame.FrameLength, frame.BitRate);
            decompressor = new AcmMp3FrameDecompressor(waveFormat);
            //this.MaxBufferLength = MaxBufferLength;
            //this.MinBufferLength = MinBufferLength;

            bufferedWaveProvider = new BufferedWaveProvider(decompressor.OutputFormat);
            bufferedWaveProvider.BufferDuration = TimeSpan.FromSeconds(10); // allow us to get well ahead of ourselves

            th = new Thread(Read_Loop);
            th.Start();
        }

        public Stream BaseStream { get; private set; }

        public override bool CanRead { get { return true; } }

        public override bool CanSeek { get { return true; } }

        public override bool CanWrite { get { return false; } }

        public override long Length { get { return bufferedWaveProvider.BufferLength; } }
        
        public override long Position
        {
            get { return bufferedWaveProvider.BufferedBytes; }set {}
        }

        public override void Flush() { }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return bufferedWaveProvider.Read(buffer, offset, count); 
        }
        
        public int MaxBufferLength { get { return bufferedWaveProvider.BufferLength; } set { bufferedWaveProvider.BufferLength = value; } }
        public TimeSpan TotalTime { get { return bufferedWaveProvider.BufferDuration; } set { bufferedWaveProvider.BufferDuration = value; } }
        public TimeSpan CurrentTime { get { return bufferedWaveProvider.BufferedDuration; } }
        void Read_Loop()
        {
            while(true)
            {
                try
                {
                    if (bufferedWaveProvider.BufferedBytes < (MaxBufferLength * 0.99f))
                    {
                        Mp3Frame frame = Mp3Frame.LoadFromStream(BaseStream);
                        if (frame != null)
                        {
                            byte[] buffer = new byte[1024 * 10];
                            int decompressed = decompressor.DecompressFrame(frame, buffer, 0);
                            //Debug.WriteLine(String.Format("Decompressed a frame {0}", decompressed));
                            bufferedWaveProvider.AddSamples(buffer, 0, decompressed);
                        }
                        else { Thread.Sleep(1); continue; }
                    }
                }
                catch { }
            }
        }

        public override long Seek(long offset, SeekOrigin origin) { return 0; }

        public override void SetLength(long value) { }

        public override void Write(byte[] buffer, int offset, int count) { }

#if NETSTANDARD
        public void Dispose()
#else
        public override void Close()
#endif
        {
            
        }
    }

    public class OggFixStream : Stream
    {
        CircularBuffer cir;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Buffer">1 = 1KB, 1024 = 1MB</param>
        public OggFixStream(Stream Source, int SizeKB = 1024) //1MB
        {
            BaseStream = Source;
            this.SizeKB = SizeKB;
            cir = new CircularBuffer(1024 * SizeKB);

            byte[] buf = new byte[1024];
            for (int i = 0; i < SizeKB; i++)
            {
                int read = Source.Read(buf, 0, 1024);
                int write = cir.Write(buf, 0, read);
            }
        }

        public Stream BaseStream { get; private set; }
        public int SizeKB { get; private set; }

        public override bool CanRead { get { return true; } }

        public override bool CanSeek { get { return true; } }

        public override bool CanWrite { get { return false; } }

        public override long Length { get { return BaseStream.Length; } }

        long _Position;
        long ab;
        public override long Position
        {
            get { return _Position; }
            set
            {
                _Position = value;
                ab = value - _Position;
            }
        }

        bool IsOver;
        public override int Read(byte[] buffer, int offset, int count)
        {
            /*
            long ab = this.ab;
            this.ab = 0;
            int len_read = 0;
            if (!IsOver && count + Position > SizeKB)
            {
                cir.Read();

                int first = (int)(SizeKB - Position); //200
                int second = count - first; //100
                cir.Read(buffer, )
                Buffer.BlockCopy(this.buffer, (int)Position, buffer, offset, first);//
                int read = BaseStream.Read(buffer, first, second);
                IsOver = true;
                _Position = MaxBuffer;
                return first + read;
            }
            if (IsOver) { len_read = BaseStream.Read(buffer, offset, count); _Position += len_read; }
            else { Buffer.BlockCopy(this.buffer, (int)Position, buffer, offset, count); len_read = count; _Position += count; }
            */
            return 0;
        }
        
        public override void Flush() { }
        public override long Seek(long offset, SeekOrigin origin) { return 0; }

        public override void SetLength(long value) { }

        public override void Write(byte[] buffer, int offset, int count) { }


        /// <summary>
        /// A very basic circular buffer implementation
        /// </summary>
        public class CircularBuffer
        {
            private readonly byte[] buffer;
            private readonly object lockObject;
            private int writePosition;
            private int readPosition;
            private int byteCount;

            /// <summary>
            /// Create a new circular buffer
            /// </summary>
            /// <param name="size">Max buffer size in bytes</param>
            public CircularBuffer(int size)
            {
                buffer = new byte[size];
                lockObject = new object();
            }

            /// <summary>
            /// Write data to the buffer
            /// </summary>
            /// <param name="data">Data to write</param>
            /// <param name="offset">Offset into data</param>
            /// <param name="count">Number of bytes to write</param>
            /// <returns>number of bytes written</returns>
            public int Write(byte[] data, int offset, int count)
            {
                lock (lockObject)
                {
                    var bytesWritten = 0;
                    if (count > buffer.Length - byteCount)
                    {
                        count = buffer.Length - byteCount;
                    }
                    // write to end
                    int writeToEnd = Math.Min(buffer.Length - writePosition, count);
                    Array.Copy(data, offset, buffer, writePosition, writeToEnd);
                    writePosition += writeToEnd;
                    writePosition %= buffer.Length;
                    bytesWritten += writeToEnd;
                    if (bytesWritten < count)
                    {
#if DEBUG
                        Debug.Assert(writePosition == 0);
#endif
                        // must have wrapped round. Write to start
                        Array.Copy(data, offset + bytesWritten, buffer, writePosition, count - bytesWritten);
                        writePosition += (count - bytesWritten);
                        bytesWritten = count;
                    }
                    byteCount += bytesWritten;
                    return bytesWritten;
                }
            }

            /// <summary>
            /// Read from the buffer
            /// </summary>
            /// <param name="data">Buffer to read into</param>
            /// <param name="offset">Offset into read buffer</param>
            /// <param name="count">Bytes to read</param>
            /// <returns>Number of bytes actually read</returns>
            public int Read(byte[] data, int offset, int count)
            {
                lock (lockObject)
                {
                    if (count > byteCount)
                    {
                        count = byteCount;
                    }
                    int bytesRead = 0;
                    int readToEnd = Math.Min(buffer.Length - readPosition, count);
                    Array.Copy(buffer, readPosition, data, offset, readToEnd);
                    bytesRead += readToEnd;
                    readPosition += readToEnd;
                    readPosition %= buffer.Length;

                    if (bytesRead < count)
                    {
                        // must have wrapped round. Read from start
                        Debug.Assert(readPosition == 0);
                        Array.Copy(buffer, readPosition, data, offset + bytesRead, count - bytesRead);
                        readPosition += (count - bytesRead);
                        bytesRead = count;
                    }

                    byteCount -= bytesRead;
                    Debug.Assert(byteCount >= 0);
                    return bytesRead;
                }
            }

            /// <summary>
            /// Maximum length of this circular buffer
            /// </summary>
            public int MaxLength
            {
                get { return buffer.Length; }
            }

            /// <summary>
            /// Number of bytes currently stored in the circular buffer
            /// </summary>
            public int Count
            {
                get { return byteCount; }
            }

            /// <summary>
            /// Resets the buffer
            /// </summary>
            public void Reset()
            {
                byteCount = 0;
                readPosition = 0;
                writePosition = 0;
            }

            /// <summary>
            /// Advances the buffer, discarding bytes
            /// </summary>
            /// <param name="count">Bytes to advance</param>
            public void Advance(int count)
            {
                if (count >= byteCount)
                {
                    Reset();
                }
                else
                {
                    byteCount -= count;
                    readPosition += count;
                    readPosition %= MaxLength;
                }
            }
        }
    }

    class QueueStream
    {
        Queue<byte> queue = new Queue<byte>();

        public long Length { get { return queue.Count; } }

        public void Clear() { queue.Clear(); }

        public int Read(byte[] buffer, int offset, int count)
        {
            int i = 0;
            for (i = offset; i < count; i++) buffer[i] = queue.Dequeue();
            return i;
        }
        
        public long MaxBufferLength { get; set; }

        public void Write(byte[] buffer, int offset, int count)
        {
            if (Length < MaxBufferLength)
                for (int i = offset; i < count; i++)
                 queue.Enqueue(buffer[i]);
        }
    }


    public class StreamFix : Stream
    {
        private readonly Stream sourceStream;
        private long pos = 1; // psuedo-position
        private readonly byte[] readAheadBuffer;
        private int readAheadLength;
        private int readAheadOffset;

        public StreamFix(Stream sourceStream)
        {
            this.sourceStream = sourceStream;
            readAheadBuffer = new byte[4096];
        }
        public override bool CanRead {get { return true; } }
        public override bool CanSeek { get { return false; } }
        public override bool CanWrite {get { return false; } }

        public override void Flush() {}

        public override long Length { get { return pos; } }

        public override long Position { get { return pos; } set { } }


        public override int Read(byte[] buffer, int offset, int count)
        {
            int bytesRead = 0;
            while (bytesRead < count)
            {
                int readAheadAvailableBytes = readAheadLength - readAheadOffset;
                int bytesRequired = count - bytesRead;
                if (readAheadAvailableBytes > 0)
                {
                    int toCopy = Math.Min(readAheadAvailableBytes, bytesRequired);
                    Array.Copy(readAheadBuffer, readAheadOffset, buffer, offset + bytesRead, toCopy);
                    bytesRead += toCopy;
                    readAheadOffset += toCopy;
                }
                else
                {
                    readAheadOffset = 0;
                    readAheadLength = sourceStream.Read(readAheadBuffer, 0, readAheadBuffer.Length);
                    if (readAheadLength == 0)
                    {
                        break;
                    }
                }
            }
            pos += bytesRead;
            return bytesRead;
        }

        public override long Seek(long offset, SeekOrigin origin) {throw new InvalidOperationException();}
        public override void SetLength(long value) { throw new InvalidOperationException(); }
        public override void Write(byte[] buffer, int offset, int count) {throw new InvalidOperationException(); }
    }
}
