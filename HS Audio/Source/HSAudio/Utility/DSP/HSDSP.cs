﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSAudio.Utility.DSP
{
    public static partial class HSDSP
    {
        #region Volume
        public static double Volume(this double Sample, double Gain)
        {
            if (Gain == 0) return Sample;
            double a = Sample * Math.Pow(10.0, Gain * 0.05);
            /*
            if (a == double.PositiveInfinity ||
               a == double.NegativeInfinity ||
               a == double.NaN) a = double.MinValue;*/
            return a;
        }
        public static float Volume(this float Sample, float Gain)
        {
            if (Gain == 0) return Sample;
            float a = Sample * (float)Math.Pow(10.0, Gain * 0.05);
            /*
            if (a == float.PositiveInfinity ||
               a == float.NegativeInfinity ||
               a == float.NaN) a = float.MinValue;*/
            return a;
        }
        public static double Volume(this double Sample, int Gain)
        {
            double result = 1;
            while (Gain > 0)
            {
                if (Gain % 2 == 1)
                    result *= 10;
                Gain >>= 1;
            }
            return Sample * result;
        }
        public static float Volume(this float Sample, int Gain)
        {
            float result = 1;
            while (Gain > 0)
            {
                if (Gain % 2 == 1)
                    result *= 10;
                Gain >>= 1;
            }
            return Sample * result;
        }
        #endregion
    }
}
