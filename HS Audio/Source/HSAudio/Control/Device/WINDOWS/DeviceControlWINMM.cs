﻿#if WINFORM && (WIN_XP || WIN_VISTA || WIN_10_1703)
using HSAudio.Lib.Windows.WaveLib.AudioMixer;
using HSAudio.Wave.Device;
using System;

namespace HSAudio.Control.Device
{
    public class DeviceControlWINMM : DeviceControl
    {
        internal static DeviceControlWINMM[] GetInstance(WaveDevice Device, MixerLines lines, int index, int count)
        {
            DeviceControlWINMM[] w = new DeviceControlWINMM[count];
            for (int i = index; i < count; i++) w[i] = new DeviceControlWINMM(Device, lines[i]);
            return w;
        }

        internal MixerLine _Control;
        protected internal DeviceControlWINMM(WaveDevice Device, MixerLine Control)
        {
            this._Control = Control;
            this.Device = Device;
        }

        public WaveDevice Device { get; private set; }

        public float VolumeMax { get { return _Control.VolumeMax; } }
        public float VolumeMin { get { return _Control.VolumeMin; } }
        public float Volume { get { return ((float)_Control.Volume / _Control.VolumeMax); } set { _Control.Volume = (int)Math.Min(value * _Control.VolumeMax, _Control.VolumeMax); } }
        public float VolumePercent { get { return _Control.VolumePercent; } set { _Control.VolumePercent = value; } }
        public float VolumeScalar { get { return _Control.VolumeScala; } set { _Control.VolumeScala = value; } }
        public bool Mute { get { return _Control.Mute; } set { _Control.Mute = value; } }

        public string Name { get { return _Control.Name; } }
        public object Control { get { return _Control; } }

        public override bool Equals(object obj)
        {
            return obj is DeviceControlWINMM ? ((DeviceControlWINMM)obj)._Control.HMixer == _Control.HMixer : false;
        }
        public override int GetHashCode() { return base.GetHashCode(); }
    }
}
#endif