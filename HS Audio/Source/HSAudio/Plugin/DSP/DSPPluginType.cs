﻿using HSAudio.Lib.FMOD;

namespace HSAudio.PlugIn.DSP
{
    public enum DSPPluginType
    {
        HSAudioPlugin = DSP_TYPE.UNKNOWN,
#if !XBOX
        VST = DSP_TYPE.VSTPLUGIN,
#endif
#if WIN_XP || WIN_VISTA || WIN_10_1703
        WinAMP = DSP_TYPE.WINAMPPLUGIN,
#endif
#if LINUX
        LADSPA = DSP_TYPE.LADSPAPLUGIN
#endif
    }
}