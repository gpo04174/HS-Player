﻿using HSAudio.Lib.FMOD;

namespace HSAudio.Speakers
{
    public enum SpeakerMode
    {
        Default = 0,
        Custom = -1,
        Mono = 1,
        Stereo = 2,
        Quad = 4,
        Surround = 5,
        System5_1 = 6,
        System7_1 = 8,
#if WEB
        ForceInteger
#else
        System7_1_4 = 12
#endif

    }
    
    internal static class ConvertSpeakerMode
    {
        public static SPEAKERMODE ConvertFMOD(this SpeakerMode Mode)
        {
            switch (Mode)
            {
                case SpeakerMode.Default: return SPEAKERMODE.DEFAULT;
                case SpeakerMode.Mono: return SPEAKERMODE.MONO;
                case SpeakerMode.Stereo: return SPEAKERMODE.STEREO;
                case SpeakerMode.Quad: return SPEAKERMODE.QUAD;
                case SpeakerMode.Surround: return SPEAKERMODE.SURROUND;
                case SpeakerMode.System5_1: return SPEAKERMODE._5POINT1;
                case SpeakerMode.System7_1: return SPEAKERMODE._7POINT1;
                case SpeakerMode.System7_1_4: return SPEAKERMODE._7POINT1POINT4;
                default: return SPEAKERMODE.RAW;
            }
        }

        public static SpeakerMode ConvertInternal(this SPEAKERMODE Mode)
        {
            switch(Mode)
            {
                case SPEAKERMODE.DEFAULT: return SpeakerMode.Default;
                case SPEAKERMODE.MONO: return SpeakerMode.Mono;
                case SPEAKERMODE.STEREO: return SpeakerMode.Stereo;
                case SPEAKERMODE.QUAD: return SpeakerMode.Quad;
                case SPEAKERMODE.SURROUND: return SpeakerMode.Surround;
                case SPEAKERMODE._5POINT1: return SpeakerMode.System5_1;
                case SPEAKERMODE._7POINT1: return SpeakerMode.System7_1;
                case SPEAKERMODE._7POINT1POINT4: return SpeakerMode.System7_1_4;
                default: return SpeakerMode.Custom;
            }
        }
    }

}
