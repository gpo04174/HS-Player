using System;
using HS.Setting;
using HSAudio.Core;
using HSAudio.Lib.FMOD;

namespace HSAudio.Core
{
    public class ReverbManager : ISettingManager
    {
        int REVERB_INSTANCE = 0;
        HSAudio Audio;
        RESULT Result;
        FD_Reverb3D rev;
        Reverb _Reverb;
        internal ReverbManager(HSAudio Audio) { this.Audio = Audio; Reverb3D = new Reverb3D(this); }

        public Reverb Reverb { get { Refresh(); return _Reverb; } set { _Reverb = value; Update(); } }
        public Reverb3D Reverb3D { get; private set; }

        bool _Enable3D;
        public bool Enable3D
        { 
            get { return _Enable3D; } 
            set 
            {
                if (value != Enable3D)
                {
                    _Enable3D = value;
                    if (value)
                    {
                        Result = Audio.system.createReverb3D(out rev);
                        Result = rev.setActive(true);
                        Update();
                        //Logging here...
                    }
                    else
                    {
                        Update();
                        if (rev != null) { rev.setActive(false); rev.release(); rev = null; }
                        Reverb = _Reverb;
                    }
                    Audio.Update();
                }
            } 
        }

        internal void Refresh()
        {
            if (rev == null) Result = Audio.system.getReverbProperties(REVERB_INSTANCE, out _Reverb.reverb);
            else
            {
                Result = rev.getProperties(out _Reverb.reverb);
                Result = rev.get3DAttributes(out Reverb3D._Vector, out Reverb3D._MidDistance, out Reverb3D._MaxDistance);
            }
        }
        internal void Update()
        {
            if (rev == null) Result = Audio.system.setReverbProperties(REVERB_INSTANCE, ref _Reverb.reverb);
            else
            {
                Result = rev.setProperties(ref _Reverb.reverb);
                Result = rev.set3DAttributes(ref Reverb3D._Vector, Reverb3D._MidDistance, Reverb3D._MaxDistance);
            }
        }
        internal void Reload()
        {
            if (rev != null)
            {
                Result = Audio.system.createReverb3D(out rev);
                Result = rev.setActive(true);
            }
            Update();
        }

        public virtual Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Enable3D", Enable3D);
            s.SubSetting.SetValue("Reverb", Reverb.SaveSetting());
            s.SubSetting.SetValue("Reverb3D", Reverb3D.SaveSetting());
            return s;
        }

        public virtual bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new NotImplementedException();
        }

        internal void Dispose(){if(rev != null)rev.release();}
    }
}