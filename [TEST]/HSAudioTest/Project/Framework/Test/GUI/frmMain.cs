﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace HSAudioTest.Test.GUI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        string MusicPath;
        private void button2_Click(object sender, EventArgs e)
        {
            if(openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = MusicPath = openFileDialog1.FileName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoopTest.Test(MusicPath);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PlayTest.Test(MusicPath);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            StreamingTest.Test("D:\\未完成タイムリミッター.raw");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            new SoundDeviceTest().Show();
        }
    }
}
