﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class CODEC : ISoundFormat
    {
        public CODEC(byte[] PassKey = null) : base(SoundType.Codec, null, null, null){ this.PassKey = PassKey; }
        /// <summary>
        /// DRM Key or when sound key is support encryption and encrypted. (DRM 키 또는 사운드가 암호화를 지원하고 암호화되었을때 키 입니다.)
        /// </summary>
        public byte[] PassKey { get; private set; }

        public override string ToString()
        {
            throw new NotImplementedException();
        }
    }
}
