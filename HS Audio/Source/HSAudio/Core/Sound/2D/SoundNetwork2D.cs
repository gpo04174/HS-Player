﻿using HS.Setting;
using HSAudio.Core.Sound.Kind;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System;

namespace HSAudio.Core.Sound._2D
{
    public class SoundNetwork2D : SoundNetwork, Sound2D
    {
        public SoundNetwork2D(HSAudio Audio, string NetworkURL, uint BufferLength = 1024 * 64) : this(Audio, NetworkURL, null, BufferLength) { }
        public SoundNetwork2D(HSAudio Audio, string NetworkURL, ISoundFormat Type, uint BufferLength = 1024 * 64) : base(Audio, NetworkURL, Type, BufferLength) { }

        public override bool Load(bool ThrowException = true)
        {
            if (IsValid) return true;
            else
            {
                try
                {
                    Result = Audio.system.createStream(NetworkURL, MODE._2D | MODE.IGNORETAGS, out sound);
                    if (Result != RESULT.OK) { sound.release(); throw new HSAudioException(Result); }
                    else
                    {
                        _Format = getFormat();
                        if(_SoundType == null) _SoundType = getType();
                        Audio.BufferSizeStream = _BufferLength;
                        _Tag = SoundTag.GetTag(sound);
                    }
                    base.OnLoaded();
                    return true;
                }
                catch (Exception ex) { if (ThrowException) throw ex; }
                return false;
            }
        }

        public override Settings SaveSetting()
        {
            Settings s = base.SaveSetting();
            s.SetValue("Type", "Network");
            s.SetValue("Dimension", "2D");
            s.SetValue("BufferLength", _BufferLength);
            s.SetValue("NetworkURL", new SettingsData(NetworkURL, false));
            return s;
        }
        public static SoundNetwork2D LoadSetting(HSAudio Audio, Settings Setting, bool ThrowException = false)
        {
            string NetworkURL = null;
            uint BufferLength = Audio.BufferSizeStream;
            ISoundFormat type = null;
            //WaveFormat format = null;

            try { if (Setting.Exist("NetworkURL")) NetworkURL = Setting["NetworkURL"]; } catch (Exception ex) { if (ThrowException) throw ex; return null; }
            try { if (Setting.Exist("BufferLength")) BufferLength = Convert.ToUInt32(Setting["BufferLength"]); } catch (Exception ex) { if (ThrowException) throw ex; return null; }
            try { if (Setting.SubSetting.Exist("Type")) type = ISoundFormat.FromSetting(Setting.SubSetting["Type"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            //try { if (Setting.SubSetting.Exist("Format")) format = WaveFormat.Load(Setting.SubSetting["Format"], ThrowException); } catch (Exception ex) { if (ThrowException) throw ex; }
            return new SoundNetwork2D(Audio, NetworkURL, type, BufferLength);
        }
    }
}
