﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MarshalTest__CSharp_
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ShowString("가나다"));
        }

        [DllImport("MarshalTest (CPP).dll", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
        static extern StringWrapper ShowString(StringWrapper ptr);
    }


    #region wrapperinternal
    [StructLayout(LayoutKind.Sequential)]
    public struct StringWrapper
    {
        IntPtr nativeUtf8Ptr;

        public static implicit operator string(StringWrapper fstring)
        {
            if (fstring.nativeUtf8Ptr == IntPtr.Zero)
            {
                return "";
            }

            int strlen = 0;
            while (Marshal.ReadByte(fstring.nativeUtf8Ptr, strlen) != 0)
            {
                strlen++;
            }
            if (strlen > 0)
            {
                byte[] bytes = new byte[strlen];
                Marshal.Copy(fstring.nativeUtf8Ptr, bytes, 0, strlen);
                return Encoding.UTF8.GetString(bytes, 0, strlen);
            }
            else
            {
                return "";
            }
        }
        public static implicit operator StringWrapper(string str)
        {
            if (str == null) return null;
            else
            {
                //byte[] bytes = Encoding.UTF8.GetBytes(str);
                //IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * bytes.Length);
                //Marshal.Copy(bytes, 0, ptr, bytes.Length);
                //return new StringWrapper() { nativeUtf8Ptr = ptr };
                return new StringWrapper() { nativeUtf8Ptr = Marshal.StringToHGlobalUni(str) };
            }
        }
    }
    #endregion
}
