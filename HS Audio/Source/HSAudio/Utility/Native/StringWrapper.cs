﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.Utility.Native
{
    [StructLayout(LayoutKind.Sequential)]
    public class StringWrapper : IDisposable
    {
        public StringWrapper() { }
        public StringWrapper(string Text) { nativeUtf8Ptr = (StringWrapper)Text; }

        IntPtr nativeUtf8Ptr;

        public static implicit operator IntPtr(StringWrapper fstring) { return fstring.nativeUtf8Ptr; }
        public static implicit operator string(StringWrapper fstring)
        {
            if (fstring.nativeUtf8Ptr == IntPtr.Zero) return null;
            else return Marshal.PtrToStringUni(fstring.nativeUtf8Ptr);
        }
        public static implicit operator StringWrapper(string str)
        {
            if (str == null) return null;
            else return new StringWrapper() { nativeUtf8Ptr = Marshal.StringToHGlobalUni(str) };
        }
        public unsafe static implicit operator StringWrapper(char[] str)
        {
            if (str == null || str.Length == 0) return null;
            else
            {
#if !NETSTANDARD
                //MemorySave
                int num = Encoding.UTF8.GetByteCount(str);
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * num);
                byte* b = (byte*)ptr;
                fixed(char* c = str)
                    Encoding.UTF8.GetBytes(c, num, b, sizeof(byte) * num);
#else
                byte[] tmp = Encoding.UTF8.GetBytes(str);
                IntPtr ptr = Marshal.AllocHGlobal(sizeof(byte) * tmp.Length);
#endif
                return new StringWrapper() { nativeUtf8Ptr = ptr };
            }
        }

        public static IntPtr Copy(char[] str)
        {
            int len = str.Length * sizeof(char);
            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.Copy(str, 0, ptr, len);
            return ptr;
        }
        public void Dispose() { if (nativeUtf8Ptr != IntPtr.Zero) Marshal.FreeHGlobal(nativeUtf8Ptr); }
        ~StringWrapper() { if(nativeUtf8Ptr != IntPtr.Zero) Marshal.FreeHGlobal(nativeUtf8Ptr); }
    }
}
