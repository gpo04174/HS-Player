﻿using HSAudio.Control.Device;
using System;

namespace HSAudio.Wave.Device
{
    //public delegate void DeviceChangedEventHandler(WaveDevice sender, DeviceChangedKind kind);
    public delegate void DeviceControlChangedEventHandler(WaveDevice sender, DeviceChangedKind kind, int index);
    public delegate void DevicePropertyChangedEventHandler(WaveDevice sender, WaveDevicePropertyChangedEventArgs e);

    public enum DeviceChangedKind { Unknown, Add, Removed, Disabled }
    public enum DeviceKind { Input, Output }

    public interface WaveDevice : IDisposable
    {
        event DevicePropertyChangedEventHandler DevicePropertyChanged;

        /// <summary>
        /// 장치의 이름을 가져옵니다.
        /// </summary>
        string DeviceName { get; }
        /// <summary>
        /// 장치를 가져옵니다.
        /// </summary>
        object Device { get; }
        /// <summary>
        /// 장치의 종류를 가져옵니다.
        /// </summary>
        DeviceKind Kind { get; }
        /// <summary>
        /// 장치가 지원하는 웨이브 형식을 가져옵니다.
        /// </summary>
        WaveFormat[] Formats { get; }

        /// <summary>
        /// 메인 장치를 가져옵니다.
        /// </summary>
        DeviceControl MasterControl { get; }
        /// <summary>
        /// 하위 장치를 가져옵니다.
        /// </summary>
        DeviceControl[] SubControls { get; }


        /// <summary>
        /// 현재 인스턴스가 DIsposed() 되었는지의 여부를 가져옵니다.
        /// </summary>
        bool IsDisposed { get; }
    }
}

