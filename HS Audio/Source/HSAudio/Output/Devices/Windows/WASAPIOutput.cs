﻿#if WIN_VISTA || WIN_10_1703 || WIN_METRO
using System;
using System.Collections.Generic;
using System.Text;
using HSAudio.Lib.FMOD;
using HSAudio.Wave.Device;
using HS.Setting;
using HSAudio.Log;
using HSAudio.Speakers;
using HSAudio.Output.Kind;
using System.Runtime.InteropServices;

namespace HSAudio.Output.Devices
{
    /// <summary>
    /// Windows WASAPI output [Vista Higher] (윈도우 WASAPI 츨력 [비스타 이상 기본값])
    /// </summary>
    public class WASAPIOutput : DeviceOutput
    {
        public static WASAPIOutput[] GetOutputs(HSAudio Audio)
        {
#if !NETSTANDARD
            if (Environment.OSVersion.Version.Major < 6) return new WASAPIOutput[0];
#else
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return new WASAPIOutput[0];
#endif
            Audio.system_sub.setOutput(OUTPUTTYPE.WASAPI);
            int num;
            Audio.system_sub.getNumDrivers(out num);
            List<WASAPIOutput> output = new List<WASAPIOutput>();
            WaveDeviceWASAPI[] device = WaveDeviceWASAPI.GetDevices(false);
            for (int i = 0; i < num; i++)
            {
                StringBuilder sb = new StringBuilder(256);
                OutputFormat format = new OutputFormat();

                Guid guid; SPEAKERMODE spk; int ch;
                Audio.system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch);format.channel = (short)ch; format.Mode = (SpeakerMode)spk;
                output.Add(new WASAPIOutput(i, format, guid, sb.ToString()));
                for (int j = 0; j < device.Length; j++)
                    if (guid.Equals(device[j].Guid)) { output[i].Device = device[j]; break; }
            }
            return output.ToArray();
        }
        public static WASAPIOutput[] GetOutputs()
        {
#if !NETSTANDARD
            if (Environment.OSVersion.Version.Major < 6) return new WASAPIOutput[0];
#else
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return new WASAPIOutput[0];
#endif
            Lib.FMOD.FD_System system_sub;
            Lib.FMOD.Factory.System_Create(out system_sub);
            if (system_sub != null)
            {
                try
                {
                    system_sub.setOutput(OUTPUTTYPE.WASAPI);
                    int num;
                    system_sub.getNumDrivers(out num);
                    List<WASAPIOutput> output = new List<WASAPIOutput>();
                    WaveDeviceWASAPI[] device = WaveDeviceWASAPI.GetDevices(false);
                    for (int i = 0; i < num; i++)
                    {
                        StringBuilder sb = new StringBuilder(256);
                        OutputFormat format = new OutputFormat();

                        Guid guid; SPEAKERMODE spk; int ch;
                        system_sub.getDriverInfo(i, sb, sb.Capacity, out guid, out format.sampleRate, out spk, out ch); format.channel = (short)ch; format.Mode = (SpeakerMode)spk;
                        output.Add(new WASAPIOutput(i, format, guid, sb.ToString()));
                        for (int j = 0; j < device.Length; j++)
                            if (guid.Equals(device[j].Guid)) { output[i].Device = device[j]; break; }
                    }
                    return output.ToArray();
                }
                finally { if (system_sub != null) system_sub.release(); }
            }
            else return null;
        }

        OutputFormat format;
        internal WASAPIOutput(int index, OutputFormat Format, Guid guid, string Name)
        {
            Type = OutputType.WASAPI;
            ID = index;
            this.Format = Format;
            base.Name = Name;
            Guid = guid;
        }
        private WASAPIOutput(WASAPIOutput before)
        {
            Type = OutputType.WASAPI;
            ID = before.ID;
            Name = before.Name;
            Exclusive = before.Exclusive;
            Guid = before.Guid;
            Parent = before.Parent;
            base.Device = before.Device;
            Format = before.Format;
        }

        //public new WaveDeviceWASAPI Device { get { return (WaveDeviceWASAPI)base.Device; } internal set { base.Device = value; } }
        public override Output Parent { get; internal set; }

        [HS.Attributes.Reserve]
        public bool Exclusive { get; internal set; }

        public Guid Guid { get; private set; }

        public override object Clone(){ return new WASAPIOutput(this);}

        public override Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("Name", Name);
            s.SetValue("Guid", Guid.ToString());
            s.SetValue("Exclusive", new SettingsData(Exclusive.ToString(), false));
            s.SubSetting.SetValue("Format", Format.SaveSetting());
            return s;
        }
        public override bool Equals(object obj) { try { return obj is WASAPIOutput ? Guid.Equals(((WASAPIOutput)obj).Guid) : false; } catch (Exception ex) { return false; } }
        public override bool Equals(DeviceOutput other) { try { return other is WASAPIOutput ? Guid.Equals(((WASAPIOutput)other).Guid) : false; } catch (Exception ex) { return false; } }
        //public override int GetHashCode() { if (Guid == null || Guid.Equals(Guid.Empty)) return (int)new Utility.Security.Crc32().Get(Guid.ToByteArray()); else return base.GetHashCode(); }

        public override string ToString() { return Name; }

        protected internal override void Dispose()
        {
            try { Device.Dispose(); } catch(Exception ex) { new HSAudioLog(ex.Message, "WASAPIOutput::Dispose();", LogKind.Engine, HS.Log.LogLevel.Error).Logging(); }
        }
    }
}
#endif