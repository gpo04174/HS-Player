﻿#if XBOX
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Core.Sound.Type
{
    public class XMA : ISoundFormat
    {
        public XMA() : base(SoundType.XMA, MIME, Extension, Description) { }
        public static string[] MIME { get { return new string[] { "audio/xma" }; } }
        public static string[] Extension { get { return new string[] { "xma" }; } }
        public static string[] Description { get { return new string[] { "XBOX 360 Audio" }; } }

        public override string ToString() { return Description[0]; }
    }
}
#endif