﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace HSPlayer.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MusicItemIconSmall : ContentView
    {
        public MusicItemIconSmall()
        {
            InitializeComponent();
            //Init();
        }
        public MusicItemIconSmall(string Title, string Artist, string Album)
        {
            InitializeComponent();
            this.Title = Title; this.Artist = Artist; this.Album = Album;
            //Init();
        }

        void Init()
        {
            this.AddHandler(PointerEnteredEvent, new PointerEventHandler((object o, PointerRoutedEventArgs e) =>
            {
                control.Visibility = Visibility.Visible;
            }), true);
            this.AddHandler(PointerExitedEvent, new PointerEventHandler((object o, PointerRoutedEventArgs e) =>
            {
                control.Visibility = Visibility.Collapsed;
            }), true);
        }

        public string Title { get { return lbl_title.Text; } set { lbl_title.Text = value == null ? "" : value; } }
        public string Artist { get { return lbl_artist.Text; } set { lbl_artist.Text = value == null ? "" : value; ; } }
        public string Album { get { return lbl_album.Text; } set { lbl_album.Text = value == null ? "" : value; ; } }
        public string AlbumArtURL { get; set; }
        internal protected Image AlbumArtControl { get { return pic_album; } set { pic_album = value; } }

        public new double Height { get { return base.Height; }set { base.Height = value; cd_album.Width = new GridLength(value, GridUnitType.Pixel); } }

        public bool VisiblePlay { get { return btn_play.Visibility == Visibility.Visible; } set { btn_play.Visibility = value ? Visibility.Visible : Visibility.Collapsed; } }

        public void UpdateAlbumArt()
        {
            pic_album.Source = new BitmapImage(new Uri(AlbumArtURL));
        }
        public void DefaultAlbumArt()
        {
            pic_album.Source = new BitmapImage(new Uri("ms-appx:///Resources/default_album_orgin.jpg"));
        }
    }
}
