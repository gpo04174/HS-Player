﻿using HS.Setting;
using HSAudio.Speakers;
using System.Collections.Generic;
using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.Output
{
    public class OutputSpeaker : ISettingManager
    {
        RESULT Result;
        HSAudio Audio;
        Dictionary<SpeakerKind, Speaker> dic = new Dictionary<SpeakerKind, Speaker>();
        internal OutputSpeaker(HSAudio Audio)
        {
            this.Audio = Audio;
            for (int i = 0; i < 12; i++)
                dic[(SpeakerKind)i] = new Speaker((SpeakerKind)i);
        }

        public Speaker this[SpeakerKind Speaker]
        {
            get { return Update(Speaker); }
            set { Result = Audio.system.setSpeakerPosition((SPEAKER)Speaker, value.X, value.Y, value.Active); }
        }

        Speaker Update(SpeakerKind kind)
        { Speaker speaker = dic[kind]; Result = Audio.system.getSpeakerPosition((SPEAKER)kind, out speaker._X, out speaker._Y, out speaker._Active); return speaker; }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            for (int i = 0; i < 12; i++)
            {
                SpeakerKind kind = (SpeakerKind)i;
                if (Setting.Exist(kind.ToString()))
                {
                    Speaker spk = dic[kind];
                    spk.LoadSetting(Setting.SubSetting[kind.ToString()], ThrowException);
                    Result = Audio.system.setSpeakerPosition((SPEAKER)i, spk.X, spk.Y, spk.Active);
                }
            }
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            for (int i = 0; i < 12; i++)
                s.SubSetting.SetValue(((SpeakerKind)i).ToString(), dic[(SpeakerKind)i].SaveSetting());
            return s;
        }

        public void DefaultFromMode(SpeakerMode mode, bool Clear = false)
        {
            Speaker spk;
            #region Mono
            if (mode == SpeakerMode.Mono)
            {
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set(0, 0, true);
                for (int i = 1; i < 12; i++)this[(SpeakerKind)i] = Clear ? dic[(SpeakerKind)i].Set(0, 0, false) : dic[(SpeakerKind)i].Set(false);
            }
            #endregion
            #region Stereo
            else if (mode == SpeakerMode.Stereo)
            {
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set(0, 0, true);
                this[SpeakerKind.FRONT_RIGHT] = dic[SpeakerKind.FRONT_RIGHT].Set(0, 0, true);
                for (int i = 2; i < 12; i++) this[(SpeakerKind)i] = Clear ? dic[(SpeakerKind)i].Set(0, 0, false) : dic[(SpeakerKind)i].Set(false);
            }
            #endregion
            #region Quad
            else if (mode == SpeakerMode.Quad)
            {
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-45)), (float)Math.Cos(Speaker.DegreeToRadian(-45)), true);
                this[SpeakerKind.FRONT_RIGHT] = dic[SpeakerKind.FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(45)), (float)Math.Cos(Speaker.DegreeToRadian(45)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-135)), (float)Math.Cos(Speaker.DegreeToRadian(-135)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(135)), (float)Math.Cos(Speaker.DegreeToRadian(135)), true);

                this[SpeakerKind.FRONT_CENTER] = Clear ? dic[SpeakerKind.SUBWOOFER].Set(0, 0, false) : dic[SpeakerKind.SUBWOOFER].Set(false);
                this[SpeakerKind.SUBWOOFER] = Clear ? dic[SpeakerKind.SUBWOOFER].Set(0, 0, false) : dic[SpeakerKind.SUBWOOFER].Set(false);
                this[SpeakerKind.BACK_LEFT] = Clear ? dic[SpeakerKind.BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.BACK_LEFT].Set(false);
                this[SpeakerKind.BACK_RIGHT] = Clear ? dic[SpeakerKind.BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.BACK_RIGHT].Set(false);
                this[SpeakerKind.TOP_FRONT_LEFT] = Clear ? dic[SpeakerKind.TOP_FRONT_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_LEFT].Set(false);
                this[SpeakerKind.TOP_FRONT_RIGHT] = Clear ? dic[SpeakerKind.TOP_FRONT_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_RIGHT].Set(false);
                this[SpeakerKind.TOP_BACK_LEFT] = Clear ? dic[SpeakerKind.TOP_BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_LEFT].Set(false);
                this[SpeakerKind.TOP_BACK_RIGHT] = Clear ? dic[SpeakerKind.TOP_BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_RIGHT].Set(false);
            }
            #endregion
            #region Surround
            else if(mode == SpeakerMode.Surround)
            {
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-45)), (float)Math.Cos(Speaker.DegreeToRadian(-45)), true);
                this[SpeakerKind.FRONT_RIGHT] = dic[SpeakerKind.FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(45)), (float)Math.Cos(Speaker.DegreeToRadian(45)), true);
                this[SpeakerKind.FRONT_CENTER] = dic[SpeakerKind.FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-135)), (float)Math.Cos(Speaker.DegreeToRadian(-135)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(135)), (float)Math.Cos(Speaker.DegreeToRadian(135)), true);

                this[SpeakerKind.SUBWOOFER] = Clear ? dic[SpeakerKind.SUBWOOFER].Set(0, 0, false) : dic[SpeakerKind.SUBWOOFER].Set(false);
                this[SpeakerKind.BACK_LEFT] = Clear ? dic[SpeakerKind.BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.BACK_LEFT].Set(false);
                this[SpeakerKind.BACK_RIGHT] = Clear ? dic[SpeakerKind.BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.BACK_RIGHT].Set(false);
                this[SpeakerKind.TOP_FRONT_LEFT] = Clear ? dic[SpeakerKind.TOP_FRONT_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_LEFT].Set(false);
                this[SpeakerKind.TOP_FRONT_RIGHT] = Clear ? dic[SpeakerKind.TOP_FRONT_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_RIGHT].Set(false);
                this[SpeakerKind.TOP_BACK_LEFT] = Clear ? dic[SpeakerKind.TOP_BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_LEFT].Set(false);
                this[SpeakerKind.TOP_BACK_RIGHT] = Clear ? dic[SpeakerKind.TOP_BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_RIGHT].Set(false);
            }
            #endregion
            #region 5.1 ch
            else if (mode == SpeakerMode.System5_1)
            {
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-45)), (float)Math.Cos(Speaker.DegreeToRadian(-45)), true);
                this[SpeakerKind.FRONT_RIGHT] = dic[SpeakerKind.FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(45)), (float)Math.Cos(Speaker.DegreeToRadian(45)), true);
                this[SpeakerKind.FRONT_CENTER] = dic[SpeakerKind.FRONT_CENTER].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SUBWOOFER] = dic[SpeakerKind.SUBWOOFER].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-135)), (float)Math.Cos(Speaker.DegreeToRadian(-135)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(135)), (float)Math.Cos(Speaker.DegreeToRadian(135)), true);
                
                this[SpeakerKind.BACK_LEFT] = Clear ? dic[SpeakerKind.BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.BACK_LEFT].Set(false);
                this[SpeakerKind.BACK_RIGHT] = Clear ? dic[SpeakerKind.BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.BACK_RIGHT].Set(false);
                this[SpeakerKind.TOP_FRONT_LEFT] = Clear ? dic[SpeakerKind.TOP_FRONT_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_LEFT].Set(false);
                this[SpeakerKind.TOP_FRONT_RIGHT] = Clear ? dic[SpeakerKind.TOP_FRONT_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_RIGHT].Set(false);
                this[SpeakerKind.TOP_BACK_LEFT] = Clear ? dic[SpeakerKind.TOP_BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_LEFT].Set(false);
                this[SpeakerKind.TOP_BACK_RIGHT] = Clear ? dic[SpeakerKind.TOP_BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_RIGHT].Set(false);
            }
            #endregion
            #region 7.1 ch
            else if (mode == SpeakerMode.System7_1)
            {
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-30)), (float)Math.Cos(Speaker.DegreeToRadian(-30)), true);
                this[SpeakerKind.FRONT_RIGHT] = dic[SpeakerKind.FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(30)), (float)Math.Cos(Speaker.DegreeToRadian(30)), true);
                this[SpeakerKind.FRONT_CENTER] = dic[SpeakerKind.FRONT_CENTER].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SUBWOOFER] = dic[SpeakerKind.SUBWOOFER].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-90)), (float)Math.Cos(Speaker.DegreeToRadian(-90)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(90)), (float)Math.Cos(Speaker.DegreeToRadian(90)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-150)), (float)Math.Cos(Speaker.DegreeToRadian(-150)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(150)), (float)Math.Cos(Speaker.DegreeToRadian(150)), true);

                this[SpeakerKind.TOP_FRONT_LEFT] = Clear ? dic[SpeakerKind.TOP_FRONT_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_LEFT].Set(false);
                this[SpeakerKind.TOP_FRONT_RIGHT] = Clear ? dic[SpeakerKind.TOP_FRONT_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_FRONT_RIGHT].Set(false);
                this[SpeakerKind.TOP_BACK_LEFT] = Clear ? dic[SpeakerKind.TOP_BACK_LEFT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_LEFT].Set(false);
                this[SpeakerKind.TOP_BACK_RIGHT] = Clear ? dic[SpeakerKind.TOP_BACK_RIGHT].Set(0, 0, false) : dic[SpeakerKind.TOP_BACK_RIGHT].Set(false);
            }
            #endregion
            #region 7.1.4 ch
            else if (mode == SpeakerMode.System7_1_4)
            {
                //https://www.auro-3d.com/wp-content/uploads/documents/Auro-3D-Home-Theater-Setup-Guidelines_lores.pdf / 21p
                this[SpeakerKind.FRONT_LEFT] = dic[SpeakerKind.FRONT_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-30)), (float)Math.Cos(Speaker.DegreeToRadian(-30)), true);
                this[SpeakerKind.FRONT_RIGHT] = dic[SpeakerKind.FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(30)), (float)Math.Cos(Speaker.DegreeToRadian(30)), true);
                this[SpeakerKind.FRONT_CENTER] = dic[SpeakerKind.FRONT_CENTER].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SUBWOOFER] = dic[SpeakerKind.SUBWOOFER].Set((float)Math.Sin(Speaker.DegreeToRadian(0)), (float)Math.Cos(Speaker.DegreeToRadian(0)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-110)), (float)Math.Cos(Speaker.DegreeToRadian(-110)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(110)), (float)Math.Cos(Speaker.DegreeToRadian(110)), true);
                this[SpeakerKind.SURROUND_LEFT] = dic[SpeakerKind.SURROUND_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-135)), (float)Math.Cos(Speaker.DegreeToRadian(-135)), true);
                this[SpeakerKind.SURROUND_RIGHT] = dic[SpeakerKind.SURROUND_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(135)), (float)Math.Cos(Speaker.DegreeToRadian(135)), true);
                this[SpeakerKind.TOP_FRONT_LEFT] = dic[SpeakerKind.TOP_FRONT_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-22)), (float)Math.Cos(Speaker.DegreeToRadian(-22)), true);
                this[SpeakerKind.TOP_FRONT_RIGHT] = dic[SpeakerKind.TOP_FRONT_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(22)), (float)Math.Cos(Speaker.DegreeToRadian(22)), true);
                this[SpeakerKind.TOP_BACK_LEFT] = dic[SpeakerKind.TOP_BACK_LEFT].Set((float)Math.Sin(Speaker.DegreeToRadian(-90)), (float)Math.Cos(Speaker.DegreeToRadian(-90)), true);
                this[SpeakerKind.TOP_BACK_RIGHT] = dic[SpeakerKind.TOP_BACK_RIGHT].Set((float)Math.Sin(Speaker.DegreeToRadian(90)), (float)Math.Cos(Speaker.DegreeToRadian(90)), true);
            }
            #endregion
        }
    }
}
