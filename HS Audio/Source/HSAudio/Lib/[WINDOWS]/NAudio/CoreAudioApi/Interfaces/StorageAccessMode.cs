﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio.Lib.Windows.NAudio.CoreAudioApi.Interfaces
{
    /// <summary>
    /// MMDevice STGM enumeration
    /// </summary>
    enum StorageAccessMode
    {
        Read,
        Write,
        ReadWrite
    }
}
