﻿using HS.Setting;
using HSAudio.Core.Audio;
using HSAudio.Lib.FMOD;
using System;
using System.Collections.Generic;
using System.Text;

namespace HSAudio
{
    public class MixMatrix : ISettingManager
    {
        Audio Audio;
        FD_DSPConnection conn;
        RESULT Result;

        float[] _Matrix;
        int _ChannelOutput;
        int _ChannelInput;
        int _ChannelInputHop = 0;
        internal MixMatrix(Audio Audio){this.Audio = Audio; }
        internal MixMatrix(FD_DSPConnection conn){ this.conn = conn; }

        internal void Update(bool IsGet)
        {
            _Matrix = new float[4];
            if (IsGet) Result = Audio != null ? 
                    Audio.channel.getMixMatrix(_Matrix, out _ChannelOutput, out _ChannelInput, _ChannelInputHop):
                    conn.         getMixMatrix(_Matrix, out _ChannelOutput, out _ChannelInput, _ChannelInputHop);
            else Result = Audio.channel.setMixMatrix(Matrix, ChannelOutput, ChannelInput, ChannelInputHop);
        }

        public float[] Matrix { get { return _Matrix; } }
        public int ChannelOutput { get { return _ChannelOutput; }set { Update(false); } }
        public int ChannelInput { get { return _ChannelInput; } set { Update(false); } }
        public int ChannelInputHop { get { return _ChannelInputHop; } set { Update(false); } }

        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            throw new NotImplementedException();
        }

        public Settings SaveSetting()
        {
            throw new NotImplementedException();
        }
    }
}
