﻿using HS;
using HS.Setting;
using System;
using System.Runtime.InteropServices;
using System.Text;

namespace HSAudio.DSP
{
    public class DataValue : ISettingManager, IDisposable
    {
        internal DataValue(Settings SaveData) { LoadSetting(SaveData); }

        public DataValue(IntPtr Data, uint Length, DataType DataType)
        {
            this.Data = Data;
            this.Length = Length;
            this.DataType = DataType;
        }
        
        public uint Length { get; protected internal set; }
        public IntPtr Data { get; protected internal set; }
        /// <summary>
        /// You can create a custom type. (int)
        /// </summary>
        public DataType DataType { get; protected internal set; }


        public byte[] GetBytesData() { return GetBytesData(Data, (int)Length); }
        public static byte[] GetBytesData(IntPtr data, int length)
        {
            if (data == IntPtr.Zero) return null;
            byte[] br = new byte[length];
            Marshal.Copy(data, br, 0, (int)length);
            return br;
        }

        #region Settings
        public bool LoadSetting(Settings Setting, bool ThrowException = false)
        {
            try
            {
                if (Setting.Exist("Data")) setData(Setting["Data"]);
                else throw new HSException(Resource.StringResource.Resource["STR_ERR_SETTING_WRONG"]);
            }
            catch (Exception ex) { if (ThrowException) throw ex; return false; }

            if (Setting.Exist("DataType")) DataType = (DataType)Enum.Parse(typeof(DataType), Setting["DataType"]);
            return true;
        }

        public Settings SaveSetting()
        {
            Settings s = new Settings();
            s.SetValue("DataType", DataType.ToString());
            s.SetValue("Data", getData());
            return s;
        }

        unsafe void setData(string value)
        {
            string[] val = value.Split(new char[' '], StringSplitOptions.RemoveEmptyEntries);
            IntPtr data = Marshal.AllocHGlobal(sizeof(byte) * val.Length);

            byte* p = (byte*)data;
            for (int i = 0; i < val.Length; i++)
                if (!string.IsNullOrEmpty(val[i])) p[i] = Convert.ToByte(val[i], 16);
            Data = data;
            Length = (uint)val.Length;
        }

        unsafe string getData()
        {
            if (Data != IntPtr.Zero && Length > 0)
            {
                byte* data = (byte*) Data;
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < Length; i++)
                    sb.AppendFormat("{0:X2} ", data[i]);
                return sb.ToString();
            }
            else return null;
        }
        #endregion

        public static implicit operator IntPtr(DataValue data) { return data.Data; }

        public void Dispose()
        {
            Marshal.FreeHGlobal(Data);
            Data = IntPtr.Zero;
        }
        ~DataValue() { }
    }
}
