﻿using HSAudio.Lib.FMOD;
using System.Text;

namespace FMODTest.Test
{
    public class PluginTest
    {
        public static void Test(string Plugin)
        {
            FD_System system;
            FD_DSP dsp;

            Factory.System_Create(out system);

            RESULT result = RESULT.OK;
            uint h;
            result = system.loadPlugin(Plugin, out h);

            system.createDSPByPlugin(h, out dsp);
            StringBuilder name = new StringBuilder(256);
            uint version;
            int channel;
            int width;
            int height;
            result = dsp.getInfo(name, out version, out channel, out width, out height);
            DSP_TYPE type;
            dsp.getType(out type);
        }
    }
}
