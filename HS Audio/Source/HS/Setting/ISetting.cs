﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace HS.Setting
{
    public interface ISetting<T>
    {
        T this[string Key] { get; set; }
        int Count { get; }
        bool AutoAdd { get; set; }
        //string Name { get; set; }

        T GetValue(string Key, T Default);
        void SetValue(string Key, T Value);
        bool Exist(string Key);

        IEnumerator<KeyValuePair<string, T>> GetEnumerator();
    }
}
