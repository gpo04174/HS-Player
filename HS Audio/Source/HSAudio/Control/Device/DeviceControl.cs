﻿using HSAudio.Wave.Device;

namespace HSAudio.Control.Device
{
    public interface DeviceControl
    {
        /// <summary>
        /// 웨이브 장치를 가져옵니다.
        /// </summary>
        WaveDevice Device { get; }

        /// <summary>
        /// 원시 볼륨값의 최소값을 가져옵니다.
        /// </summary>
        float VolumeMax { get; }
        /// <summary>
        /// 원시 볼륨값의 최대값을 가져옵니다.
        /// </summary>
        float VolumeMin { get; }
        /// <summary>
        /// 원시 볼륨값을 설정하거나 가져옵니다.
        /// </summary>
        float Volume { get; set; }
        /// <summary>
        /// 볼륨을 0.0과 1.0 사이 퍼센테이지로 설정하거나 가져옵니다.
        /// </summary>
        float VolumePercent { get; set; }
        /// <summary>
        /// 볼륨을 0.0과 1.0 사이에서 설정하거나 가져옵니다.
        /// </summary>
        float VolumeScalar { get; set; }
        /// <summary>
        /// 음소거 상태를 설정하거나 가져옵니다.
        /// </summary>
        bool Mute { get; set; }

        /// <summary>
        /// 이름을 가져옵니다.
        /// </summary>
        string Name { get; }
        /// <summary>
        /// 컨트롤 인스턴스를 가져옵니다.
        /// </summary>
        object Control { get; }
    }
}
