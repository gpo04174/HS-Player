﻿using HSAudio.Lib.FMOD;
using System;
using System.Threading;

namespace FMODTest.Test
{
    class CodecTest_dll
    {
        public static void Test(string MusicPath)
        {
            FD_System system;
            FD_Sound sound;
            FD_Channel channel;

            Factory.System_Create(out system);

            RESULT result = RESULT.OK;
            uint aac;
            result = system.loadPlugin("codec_aac.dll", out aac);
            result = system.init(16, INITFLAGS.NORMAL, IntPtr.Zero);
            result = system.createSound(MusicPath, MODE.DEFAULT, out sound);
            result = system.playSound(sound, null, false, out channel);

            result = channel.setMode(MODE._2D);
            MODE mode;
            result = channel.getMode(out mode);

            channel.setPaused(false);

            OPENSTATE state; uint percent; bool starving; bool diskbusy;
            result = sound.getOpenState(out state, out percent, out starving, out diskbusy);
            result = channel.setPitch(30f);
            result = channel.setFrequency(-44100);
            float Freq;
            channel.getFrequency(out Freq);
            result = channel.setPitch(2f);
            while (true) Thread.Sleep(10);
        }
    }
}
