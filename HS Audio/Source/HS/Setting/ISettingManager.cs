﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HS.Setting
{
    public interface ISettingManager
    {
        Settings SaveSetting();
        bool LoadSetting(Settings Setting, bool ThrowException = false);
    }
}
