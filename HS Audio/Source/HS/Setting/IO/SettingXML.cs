﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace HS.Setting.IO
{
    public static class SettingXML// : SettingIO<XmlDocument>
    {
        public static Settings Load(XmlDocument Data) { return Load(Data, "HSAudioSettings"); }
        public static Settings Load(XmlDocument Data, string Name = "HSAudioSettings")
        {
            throw new NotImplementedException();
        }

        public static XmlDocument Save(Settings Data) { return Save(Data, "HSAudioSettings"); }
        public static XmlDocument Save(Settings Data, string Name = "HSAudioSettings")
        {
            XmlDocument doc = new XmlDocument();
            if (string.IsNullOrEmpty(Name)) Name = "HSAudioSettings";
            XmlNode root = doc.CreateElement(Name);
            SaveAttribute(Data, doc, root);

            IEnumerator<KeyValuePair<string, Settings>> a = Data.SubSetting.GetEnumerator();
            while (a.MoveNext())
            {
                XmlNode node = doc.CreateElement(a.Current.Key);
                SaveAttribute(a.Current.Value, doc, node);
                IEnumerator<KeyValuePair<string, Settings>> b = a.Current.Value.SubSetting.GetEnumerator();
                if (b.MoveNext())
                {
                    Stack<SaveRecursive> stack = new Stack<SaveRecursive>();
                    stack.Push(new SaveRecursive(node, b.Current));
                    while (stack.Count > 0)
                    {
                        SaveRecursive key = stack.Pop();

                        XmlNode nodesub = doc.CreateElement(key.pair.Key);
                        SaveAttribute(key.pair.Value, doc, nodesub);
                        key.node.AppendChild(nodesub);
                        try
                        {
                            IEnumerator<KeyValuePair<string, Settings>> c = key.pair.Value.SubSetting.GetEnumerator();
                            if(c != null)
                                while (c.MoveNext()) stack.Push(new SaveRecursive(nodesub, c.Current));
                        }
                        catch(Exception ex) { }
                    }
                }
                root.AppendChild(node);
            }
            doc.AppendChild(root);
            return doc;
        }

        private static void SaveAttribute(Settings Data, XmlDocument doc, XmlNode node)
        {
            IEnumerator<KeyValuePair<string, SettingsData>> a = Data.GetEnumerator();
            while (a.MoveNext())
            {
                if (a.Current.Value.IsHead)
                {
                    XmlAttribute attr = doc.CreateAttribute(a.Current.Key);
                    attr.Value = a.Current.Value;
                    node.Attributes.Append(attr);
                }
                else
                {
                    XmlNode node1 = doc.CreateElement(a.Current.Key);
                    node1.InnerText = a.Current.Value;
                    node.AppendChild(node1);
                }
            }
        }

        class SaveRecursive
        {
            public SaveRecursive(XmlNode node, KeyValuePair<string, Settings> pair) { this.node = node; this.pair = pair; }
            public XmlNode node;
            public KeyValuePair<string, Settings> pair;
        }
    }
}
