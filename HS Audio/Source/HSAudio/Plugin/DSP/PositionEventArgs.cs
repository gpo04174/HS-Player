﻿using HSAudio.Lib.FMOD;
using System;

namespace HSAudio.PlugIn.DSP
{
    public class PositionEventArgs : EventArgs
    {
        internal PositionEventArgs(ref DSP_STATE state, uint pos) { State = state; Position = pos; }

        protected internal DSP_STATE State { get; private set; }
        public uint Position { get; private set; }
    }
}
