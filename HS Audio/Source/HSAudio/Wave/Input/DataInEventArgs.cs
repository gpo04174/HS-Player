﻿using System;

namespace HSAudio.Wave.Input
{
    public delegate void DataInEventHandler(object sender, DataInEventArgs e);
    public delegate void DataInStoppedEventHandler(object sender, EventArgs e);
    public class DataInEventArgs : EventArgs
    {
        protected internal DataInEventArgs() { }
        protected internal DataInEventArgs(WaveFormat Format) { this.Format = Format; }
        protected internal DataInEventArgs(WaveFormat Format, byte[] Data, int BytesRecorded) { this.Format = Format; this.Data = Data; this.Length = BytesRecorded; }

        public WaveFormat Format { get; internal protected set; }
        public byte[] Data { get; internal protected set; }
        public int Length { get; internal protected set; }

        public float[] GetSample()
        {
            if (Format == null || Data == null) return null;

            int length = Length;
            float[] buf = new float[0];
            byte[] buffer = Data;
            if (Format.Bit == 8)
            {
                try
                {
                    buf = new float[length];
                    for (int i = 0, j = 0; i < length; i++, j++)
                    {
                        sbyte sample = (sbyte)(sbyte.MaxValue - buffer[i]);
                        buf[j] = sample / (float)sbyte.MaxValue;
                    }
                }
                catch (Exception ex) { }
            }
            else if (Format.Bit == 16)
            {
                buf = new float[length / 2];
                for (int i = 0, j = 0; i < length; i += 2, j++)
                {
                    short sample = (short)(buffer[i + 1] << 8 | buffer[i + 0]);
                    buf[j] = sample / (float)short.MaxValue;
                }
            }
            else if (Format.Bit == 24)
            {
                buf = new float[length / 3];
                for (int i = 0, j = 0; i < length; i += 3, j++)
                {
                    int sample = buffer[i + 2] << 16 | buffer[i + 1] << 8 | buffer[i + 0];
                    buf[j] = sample / (float)0xFFFFFF;
                }
            }
            else if (Format.Bit == 32)
            {
                buf = new float[length / 4];
                for (int i = 0, j = 0; i < length; i += 3, j++)
                {
                    int sample = buffer[i + 3] << 24 | buffer[i + 2] << 16 | buffer[i + 1] << 8 | buffer[i + 0];
                    buf[j] = sample / (float)int.MaxValue;
                }
            }
            else if(Format.Type == WaveType.FLOAT)
            {
                try
                {
                    buf = new float[length / 4];
                    byte[] tmpFloatBuffer = new byte[4];
                    for (int i = 0, j = 0; i < length; i += 4, j++)
                    {
                        tmpFloatBuffer[0] = buffer[i + 0];
                        tmpFloatBuffer[1] = buffer[i + 1];
                        tmpFloatBuffer[2] = buffer[i + 2];
                        tmpFloatBuffer[3] = buffer[i + 3];

                        float sample = BitConverter.ToSingle(tmpFloatBuffer, 0);
                        buf[j] = sample;
                    }
                }
                catch (Exception ex) { }
            }
            return buf;
        }
    }
}