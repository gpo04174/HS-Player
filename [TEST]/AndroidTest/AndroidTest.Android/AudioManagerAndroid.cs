﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using CrossCallback;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;

using HSAudio.Core.Sound;
using Android.Util;
using HSAudio.Output.Devices;
using HSAudio.Output.Kind;

namespace AndroidTest.Droid
{
    class AudioManagerAndroid
    {
        public static AudioManagerAndroid Instance { get; private set; }
        public static AudioManagerAndroid Init(Context context)
        {
            if (Instance == null) Instance = new AudioManagerAndroid(context);
            else Instance.context = context;
            return Instance;
        }

        Context context;
        private AudioManagerAndroid(Context context)
        {
            this.context = context;

            //manager = new FunctionManager();
            AudioManager.AudioChange = AudioChanged_Callback;
        }

        HSAudio.HSAudio Audio;
        HSAudio.Core.Audio.Audio audio;
        Sound sound;
        DeviceOutput[] output;
        async Task<Exception> AudioChanged_Callback(string Path)
        {
            Exception exception = null;
            try
            {
                if (Audio == null) Audio = new HSAudio.HSAudio(false);
                if (output == null || output.Length == 0) output = OpenSLOutput.GetOutputs(Audio);
                if (output == null || output.Length == 0) output = AudioTrackOutput.GetOutputs(Audio);
                Audio.Output.Open(new NoOutput());

                await Task.Run(() =>
                {
                    sound = Sound.FromFile(Audio, Path);

                    if (audio == null) audio = Audio.Audio.Add(sound);
                    else audio.setSound(sound);
                });
                audio.Play();
            }
            catch(Exception ex) { exception = ex; }

            Log.Debug("FMOD Init", exception.ToString());
            return exception;
        }
    }
}