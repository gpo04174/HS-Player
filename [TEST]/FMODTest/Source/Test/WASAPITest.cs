﻿#if !LINUX
using HSAudio.Lib.FMOD;
using System;
using System.Text;
using System.Threading;

namespace FMODTest.Test
{
    class WASAPITest
    {
        public static void Test(string MusicPath)
        {
            FD_System system;
            Factory.System_Create(out system);
            //Output[] o = Program.GetOutputs(system, OUTPUTTYPE.ASIO);

            if (MusicPath != null)
            {
                FD_Sound sound;
                FD_Channel channel;
                RESULT result = RESULT.OK;
                result = system.setOutput(OUTPUTTYPE.WASAPI);
                result = system.setDriver(0);
                result = system.setSoftwareFormat(96000, SPEAKERMODE.RAW, 2);

                StringBuilder sb = new StringBuilder(256); Guid guid; int rate; SPEAKERMODE mode; int ch;
                result = system.getDriverInfo(9, sb, 256, out guid, out rate, out mode, out ch);
                //result = system.setOutput
                result = system.init(16, INITFLAGS.PREFER_DOLBY_DOWNMIX | INITFLAGS.PROFILE_METER_ALL | INITFLAGS.PROFILE_ENABLE, IntPtr.Zero);
                result = system.createStream(MusicPath, MODE.DEFAULT, out sound);
                result = system.playSound(sound, null, false, out channel);

                int samplate; int rawspk;
                system.getSoftwareFormat(out samplate, out mode, out rawspk);
                system.getSoftwareChannels(out ch);

                OPENSTATE state; uint percent; bool starving; bool diskbusy;
                result = sound.getOpenState(out state, out percent, out starving, out diskbusy);
                result = channel.setPaused(false);
            }
            //while (true) {  }
            Thread.Sleep(3000);
            system.close();
            system.release();
        }
    }
}
#endif
