﻿using HS.Setting;
using HSAudio.Core.Sound.Type;
using HSAudio.Lib.FMOD;
using HSAudio.Wave;
using System.IO;

namespace HSAudio.Core.Sound.Kind
{
    public abstract partial class SoundStream : Sound
    {
        protected Stream stream;
        protected bool _CanReverse;
        protected ISoundFormat _SoundType;
        protected WaveFormat _Format;
        protected SoundTag _Tag;

        protected CREATESOUNDEXINFO info = new CREATESOUNDEXINFO();

        //internal SoundStream(HSAudio Audio, Stream Stream, RAW Raw, bool LoadMemory) : this(Audio, Stream, Raw, (uint)Stream.Length, Seekable, LoadMemory) { }
        internal SoundStream(HSAudio Audio, Stream Stream, RAW Raw, bool Seekable, bool LoadMemory) : this(Audio, Stream, Raw, (uint)Stream.Length, Seekable, LoadMemory) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Audio"></param>
        /// <param name="stream"></param>
        /// <param name="Format"></param>
        /// <param name="Length">Samplate × Channels × sizeof() × Sececond</param>
        /// <param name="Raw"></param>
        /// <param name="LoadTagManager"></param>
        /// <param name="LoadMemory"></param>
        /// <param name="Compress"></param>
        internal SoundStream(HSAudio Audio, Stream Stream, RAW Raw, uint Length, bool Seekable, bool LoadMemory) : base(Audio)
        {
            this.stream = Stream;
            this.LoadMemory = LoadMemory;
            this.Seekable = Seekable;
            _CanReverse = LoadMemory;
            _Format = Raw.Format;
            _SoundType = Raw;

            info.length = Length;
            info.pcmreadcallback = new SOUND_PCMREADCALLBACK(SOUND_PCMREADCALLBACK);
            info.pcmsetposcallback = new SOUND_PCMSETPOSCALLBACK(SOUND_PCMSETPOSCALLBACK);
            //info.nonblockcallback = new SOUND_NONBLOCKCALLBACK(SOUND_NONBLOCKCALLBACK);
            InitFormat(ref info, Format);
            Load();
        }
        public override void Unload()
        {
            _Tag = null;
            base.Unload();
        }
        
        public new Stream Stream { get { return stream; } }
        public uint Length { get { return info.length; } }

        public override WaveFormat Format { get { return _Format; } }
        public override ISoundFormat Type { get { return _SoundType; } }
        public override SoundTag Tag { get { return _Tag; } }

        public bool LoadMemory { get; private set; }
        public bool Seekable { get; private set; }

        public override bool CanPitch { get { return true; } }
        public override bool CanPosition { get { return stream.CanSeek; } }
        public override bool CanReverse { get { return _CanReverse; } }


        public override Settings SaveSetting() { return null; }
    }
}
